﻿/*
 * CEDAR Operator Workload Assessment Tool
 * Copyright (C) 2023  aurizn
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using CocosSharp;
namespace CEDAR.Shared.Modules.Task.Ext {

    /**
     @class ExtViewButton
    
     @brief Ext button view component.
    
     @author    Ryan Beruldsen
     @date  10/10/2016
     */

    class ExtViewButton : DivisionContent, IDivisionContent {
        /** @brief The main non-warning button sprite. */
        private readonly CCSprite _buttonPassive;

        /** @brief The button warning sprite. */
        private readonly CCSprite _buttonWarning;

        /** @brief Button active text colour */
        private readonly CCColor3B _colourActive;

        /** @brief Button warning colour */
        private readonly CCColor4B _colourWarning;

        /** @brief The x coordinate. */
        private readonly float _x;

        /** @brief The y coordinate. */
        private readonly float _y;

        /** @brief The button width. */
        private readonly float _width;

        /** @brief The button height. */
        private readonly float _height;

        /** @brief true to warning active. */
        private bool _warningActive = false;

        /** @brief The button label. */
        private readonly string _label;

        /** @brief The minimum bounds. */
        public CCPoint minButtonBounds;

        /** @brief The maximum bounds. */
        public CCPoint maxButtonBounds;

        /**
         @property  public bool warningActive
        
         @brief Gets or sets a value indicating whether the warning active.
        
         @return    true if warning active, false if not.
         */

        public bool WarningActive {
            get {
                return _warningActive;
            }
            set {
                _warningActive = value;
                if (_warningActive) {
                    _buttonPassive.Visible = false;
                    _buttonWarning.Visible = true;
                } else {
                    _buttonPassive.Visible = true;
                    _buttonWarning.Visible = false;
                }
            }
        }

        /**
         @fn    public ExtViewButton(float x, float y, float width, float height, string label, CCColor3B colourActive, CCColor4B colourWarning, Division division) : base(division)
        
         @brief Ext button view constructor.
        
         @author    Ryan Beruldsen
         @date  10/10/2016
        
         @param x               The x coordinate.
         @param y               The y coordinate.
         @param width           The width.
         @param height          The height.
         @param label           The label.
         @param colourActive    The button active colour.
         @param colourWarning   The button warning colour.
         @param division        The division.
         */

        public ExtViewButton(float x,
                                            float y,
                                            float width,
                                            float height,
                                            string label,
                                            CCColor3B colourActive,
                                            CCColor4B colourWarning,
                                            Division division) : base(division) {

            _buttonPassive = new CCSprite();
            _buttonWarning = new CCSprite();
            _colourWarning = colourWarning;
            _colourActive = colourActive;
            _x = x;
            _y = y;
            _width = width;
            _height = height;
            _label = label;
        }

        /**
         @fn    public void Init()
        
         @brief Initialise ext button view.
        
         @author    Ryan Beruldsen
         @date  10/10/2016
         */

        public void Init() {
            var passiveDrawNode = new CCDrawNode();
            var warningDrawNode = new CCDrawNode();

            //calculate hit bounds
            minButtonBounds.X = _x;
            minButtonBounds.Y = _y;
            maxButtonBounds.X = _x + _width;
            maxButtonBounds.Y = _y + _height;

            //draw button rectangle
            passiveDrawNode.DrawRect(
                new CCRect(x: _x,
                            y: _y,
                            width: _width,
                            height: _height),
                fillColor: new CCColor4B(100, 100, 100, 100),
                borderWidth: 0,
                borderColor: CCColor4B.White
            );
            warningDrawNode.DrawRect(
                new CCRect(x: _x,
                            y: _y,
                            width: _width,
                            height: _height),
                fillColor: _colourWarning,
                borderWidth: 0,
                borderColor: CCColor4B.White
            );

            //place button labels
            float spacer = _width * 0.01f;
            CCLabel buttonLabelPassive = new CCLabel(_label, "robotob", 14, CCLabelFormat.SpriteFont) {
                Position = new CCPoint(_x + spacer, _y + spacer),
                Color = _colourActive,
                AnchorPoint = CCPoint.AnchorLowerLeft,
            };
            CCLabel buttonLabelWarning = new CCLabel(_label, "robotob", 14, CCLabelFormat.SpriteFont) {
                Position = new CCPoint(_x + spacer, _y + spacer),
                Color = CCColor3B.Black,
                AnchorPoint = CCPoint.AnchorLowerLeft,
            };

            _buttonWarning.AddChild(warningDrawNode);
            _buttonWarning.AddChild(buttonLabelWarning);
            _buttonWarning.Visible = false;
            _buttonPassive.AddChild(passiveDrawNode);
            _buttonPassive.AddChild(buttonLabelPassive);

            AddChild(_buttonPassive);
            AddChild(_buttonWarning);

        }
    }
}
