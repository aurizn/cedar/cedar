﻿/*
 * CEDAR Operator Workload Assessment Tool
 * Copyright (C) 2023  aurizn
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;

namespace CEDAR.Shared.Modules.Task.Sys {
    /**
     @class SysModelChannelEventArgs
    
     @brief Systems channel event parameters
    
     @author    Ryan Beruldsen
     @date  15/09/2016
     */

    internal class SysModelChannelEventArgs : EventArgs {

        /**
         @property  public bool valueChange

         @brief System Monitoring level change event type

         @return    true if value System Monitoring level change event type.
        */

        public bool ValueChange { get; set; }

        /**
         @property  public bool warnIndicateChange
        
         @brief System Monitoring warning (level is outside safe range) raised event type
        
         @return    true if warning raised, false if not.
         */

        public bool WarnIndicateChange { get; set; }

        /**
         @property  public bool selectionChange
        
         @brief Selection change event, user has interacted with a component
        
         @return    true if selection change, false if not.
         */

        public bool SelectionChange { get; set; }

        /**
         @property  public SysModelChannel sys
        
         @brief Gets or sets the System Monitoring model reference (reference for model pertaining to event).
        
         @return    The system.
         */

        public SysModelChannel Sys { get; set; }
    }
}
