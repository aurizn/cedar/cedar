﻿/*
 * CEDAR Operator Workload Assessment Tool
 * Copyright (C) 2023  aurizn
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;

namespace CEDAR.Shared.Modules.Task.Comms {
    /**
     @class CommsModelChannelEventArgs
    
     @brief Communication channel event parameters
    
     @author    Ryan Beruldsen
     @date  15/09/2016
     */

    internal class CommsModelChannelEventArgs : EventArgs {

        /**
         @property  public bool valueChange
        
         @brief Frequency value change event.
        
         @return    true if value change, false if not.
         */

        public bool ValueChange { get; set; }

        /**
         @property  public bool warnIndicateChange
        
         @brief Warning indication change event (warning is raised or resolved for channel)
        
         @return    true if warning indicate change, false if not.
         */

        public bool WarnIndicateChange { get; set; }

        /**
         @property  public bool selectionChange
        
         @brief Channel selection change event.
        
         @return    true if selection change, false if not.
         */

        public bool SelectionChange { get; set; }

        /**
         @property  public CommsModelChannel comms
        
         @brief Reference to relevant comms model.
        
         @return    The communications.
         */

        public CommsModelChannel Comms { get; set; }
    }
}
