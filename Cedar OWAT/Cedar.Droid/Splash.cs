﻿/*
 * CEDAR Operator Workload Assessment Tool
 * Copyright (C) 2023  aurizn
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using Android;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace CEDAR.Droid
{
    [Activity(Theme = "@style/MyTheme.Splash",
              MainLauncher = true,
              NoHistory = true)]
    public class SplashActivity : Activity
    {
        private bool CheckAppPermissions() {
            if ((int)Build.VERSION.SdkInt < 23) {
                return true;
            }
            if ((int)Build.VERSION.SdkInt > 30) {
                if (!Android.OS.Environment.IsExternalStorageManager) {
                    Android.Net.Uri uri = Android.Net.Uri.Parse("package:" + Application.Context.ApplicationInfo.PackageName);
                    Intent intent = new Intent(Android.Provider.Settings.ActionManageAppAllFilesAccessPermission, uri);
                    this.StartActivity(intent);
                    return false;
                }
                return true;
            } else {
                if (PackageManager.CheckPermission(Manifest.Permission.ReadExternalStorage, PackageName) != Permission.Granted
                    && PackageManager.CheckPermission(Manifest.Permission.WriteExternalStorage, PackageName) != Permission.Granted) {
                    var permissions = new string[] { Manifest.Permission.ReadExternalStorage, Manifest.Permission.WriteExternalStorage };
                    RequestPermissions(permissions, 1);
                    return false;
                } else {
                    return true;
                }
            }
        }
        protected override void OnCreate(Bundle bundle) {
            base.OnCreate(bundle);
            bool permissionsGranted = CheckAppPermissions();
            System.Threading.Thread.Sleep(3500);
            if ((int)Build.VERSION.SdkInt < 30) {
                while (!permissionsGranted) {
                    permissionsGranted = CheckAppPermissions();
                    if (!permissionsGranted) {
                        System.Threading.Thread.Sleep(3500);
                    }
                }
            } else {
                while (!Android.OS.Environment.IsExternalStorageManager) {
                    System.Threading.Thread.Sleep(3500);
                }
            }
            this.StartActivity(typeof(MainActivity));
        }
    }
}