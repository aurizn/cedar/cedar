﻿/*
 * CEDAR Operator Workload Assessment Tool
 * Copyright (C) 2023  aurizn
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using CocosSharp;
using CEDAR.Shared.Modules;
using System;
using System.Collections.Generic;
using System.Text;

namespace CEDAR.Shared.Modules.Menu {

    /**
     @class MenuViewDialog
    
     @brief Simple Yes/No/Ok dialog
    
     @author    Ryan Beruldsen
     @date  15/09/2016
     */

    class MenuViewDialog : CCNode {




        private readonly CCDrawNode _drawNode;

        /** @brief Dialog message string - in the form of a question to be responded by ok/cancel.*/
        private readonly string _message;

        /** @brief Dialog message label.*/
        private CCLabel _messageLabel;

        /** @brief Callback function, to enable custom logic to handle user ok/cancel dialog input.*/
        readonly Func<bool, bool> _callback;

        /** @brief Dialog ok/yes button. */
        private MenuViewButton _ok;

        /** @brief Dialog cancel/no button. */
        private MenuViewButton _cancel;

        /** @brief Listener for button touch events. */
        private readonly CCEventListenerTouchAllAtOnce _touchListener;

        /**
         @fn    public MenuViewDialog(string message, Func<bool, bool> callback)
        
         @brief Simple Yes/No/Ok dialog constructor.
        
         @author    Ryan Beruldsen
         @date  16/09/2016
        
         @param message     Message string of dialog.
         @param callback    Function callback of dialog (to resume whatever was paused by dialog).
         */
        public MenuViewDialog(string message, Func<bool, bool> callback) : base() {
            _drawNode = new CCDrawNode();
            _message = message;


            _touchListener = new CCEventListenerTouchAllAtOnce {
                OnTouchesBegan = HandleInput,
                OnTouchesEnded = HandleInputEnd
            };
            _callback = callback;
            AddEventListener(_touchListener, this);
            Init();

        }

        /**
         @fn    private void Init()


         @brief Simple Yes/No/Ok dialog init.
            */
        private void Init() {
            AddChild(_drawNode);

            //fade background to enhance visibility of dialog
            _drawNode.DrawRect(
                    new CCRect(0, 0, ModelData.Width, ModelData.Height),
                    fillColor: new CCColor4B(0, 0, 0, 100),
                    borderWidth: 0,
                    borderColor: CCColor4B.White);

            //dialog size variables
            float dialogHeight = ModelData.Height * 0.15f;
            float dialogWidth = ModelData.Width * 0.7f;
            float dialogX = ModelData.Width / 2f - ModelData.Width * 0.35f;
            float dialogY = ModelData.Height / 2f - dialogHeight / 2f;

            //draw main dialog rectangle
            _drawNode.DrawRect(
                    new CCRect(dialogX, dialogY, dialogWidth, dialogHeight),
                    fillColor: new CCColor4B(50, 50, 50, 255),
                    borderWidth: 0,
                    borderColor: CCColor4B.White);

            //draw dividing line between dialog message and response buttons
            _drawNode.DrawLine(
            from: new CCPoint(ModelData.Width / 2 - dialogWidth / 4,
                                ModelData.Height / 2),
            to: new CCPoint(ModelData.Width / 2 + dialogWidth / 4,
                                ModelData.Height / 2),
            lineWidth: 1,
            color: CCColor4B.White);

            //draw dialog message
            _messageLabel = new CCLabel(
                _message,
                "robotob",
                14,
                CCLabelFormat.SpriteFont
            ) {
                Position = new CCPoint(
                            ModelData.Width / 2f,
                            ModelData.Height / 2f + dialogHeight / 4),

                Color = CCColor3B.White,
                AnchorPoint = CCPoint.AnchorMiddle
            };

            //draw response ok/cancel buttons
            float buttonX = ModelData.Width / 2f;
            float buttonY = ModelData.Height / 2f - dialogHeight / 4;
            _ok = new MenuViewButton(
                label: "Yes",
                highlightColour: ModelData.Instance.GetColourPallete4B(ModelData.COLOUR_PARAM_ACTIVE),
                width: ModelData.Width * 0.175f,
                height: ModelData.Height * 0.04f,
                x: buttonX - dialogWidth / 4,
                y: buttonY);

            _cancel = new MenuViewButton(
                label: "No",
                highlightColour: ModelData.Instance.GetColourPallete4B(ModelData.COLOUR_PARAM_WARNING),
                width: ModelData.Width * 0.175f,
                height: ModelData.Height * 0.04f,
                x: buttonX + dialogWidth / 4,
                y: buttonY);

            AddChild(_messageLabel);
            AddChild(_ok);
            AddChild(_cancel);
        }

        /**
        @fn    private void MenuButtonHitTest(System.Collections.Generic.List<CCTouch> touches, MenuViewButton m)

        @brief Hit test for menu button, shorthand function.

        @author    Ryan Beruldsen
        @date  16/09/2016

        @param touches CocosSharp injected touch list (from touch handler)
        @param m       The MenuViewButton to test for bounding box hit.

        @return    True if within button/drop down bounds, otherwise false
        */

        private bool MenuButtonHitTest(System.Collections.Generic.List<CCTouch> touches, MenuViewButton m) {
            if (touches[0].Location.X > m.minButtonBounds.X &&
                     touches[0].Location.X < m.maxButtonBounds.X &&
                     touches[0].Location.Y > m.minButtonBounds.Y &&
                     touches[0].Location.Y < m.maxButtonBounds.Y &&
                     m.Visible) {
                return true;
            }
            return false;
        }

        /**
         @fn    private void HandleInputEnd(System.Collections.Generic.List<CCTouch> touches, CCEvent touchEvent)

         @brief Handles touch input end

         @author    Ryan Beruldsen
         @date  16/09/2016

         @param touches     CocosSharp injected touch list
         @param touchEvent  CocosSharp injected touch event
        */
        private void HandleInputEnd(System.Collections.Generic.List<CCTouch> touches, CCEvent touchEvent) {

            if (_ok.Active && MenuButtonHitTest(touches, _ok)) {
                _ok.Active = false;

                RemoveAllChildren();
                this.RemoveFromParent();
                _callback(true);

            }
            if (_cancel.Active && MenuButtonHitTest(touches, _cancel)) {
                _cancel.Active = false;
                RemoveAllChildren();
                this.RemoveFromParent();
                _callback(false);


            }

        }

        /**
         @fn    private void HandleInput(System.Collections.Generic.List<CCTouch> touches, CCEvent touchEvent)

         @brief Handles touch input end

         @author    Ryan Beruldsen
         @date  16/09/2016

         @param touches     CocosSharp injected touch list
         @param touchEvent  CocosSharp injected touch event
         */
        private void HandleInput(System.Collections.Generic.List<CCTouch> touches, CCEvent touchEvent) {
            if (touches.Count > 0) {
                if (MenuButtonHitTest(touches, _ok)) {
                    _ok.Active = true;

                }
                if (MenuButtonHitTest(touches, _cancel)) {
                    _cancel.Active = true;

                }
            }
        }

    }
}
