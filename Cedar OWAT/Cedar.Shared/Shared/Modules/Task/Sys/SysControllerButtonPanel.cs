﻿/*
 * CEDAR Operator Workload Assessment Tool
 * Copyright (C) 2023  aurizn
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using CocosSharp;
using System.Collections.Generic;
using CEDAR.Shared.CedarCollection;

namespace CEDAR.Shared.Modules.Task.Sys {

    /**
     @class SysControllerButtonPanel
    
     @brief Controller for panel of system buttons (SysViewButton), representing system monitoring toggles (non level variable).
    
     @author    Ryan Beruldsen
     @date  15/09/2016
     */

    class SysControllerButtonPanel : DivisionContent, IDivisionContent {

        /** @brief List of system channels (models). */
        private readonly List<SysModelChannel> _sysChannelList;

        /** @brief Listener for button/gauge touch events. */
        private readonly CCEventListenerTouchAllAtOnce _touchListener;

        /** @brief List of system buttons. */
        private readonly List<SysViewButton> _sysButtonList;

        /** @brief Hash map of buttons (views) to respective models. */
        private readonly Dictionary<SysModelChannel, SysViewButton> _sysButtonDict;

        /**
         @fn    public SysControllerButtonPanel(List<SysModelChannel> SysChannelList, Division division) : base(division)
        
         @brief Constructor of System Monitoring Button Panel.
         @date  22/09/2016
        
         @param SysChannelList  List of system channels (models).
         @param division        The parent division.
         */

        public SysControllerButtonPanel(List<SysModelChannel> SysChannelList, Division division) : base(division) {
            _sysChannelList = SysChannelList;
            _sysButtonList = new List<SysViewButton>();
            _sysButtonDict = new Dictionary<SysModelChannel, SysViewButton>();

            _touchListener = new CCEventListenerTouchAllAtOnce {
                OnTouchesBegan = HandleInput
            };
            AddEventListener(_touchListener, this);
        }

        /**
         @fn    public void Init()
        
         @brief Initialise System Monitoring Button Panel.
        
         @author    Ryan Beruldsen
         @date  22/09/2016
         */

        public void Init() {

            float width = _division.Width * 0.250f;
            float height = _division.Height * 0.2f;

            float positionX = _division.Width * 0.025f;
            float positionY = _division.PrevDivisionY + (_division.Height * 0.5f);
            float positionY2 = _division.PrevDivisionY + (_division.Height * 0.20f);

            //button number, tracking which of (two) button 
            // vertically stacked panel is being created
            int buttonNum = 0;

            //instantiate button panel views
            for (int j = 0; j < _sysChannelList.Count; j++) {

                //model list contains guage and button models, button
                // models are tagged with warn only mode
                if (_sysChannelList[j].WarnOnlyMode && buttonNum < 2) {
                    var sysChannel = _sysChannelList[j];
                    sysChannel.SysModelChannelEventHandler += SysModelChanged;

                    //first vertically stacked button
                    if (buttonNum == 0) {
                        SysViewButton sysButton = new SysViewButton(
                            division: _division,
                            label: sysChannel.Label,
                            colourActiveText: ModelData.Instance.GetColourPallete3B(ModelData.COLOUR_PARAM_ACTIVE),
                            colourWarning: ModelData.Instance.GetColourPallete4B(ModelData.COLOUR_PARAM_WARNING),
                            width: width,
                            height: height,
                            x: positionX,
                            y: positionY
                        );

                        sysButton.Init();
                        AddChild(sysButton);

                        _sysButtonList.Add(sysButton);
                        _sysButtonDict[sysChannel] = sysButton;

                        //create next button
                        buttonNum++;
                    }
                    //second vertically stacked button
                    else {
                        SysViewButton sysButton2 = new SysViewButton(
                            division: _division,
                            label: sysChannel.Label,
                            colourActiveText: ModelData.Instance.GetColourPallete3B(ModelData.COLOUR_PARAM_ACTIVE),
                            colourWarning: ModelData.Instance.GetColourPallete4B(ModelData.COLOUR_PARAM_WARNING),
                            width: width,
                            height: height,
                            x: positionX,
                            y: positionY2
                        );
                        sysButton2.Init();
                        AddChild(sysButton2);
                        _sysButtonList.Add(sysButton2);
                        _sysButtonDict[sysChannel] = sysButton2;
                    }
                }
            }
        }

        /**
         @fn    private void SysModelChanged(object sender, SysModelChannelEventArgs e)
        
         @brief Handle sys model events.
        
         @author    Ryan Beruldsen
         @date  22/09/2016
        
         @param sender  Source of the event.
         @param e       System model channel event information.
         */

        private void SysModelChanged(object sender, SysModelChannelEventArgs e) {
            
            //button has raised warning
            if (e.WarnIndicateChange && 
                e.Sys.WarnOnlyMode && 
                _sysButtonDict.ContainsKey(e.Sys)) {
                _sysButtonDict[e.Sys].WarnIndicate = e.Sys.WarnIndicate;
            }
            
            //button has been selected
            else if (e.SelectionChange) {
            }
            
        }

        /**
         @fn    private void HandleInput(System.Collections.Generic.List<CCTouch> touches, CCEvent touchEvent)
        
         @brief Handles touch input.
        
         @author    Ryan Beruldsen
         @date  22/09/2016
        
         @param touches     CocosSharp injected touch list
         @param touchEvent  CocosSharp injected touch event
         */

        private void HandleInput(System.Collections.Generic.List<CCTouch> touches, CCEvent touchEvent) {
            if (touches.Count > 0) {
                int warnOnlyCount = 0;
                bool hit = false;

                for (int j = 0; j < _sysChannelList.Count; j++) {

                    if (_sysChannelList[j].WarnOnlyMode) {
                        if (    touches[0].Location.X > _sysButtonList[warnOnlyCount].minButtonBounds.X && 
                                touches[0].Location.X < _sysButtonList[warnOnlyCount].maxButtonBounds.X &&
                                touches[0].Location.Y > _sysButtonList[warnOnlyCount].minButtonBounds.Y && 
                                touches[0].Location.Y < _sysButtonList[warnOnlyCount].maxButtonBounds.Y) {

                            hit = true;
                            _sysChannelList[j].IsSelected = true;

                            if (_sysChannelList[j].WarnIndicate) {
                                _sysChannelList[j].WarnIndicate = false;

                                ModelDataIOLog.Instance.LogTouch(
                                    x: touches[0].Location.X,
                                    y: touches[0].Location.Y,
                                    status: CDRTouchData.StatusEnum.Hit
                                );
                            }else {
                                ModelDataIOLog.Instance.LogTouch(
                                    x: touches[0].Location.X,
                                    y: touches[0].Location.Y,
                                    status: CDRTouchData.StatusEnum.FalsePos
                                );
                            }
                        }
                        warnOnlyCount++;
                    }
                };

                if (!hit) {
                    ModelDataIOLog.Instance.LogTouch(
                        x: touches[0].Location.X,
                        y: touches[0].Location.Y,
                        status: CDRTouchData.StatusEnum.Miss
                    );
                }
                
            }
        }

    }
}
