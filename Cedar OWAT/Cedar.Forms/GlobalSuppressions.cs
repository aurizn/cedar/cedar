﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Style", "IDE0059:Unnecessary assignment of a value", Justification = "<Pending>", Scope = "member", Target = "~M:CEDAR.Shared.Modules.Task.Comms.CommsControllerSimple.SchedTrigger(System.Collections.Generic.Dictionary{System.String,System.String})~System.Int32")]
[assembly: SuppressMessage("Style", "IDE0059:Unnecessary assignment of a value", Justification = "<Pending>", Scope = "member", Target = "~M:CEDAR.Shared.Modules.Task.Ext.ExtController.SchedTrigger(System.Collections.Generic.Dictionary{System.String,System.String})~System.Int32")]
[assembly: SuppressMessage("Style", "IDE0059:Unnecessary assignment of a value", Justification = "<Pending>", Scope = "member", Target = "~M:CEDAR.Shared.Modules.Task.Res.ResController.SchedTrigger(System.Collections.Generic.Dictionary{System.String,System.String})~System.Int32")]
[assembly: SuppressMessage("CodeQuality", "IDE0052:Remove unread private members", Justification = "<Pending>", Scope = "member", Target = "~F:CEDAR.Shared.Modules.Task.Res.ResViewGauge._maxLevel")]
[assembly: SuppressMessage("Style", "IDE0059:Unnecessary assignment of a value", Justification = "<Pending>", Scope = "member", Target = "~M:CEDAR.Shared.Modules.Task.Res.ResViewPumpControl.MajorToMajor")]
[assembly: SuppressMessage("Style", "IDE0059:Unnecessary assignment of a value", Justification = "<Pending>", Scope = "member", Target = "~M:CEDAR.Shared.Modules.Task.Res.ResViewPumpControl.MinorToMajor")]
[assembly: SuppressMessage("Style", "IDE0059:Unnecessary assignment of a value", Justification = "<Pending>", Scope = "member", Target = "~M:CEDAR.Shared.Modules.Task.Res.ResViewPumpControl.MinorToMinor")]
[assembly: SuppressMessage("Style", "IDE0059:Unnecessary assignment of a value", Justification = "<Pending>", Scope = "member", Target = "~M:CEDAR.Shared.Modules.Task.Sys.SysController.SchedTrigger(System.Collections.Generic.Dictionary{System.String,System.String})~System.Int32")]
