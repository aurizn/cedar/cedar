﻿/*
 * CEDAR Operator Workload Assessment Tool
 * Copyright (C) 2023  aurizn
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using CocosSharp;
using System;
using System.Collections.Generic;
using System.Text;
using CEDAR.Shared.Modules;
using CEDAR.Shared.CedarCollection;

namespace CEDAR.Shared.Modules.Task.Comms {

    /**
     @class CommsControllerSelectBar
    
     @brief Communications task select bar, communications channels are selected from a series of buttons (CommsViewSelectButton) on a horizontal bar.
    
     @author    Ryan Beruldsen
     @date  15/09/2016
     */

    class CommsControllerSelectBar : DivisionContent, IDivisionContent {

        /** main internal draw node. */
        private readonly CCDrawNode _drawNode;

        /** Global model data singleton. */
        private readonly ModelData _modelI = ModelData.Instance;

        /** The currently selected comms channel. */
        private int _selectedChannel = -1;

        /** List of communications channel models. */
        private readonly List<CommsModelChannel> _commsChannelList;

        /** @brief Listener for button touch events. */
        private readonly CCEventListenerTouchAllAtOnce _touchListener;

        /** @brief List of communications channel select buttons. */
        private readonly List<CommsViewSelectButton> _commsButtonList;

        /** @brief Hash map of communications channel buttons (views) to respective models. */
        private readonly Dictionary<CommsModelChannel,
            CommsViewSelectButton> _commsChannelButtonDict;

        /**
         @fn    public CommsControllerSelectBar(List<CommsModelChannel> CommChannelList, ControllerDivision division) : base(division)
        
         @brief Constructor of communications channel select bar.
        
         @author    Ryan Beruldsen
         @date  20/09/2016
        
         @param CommChannelList List of communications channels.
         @param division        Parent division.
         */

        public CommsControllerSelectBar(List<CommsModelChannel> CommChannelList,
                                            Division division) : base(division) {

            _commsChannelList = CommChannelList;
            _commsButtonList = new List<CommsViewSelectButton>();
            _drawNode = new CCDrawNode();
            _commsChannelButtonDict = new Dictionary<CommsModelChannel, CommsViewSelectButton>();
            _touchListener = new CCEventListenerTouchAllAtOnce {
                OnTouchesBegan = HandleInput
            };
            AddEventListener(_touchListener, this);
        }

        /**
         @fn    public void Init()
        
         @brief Initialise communications channel select bar.
        
         @author    Ryan Beruldsen
         @date  20/09/2016
         */

        public void Init() {

            AddChild(_drawNode);

            //draw select bar backing rectangle
            float width = _division.Width * 0.9f;
            float height = _division.Height * 0.2f;
            float positionY = _division.PrevDivisionY + (_division.Height * 0.6f);
            float positionX = _division.Width * 0.05f;

            _drawNode.DrawRect(
                new CCRect(positionX, positionY, width, height),
                fillColor: new CCColor4B(100, 100, 100, 100),
                borderWidth: 0,
                borderColor: CCColor4B.White);

            //create buttons for each selectable channel model
            for (int j = 0; j < _commsChannelList.Count; j++) {
                var channel = _commsChannelList[j];
                if (channel.IsSelected) _selectedChannel = j;

                CommsViewSelectButton button = new CommsViewSelectButton(
                    label: channel.LabelStr,
                    index: channel.index,
                    selected: channel.IsSelected,
                    warnIndicate: channel.WarnIndicate,
                    displayValue: channel.ValueStr,
                    targetValue: channel.WarnIndicate ? channel.TargetStr : "",
                    colourActive:
                        _modelI.GetColourPallete4B(ModelData.COLOUR_PARAM_ACTIVE),
                    colourActiveText:
                        _modelI.GetColourPallete3B(ModelData.COLOUR_PARAM_ACTIVE),
                    colourWarning:
                       _modelI.GetColourPallete4B(ModelData.COLOUR_PARAM_WARNING),
                    width: width,
                    barHeight: height,
                    position: new CCPoint(positionX, positionY),
                    division: _division
                );

                button.Init();

                AddChild(button);
                _commsButtonList.Add(button);
                _commsChannelButtonDict[channel] = button;
                channel.CommsModelEventHandlerQueue += new
                    CommsModelEventHandler(HandleComms);

            }

        }

        /**
         @fn    private void HandleComms(object sender, CommsModelChannelEventArgs e)
        
         @brief Handles for comms channel events (new frequency target ect).
        
         @author    Ryan Beruldsen
         @date  20/09/2016
        
         @param sender  Source of the event.
         @param e       Communications model channel event information.
         */

        private void HandleComms(object sender, CommsModelChannelEventArgs e) {
            bool silentMode = ModelData.Instance.Settings[ModelData.SILENT_MODE] == ModelData.PARAM_ON;
            bool trainMode = ModelData.Instance.Settings[ModelData.TRAINING_MODE] == ModelData.PARAM_ON;

            //if selected frequency is changed
            if (e.ValueChange) {
                if (_commsChannelButtonDict.ContainsKey(e.Comms)) {

                    if (e.Comms.WarnIndicate &&
                        (silentMode || trainMode)) {

                        _commsChannelButtonDict[e.Comms].SetDisplayStr(
                            displayValue: e.Comms.ValueStr,
                            targetValue: e.Comms.TargetStr
                        );
                    }
                    else {
                        _commsChannelButtonDict[e.Comms].SetDisplayStr(e.Comms.ValueStr);
                    }
                }
                //if channel selection is changed
            }
            else if (e.SelectionChange) {
                if (e.Comms.IsSelected) _selectedChannel = e.Comms.index;

                if (_commsChannelButtonDict.ContainsKey(e.Comms)) {
                    _commsChannelButtonDict[e.Comms].IsSelected =
                        e.Comms.IsSelected;
                }
                //if a warning is raised (a channel is not at target frequency)
            }
            else if (e.WarnIndicateChange && (silentMode || trainMode)) {
                if (_commsChannelButtonDict.ContainsKey(e.Comms)) {
                    var commButton = _commsChannelButtonDict[e.Comms];
                    commButton.WarnIndicate = e.Comms.WarnIndicate;

                    if (e.Comms.WarnIndicate &&
                        e.Comms.Frequency != e.Comms.TargetFrequency) {

                        commButton.SetDisplayStr(
                            displayValue: e.Comms.ValueStr,
                            targetValue: e.Comms.TargetStr
                        );
                    }
                    else {
                        commButton.SetDisplayStr(e.Comms.ValueStr);
                    }
                }
            }
        }

        /**
         @fn    private void HandleInput(System.Collections.Generic.List<CCTouch> touches, CCEvent touchEvent)
        
         @brief Handles touch input
        
         @author    Ryan Beruldsen
         @date  20/09/2016
        
         @param touches     CocosSharp injected touch list
         @param touchEvent  CocosSharp injected touch event
         */

        private void HandleInput(System.Collections.Generic.List<CCTouch> touches,
                                    CCEvent touchEvent) {

            if (touches.Count > 0) {
                bool hit = false;
                for (int j = 0; j < _commsChannelList.Count; j++) {

                    if (touches[0].Location.X > _commsButtonList[j].MinButtonBounds.X &&
                            touches[0].Location.X < _commsButtonList[j].MaxButtonBounds.X &&
                            touches[0].Location.Y > _commsButtonList[j].MinButtonBounds.Y &&
                            touches[0].Location.Y < _commsButtonList[j].MaxButtonBounds.Y) {
                        _selectedChannel = j;
                        CDRTouchData.StatusEnum status;
                        if (_commsChannelList[_selectedChannel].WarnIndicate &&
                            !_commsChannelList[_selectedChannel].IsSelected &&
                            _commsChannelList[_selectedChannel].Frequency !=
                            _commsChannelList[_selectedChannel].TargetFrequency) {
                            status = CDRTouchData.StatusEnum.Hit;
                        }
                        else {
                            _commsChannelList[_selectedChannel].other = true;
                            status = CDRTouchData.StatusEnum.FalsePos;
                        }


                        ModelDataIOLog.Instance.LogTouch(
                                          x: touches[0].Location.X,
                                          y: touches[0].Location.Y,
                                          status: status
                       );

                        hit = true;

                    }
                };

                if (!hit) {
                    ModelDataIOLog.Instance.LogTouch(
                                         x: touches[0].Location.X,
                                         y: touches[0].Location.Y,
                                         status: CDRTouchData.StatusEnum.Miss
                    );
                }

                for (int j = 0; j < _commsChannelList.Count; j++) {
                    if (_selectedChannel == j && !_commsChannelList[j].IsSelected) {
                        _commsChannelList[j].IsSelected = true;
                    }
                    if (_selectedChannel != j && _commsChannelList[j].IsSelected) {
                        _commsChannelList[j].IsSelected = false;
                    }
                }
            }
        }

    }
}
