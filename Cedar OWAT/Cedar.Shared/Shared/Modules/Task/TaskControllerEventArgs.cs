﻿/*
 * CEDAR Operator Workload Assessment Tool
 * Copyright (C) 2023  aurizn
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


using System;

namespace CEDAR.Shared.Modules.Task {
    /**
     @class TaskControllerEventArgs
    
     @brief Test run task related data (warinings raised ect) event parameters
    
     @author    Ryan Beruldsen
     @date  15/09/2016
     */

    internal class TaskControllerEventArgs : EventArgs {
        /**
         @property  public bool warningRaised
        
         @brief Gets or sets a value indicating a warning raise event.
        
         @return    true if warning raise event, false if not.
         */
        public bool WarningRaised { get; set; }

        /**
         @property  public bool warningRaised
        
         @brief Gets or sets a value indicating a warning resolved event.
        
         @return    true if warning resolved event, false if not.
         */
        public bool WarningResolved { get; set; }

        /**
         @property  public bool penaltyRaised
        
         @brief Gets or sets a value indicating a penalty raised event.

            -this is when a task requiring user input has timed out, score is deducted for training
        
         @return    true if penalty raised event, false if not.
         */
        public bool PenaltyRaised { get; set; }

        /**
         @property  public bool enabledEvent
        
         @brief Gets or sets a value indicating an enabled event - allows quick updating of enabled info to user.
        
         @return    true if enabled event, false if not.
         */
        public bool EnabledEvent { get; set; }

        /**
         @property  public int penaltyNum
        
         @brief Gets or sets a value indicating a penalty value raised in a penalty event.

            -this is when a task requiring user input has timed out, score is deducted for training
        
         @return    penalty value raised in a penalty event.
         */
        public int PenaltyNum { get; set; }

        /**
         @property  public Task.ITaskContent task
        
         @brief Gets or sets a ITaskContent reference to task which raised event
        
         @return    ITaskContent reference to task which raised event
         */
        public ITaskContent Task { get; set; }
    }
}
