﻿/*
 * CEDAR Operator Workload Assessment Tool
 * Copyright (C) 2023  aurizn
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

/*! \mainpage Cedar Operator Workload Assessment Tool
 *
 * \section intro_sec Introduction
 *
 * Cedar is an implementation of the NASA MATB-II program for the tablet form factor. The Cedar source implements the Model-View-Controller software architectural pattern - classes are named with their role in the MVC pattern prefixed.
 */

using System;
using System.Collections.Generic;
using CocosSharp;

/*! \namespace Cedar
 * @brief Cedar: NASA MATB II tablet implementation with Xamarin Cocos Sharp
 */

/*! \namespace CEDAR.Shared 
 * @brief Cross-platform shared codebase.
 * 
 * Contains most of app logic, only platform-specific actions such as file-loading should be handled in non-shared packages (not documented here, as code is very sparse).
 */


namespace CEDAR.Shared {
    /**
     @class CSGameDelegate
    
     @brief CocosSharp required game delegate class, initial setup, screen resolution ect.
    
     @author    Ryan Beruldsen
     @date  14/09/2016
     */

    public static class CSGameDelegate {
        public static void PauseGame(object sender, EventArgs e) {
            System.Diagnostics.Debug.WriteLine("PAUSE");
        }
        public static void LoadGame(object sender, EventArgs e) {
            if (sender is CCGameView gameView) {
                var contentSearchPaths = new List<string>() { "Fonts", "Sounds" };

                CCTexture2D.DefaultIsAntialiased = true;
                //initialise screen dimensions
                int height = ModelData.Height;
                int width = ModelData.Width;

                // Set world dimensions
                gameView.DesignResolution = new CCSizeI(width, height);
                gameView.ResolutionPolicy = CCViewResolutionPolicy.ShowAll;

                CCSprite.DefaultTexelToContentSizeRatio = 2.0f;
                gameView.ContentManager.SearchPaths = contentSearchPaths;

                CCScene gameScene = new CCScene(gameView);
                gameScene.AddLayer(new CSGameLayer());
                gameView.RunWithScene(gameScene);
            }
        }

    }
}