/*
 * CEDAR Operator Workload Assessment Tool
 * Copyright (C) 2023  aurizn
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

/*
\module{GameLayer} %%%%%%%%%%%%%%%%%%%%%%

The main Game Layer
*/

using System;
using System.Collections.Generic;
using CocosSharp;
using Microsoft.Xna.Framework;
using CEDAR.Shared;
using CEDAR.Shared.Modules.Menu;

namespace CEDAR.Shared {
    /**
     @class CSGameLayer
    
     @brief CocosSharp required game layer instantiates all views, controllers and models for Cedar.
    
     @author    Ryan Beruldsen
     @date  14/09/2016
     */

    public class CSGameLayer : CCLayerColor {
        
        /** @brief The screen dimensions. */
        CCPoint windowMax = new CCPoint(ModelData.Width, ModelData.Height);

        private readonly int _RATE_SCREEN_INDEX = 10;
        /** @brief List of screen divisions. */
        private readonly List<Modules.Division> _divisionList;

        /** @brief List of division tasks. */
        private readonly List<Modules.Task.ITaskContent> _taskList;

        /** @brief The task controller - contains taskings logic. */
        private Modules.Task.TaskScheduleController _taskings;

        /**
         @fn    public void PaintScreen()
        
         @brief Paints the screen - instantiates all division and task controllers.
        
         @author    Ryan Beruldsen
         @date  6/10/2016
         */

        public void PaintScreen() {
            windowMax = new CCPoint(ModelData.Width, ModelData.Height);
            bool displayMode = ModelData.Instance.Settings[ModelData.MODE_DISPLAY] == ModelData.PARAM_ON;

            if (displayMode){
				windowMax.Y = ModelData.Height * 0.80f;

                //draw version information
                CCLabel versionInfo = new CCLabel("Cedar OWAT Open Source Version " + ModelData.CEDAR_VERSION,
                                                   "roboto",
                                                   14,
                                                   CCLabelFormat.SpriteFont
                ) {
                    Position = new CCPoint(windowMax.X / 2, ModelData.Height * 0.85f),
                    Color = CCColor3B.White,
                    AnchorPoint = CCPoint.AnchorMiddleTop
                };

                AddChild(versionInfo);
			}

            //base draw node - used here only to draw division lines
            CCDrawNode drawNode = new CCDrawNode();
            this.AddChild(drawNode);

            //divide window into vertical divisions
            int divisions = ModelData.NUM_DIVISIONS;
            float divisionHeight = windowMax.Y / divisions;
            float divisionWidth = windowMax.X;

            //load any custom Cedar settings present in config
            var configCedarSettings = ModelDataIOConfig.Instance.GetConfigType(ModelData.CEDAR_SETTINGS);
            foreach (KeyValuePair<string, string> setting in configCedarSettings) {
                ModelData.Instance.ChangeSetting(key: setting.Key, value: setting.Value, notify: false);
            }

            //intialise each division
            for (int i = 1; i <= divisions; i++) {
                int divisionRef = divisions - i + 1;

                //load the tag of the task to be loaded into the current division
                string divisionModule = ModelData.Instance.Settings["TASK_DIVISION_" + divisionRef];

                //calculate division bounds
				float currentDivisionY = (windowMax.Y / divisions) * i;
				float prevDivisionY = (divisions - 1) != 0 ? (windowMax.Y / divisions) * (i - 1) : 0;

                //draw the division label
                CCLabel divisionLabel = new CCLabel(
                    ModelData.Instance.userConfigStr[divisionModule], 
                    "robotob", 
                    14, 
                    CCLabelFormat.SpriteFont
                ) {
                    Position = new CCPoint(windowMax.X / 2, prevDivisionY + (divisionHeight * 0.9f)),
                    Color = CCColor3B.White,
                    AnchorPoint = CCPoint.AnchorMiddle,
                };
				if (!displayMode) {
					AddChild(divisionLabel);
				}
                

                //handle each task type, instantiate, register with task schedule controller
                
                if (divisionModule == ModelData.TASK_COMMUNICATIONS) {
                    var commModule = new Modules.Division(
                            width: divisionWidth,
                            height: divisionHeight,
                            x: 0,
                            y: currentDivisionY,
                            prevDivisionY: prevDivisionY,
                            windowMax: windowMax);
                    commModule.Content = new Modules.Task.Comms.CommsController(commModule);
                    _taskList.Add((Modules.Task.ITaskContent)commModule.Content);
                    _divisionList.Add(commModule);
                    AddChild(commModule);

                }

                if (divisionModule == ModelData.TASK_COMMUNICATIONS_SIMPLE_1 ||
                    divisionModule == ModelData.TASK_COMMUNICATIONS_SIMPLE_2 ||
                    divisionModule == ModelData.TASK_COMMUNICATIONS_SIMPLE_3) {
                    Modules.Task.Comms.CommsControllerSimple.CommsSimpleConfig config = 
                        Modules.Task.Comms.CommsControllerSimple.CommsSimpleConfig.SC1;

                    if (divisionModule == ModelData.TASK_COMMUNICATIONS_SIMPLE_2) {
                        config = Modules.Task.Comms.CommsControllerSimple.CommsSimpleConfig.SC2;
                    } else if (divisionModule == ModelData.TASK_COMMUNICATIONS_SIMPLE_3) {
                        config = Modules.Task.Comms.CommsControllerSimple.CommsSimpleConfig.SC3;
                    }
                    var commModule = new Modules.Division(
                            width: divisionWidth,
                            height: divisionHeight,
                            x: 0,
                            y: currentDivisionY,
                            prevDivisionY: prevDivisionY,
                            windowMax: windowMax);
                    commModule.Content = new Modules.Task.Comms.CommsControllerSimple(commModule, config);
                    _taskList.Add((Modules.Task.ITaskContent)commModule.Content);
                    _divisionList.Add(commModule);
                    AddChild(commModule);

                }

                if (divisionModule == ModelData.TASK_SYSTEM_MONITORING) {
                    var sysModule = new Modules.Division(
                            width: divisionWidth,
                            height: divisionHeight,
                            x: 0,
                            y: currentDivisionY,
                            prevDivisionY: prevDivisionY,
                            windowMax: windowMax);
                    sysModule.Content = new Modules.Task.Sys.SysController(sysModule);
                    _taskList.Add((Modules.Task.ITaskContent)sysModule.Content);
                    _divisionList.Add(sysModule);
                    AddChild(sysModule);

                }

                if (divisionModule == ModelData.TASK_RESOURCE_MANAGEMENT) {
                    var resModule = new Modules.Division(
                            width: divisionWidth,
                            height: divisionHeight,
                            x: 0,
                            y: currentDivisionY,
                            prevDivisionY: prevDivisionY,
                            windowMax: windowMax);
                    resModule.Content = new Modules.Task.Res.ResController(resModule);
                    _taskList.Add((Modules.Task.ITaskContent)resModule.Content);
                    _divisionList.Add(resModule);
                    AddChild(resModule);

                }

                if (divisionModule == ModelData.TASK_EXTENSION_SAMPLE) {
                    var extModule = new Modules.Division(
                            width: divisionWidth,
                            height: divisionHeight,
                            x: 0,
                            y: currentDivisionY,
                            prevDivisionY: prevDivisionY,
                            windowMax: windowMax);
                    extModule.Content = new Modules.Task.Ext.ExtController(extModule);
                    _taskList.Add((Modules.Task.ITaskContent)extModule.Content);
                    _divisionList.Add(extModule);
                    AddChild(extModule);

                }

                //draw division dividing line on base draw node
				if (i != divisions || displayMode) {
                    drawNode.DrawLine(
                        from: new CCPoint(0, currentDivisionY),
                        to: new CCPoint(windowMax.X, currentDivisionY),
                        lineWidth: 2,
                        color: ModelData.Instance.GetColourPallete4B(
                            ModelData.COLOUR_PARAM_ACTIVE));
                }

            }

        }

        /**
         @fn    public CSGameLayer() : base(CCColor4B.Black)
        
         @brief Base CocosSharp "game" layer constructor.
        
         @author    Ryan Beruldsen
         @date  6/10/2016
         */

        public CSGameLayer() : base(CCColor4B.Black) {
            _taskList = new List<Modules.Task.ITaskContent>();
            _divisionList = new List<Modules.Division>();
            AddChild(new Modules.Menu.MenuControllerTitleScreen(windowMax));
        }

        /**
         @fn    protected override void AddedToScene()
        
         @brief Added to scene override - CocosSharp generated function.
        
         @author    Ryan Beruldsen
         @date  6/10/2016
         */

        protected override void AddedToScene() {
            base.AddedToScene();

            // Use the bounds to layout the positioning of our drawable assets
            var bounds = VisibleBoundsWorldspace;

            // Register for touch events
            var touchListener = new CCEventListenerTouchAllAtOnce();
            ModelData.Instance.ModelDataEventHandler += 
                new ModelDataChangedEventHandler(ModelDataChanged);
            AddEventListener(touchListener, this);
        }

        /**
         @fn    private void ModelDataChanged(Object sender, ModelDataEventArgs e)
        
         @brief Handle model data change events - this handles page navigate changes (eg. from "title" to "settings").
        
         @author    Ryan Beruldsen
         @date  6/10/2016
        
         @param sender  Source of the event.
         @param e       Model data event information.
         */

        private void ModelDataChanged(Object sender, ModelDataEventArgs e) {
            if (e.PageNavigate) {
				windowMax.Y = ModelData.Height;

				//test screen - main Cedar test session screen
                if (ModelData.CurrentPageStr.Equals("test")) {
                    _taskList.Clear();
                    _divisionList.Clear();
                    this.RemoveAllChildren();
                    this.PaintScreen();
                    _taskings = new Modules.Task.TaskScheduleController(_taskList);
                    AddChild(_taskings);

                //test screen return from a workload rating pause
                } else if (ModelData.CurrentPageStr.Equals("test_return")) {
                    _taskings.TaskPause = 1;
                    RemoveChildByTag(_RATE_SCREEN_INDEX);

                    //workload rating screen - Cedar test session is paused 
                    //during the period the "rate" page is active
                } else if (ModelData.CurrentPageStr.Equals("rate") || ModelData.CurrentPageStr.Equals("wrs")) {
                    AddChild(new Modules.Menu.MenuControllerRateScreen(windowMax), 10, _RATE_SCREEN_INDEX);

                    //settings screen - allows the user to change settings not specified
                    //in events or config input files
                } else if (ModelData.CurrentPageStr == "settings") {
                    this.RemoveAllChildren();
                    AddChild(new Modules.Menu.MenuControllerSettingsScreen(windowMax));

                    //title screen - main Cedar OWAT title screen, first screen on 
                    //cold start, main menu
                } else if (ModelData.CurrentPageStr.Equals("title")) {
                    this.RemoveAllChildren();
                    AddChild(new Modules.Menu.MenuControllerTitleScreen(windowMax));

                    //about screen - Cedar OWAT version and other information
                } else if (ModelData.CurrentPageStr.Equals("about")) {
                    this.RemoveAllChildren();
                    AddChild(new Modules.Menu.MenuControllerAboutScreen(windowMax));
                } else if (ModelData.CurrentPageStr.Equals("q")) {
                    this.RemoveAllChildren();
                    AddChild(new Modules.Menu.MenuControllerQScreen(windowMax));
                }

            } else if (e.Dialog) {
                AddChild(new MenuViewDialog(
                   e.DialogMessage,
                   e.DialogCallback
               ));
            }
        }

    }
}