﻿/*
 * CEDAR Operator Workload Assessment Tool
 * Copyright (C) 2023  aurizn
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;

using Xamarin.Forms;
using CocosSharp;
using System.Collections.Generic;
using CEDAR.Shared;

namespace CEDAR {
    public class MainPage : ContentPage {
        public MainPage() {
            NavigationPage.SetHasNavigationBar(this, false);
            var grid = new Grid();
            var stack = new StackLayout() {
                Padding = new Thickness(ModelData.XOffset, 0, 0, 0),
                VerticalOptions = LayoutOptions.FillAndExpand,
                HorizontalOptions = LayoutOptions.FillAndExpand,
            };
            this.BackgroundColor = Color.Black;
            grid.RowSpacing = 0;
            this.Content = stack;
            grid.RowDefinitions = new RowDefinitionCollection {
				new RowDefinition{ Height = GridLength.Star},
			};

            var gameView = new CocosSharpView {
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.FillAndExpand,
                ViewCreated = HandleViewCreated,
                DesignResolution = new Size(CEDAR.Shared.ModelData.Width, CEDAR.Shared.ModelData.Height),
                ResolutionPolicy = CocosSharpView.ViewResolutionPolicy.ShowAll,
            };
            CCSprite.DefaultTexelToContentSizeRatio = 2.0f;
            stack.Children.Add(gameView);
        }

        void HandleViewCreated(object sender, EventArgs e) {
            if (sender is CCGameView gameView) {
                int width = CEDAR.Shared.ModelData.WidthOR;
                if (Device.RuntimePlatform == Device.iOS) {
                    width = CEDAR.Shared.ModelData.Width;
                }
                gameView.DesignResolution = new CCSizeI(width, CEDAR.Shared.ModelData.Height);
                var contentSearchPaths = new List<string>() { "Fonts", "Sounds" };
                gameView.ContentManager.SearchPaths = contentSearchPaths;
                var gameScene = new GameScene (gameView);

                CCSprite.DefaultTexelToContentSizeRatio = 2.0f;

                gameScene.AddLayer(new CEDAR.Shared.CSGameLayer());
                gameView.RunWithScene(gameScene);

            }

        }
    }
}


