﻿/*
 * CEDAR Operator Workload Assessment Tool
 * Copyright (C) 2023  aurizn
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using CocosSharp;
using System;

namespace CEDAR.Shared.Modules.Task.Sys {
    /**
     @class SysViewGauge
    
     @brief A gauge component representing a system monitoring level, displayed in series in SysControllerGaugePanel
    
     If in warning mode (when the gauge level is above or below the standard operating levels) can be fixed by tapping on the gauge.

     @author    Ryan Beruldsen
     @date  15/09/2016
     */

    class SysViewGauge : DivisionContent, IDivisionContent {

        /** @brief The x coordinate. */
        private readonly float _x;

        /** @brief The y coordinate. */
        private readonly float _y;

        /** @brief The width. */
        private readonly float _width;

        /** @brief The height. */
        private readonly float _height;

        /** @brief The minimum value. */
        private readonly double _minSafeLevel;

        /** @brief The maximum value. */
        private readonly double _maxSafeLevel;

        /** @brief The system monitoring level. */
        private double _level;

        /** @brief The gauge label. */
        private readonly string _label;

        /** @brief The system monitoring level, formatted in correct units. */
        private string _valueStr;

        /** @brief Gauge warning colour (user to show current level during warning state). */
        private readonly CCColor4B _colourWarning;

        /** @brief Gauge warning colour (user to show current level during warning state). */
        private readonly CCColor3B _colourWarningText;

        /** @brief Gauge active text colour (used to show current levels). */
        private readonly CCColor4B _colourActive;

        /** @brief Gauge active text colour (used to show current levels). */
        private readonly CCColor4B _colourRange;

        /** @brief The warning tolerance (height of gauge indicator). */
        private readonly float _tolerance;

        /** @brief The safe range of the gauge - in display pixels for drawing purposes. */
        private readonly float _range;

        /** @brief The main gauge draw node. */
        private readonly CCDrawNode _gaugeDrawNode;

        /** @brief The critical range draw node. */
        private readonly CCDrawNode _criticalRangeDrawNode;
        private bool _warnIndicate;
        private readonly CCSprite _levelIndicate;
        private readonly CCSprite _levelWarnIndicate;

        /** @brief The minimum button bounds. */
        public CCPoint minButtonBounds;

        /** @brief The maximum button bounds. */
        public CCPoint maxButtonBounds;

        /**
         @property  public bool warnIndicate
        
         @brief Gets or sets a value indicating whether the warning indicate.
        
         @return    true if warning indicate, false if not.
         */

        public bool WarnIndicate {
            get { return _warnIndicate; }
            set {
                _warnIndicate = value;
                _levelWarnIndicate.Visible = _warnIndicate;
            }
        }

        /**
         @fn    public void SetLevel(double level, string valueStr)
        
         @brief Sets the gauge system monitoring level draws relevant graphical representation.
        
         @author    Ryan Beruldsen
         @date  22/09/2016
        
         @param level       The system monitoring level.
         @param valueStr    The level string in correct units.
         */

        public void SetLevel(double level, string valueStr) {
            _valueStr = valueStr;
            _level = level;

            ((CCLabel)_levelWarnIndicate.GetChildByTag(1)).Text = valueStr;
            ((CCLabel)_levelIndicate.GetChildByTag(1)).Text = valueStr;
            _levelIndicate.RunAction(
                    new CCMoveTo(
                        duration: 0.2f, 
                        position: new CCPoint(
                            x: _levelWarnIndicate.PositionX, 
                            y: Convert.ToSingle(level) * (_height - (_height * _tolerance))
                        )
                    )
            );
            _levelWarnIndicate.RunAction(
                    new CCMoveTo(
                        duration: 0.2f,
                        position: new CCPoint(
                            x: _levelWarnIndicate.PositionX,
                            y: Convert.ToSingle(level) * (_height - (_height * _tolerance))
                        )
                    )
            );
            _levelWarnIndicate.PositionY = _levelIndicate.PositionY;
        }

        /**
         @fn    public SysViewGauge( Division division, string label, CCColor4B colourActive, CCColor4B colourRange, CCColor4B colourWarning, CCColor3B colourWarningText, double level, string valueStr, double minSafeLevel, double maxSafeLevel, double tolerance, float width, float height, float x, float y) : base(division)
        
         @brief Constructor.
        
         @author    Ryan Beruldsen
         @date  22/09/2016
        
         @param division            The parent division.
         @param label               The label.
         @param colourActive        The active colour.
         @param colourRange         The range colour.
         @param colourWarning       The warning colour.
         @param colourWarningText   The warning text colour.
         @param level               The initial gauge level.
         @param valueStr            The initial gauge level string in units.
         @param minSafeLevel        The minimum safe value.
         @param maxSafeLevel        The maximum safe value.
         @param tolerance           The tolerance of levels outside safe range (width of gauge level indicator).
         @param width               The gauge width.
         @param height              The gauge height.
         @param x                   The x coordinate.
         @param y                   The y coordinate.
         */

        public SysViewGauge(    Division division, 
                                string label,
                                CCColor4B colourActive,
                                CCColor4B colourRange,
                                CCColor4B colourWarning,
                                CCColor3B colourWarningText,
                                double level,
                                string valueStr,
                                double minSafeLevel, 
                                double maxSafeLevel, 
                                double tolerance,
                                float width, 
                                float height, 
                                float x, 
                                float y) : base(division) {

            _colourActive = colourActive;
            _colourRange = colourRange;
            _colourWarning = colourWarning;
            _colourWarningText = colourWarningText;
            _label = label;
            _valueStr = valueStr;
            //derived initial warning indicate state
            _warnIndicate = level < minSafeLevel || level > maxSafeLevel;
            _height = height;
            _width = width;
            _level = level;
            _minSafeLevel = minSafeLevel;
            _maxSafeLevel = maxSafeLevel;
            _x = x;
            _y = y;
            _gaugeDrawNode = new CCDrawNode();
            _criticalRangeDrawNode = new CCDrawNode();

			_tolerance = Convert.ToSingle(tolerance);
			_range = (_height * Convert.ToSingle(_maxSafeLevel - _minSafeLevel));

			_levelIndicate = new CCSprite();
            _levelWarnIndicate = new CCSprite();
        }

        /**
         @fn    public void DrawCriticalRange(CCDrawNode drawNode)
        
         @brief Draws critical (safe) range on sys gauge.
        
         @author    Ryan Beruldsen
         @date  22/09/2016
        
         @param drawNode    The target draw node to draw to.
         */

        public void DrawCriticalRange(CCDrawNode drawNode) {
            drawNode.DrawRect(
				new CCRect(
                    _x -_division.Width * 0.01f, 
                    _y + Convert.ToSingle(_minSafeLevel) * _height - _height * _tolerance, 
                    _division.Width * 0.01f, 
                    _range + _height * _tolerance
                ),
                fillColor: _colourRange,
                borderWidth: 0,
                borderColor: CCColor4B.White
            );

            drawNode.DrawRect(
				new CCRect(_x + _width, _y + Convert.ToSingle(_minSafeLevel) * _height - _height * _tolerance, _division.Width * 0.01f, _range + _height * _tolerance),
                fillColor: _colourRange,
                borderWidth: 0,
                borderColor: CCColor4B.White);
        }

        /**
         @fn    public void Init()
        
         @brief Initialises, draws a system monitoring gauge.
        
         @author    Ryan Beruldsen
         @date  22/09/2016
         */

        public void Init() {

            CCDrawNode levelIndicatorDrawNode = new CCDrawNode();
            CCDrawNode levelIndicatorWarningDrawNode = new CCDrawNode();
            _levelIndicate.AddChild(levelIndicatorDrawNode);
            _levelWarnIndicate.AddChild(levelIndicatorWarningDrawNode);

            _levelWarnIndicate.Visible = _warnIndicate;

            _levelIndicate.PositionY = Convert.ToSingle(_level) * (_height - (_height * 0.2f));
            _levelWarnIndicate.PositionY = _levelIndicate.PositionY;

            AddChild(_gaugeDrawNode);
            AddChild(_levelIndicate);
            AddChild(_levelWarnIndicate);
            AddChild(_criticalRangeDrawNode);

            minButtonBounds = new CCPoint(_x, _y);
            maxButtonBounds = new CCPoint(_x+ _width, _y + _height);

            //scaled label padding
            float padding = Convert.ToSingle(10.0 / 768.0) * _division.WindowMax.X;

            //draw labels
            CCLabel sysLabel = new CCLabel(_label, "roboto", 12) {
                Position = new CCPoint(_x - padding, _y),
                Color = CCColor3B.White,
                AnchorPoint = CCPoint.AnchorLowerRight
            };
            CCLabel sysLevelUnits = new CCLabel(_valueStr, "roboto", 12) {
                Position = new CCPoint(_x + _width + padding, _y),
                Color = CCColor3B.White,
                AnchorPoint = CCPoint.AnchorLowerLeft
            };
            CCLabel sysLevelWarn = new CCLabel(_valueStr, "roboto", 12) {
                Position = new CCPoint(_x + _width + padding, _y),
                Color = _colourWarningText,
                AnchorPoint = CCPoint.AnchorLowerLeft
            };
            AddChild(sysLabel);
            _levelIndicate.AddChild(sysLevelUnits, 1, 1);
            _levelWarnIndicate.AddChild(sysLevelWarn, 1, 1);

            //draw gauge backing light grey rect
            _gaugeDrawNode.DrawRect(
                   new CCRect(_x, _y, _width, _height),
                   fillColor: new CCColor4B(100, 100, 100, 255),
                   borderWidth: 0,
                   borderColor: CCColor4B.White);

            //draw level indicator
            levelIndicatorDrawNode.DrawRect(
                new CCRect(_x, _y, _width, _height * _tolerance),
                fillColor: _colourActive,
                borderWidth: 0,
                borderColor: CCColor4B.White);

            //draw level indicator warning variant
            levelIndicatorWarningDrawNode.DrawRect(
                new CCRect(_x, _y, _width, _height * _tolerance),
                fillColor: _colourWarning,
                borderWidth: 0,
                borderColor: CCColor4B.White);

            //draw safe range
            DrawCriticalRange(_criticalRangeDrawNode);
        }
    }
}
