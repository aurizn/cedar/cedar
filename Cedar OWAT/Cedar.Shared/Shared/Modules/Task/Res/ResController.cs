﻿/*
 * CEDAR Operator Workload Assessment Tool
 * Copyright (C) 2023  aurizn
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using CocosSharp;
using CEDAR.Shared;
using CEDAR.Shared.CedarCollection;
using System;
using System.Collections.Generic;
using System.Text;

/*! \r\namespace CEDAR.Shared.Modules.Task.Res
 * @brief Resource Management task content
 * 
 */

namespace CEDAR.Shared.Modules.Task.Res {

    /**
     @class ResController
    
     @brief Resource management task content controller - handles instantiating instances of child Res models and controllers
    
     @author    Ryan Beruldsen
     @date  15/09/2016
     */

    class ResController : DivisionContent, IDivisionContent, ITaskContent {

        /** @brief Tank A list index constant. */
        private const int _TANK_A = 2;

        /** @brief Tank B list index constant. */
        private const int _TANK_B = 3;

        /** @brief Tank C list index constant. */
        private const int _TANK_C = 1;

        /** @brief Tank D list index constant. */
        private const int _TANK_D = 5;

        /** @brief The event brief description in task log file. */
        private static readonly string _EVENT_BRIEF = "Resource Management";

        /** @brief Number of pumps. */
        private const int _NUM_PUMPS = 8;

        /** @brief Number of tanks. */
        private const int _NUM_TANKS = 6;

        /** @brief Unique tag of Resources management task. */
        private const string RES_TAG = "resman";

        /** @brief Critical tank capacity, supply tank capacity is half. */
        private readonly int _tank_capacity = 4000;

        /** @brief Initial critical tank capacity
         * 
         * - supply tank capacity is 50% (default 1000) of full. */
        private readonly int _tank_initial = 2500;

        /** @brief Critical tank minimum capacity. */
        private readonly int _tank_min = 2000;

        /** @brief Critical tank maximum capacity. */
        private readonly int _tank_max = 3000;


        /** @brief Map of pump index to timestamp of fail fix. */
        private readonly Dictionary<int, long> _pump_fail_dict;

        /** @brief List of resource tanks. */
        private readonly List<ResModelTank> _resTankList;

        /** @brief List of resource pumps. */
        private readonly List<ResModelPump> _resPumpList;

        /** @brief IO event to function register. */
        private readonly CDRIOEventFn _ioEventDict;

        /** @brief Enabled status of task (sched start/stop event).*/
        private bool _enabled = false;

        /** @brief Random number generator for random events. */
        private readonly Random _rnd;

        /** @brief Division status label string - contains test related information for division (see user guide).*/
        private CCLabel _divStatus;

        /**
         @fn    public ResController(Division division) : base(division)
        
         @brief Constructor for Resource Management task
        
         @author    Ryan Beruldsen
         @date  21/09/2016
        
         @param division    The division to contain the Resource Management task content.
         */

        public ResController(Division division) : base(division) {
            _resTankList = new List<ResModelTank>();
            _resPumpList = new List<ResModelPump>();
            _rnd = new Random();

            //set intial tank levels, properties - if (non-MATB default) custom set
            int tank_capacity = ModelDataIOConfig.Instance.GetConfigNum("resman_tlevels", "capacity");
            int tank_min = ModelDataIOConfig.Instance.GetConfigNum("resman_tlevels", "min");
            int tank_max = ModelDataIOConfig.Instance.GetConfigNum("resman_tlevels", "max");
            int tank_initial = ModelDataIOConfig.Instance.GetConfigNum("resman_tlevels", "initial");

            _pump_fail_dict = new Dictionary<int, long>();
            //if (non-MATB default) custom set
            if (!(tank_capacity < 0)) {
                _tank_capacity = tank_capacity;
            }
            if (!(tank_min < 0)) {
                _tank_min = tank_min;
            }
            if (!(tank_max < 0)) {
                _tank_max = tank_max;
            }
            if (!(tank_initial < 0)) {
                _tank_initial = tank_initial;
            }

            //register IO event functions
            _ioEventDict = new CDRIOEventFn {
                ["sched"] = SchedTrigger,
                [RES_TAG] = ResmanTrigger
            };

            //add plaintext log file header
            PlainTextlogFileHeader();

            //trigger recording of tank levels for log metrics
            TriggerTimeout(ModelDataIOConfig.Instance.GetConfigNum("recording_interval", "task_resman"),
                                        callback: ResmanTrigger,
                                        parameters: new CDRKeyValData() {
                                                            { "record", "interval" }
                                        });
        }

        /**
         @fn    public void PlainTextlogFileHeader()
        
         @brief Adds resources plaintext log file header.
        
         @author    Ryan Beruldsen
         @date  5/10/2016
         */

        public void PlainTextlogFileHeader() {
            //set tank flow rates
            int tA = Convert.ToInt32(ModelDataIOConfig.Instance.GetConfigValue("resman_rates", "tank_a"));
            int tB = Convert.ToInt32(ModelDataIOConfig.Instance.GetConfigValue("resman_rates", "tank_b"));

            double[] numPumps = new double[_NUM_PUMPS];

            for (int i = 0; i < _NUM_PUMPS; i++) {
                numPumps[i] = Convert.ToDouble(
                    ModelDataIOConfig.Instance.GetConfigValue("resman_rates", "pump_" + (i + 1)));
            }

            int recInterval = ModelDataIOConfig.Instance.GetConfigNum("recording_interval", "task_resman");

            var eventRow = new CDRLogRow() {
                    "-ELAPSED TIME",
                    "-PUMP #",
                    "-PUMP ACTION",
                    "-TANK UPDATE",
                    "-TANK A",
                    "-TANK B",
                    "-TANK C",
                    "-TANK D",
            };
            string header = "# Pump Flow Rates in Units Per Minute (Tank Direction)\r\n" +
            "# Pump 1 (C>A) = " + numPumps[0] + "   Pump 2 (E>A) = " + numPumps[1] + "   Pump 3 (D>B) = " + numPumps[2] + "   Pump 4 (F>B) = " + numPumps[3] + "\r\n" +
            "# Pump 5 (E>C) = " + numPumps[4] + "   Pump 6 (F>D) = " + numPumps[5] + "   Pump 7 (A>B) = " + numPumps[6] + "   Pump 8 (B>A) = " + numPumps[7] + "\r\n" +
            "#\r\n" +
            "# Tank Consumption Rates in Units Per Minute\r\n" +
            "# Tank A = " + tA + "   Tank B = " + tB + "\r\n" +
            "#\r\n" +
            "# Tank Levels in Units\r\n" +
            "# Initial = " + _tank_initial + "   Capacity = " + _tank_capacity + "   Minimum = " + _tank_min + "   Maximum = " + _tank_max + "\r\n" +
            "#\r\n" +
            "# Recording Interval is " + recInterval + " seconds\r\n" +
            "#\r\n" +
            "# N = Recording trigged by a pump action \r\n" +
            "# Y = Recording trigged by a timer, and not a pump action \r\n" +
                                        "#";
            ModelDataIOLog.Instance.LogTaskHeader(RES_TAG, header);
            ModelDataIOLog.Instance.LogTaskEvent(RES_TAG, eventRow);
        }

        /**
         @fn    public void Init()
        
         @brief Initialise the views, models and controllers for the Resource Management task.
        
         @author    Ryan Beruldsen
         @date  21/09/2016
         */

        public void Init() {
            //default status string
            _divStatus = new CCLabel("", "roboto", 12) {
                Position = new CCPoint(_division.Width / 2, _division.Y - _division.Height * 0.95f),
                Color = CCColor3B.White,
                AnchorPoint = CCPoint.AnchorMiddle
            };
            AddChild(_divStatus);

            //instantiate tank models, handle tank events
            InitTankModels();

            //instantiate pump models
            InitPumpModels();

            //add gauge panel to division
            _division.AddContent(new ResControllerGaugePanel(_resTankList, _resPumpList, _division));
        }

        /**
         @fn    public void InitTankModels()
        
         @brief Instantiate tank models, handle tank events.
        
         @author    Ryan Beruldsen
         @date  5/10/2016
         */

        public void InitTankModels() {

            for (int j = 0; j < _NUM_TANKS; j++) {
                string tankLabel = ResControllerGaugePanel.tankLabels[j];
                bool critical = j == _TANK_A || j == _TANK_B;
                bool unlimited = tankLabel.Equals("E") || tankLabel.Equals("F");
                int supplyLevel = _tank_capacity / 4;
                if (unlimited) {
                    supplyLevel = Convert.ToInt32(supplyLevel * 1.5);
                }
                _resTankList.Add(new ResModelTank(label: tankLabel,
                                                    critical: critical,
                                                    capacity: critical ? _tank_capacity : _tank_capacity / 2,
                                                    unlimited: tankLabel.Equals("E") ||
                                                                tankLabel.Equals("F"),
                                                    level: critical ? _tank_initial : supplyLevel,
                                                    minRange: _tank_min,
                                                    maxRange: _tank_max));

                _resTankList[j].ResModelTankEventHandler +=
                    new ResModelTankEventHandler(ResModelTankChanged);
            }
        }

        /**
         @fn    public void InitPumpModels()
        
         @brief Init pump models.
        
         @author    Ryan Beruldsen
         @date  5/10/2016
         */

        public void InitPumpModels() {

            double[] pumpRatesPerMinute = new double[_NUM_PUMPS];
            double[] pumpRatesPer100ms = new double[_NUM_PUMPS];

            for (int i = 0; i < _NUM_PUMPS; i++) {

                //calculate pump rates given IO rate config
                pumpRatesPerMinute[i] = Convert.ToDouble(
                    ModelDataIOConfig.Instance.GetConfigValue("resman_rates", "pump_" + (i + 1)));
                pumpRatesPer100ms[i] = (pumpRatesPerMinute[i] / 60) / 10;

                //read from ResControllerGaugePanel pump arrangement listing, configure accordingly
                // pump arrangement is in format of "A>B" where A,B are tank labels
                // "A>B" would refer to a pump from tank A to B

                int from = Array.IndexOf(ResControllerGaugePanel.tankLabels,
                    (ResControllerGaugePanel.pumpDescriptions[i].ToCharArray()[0]).ToString());

                int to = Array.IndexOf(ResControllerGaugePanel.tankLabels,
                    (ResControllerGaugePanel.pumpDescriptions[i].ToCharArray()[2]).ToString());

                _resPumpList.Add(
                    new ResModelPump(description: ResControllerGaugePanel.pumpDescriptions[i],
                                        rate: pumpRatesPer100ms[i],
                                        fault: false,
                                        channelFrom: _resTankList[from],
                                        channelTo: _resTankList[to])
                );

                _resPumpList[i].ResPumpEventHandler += new
                    ResModelPumpEventHandler(ResModelPumpChanged);
            }
        }


        /**
         @fn    void EventExternal(string type, Dictionary<string, string> parameters)
        
         @brief Trigger for task event.
        
         @param type        Distinguishing event type (ie. task-level schedule or component level action).
         @param parameters  Event parameters.
         */

        public void EventExternal(string type, Dictionary<string, string> parameters) {
            if (_ioEventDict.ContainsKey(type)) {
                _ioEventDict[type](parameters);
            }
        }

        /**
         @fn    bool getEnabled();
        
         @brief Enabled status of task (sched start/stop event).
        
         */
        public bool GetEnabled() {
            return _enabled;
        }

        /**
         @fn    void SetEnabled(bool enabled);
        
         @brief Enabled status of task.
        
         @param enabled    Enabled status of task (sched start/stop event).
         */
        public void SetEnabled(bool enabled) {
            _enabled = enabled;

            var t = new TaskControllerEventArgs {
                EnabledEvent = true,
                Task = this
            };
            TasksControllerChanged(t);

            foreach (ResModelPump pump in _resPumpList) {
                pump.Enabled = enabled;
            }

            foreach (ResModelTank tank in _resTankList) {
                tank.Enabled = enabled;
            }

            if (enabled) {
                Schedule(Pump, interval: 0.1f);
            }
            else {
                UnscheduleAll();
            }
        }

        /**
         @fn    private void Pump(float time)
        
         @brief Pump event - single pump event, expected to run every 100ms.

            - Tank drain (level units decreasing without pump model) is expected to be countered by pumping resources into draining tanks by the user.
        
         @author    Ryan Beruldsen
         @date  5/10/2016
        
         @param time    The time.
         */

        private void Pump(float time) {
            double tankARatePerMinute = Convert.ToDouble(
                ModelDataIOConfig.Instance.GetConfigValue("resman_rates", "tank_a"));
            double tankBRatePerMinute = Convert.ToDouble(
                ModelDataIOConfig.Instance.GetConfigValue("resman_rates", "tank_b"));
            double tankARatePer100ms = (tankARatePerMinute / 60) / 10;
            double tankBRatePer100ms = (tankBRatePerMinute / 60) / 10;

            _resTankList[_TANK_A].LevelUnits -= tankARatePer100ms;
            _resTankList[_TANK_B].LevelUnits -= tankBRatePer100ms;


            for (int i = 0; i < _resPumpList.Count; i++) {
                if (_resPumpList[i].IsSelected && !_resPumpList[i].PumpFault) {
                    _resPumpList[i].Pump();
                }
            }

        }

        /**
         @fn    public void SetDivisionStatus(string statusText)
        
         @brief Sets the division status string (_divStatus).
        
         @author    Ryan Beruldsen
         @date  21/09/2016
        
         @param statusText  The new status text string.
         */

        public void SetDivisionStatus(string statusText) {
            _divStatus.Text = statusText;
        }

        /**
         @fn    public string GetTag()
        
         @brief Gets the tag of the division for task scheduler identification purposes
        
         @author    Ryan Beruldsen
         @date  21/09/2016
        
         @return    The unique string tag of the division ("res").
         */

        public string GetTag() { return RES_TAG; }

        /**
         @fn    private void ResModelTankChanged(object sender, ResModelTankEventArgs e)
        
         @brief Listener for resource tank events on a task level, raises Division level event for TasksController.
        
         @author    Ryan Beruldsen
         @date  21/09/2016
        
         @param sender  Source of the event.
         @param e       Resource model tank event information.
         */

        private void ResModelTankChanged(object sender, ResModelTankEventArgs e) {

            //if a primary (critical) tank has raised a warning
            if (e.WarnIndicateChange && e.Res.WarnIndicate && e.Res.Critical) {
                TaskControllerEventArgs t = new TaskControllerEventArgs {
                    WarningRaised = true,
                    Task = this
                };
                _warnings++;
                TasksControllerChanged(t);

                //if a primary tank warning has been resolved
            }
            else if (e.WarnIndicateChange &&
                        !e.Res.WarnIndicate &&
                        e.Res.Critical) {
                TaskControllerEventArgs t = new TaskControllerEventArgs {
                    WarningResolved = true
                };
                _warnings--;
                t.Task = this;
                TasksControllerChanged(t);
            }

        }

        /**
         @fn    private void ResModelPumpChanged(object sender, ResModelPumpEventArgs e)
        
         @brief Listener for resource tank events on a task level - notes pump selection for log metrics.
        
         @author    Ryan Beruldsen
         @date  5/10/2016
        
         @param sender  Source of the event.
         @param e       Resource model pump event information.
         */

        private void ResModelPumpChanged(object sender, ResModelPumpEventArgs e) {
            if (e.SelectionChange) {
                var eventRow = new CDRLogRow() {
                        ModelData.Instance.DurationStr24Hr,
                        (_resPumpList.IndexOf(e.Pumpref)+1)+"",
                        e.Pumpref.IsSelected ? "on" : "off",
                        "n",
                        _resTankList[_TANK_A].LevelUnitsStr,
                        _resTankList[_TANK_B].LevelUnitsStr,
                        _resTankList[_TANK_C].LevelUnitsStr,
                        _resTankList[_TANK_D].LevelUnitsStr,
                };
                ModelDataIOLog.Instance.LogTaskEvent(RES_TAG, eventRow);
                ModelDataIOLog.Instance.LogEventBrief(_EVENT_BRIEF);
            }
        }

        /**
         @fn    private int resmanTrigger(Dictionary<string, string> parameters)
        
         @brief Trigger function for the "res" IO event tag.

            - handles all main Resource Management events, currently fail/fix pump
        
         @author    Ryan Beruldsen
         @date  5/10/2016
        
         @param parameters  IO Event parameters hash map.
        
         @return    An int - this result is ignored.
         */

        private int ResmanTrigger(Dictionary<string, string> parameters) {
            if (_enabled) {
                //pump fail or fix event - a pump is disabled while fail
                if (parameters.ContainsKey("fail") || parameters.ContainsKey("fix")) {
                    bool fail = parameters.ContainsKey("fail");

                    //index number of failed or fixed pump
                    int pump = fail ?
                                Convert.ToInt32(parameters["fail"][1] + "") :
                                Convert.ToInt32(parameters["fix"][1] + "");

                    //add log row
                    var eventRow = new CDRLogRow() {
                        ModelData.Instance.DurationStr24Hr,
                        pump+"",
                        fail ? "fail" : "fix",
                        "n",
                        _resTankList[_TANK_A].LevelUnitsStr,
                        _resTankList[_TANK_B].LevelUnitsStr,
                        _resTankList[_TANK_C].LevelUnitsStr,
                        _resTankList[_TANK_D].LevelUnitsStr,
                    };
                    ModelDataIOLog.Instance.LogTaskEvent(RES_TAG, eventRow);
                    ModelDataIOLog.Instance.LogEvent(_EVENT_BRIEF);

                    //update pump model
                    if (fail) {
                        _resPumpList[pump - 1].PumpFault = true;
                    }
                    else {
                        _resPumpList[pump - 1].PumpFault = false;
                    }
                    //record callback event - for logging
                }
                else if (parameters.ContainsKey("record")) {

                    //add log row
                    var eventRow = new CDRLogRow() {
                        ModelData.Instance.DurationStr24Hr,
                        "",
                        "",
                        "y",
                        _resTankList[_TANK_A].LevelUnitsStr,
                        _resTankList[_TANK_B].LevelUnitsStr,
                        _resTankList[_TANK_C].LevelUnitsStr,
                        _resTankList[_TANK_D].LevelUnitsStr,
                    };
                    ModelDataIOLog.Instance.LogTaskEvent(RES_TAG, eventRow);

                    int timeoutDuration =
                        ModelDataIOConfig.Instance.GetConfigNum("recording_interval", "task_resman");
                    //trigger next record callback
                    TriggerTimeout(timeoutDuration,
                                    callback: ResmanTrigger,
                                    parameters: new CDRKeyValData() {
                                        { "record", "interval" }
                                    }
                    );
                }

            }

            return 1;
        }

        /**
         @fn    private int SchedTrigger(Dictionary<string, string> parameters)
        
         @brief Trigger function for the "sched" IO event tag - starts and stops resmon task.
        
         @author    Ryan Beruldsen
         @date  5/10/2016
        
         @param parameters  IO Event parameters hash map.
        
         @return    An int - this result is ignored.
         */
        private int SchedTrigger(Dictionary<string, string> parameters) {
            string task = "NULL", action = "NULL", update = "NULL", response = "NULL";

            if (parameters.ContainsKey("task")) task = parameters["task"];
            if (parameters.ContainsKey("action")) action = parameters["action"];
            if (parameters.ContainsKey("update")) update = parameters["update"];
            if (parameters.ContainsKey("response")) response = parameters["response"];

            switch (task) {
                case "ressys":
                    switch (action) {
                        case "start":
                            ModelDataIOLog.Instance.LogEvent(
                                ModelData.Instance.DurationStr24Hr,
                                "Scheduling - RESMAN Active"
                            );
                            SetEnabled(true);
                            break;
                        default:
                            SetEnabled(false);
                            break;
                    }
                    break;
            }

            return 1;
        }

        /**
         @fn    public void Trigger(float time)
        
         @brief Random task trigger for user - used in random mode.

            - also triggers regular pumps, expected to be triggered every 100ms
        
         @author    Ryan Beruldsen
         @date  21/09/2016
        
         @param time    Time elapsed in seconds (not used).
         */

        public void Trigger(float time) {
            if (_enabled) {
                double tankARatePerMinute = Convert.ToDouble(
                    ModelDataIOConfig.Instance.GetConfigValue("resman_rates", "tank_a"));
                double tankBRatePerMinute = Convert.ToDouble(
                    ModelDataIOConfig.Instance.GetConfigValue("resman_rates", "tank_b"));

                double tankARatePer100ms = (tankARatePerMinute / 60) / 10;
                double tankBRatePer100ms = (tankBRatePerMinute / 60) / 10;

                _resTankList[_TANK_A].LevelUnits -= tankARatePer100ms;
                _resTankList[_TANK_B].LevelUnits -= tankBRatePer100ms;

                List<int> indexSet = new List<int>();

                //randomise pump faults
                for (int j = 0; j < _resPumpList.Count; j++) {
                    int i;
                    do {
                        i = Convert.ToInt32(Math.Round(_rnd.NextDouble() * (_resPumpList.Count - 1)));
                    } while (indexSet.Contains(i));
                    indexSet.Add(i);
                    var pump = _resPumpList[i];
                    if (!pump.PumpFault) {
                        pump.Pump();
                    }
                    //randomly raise pump fault if less than two pump faults already raised
                    if (!pump.PumpFault && _rnd.NextDouble() < 0.0005 && _pump_fail_dict.Count < 2) {
                        pump.PumpFault = true;

                        //fault duration 10-50 seconds
                        long faultDuration = 10 + Convert.ToInt64(40.0 * _rnd.NextDouble());
                        _pump_fail_dict.Add(i, ModelData.Instance.Duration + faultDuration);

                        //log pump fault
                        var eventRow = new CDRLogRow() {
                                            ModelData.Instance.DurationStr24Hr,
                                            (i+1)+"",
                                            "fail",
                                            "n",
                                            _resTankList[_TANK_A].LevelUnitsStr,
                                            _resTankList[_TANK_B].LevelUnitsStr,
                                            _resTankList[_TANK_C].LevelUnitsStr,
                                            _resTankList[_TANK_D].LevelUnitsStr,
                                        };
                        ModelDataIOLog.Instance.LogTaskEvent(RES_TAG, eventRow);
                        ModelDataIOLog.Instance.LogEvent(_EVENT_BRIEF);
                    }

                }
                //fix pump (remove from the fail dictionary) when fault duration expires
                var deleteKeyList = new List<int>();
                foreach (KeyValuePair<int, long> pumpFailData in _pump_fail_dict) {
                    if (ModelData.Instance.Duration == pumpFailData.Value) {
                        _resPumpList[pumpFailData.Key].PumpFault = false;

                        //log pump fix
                        var eventRow = new CDRLogRow() {
                                            ModelData.Instance.DurationStr24Hr,
                                            (pumpFailData.Key+1)+"",
                                            "fix",
                                            "n",
                                            _resTankList[_TANK_A].LevelUnitsStr,
                                            _resTankList[_TANK_B].LevelUnitsStr,
                                            _resTankList[_TANK_C].LevelUnitsStr,
                                            _resTankList[_TANK_D].LevelUnitsStr,
                                        };
                        ModelDataIOLog.Instance.LogTaskEvent(RES_TAG, eventRow);
                        ModelDataIOLog.Instance.LogEvent(_EVENT_BRIEF);
                        deleteKeyList.Add(pumpFailData.Key);
                    }
                }
                foreach (var key in deleteKeyList) {
                    _pump_fail_dict.Remove(key);
                }

                //old unlimited pump fail random logic
                /*
                for (int i = 0; i < _resPumpList.Count; i++) {
                    if (_resPumpList[i].isSelected && !_resPumpList[i].pumpFault) {
                        _resPumpList[i].pump();
                        if (_rnd.NextDouble() < 0.0005) {
                            _resPumpList[i].pumpFault = true;

                            //log pump fault
                            var eventRow = new CDRLogRow() {
                                ModelData.instance.durationStr24Hr,
                                (i+1)+"",
                                "fail",
                                "n",
                                _resTankList[_TANK_A].levelUnitsStr,
                                _resTankList[_TANK_B].levelUnitsStr,
                                _resTankList[_TANK_C].levelUnitsStr,
                                _resTankList[_TANK_D].levelUnitsStr,
                            };
                            ModelDataIOLog.instance.logTaskEvent(RES_TAG, eventRow);
                            ModelDataIOLog.instance.logEvent(_EVENT_BRIEF);

                        }
                    } else if (_resPumpList[i].isSelected && _resPumpList[i].pumpFault) {
                        if (_rnd.NextDouble() < 0.005) {
                            _resPumpList[i].pumpFault = false;

                            //log pump fix
                            var eventRow = new CDRLogRow() {
                                ModelData.instance.durationStr24Hr,
                                (i+1)+"",
                                "fix",
                                "n",
                                _resTankList[_TANK_A].levelUnitsStr,
                                _resTankList[_TANK_B].levelUnitsStr,
                                _resTankList[_TANK_C].levelUnitsStr,
                                _resTankList[_TANK_D].levelUnitsStr,
                            };
                            ModelDataIOLog.instance.logTaskEvent(RES_TAG, eventRow);
                            ModelDataIOLog.instance.logEvent(_EVENT_BRIEF);
                        }
                    }
                }
                */

            }
        }
    }
}
