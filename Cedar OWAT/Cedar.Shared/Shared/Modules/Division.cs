﻿/*
 * CEDAR Operator Workload Assessment Tool
 * Copyright (C) 2023  aurizn
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


/*! \namespace CEDAR.Shared.Modules
 * @brief Cedar high level MVC modules
 * 
 */

using CocosSharp;
using System;

namespace CEDAR.Shared.Modules {
    /**
     @class Division
    
     @brief Screen division instance: instantiates DivisionContent to add content.

        This class has both view, model and controller aspects and so is not labelled with MVC purpose.
        Division content can be added as either interactive content(controller) or static content (view).

     @author    Ryan Beruldsen
     @date  15/09/2016
     */

    class Division : CCNode {

        /**
         @property  public float width
        
         @brief Gets or sets the division width.
        
         @return    The width.
         */

        public float Width { get; set; }

        /**
         @property  public float height
        
         @brief Gets or sets the division height.
        
         @return    The height.
         */

        public float Height { get; set; }

        /**
         @property  public float y
        
         @brief Gets or sets the division y coordinate.
        
         @return    The y coordinate.
         */

        public float Y { get; set; }

        /**
         @property  public float prevDivisionY
        
         @brief Gets or sets the previous division y coordinate (this is the top most bound).

            - same as maxButtonBounds.Y
        
         @return    The previous division y coordinate.
         */

        public float PrevDivisionY { get; set; }

        /** @brief The internal content. */
        private IDivisionContent internalContent;

        /** @brief The minimum division bounds. */
        public CCPoint minButtonBounds;

        /** @brief The maximum division bounds. */
        public CCPoint maxButtonBounds;

        /** @brief The full window size containing the division. */
        private CCPoint _windowMax;

        /**
         @property  public CCPoint windowMax
        
         @brief Gets the full window size containing the division.
        
         @return    The full window size containing the division.
         */

        public CCPoint WindowMax { get { return _windowMax; } }

        /**
         @property  public IDivisionContent content
        
         @brief Gets or sets the division content.
        
         @return    The division content.
         */

        public IDivisionContent Content {
            get { return internalContent;  }
            set {
                if(internalContent != null) RemoveChild((DivisionContent) internalContent);
                internalContent = value;
                internalContent.Init();
                if (internalContent != null)  AddChild((DivisionContent) internalContent);
            }
        }

        /**
         @fn    public Division(float width, float height, float x, float y, float prevDivisionY, CCPoint windowMax) : base()
        
         @brief Constructor.
        
         @author    Ryan Beruldsen
         @date  6/10/2016
        
         @param width           The width.
         @param height          The height.
         @param x               The x coordinate.
         @param y               The y coordinate.
         @param prevDivisionY   Previous division y coordinate (this is the top most bound).
         @param windowMax       Full window size containing the division.
         */

        public Division(    float width, 
                            float height, 
                            float x, 
                            float y, 
                            float prevDivisionY, 
                            CCPoint windowMax) : base() {

            minButtonBounds = new CCPoint(x, y);
            maxButtonBounds = new CCPoint(x + width, y + height);
            this.Width = width;
            this.Height = height;
            _windowMax = windowMax;
            this.Y = y;
            this.PrevDivisionY = prevDivisionY;
        }

        /**
         @fn    public void AddContent(IDivisionContent content)
        
         @brief Adds division content.
        
         @author    Ryan Beruldsen
         @date  6/10/2016
        
         @param content The content.
         */

        public void AddContent(IDivisionContent content) {
            if (content != null) {
                content.Init();
                AddChild((DivisionContent)content);
            }
        }
        /**
         @fn    public void AddContent(IDivisionContent content)
        
         @brief Removes division content.
        
         @author    Ryan Beruldsen
         @date  6/10/2016
        
         @param content The content.
         */
        public void RemoveContent(IDivisionContent content) {
            RemoveChild((CCNode) content);
        }

        /**
         @fn    public void ClearContent(IDivisionContent content)
        
         @brief Clears the division content.
        
         @author    Ryan Beruldsen
         @date  6/10/2016
        
         @param content The content.
         */

        public void ClearContent(IDivisionContent content) {
            if (content is null) {
                return;
            }

            RemoveAllChildren();
        }
    }
}
