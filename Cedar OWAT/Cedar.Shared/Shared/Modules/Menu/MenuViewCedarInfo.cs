﻿/*
 * CEDAR Operator Workload Assessment Tool
 * Copyright (C) 2023  aurizn
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using CocosSharp;
using CEDAR.Shared.Modules;
using System;
using System.Collections.Generic;
using System.Text;

namespace CEDAR.Shared.Modules.Menu {

    /**
     @class MenuViewCedarInfo
    
     @brief Subtitle information (error, config) display for title screen.
    
     @author    Ryan Beruldsen
     @date  15/09/2016
     */

    class MenuViewCedarInfo : CCNode {

        /**
         @fn    public MenuViewCedarInfo(CCPoint windowMax): base()
        
         @brief Basic info subtitle constructor.
        
         @author    Ryan Beruldsen
         @date  16/09/2016
        
         @param windowMax   Max window bounds.
         */

        public MenuViewCedarInfo(CCPoint windowMax) : base() {
            this.windowMax = windowMax;
            Init();
        }

        private CCPoint windowMax;

        private void Init() {
            RemoveAllChildren();
            String primaryTitleStr = "";
            String subtitleStr = "";

            _ = ModelDataIOConfig.Instance.GetConfigBool("mode", "train_mode");
            bool trainMode;

            if (ModelData.Instance.Settings[ModelData.TRAINING_MODE] == ModelData.PARAM_ON) {
                trainMode = true;
            } else {
                trainMode = false;
            }

            if (!ModelData.CEDAREvents && ModelData.CEDARConfig) {
                primaryTitleStr = "Events file not loaded, config file loaded:";
                subtitleStr = ModelData.IsIOS ? "Send Cedar events file to iPad via iTunes or AirDrop" :
                                                "Place Cedar events file in USB ROOT cedar folder";

            } else if (ModelData.CEDAREvents && !ModelData.CEDARConfig) {
                primaryTitleStr = "Events file loaded, config file not loaded:";
                subtitleStr = ModelData.IsIOS ? "Send Cedar config file to iPad via iTunes or AirDrop" :
                                                "Place Cedar config file in USB ROOT cedar folder";

            } else if (!ModelData.CEDAREvents && !ModelData.CEDARConfig) {
                primaryTitleStr = "Events, config files not loaded - to load:";
                subtitleStr = ModelData.IsIOS ? "Send Cedar events, config files to iPad via iTunes or AirDrop" :
                                                "Place Cedar events, config files in USB ROOT /cedar folder";

            } else if (ModelData.CEDAREvents && ModelData.CEDARConfig) {
                primaryTitleStr = "Config, event files loaded";
            }

            CCLabel primaryTitle = new CCLabel(primaryTitleStr.ToUpper(),
                                                "robotob",
                                                14,
                                                CCLabelFormat.SpriteFont
            ) {
                Position = new CCPoint((windowMax.X / 2), (windowMax.Y / 5)),
                Color = CCColor3B.White
            };
            AddChild(primaryTitle);

            if (!subtitleStr.Equals("")) {
                CCLabel subTitle = new CCLabel(subtitleStr.ToUpper(),
                                                "robotob",
                                                14,
                                                CCLabelFormat.SpriteFont
                ) {
                    Position = new CCPoint((windowMax.X / 2), (windowMax.Y / 6)),
                    Color = CCColor3B.White
                };
                AddChild(subTitle);
            }
            string desig_1 = ModelData.Instance.userConfigStr[ModelData.Instance.Settings[ModelData.DESIGNATOR_1]];
            string desig_2 = ModelData.Instance.userConfigStr[ModelData.Instance.Settings[ModelData.DESIGNATOR_2]];
            CCLabel designator = new CCLabel("DESIGNATOR: " + desig_1.ToUpper() + " " + desig_2.ToUpper(),
                                                "robotob",
                                                14,
                                                CCLabelFormat.SpriteFont
                ) {
                Position = new CCPoint((windowMax.X / 2), (windowMax.Y * 0.9f)),
                Color = CCColor3B.Gray,
            };
            AddChild(designator);


            //draw score if in training mode
            if (ModelData.Instance.LastScore != -1 && trainMode) {
                string scoreText = "LAST SCORE: " + ModelData.Instance.LastScore +
                        " | BEST SCORE: " + ModelData.Instance.BestScore;
                CCLabel scoreLabel = new CCLabel(scoreText,
                                                    "roboto",
                                                    14,
                                                    CCLabelFormat.SpriteFont
                ) {
                    Position = new CCPoint((windowMax.X / 2), (windowMax.Y / 2.15f)),
                    Color = CCColor3B.White
                };
                AddChild(scoreLabel);
            } else if (ModelData.Instance.LoadedFile != "") {
                string scoreText = "FILE: " + ModelData.Instance.LoadedFile.ToUpper();
                CCLabel scoreLabel = new CCLabel(scoreText,
                                                    "roboto",
                                                    14,
                                                    CCLabelFormat.SpriteFont
                ) {
                    Position = new CCPoint((windowMax.X / 2), (windowMax.Y / 2.15f)),
                    Color = CCColor3B.White
                };
                AddChild(scoreLabel);
            }

        }
    }
}
