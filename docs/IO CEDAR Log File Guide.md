# IO CEDAR Log File Guide

The CEDAR log file is the top-level event log of a test session. Some task-specific events are listed in the CEDAR log as stub events that are not given an event number – these events are further described in relevant Cedar Task logs (see _Cedar Task Logs_ documentation, for example the [COMMS Log File Guide](./io-comms-log-file-guide/) for the _COMMS_ _Task_).

### Accessing Log Files

Cedar log file access methods differ for each supported platform:

#### Android

* Log files can be accessed in the internal memory of the Android device, in a folder called CEDAR, in the subfolder DATA.

#### iOS

* Log files are placed in a folder named DATA for file sharing, see: [https://support.apple.com/en-au/HT201301](https://support.apple.com/en-au/HT201301)

### Event Log Types

Cedar event log types fall into two categories:

#### Brief Events

Brief events are listed in the Cedar Log to link to the task-specific logs where those events are listed in greater detail. Example:

        00:00:23.1 Event Terminated: Monitoring

#### Full Events

Full events are listed in the fullest detail and are the main content of the CEDAR Log. They contain a unique event index which can be cross-referenced to any task-specific logs that also include event indexes. The index is the cumulative processing count of the event, so if an event has an index of 2, it is the second event processed by Cedar.

Example:

        00:00:01.0 2 Event Initiated: Scheduling - RESMAN Active

### MATB-II Compatibility

Event numbering, as described in the _Full Events_ section is cumulative based on the number of events processed, rather than the tag number of the XML, as it is in MATB-II. This is one reason event numbering may differ slightly from Cedar to MATB-II. Some events also output slightly different output, see the _Events Guide_ for more information on this.

Despite the above, Cedar events very closely match the output of MATB-II, given limited caveats.

#### Example Log File Snippet

        # 10-08-2016 20:54:00 CEDAR_2016_10082054.txt
        #
        # Events Filename: CEDAR_EVENTS.xml
        #
        # TIME      EVENT TASK 
        #---------------------------------------------------------------------------------
        00:00:00.4 1     Event Initiated: Control 
        00:00:01.0 2     Event Initiated: Scheduling - RESMAN Active 
        00:00:23.1       Event Terminated: Monitoring

#### Example JSON Log File

        {
        "rows": [
            {
            "TIME": "00:00:00.4",
            "EVENT": "1",
            "TASK": "Event Initiated: Control"
            },
            {
            "TIME": "00:00:01.0",
            "EVENT": "2",
            "TASK": "Event Initiated: Scheduling - RESMAN Active"
            },
            {
            "TIME": "00:00:05.0",
            "TASK": "Event Terminated: Monitoring"
            }
        ]
        }

#### Example Log File Snippet Explanation

The log file begins with a header detailing the timestamp and filename of the log file, then a plain text formatted table of log entries. The JSON format variant just lists the columns and rows of the plaintext file. Column titles are not listed in the JSON variant.

| Column | Format | Meaning |
| --- | --- | --- |
| TIME | 00:00:00.4 (hour:minute:second:tenth of second) | The timestamp of the event in question. |
| EVENT | Integer | The cumulative Cedar processing index of the event. |
| TASK | String | A description of the task event recorded. |