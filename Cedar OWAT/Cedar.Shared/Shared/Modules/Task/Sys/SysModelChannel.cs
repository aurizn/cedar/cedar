﻿/*
 * CEDAR Operator Workload Assessment Tool
 * Copyright (C) 2023  aurizn
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

namespace CEDAR.Shared.Modules.Task.Sys {
    internal delegate void SysModelChannelEventHandler(object sender, SysModelChannelEventArgs e);

    /**
     @class SysModelChannel
    
     @brief Model representing a systems monitoring channel, which could conceivably mean any monitored system resource.
        
     Levels can be variable (0 to 1, 0.005 for example) or set (0 or 1, as a toggle only) in warnOnlyMode
    
     @author    Ryan Beruldsen
     @date  15/09/2016
     */

    class SysModelChannel {
        /** @brief Event queue for all listeners interested in sysModelChannelEventHandler events. */
        public event SysModelChannelEventHandler SysModelChannelEventHandler;

        /** @brief Enabled status of model. */
        private bool _enabled = false;

        /**
         @property  public bool enabled
        
         @brief Gets or sets a value indicating whether the model is enabled.
        
         @return    true if enabled, false if not.
         */
        public bool Enabled {
            get { return _enabled; }
            set {
                _enabled = value;
            }
        }

        /** @brief System level warning state, if levels are outside prescribed min/max range 
        or if warning is raised manually in case of button (warn only) mode */
        private bool _warnIndicate = false;

        /** @brief The system monitoring value. */
        private double _value;

        /** @brief The minimum range. */
        private readonly double _minRange;

        /** @brief The minimum range. */
        private readonly double _maxRange;

        /** @brief Warning only mode (button) state - ignores range and raises . */
        private readonly bool _warnOnly;

        /** @brief The system gauge height, or tolerance to error. */
        private readonly double _height;

        /** @brief The sys channel label. */
        private readonly string _label;

        /** @brief The timestamp of the last event trigger. */
        public double Trigger { get; set; }

        /** @brief The current unique event index (to prevent phantom timeouts).*/
        public double Index { get; set; }

        /**
         @property  public string label
        
         @brief Gets the sys channel label.
        
         @return    The sys channel label.
         */

        public string Label { get { return _label; } }

        /**
         @property  public bool warnOnlyMode
        
         @brief Gets warn only mode (button) state
        
         @return    true if warning only mode, false if not.
         */

        public bool WarnOnlyMode { get { return _warnOnly; } }

        /**
         @property  public bool warnIndicate
        
         @brief Gets or sets a value indicating whether a warning indication is raised
        
         @return    true if warning indicate, false if not.
         */

        public bool WarnIndicate {
            get { return _warnIndicate; }
            set {
                if (_enabled) {
                    SysModelChannelEventArgs e = new SysModelChannelEventArgs {
                        WarnIndicateChange = true,
                        Sys = this
                    };
                    bool tempVal = _warnIndicate;
                    _warnIndicate = value;
                    if (tempVal != value) SysModelChannelChanged(e);
                }
            }
        }

        /**
         @property  public string valueStr
        
         @brief Gets the value string of the system model (between min, max, one decimal place).
        
         @return    The value string.
         */

        public string ValueStr {
            get {
                return _value.ToString("0.0");
            }
        }

        /**
         @property  public double maxVal
        
         @brief Gets the maximum value.
        
         @return    The maximum value.
         */

        public double MaxVal { get { return _maxRange; } }

        /**
         @property  public double minVal
        
         @brief Gets the minimum value.
        
         @return    The minimum value.
         */

        public double MinVal { get { return _minRange; } }

        /**
         @property  public double tolerance
        
         @brief Gets the tolerance (system gauge height) of the warning 
            indicator with regards to raising a warning when the level is outside safe ranges.
        
         @return    The tolerance.
         */

        public double Tolerance { get { return _height; } }

        /**
         @property  public bool isSelected
        
         @brief Sets a value indicating whether channel is selected
            
            - raises event, resets scale to center (if warningOnlyMode).
        
         @return    true if the channel is selected, false if not.
         */

        public bool IsSelected {
            set {
                if (_enabled) {
                    if (value) {
                        SysModelChannelEventArgs e = new SysModelChannelEventArgs {
                            SelectionChange = true,
                            Sys = this
                        };

                        this.Level = _minRange + (_maxRange - _minRange) / 2;
                        SysModelChannelChanged(e);
                    }
                }
            }
        }

        /**
         @property  public double level
        
         @brief Gets or sets the system monitoring level, raises warning if outside safe rage.
        
         @return    The system monitoring level.
         */

        public double Level {
            get { return _value; }
            set {
                if (_enabled) {
                    SysModelChannelEventArgs e = new SysModelChannelEventArgs();
                    if (value >= 0 && value <= 1) this._value = value;
                    if (value < 0) this._value = 0;
                    if (value > 1) this._value = 1;

                    if (this._value + this._height < _minRange && !WarnIndicate) {
                        WarnIndicate = true;
                    }
                    else if (this._value > _maxRange && !WarnIndicate) {
                        WarnIndicate = true;
                    }
                    else if (this._value >= _minRange &&
                                this._value + this._height <= _maxRange &&
                                WarnIndicate) {
                        WarnIndicate = false;
                    }
                    e.ValueChange = true;
                    e.Sys = this;
                    SysModelChannelChanged(e);
                }
            }
        }

        /**
         @fn    protected virtual void SysModelChannelChanged(SysModelChannelEventArgs e)
        
         @brief Raises system monitoring model events.
        
         @author    Ryan Beruldsen
         @date  22/09/2016
        
         @param e   System model channel event information.
         */

        protected virtual void SysModelChannelChanged(SysModelChannelEventArgs e) {
            SysModelChannelEventHandler?.Invoke(this, e);
        }

        /**
         @fn    public SysModelChannel(string label, double level, double height, double minRange, double maxRange, bool warnOnly)
        
         @brief Constructor for system monitoring model
        
         @author    Ryan Beruldsen
         @date  22/09/2016
        
         @param label       The system monitoring channel (model) label.
         @param level       The initial monitoring level.
         @param height      The gauge height (tolerance of warning raised when level outside safe range).
         @param minRange    The minimum safe range.
         @param maxRange    The maximum safe range.
         @param warnOnly    Warning only (button) mode, ignores range.
         */

        public SysModelChannel(string label,
                                double level,
                                double height,
                                double minRange = 0,
                                double maxRange = 0,
                                bool warnOnly = false) {

            this._value = level;
            _label = label;
            this._height = height;
            this._minRange = minRange;
            this._maxRange = maxRange;
            this._warnOnly = warnOnly;
        }
    }
}
