﻿/*
 * CEDAR Operator Workload Assessment Tool
 * Copyright (C) 2023  aurizn
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using CocosSharp;
using CEDAR.Shared.Modules;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace CEDAR.Shared.Modules.Menu {

    /**
     @class MenuViewButtonDropDown
    
     @brief A simple menu button with drop down functionality.
    
     @author    Ryan Beruldsen
     @date  15/09/2016
     */

    class MenuViewButtonDropDown : CCNode {

        /** @brief The x coordinate. */
        private readonly float _x;

        /** @brief The y coordinate. */
        private readonly float _y;

        /** @brief The width. */
        private readonly float _width;

        /** @brief The height. */
        private readonly float _height;

        /** @brief The minimum button bounds. */
        public CCPoint minButtonBounds;

        /** @brief The maximum button bounds. */
        public CCPoint maxButtonBounds;

        /** @brief The button text highlight colour (as part of button bar). */
        private readonly CCColor3B _highlightColourText;

        /** @brief The button highlight colour (as part of button bar). */
        private readonly CCColor4B _highlightColour;

        /** @brief map of drop down options to min button bounds. */
        public Dictionary<string, CCPoint> minButtonBoundsOptions;

        /** @brief map of drop down options to max button bounds. */
        public Dictionary<string, CCPoint> maxButtonBoundsOptions;

        /** @brief Internal window bounds. */
        private CCPoint windowMax;

        /** @brief key value map of drop down options. */
        private readonly Dictionary<string, string> _options;

        /** @brief Drop down component title (_key value). */
        private readonly string _title;

        /** @brief Drop down component key (representing the setting being managed by the drop down). */
        private readonly string _key;

        /** @brief The currently selected key of _options. */
        private string _selectedKey;

        /** @brief Inactive state sprite (not dropped down). */
        private readonly CCSprite _inactive;

        /** @brief Active state sprite (drop down menu shown). */
        private readonly CCSprite _active;

        /** @brief Disabled state sprite (only title show, button hidden). */
        private readonly CCSprite _disabled;

        /**
         @property  public Dictionary<string, string> options
        
         @brief Map of drop down options value to key
        
         @return    Drop down options dictionary
         */

        public Dictionary<string, string> Options {
            get {
                return _options;
            }
        }

        /**
         @property  public string titleKey
        
         @brief Drop down component title (_key value).
        
         @return    Drop down component title (_key value).
         */

        public string TitleKey {
            get { return _key; }
        }

        /**
         @property  public string selectedKey
        
         @brief Sets/gets the selected options key.
        
         */

        public string SelectedKey {
            set {
                _selectedKey = value;
                PopulateOptions();
                if (Options.ContainsKey(_selectedKey)) {
                    ((CCLabel)_inactive.GetChildByTag(1)).Text = Options[_selectedKey];
                }
            }get {
                return _selectedKey;
            }
        }

        /**
         @property  public bool active
        
         @brief Active state (drop down menu shown)
        
         @return    true if active, false if not.
         */

        public bool Active {
            get {
                return _active.Visible;
            }
            set {
                if (value) {
                    _inactive.Visible = false;
                    _active.Visible = true;
                } else {
                    _inactive.Visible = true;
                    _active.Visible = false;
                }
            }
        }

        /**
         @property  public bool disabled
        
         @brief Disabled state (drop down menu hidden, title shown)
        
         @return    true if disabled, false if not.
         */

        public bool Disabled {
            get {
                return _disabled.Visible;
            }
            set {
                if (value) {
                    _inactive.Visible = false;
                    _active.Visible = false;
                    _disabled.Visible = true;
                } else {
                    _inactive.Visible = true;
                    _active.Visible = false;
                    _disabled.Visible = false;
                }
            }
        }

        /**
         @fn    public MenuViewButtonDropDown(string titleKey, string titleValue, Dictionary<string, string> options, string selectedKey, CCPoint windowMax, float width, float height, float x, float y) : base()
        
         @brief Drop down menu constructor.
        
         @author    Ryan Beruldsen
         @date  16/09/2016
        
         @param titleKey    Title key.
         @param titleValue  Title display value.
         @param options     Drop down options (map of key to display value).
         @param selectedKey Initial selected key (ref options).
         @param windowMax   Window bounds.
         @param width       The width.
         @param height      The height.
         @param x           The x coordinate.
         @param y           The y coordinate.
         @param highlightColour Button highlight colour.
         @param highlightColourText Button text highlight colour.
         */

        public MenuViewButtonDropDown(  string titleKey, 
                                        string titleValue, 
                                        Dictionary<string, string> options, 
                                        string selectedKey, 
                                        CCPoint windowMax, 
                                        float width, 
                                        float height, 
                                        float x, 
                                        float y, 
                                        CCColor4B highlightColour, 
                                        CCColor3B highlightColourText) : base() {
            _height = height;
            _width = width;
            _selectedKey = selectedKey;
            _x = x;
            _y = y;
            _highlightColour = highlightColour;
            _highlightColourText = highlightColourText;
            _title = titleValue;
            _key = titleKey;
            maxButtonBoundsOptions = new Dictionary<string, CCPoint>();
            minButtonBoundsOptions = new Dictionary<string, CCPoint>();
            _options = options;

            this.windowMax = windowMax;
            _inactive = new CCSprite();
            _active = new CCSprite();
            _disabled = new CCSprite();
            Init();
        }

        /**
         @fn    public void PopulateOptions()
        
         @brief populate drop down menu based on selected item and available values in _options
        
         @author    Ryan Beruldsen
         @date  16/09/2016
         */

        public void PopulateOptions() {
            //clear existing menu items
            _active.RemoveAllChildren();
            minButtonBoundsOptions.Clear();
            maxButtonBoundsOptions.Clear();

            //current menu item counter
            int tempCount = 1;

            //determine maximum Y of menu (to see if a flip is required)
            float maxOptionY = _y - _height / 2 - _height * (_options.Count - 1);

            //determine whether to flip menu up to fit in window
            if (maxOptionY < windowMax.Y / 20) {
                tempCount = -1;
            }

            //loop each menu item to create per entry in options (except selected key)
            foreach (KeyValuePair<string, string> entry in _options) {
                if (entry.Key != _selectedKey) {
                    CCLabel optionLabel = new CCLabel(  entry.Value, 
                                                        "roboto", 
                                                        14, 
                                                        CCLabelFormat.SpriteFont
                    ) {
                        Position = new CCPoint(_x, _y - _height * tempCount),
                        Color = CCColor3B.White,
                        AnchorPoint = CCPoint.AnchorMiddle
                    };

                    CCDrawNode optionNode = new CCDrawNode();

                    float optionX = _x - _width / 2;

                    float optionY = 0;

                    if(tempCount > 0) {
                        optionY = _y - _height / 2 - _height * tempCount++;
                    } else {
                        optionY = _y - _height / 2 - _height * tempCount--;
                    }

                    minButtonBoundsOptions[entry.Key] = new CCPoint(optionX, optionY);
                    maxButtonBoundsOptions[entry.Key] = new CCPoint(optionX + _width, optionY + _height);

                    optionNode.DrawRect(
                        new CCRect(optionX, optionY, _width, _height),
                            fillColor: new CCColor4B(50, 50, 50, 255),
                            borderWidth: 0,
                            borderColor: CCColor4B.White);

                    _active.AddChild(optionLabel, 99);
                    _active.AddChild(optionNode, 98);
                }

            }

            CCDrawNode selectedItem = new CCDrawNode();

            selectedItem.DrawRect(
                new CCRect(_x - _width / 2, _y - _height / 2, _width, _height),
                fillColor: _highlightColour,
                borderWidth: 0,
                borderColor: CCColor4B.White);
            string selectedItemLabelStr = "";
            if(_options.ContainsKey(_selectedKey)){
                selectedItemLabelStr = _options[_selectedKey];
            }
            CCLabel selectedItemLabel = new CCLabel(
                selectedItemLabelStr,
                "roboto", 
                14, 
                CCLabelFormat.SpriteFont
            ) {
                Position = new CCPoint(_x, _y),
                Color = CCColor3B.Black,
                AnchorPoint = CCPoint.AnchorMiddle
            };

            CCLabel componentTitle = new CCLabel(
                _title, "robotob", 14, CCLabelFormat.SpriteFont
            ) {
                Position = new CCPoint((windowMax.X - _x) - _width / 2, _y),
                Color = _highlightColourText,
                AnchorPoint = CCPoint.AnchorMiddleLeft
            };
            _active.AddChild(selectedItem);
            _active.AddChild(selectedItemLabel);
            _active.AddChild(componentTitle);
        }

        /**
         @fn    public void Init()
        
         @brief Initialise drop down menu.
        
         @author    Ryan Beruldsen
         @date  16/09/2016
         */

        private void Init() {

            CCDrawNode dropDownDrawNode = new CCDrawNode();

            minButtonBounds = new CCPoint(_x - _width / 2, _y - _height / 2);
            maxButtonBounds = new CCPoint(minButtonBounds.X + _width, minButtonBounds.Y + _height);


            CCLabel inactiveTitleLabel = new CCLabel
                (_options[_selectedKey], 
                "roboto", 
                14, 
                CCLabelFormat.SpriteFont
            ) {
                Position = new CCPoint(_x, _y),
                Color = CCColor3B.White,
                AnchorPoint = CCPoint.AnchorMiddle
            };
            CCLabel inactiveSelectedOptionLabel = new CCLabel(
                _title, 
                "robotob", 
                14, 
                CCLabelFormat.SpriteFont
            ) {
                Position = new CCPoint((windowMax.X - _x) - _width / 2, _y),
                Color = CCColor3B.White,
                AnchorPoint = CCPoint.AnchorMiddleLeft
            };
            dropDownDrawNode.DrawLine(
                from: new CCPoint(minButtonBounds.X, minButtonBounds.Y + 2),
                to: new CCPoint(maxButtonBounds.X, minButtonBounds.Y + 2),
                lineWidth: 2,
                color: _highlightColour
            );

            dropDownDrawNode.DrawRect(
                new CCRect(_x - _width / 2, _y - _height / 2, _width, _height),
                fillColor: new CCColor4B(40, 40, 40, 40),
                borderWidth: 0,
                borderColor: CCColor4B.White
            );

            //inactive (primary, unselected) state sprite
            _inactive.AddChild(dropDownDrawNode);
            _inactive.AddChild(inactiveTitleLabel, 1, 1);
            _inactive.AddChild(inactiveSelectedOptionLabel);

            //populate drop down options and active state sprite
            PopulateOptions();

            CCLabel disabledTitleLabel = new CCLabel(_title, "robotob", 14, CCLabelFormat.SpriteFont) {
                Position = new CCPoint((windowMax.X - _x) - _width / 2, _y),
                Color = CCColor3B.DarkGray,
                AnchorPoint = CCPoint.AnchorMiddleLeft
            };

            //disabled sprite - hidden to emphasise selected drop down menu (in active state)
            _disabled.AddChild(disabledTitleLabel);

            AddChild(_active);
            AddChild(_inactive);
            AddChild(_disabled);

            //start state as inactive
            _inactive.Visible = true;
            _active.Visible = false;
            _disabled.Visible = false;
        }
    }
}
