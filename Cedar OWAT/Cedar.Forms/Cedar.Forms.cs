﻿/*
 * CEDAR Operator Workload Assessment Tool
 * Copyright (C) 2023  aurizn
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using CEDAR.Shared;
using Xamarin.Forms;

namespace CEDAR
{
	public class App : Application
	{
		public App ()
		{
            // The root page of your application
            MainPage = new NavigationPage(new CEDAR.MainPage());
		}

		protected override void OnStart ()
		{
            System.Console.WriteLine("TEST");
            // Handle when your app starts
		}

		protected override void OnSleep ()
		{
            // Handle when your app sleeps
            if (ModelData.Instance != null && ModelData.Instance.Duration > 0) {
                ModelData.Instance.SystemPause = ModelData.Timestamp();
                string minute = DateTime.Now.Minute.ToString("00");
                string hour = DateTime.Now.Hour.ToString("00");
                string second = DateTime.Now.Second.ToString("00");
                ModelDataIOLog.Instance.LogEventBrief("CEDAR paused " + hour + ":" + minute + ":" + second + ModelData.Instance.UnixOutput(), false);
                ModelData.Instance.ChangeSetting(
                    ModelData.OS_PAUSE,
                    ModelData.PARAM_ON);
            }
        }

		protected override void OnResume ()
		{
            //if test has started (prevent false pause triggers)
            //output partial log and note pause in logs
            if (ModelData.Instance != null && ModelData.Instance.Duration > 0) {
                long durationMills = ModelData.Timestamp() - ModelData.Instance.SystemPause;
                long durationSecs = durationMills / 1000;
                String timePaused = ModelData.TimeString(durationSecs);

                while (durationMills > 1000) {
                    durationMills -= 1000;
                }

                int tenth = Convert.ToInt32(((double)durationMills / 1000.0) * 10.0);
                string timeStr = timePaused + "." + tenth + ModelData.Instance.UnixOutput();

                ModelData.Instance.ChangeSetting(
                    ModelData.OS_PAUSE,
                    ModelData.PARAM_OFF
                );
                string minute = DateTime.Now.Minute.ToString("00");
                string hour = DateTime.Now.Hour.ToString("00");
                string second = DateTime.Now.Second.ToString("00");
                if (ModelData.Instance.Settings[ModelData.MODE_CLEAR_ANALYSIS_ON_PAUSE] == ModelData.PARAM_ON) {
                    ModelData.Instance.PauseTimeStamp = ModelData.Timestamp();
                    ModelDataIOLog.Instance.ClearAnalysis();
                }

                //note timing of pause for logs
                ModelDataIOLog.Instance.LogEventBrief("CEDAR resumed " + hour + ":" + minute + ":" + second, false);
                ModelDataIOLog.Instance.LogEventBrief("CEDAR lost the focus for " + timeStr + " [hr:min:sec.tenths]", false);
            }
        
        // Handle when your app resumes
    }
	}
}

