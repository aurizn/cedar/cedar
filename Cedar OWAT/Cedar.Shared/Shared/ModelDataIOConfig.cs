﻿/*
 * CEDAR Operator Workload Assessment Tool
 * Copyright (C) 2023  aurizn
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using CEDAR.Shared.CedarCollection;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;

namespace CEDAR.Shared {

    /**
     @class ModelDataIOConfig
    
     @brief The I/O config file data handler. This represents data in the the MATB_CONFIG xml file.
    
     @author    Ryan Beruldsen
     @date  6/10/2016
     */

    public sealed class ModelDataIOConfig {

        /** @brief Singleton I/O config data instance. */
        internal static readonly ModelDataIOConfig _instance =
            new ModelDataIOConfig();

        /**
         @fn    private ModelDataIOConfig()
        
         @brief Singleton I/O config data constructor (stub). 
        
         @author    Ryan Beruldsen
         @date  6/10/2016
         */

        private ModelDataIOConfig() { }


        /** @brief Dictionary of external config data (MATB_CONFIG). */
        private CDRIOConfig _ioDataDict = new CDRIOConfig();

        /**
         @property  public CDRIOConfig xmlDataDict
        
         @brief Gets a Dictionary of external config data.
        
         @return    A Dictionary of external config data.
         */

        public CDRIOConfig XmlDataDict {
            get {
                return _ioDataDict;
            }
        }

        /**
         @property  public static ModelDataIOConfig instance
        
         @brief Gets the singleton config data instance.
        
         @return    The instance.
         */

        public static ModelDataIOConfig Instance {
            get {
                return _instance;
            }
        }

        /**
         @fn    public void PrintIOConfig()
        
         @brief Print I/O config data to the command line.

            - this data will be written to an external file for metrics
        
         @author    Ryan Beruldsen
         @date  6/10/2016
         */

        public void PrintIOConfig() {
            var eventsDict = _ioDataDict;
            foreach (KeyValuePair<string, CDRConfigParams> configType in eventsDict) {
                foreach (KeyValuePair<string, string> configData
                    in eventsDict[configType.Key]) {
                    System.Diagnostics.Debug.WriteLine(
                        configType.Key + " " + configData.Key + " " + configData.Value);
                }
            }
        }

        /**
         @fn    public Dictionary<string, string> GetConfigType(string type)
        
         @brief Gets data of a configuration type

            - this is a top level node in xml, underscore property in json
        
         @author    Ryan Beruldsen
         @date  6/10/2016
        
         @param type    The configuration type.
        
         @return    Data of configuration type.
         */

        public SortedDictionary<string, string> GetConfigType(string type) {
            type = type.ToLower();
            if (_ioDataDict.ContainsKey(type)) {
                return _ioDataDict[type];
            }
            return new SortedDictionary<string, string>();
        }

        /**
         @fn    public string GetConfigValue(string type, string key)
        
         @brief Gets a configuration value, given type.
        
         @author    Ryan Beruldsen
         @date  6/10/2016
        
         @param type    The config type.
         @param key     The config key.
        
         @return    The configuration value.
         */

        public string GetConfigValue(string type, string key) {
            type = type.ToLower();
            key = key.ToLower();

            if (_ioDataDict.ContainsKey(type) && _ioDataDict[type].ContainsKey(key)) {
                return _ioDataDict[type][key];
            }
            return "";
        }

        /**
         @fn    public bool SetConfigValue(string type, string key, string value)
        
         @brief Sets configuration value, given type, key and new value.

            - only existing (loaded) configuration entries can be changed
        
         @author    Ryan Beruldsen
         @date  6/10/2016
        
         @param type    The config type.
         @param key     The config key.
         @param value   The config value.
        
         @return    true if it succeeds, false if it fails.
         */

        public bool SetConfigValue(string type, string key, string value) {
            type = type.ToLower();
            key = key.ToLower();

            if (_ioDataDict.ContainsKey(type)) {
                _ioDataDict[type][key] = value;
                HandleTrainingModesSet(key, value);
                return true;
            }
            return false;
        }

        public void HandleTrainingModesSet(string key, string value) {
            if (key.Equals("random_mode")) {
                if (value.Equals("true")) {
                    ModelData.Instance.ChangeSetting(
                        ModelData.RANDOM_MODE,
                        ModelData.PARAM_ON
                    );
                }
                else {
                    ModelData.Instance.ChangeSetting(
                        ModelData.RANDOM_MODE,
                        ModelData.PARAM_OFF
                    );
                }
            }
            if (key.Equals("train_mode")) {
                if (value.Equals("true")) {
                    ModelData.Instance.ChangeSetting(
                        ModelData.TRAINING_MODE,
                        ModelData.PARAM_ON
                    );
                }
                else {
                    ModelData.Instance.ChangeSetting(
                        ModelData.TRAINING_MODE,
                        ModelData.PARAM_OFF
                    );
                }
            }
            if (key.Equals("silent_mode")) {
                if (value.Equals("true")) {
                    ModelData.Instance.ChangeSetting(
                        ModelData.SILENT_MODE,
                        ModelData.PARAM_ON
                    );
                }
                else {
                    ModelData.Instance.ChangeSetting(
                        ModelData.SILENT_MODE,
                        ModelData.PARAM_OFF
                    );
                }
            }
        }

        /**
         @fn    public bool GetConfigBool(string type, string key, string value="")
        
         @brief Gets aconfiguration boolean, false if not a boolean.

            - assumes boolean is case insensitive string "false"/"true"
        
         @author    Ryan Beruldsen
         @date  6/10/2016
        
         @param type    The config type.
         @param key     The key.
         @param value   (Optional) The value.
        
         @return    true if it succeeds, false if it fails.
         */

        public bool GetConfigBool(string type, string key, string value = "") {
            type = type.ToLower();
            key = key.ToLower();

            if (_ioDataDict.ContainsKey(type) &&
                    _ioDataDict[type].ContainsKey(key) &&
                    value.Equals("")) {
                return _ioDataDict[type][key].Equals("true");
            }
            else if (_ioDataDict.ContainsKey(type) && _ioDataDict[type].ContainsKey(key)) {
                return _ioDataDict[type][key].Equals(value);
            }

            return false;
        }

        /**
         @fn    public int GetConfigNum(string type, string key)
        
         @brief Gets configuration number if exsiting, -1 if not.
        
         @author    Ryan Beruldsen
         @date  6/10/2016
        
         @param type    The config type.
         @param key     The config key.
        
         @return    The configuration number.
         */

        public int GetConfigNum(string type, string key) {
            type = type.ToLower();
            key = key.ToLower();

            if (_ioDataDict.ContainsKey(type) && _ioDataDict[type].ContainsKey(key)) {
                return Convert.ToInt32(_ioDataDict[type][key]);
            }
            return -1;
        }

        /**
		@fn public void ProcessTestFiles()

		@brief Processes additional test file options if available.

		@author    Ryan Beruldsen
		@date  1/8/2017

		*/

        public void ProcessTestFiles() {


            var files = _instance.GetConfigType("TEST_FILES");
            if (files.Count > 0) {
                foreach (KeyValuePair<string, string> file in files) {
                    if (!ModelData.Instance.userConfig[ModelData.FILE_NAME].Contains(file.Value)) {
                        ModelData.Instance.userConfig[ModelData.FILE_NAME].Add(file.Value);
                    }
                    if (file.Value != "") {
                        ModelData.Instance.userConfigStr[file.Value] = file.Key;
                    }
                    else {
                        ModelData.Instance.userConfigStr[ModelData.FILE_NAME_DEFAULT] = file.Key;
                    }
                }
            }
        }

        /**
         @fn    public void ParseJSONFile(string filename)
        
         @brief Parse config JSON file - formatted, case insensitive config data.

            - this is expected to be called from platform-specific code to identify the path to the config file
        
         @author    Ryan Beruldsen
         @date  6/10/2016
        
         @param filename    Filename of the file.
         */

        public void ParseJSONFile(string filename) {
            //reset data hashmap
            if (!ModelDataIOConfig.Instance.GetConfigBool("mode", "test_serials")) {
                _ioDataDict = new CDRIOConfig();
            }
            else {
                _ioDataDict["test_card"] = new CDRConfigParams();
                _ioDataDict["serial"] = new CDRConfigParams();
            }


            //record loading of config
            ModelData.CEDARConfig = true;


            JsonTextReader reader =
                new JsonTextReader(new StringReader(File.ReadAllText(filename)));

            //maintain subnode (type) record for child values
            string currentSubNode = "";

            //dictionary of key/value for a subnode (type)
            var nodeValDict = new Dictionary<string, string>();

            //flag for type entry
            bool type = false;

            //iteration level
            int level = 0;
            while (reader.Read()) {
                bool readerValid =
                    reader.TokenType == JsonToken.String ||
                    reader.TokenType == JsonToken.Integer ||
                    reader.TokenType == JsonToken.Float;

                //maintain iteration level
                if (reader.TokenType == JsonToken.StartObject) {
                    level++;
                }
                else if (reader.TokenType == JsonToken.EndObject) {
                    level--;

                    //if property is type, next value will be a type
                }
                else if (reader.TokenType == JsonToken.PropertyName &&
                            reader.Value.ToString().Equals("_type")) {
                    type = true;

                    //record sub node (type)
                }
                else if (reader.TokenType == JsonToken.PropertyName &&
                            level == 3 && !type) {
                    currentSubNode = reader.Value.ToString();

                    //record key/value for new type 
                    // (type is the last attribute enumerated, so type must be cleared)
                }
                else if (readerValid &&
                            level == 3 && !type && !currentSubNode.Equals("")) {
                    nodeValDict[currentSubNode] = reader.Value.ToString();
                    currentSubNode = "";
                    //if type (subnode) has been set, 
                    // there is enough info to add data to global config
                }
                else if (readerValid && level == 3 && type) {
                    string typeStr = reader.Value.ToString();
                    foreach (KeyValuePair<string, string> val in nodeValDict) {
                        ModelDataIOConfig.Instance.ParseIOConfig(
                            typeStr, val.Key, val.Value);
                    }
                    nodeValDict = new Dictionary<string, string>();
                    type = false;

                    //ignore unhandled enumerations
                }
                else if (reader.TokenType == JsonToken.String && level == 3 &&
                    type && !currentSubNode.Equals("")) {
                    type = false;

                }
                else {
                    type = false;
                }
            }
        }

        /**
         @fn    public void ParseXMLFile(string filename)
        
         @brief Parse config XML file.
        
         @author    Ryan Beruldsen
         @date  6/10/2016
        
         @param filename    Filename of the file.
         */

        public void ParseConfigXMLFile(string filename) {
            //reset data hashmap
            _ioDataDict = new CDRIOConfig();

            //record loading of config
            ModelData.CEDARConfig = true;

            XmlTextReader reader = new XmlTextReader(filename);

            //ennumeration data
            string currentNodeValue = "";
            string currentNode = "";
            string currentSubNode = "";
            bool hasRandomMode = false;

            //XML ennumeration level
            int level = 0;
            while (reader.Read()) {
                //set current node, value
                if (reader.NodeType == XmlNodeType.Element) {
                    if (currentNode.Equals("") && level == 1 && reader.HasAttributes) {
                        currentNodeValue = reader[0];
                        currentNode = reader.Name;
                    }
                    currentSubNode = reader.Name;
                    level++;
                }
                //add key/value data for node
                if (reader.NodeType == XmlNodeType.Text && !currentNode.Equals("")) {
                    ModelDataIOConfig.Instance.ParseIOConfig(
                        currentNodeValue, currentSubNode, reader.Value);
                }
                if (currentSubNode.ToLower().Equals("random_mode")) {
                    hasRandomMode = true;
                }


                //ignore unhandled ennumerations
                if (reader.NodeType == XmlNodeType.EndElement) {
                    level--;
                    if (reader.Name.Equals(currentNode)) {
                        currentNode = "";
                    }
                }
            }
            if (!hasRandomMode) {
                ModelDataIOConfig.Instance.ParseIOConfig(
                        "MODE", "RANDOM_MODE", "false");
            }
        }

        /**
         @fn    public void ParseIODefault()
        
         @brief Force default IO config values 
            
            - if config load fails, to enable an event run without config file load
         @param overwrite  (Optional) Whether to overwrite existing config entries.

         @author    Ryan Beruldsen
         @date  6/10/2016
         */

        public void ParseIODefault(bool overwrite = true) {
            ParseIOConfig("mode", "auto_minimize_mode", "false", overwrite);
            ParseIOConfig("mode", "auto_start_mode", "true", overwrite);
            ParseIOConfig("mode", "select_eventsfile_mode", "true", overwrite);
            ParseIOConfig("mode", "set_run_params_mode", "false", overwrite);
            ParseIOConfig("mode", "folder_per_run_mode", "true", overwrite);
            ParseIOConfig("mode", "train_mode", "false", overwrite);
            ParseIOConfig("mode", "silent_mode", "false", overwrite);
            ParseIOConfig("mode", "task_border_mode", "false", overwrite);
            ParseIOConfig("resman_rates", "pump_1", "800", overwrite);
            ParseIOConfig("resman_rates", "pump_2", "600", overwrite);
            ParseIOConfig("resman_rates", "pump_3", "800", overwrite);
            ParseIOConfig("resman_rates", "pump_4", "600", overwrite);
            ParseIOConfig("resman_rates", "pump_5", "600", overwrite);
            ParseIOConfig("resman_rates", "pump_6", "600", overwrite);
            ParseIOConfig("resman_rates", "pump_7", "400", overwrite);
            ParseIOConfig("resman_rates", "pump_8", "400", overwrite);
            ParseIOConfig("resman_rates", "tank_a", "800", overwrite);
            ParseIOConfig("resman_rates", "tank_b", "800", overwrite);

            //ensure MATB-II defaults are respected
            if (overwrite) {
                ParseIOConfig("mode", "random_mode", "true", overwrite);
                ParseIOConfig("resman_tlevels", "capacity", "4000", overwrite);
                ParseIOConfig("resman_tlevels", "initial", "2000", overwrite);
                ParseIOConfig("resman_tlevels", "min", "1400", overwrite);
                ParseIOConfig("resman_tlevels", "max", "2600", overwrite);
            }

            ParseIOConfig("timeout", "rate", "30", overwrite);
            ParseIOConfig("timeout", "task_comm", "5", overwrite);
            ParseIOConfig("timeout", "task_resman", "5", overwrite);
            ParseIOConfig("timeout", "task_sysmon_scales", "5", overwrite);
            ParseIOConfig("timeout", "task_sysmon_lights", "5", overwrite);
            ParseIOConfig("timeout", "task_sched", "5", overwrite);
            ParseIOConfig("recording_interval", "task_track", "15", overwrite);
            ParseIOConfig("recording_interval", "task_resman", "30", overwrite);
        }

        /**
                @fn    public void ParseIOConfig(string type, string key, string value)
               
                @brief Parse IO config - format independent.
               
                @author    Ryan Beruldsen
                @date  6/10/2016
               
                @param type    The config type.
                @param key     The config key.
                @param value   The config value.
                @param owrite  (Optional) Whether to overwrite existing config entries.
        */

        public void ParseIOConfig(string type, string key, string value, bool owrite = true) {
            type = type.ToLower();
            key = key.ToLower();

            //MATB-II timed from end of voice-over, not possible here so added 10 seconds
            //as average of voice-over duration is 10 seconds
            if (type.Equals("timeout") &&
               key.Equals("task_comm")) {
                int numVal = Convert.ToInt32(value);
                numVal += 10;
                value = Convert.ToString(numVal);
            }

            if (!_ioDataDict.ContainsKey(type)) {
                _ioDataDict[type] = new CDRConfigParams();
            }
            if (owrite) {
                (_ioDataDict[type])[key] = value;
                HandleTrainingModesParse(key, value);
            }
            else if (!_ioDataDict[type].ContainsKey(key)) {
                (_ioDataDict[type])[key] = value;
                HandleTrainingModesParse(key, value);
            }
        }

        public void HandleTrainingModesParse(string key, string value) {
            if (key.Equals("random_mode")) {
                if (value.Equals("true")) {
                    ModelData.Instance.ChangeSetting(
                        ModelData.RANDOM_MODE,
                        ModelData.PARAM_ON
                    );
                }
                else {
                    ModelData.Instance.ChangeSetting(
                        ModelData.RANDOM_MODE,
                        ModelData.PARAM_OFF
                    );
                }
            }
            if (key.Equals("train_mode")) {
                if (value.Equals("true")) {
                    ModelData.Instance.ChangeSetting(
                        ModelData.TRAINING_MODE,
                        ModelData.PARAM_ON
                    );
                }
                else {
                    ModelData.Instance.ChangeSetting(
                        ModelData.TRAINING_MODE,
                        ModelData.PARAM_OFF
                    );
                }
            }
            if (key.Equals("silent_mode")) {
                if (value.Equals("true")) {
                    ModelData.Instance.ChangeSetting(
                        ModelData.SILENT_MODE,
                        ModelData.PARAM_ON
                    );
                }
                else {
                    ModelData.Instance.ChangeSetting(
                        ModelData.SILENT_MODE,
                        ModelData.PARAM_OFF
                    );
                }
            }
        }

        /**
         @fn    public static string[] VoiceOverListing()
        
         @brief Hard-coded voice over audio listing by filename.
        
         @author    Ryan Beruldsen
         @date  6/10/2016
        
         @return    A string[] listing of all voice over audio files.
         */

        public static string[] VoiceOverListing() {
            return new string[] {
                "OTHER_COM1_118-325",
                "OTHER_COM1_120-825",
                "OTHER_COM1_124-350",
                "OTHER_COM1_126-175",
                "OTHER_COM1_127-725",
                "OTHER_COM1_128-525",
                "OTHER_COM1_130-875",
                "OTHER_COM1_132-950",
                "OTHER_COM1_134-175",
                "OTHER_COM1_135-225",
                "OTHER_COM2_118-275",
                "OTHER_COM2_120-775",
                "OTHER_COM2_124-500",
                "OTHER_COM2_126-025",
                "OTHER_COM2_127-675",
                "OTHER_COM2_128-475",
                "OTHER_COM2_130-725",
                "OTHER_COM2_132-800",
                "OTHER_COM2_134-025",
                "OTHER_COM2_135-175",
                "OTHER_NAV1_108-350",
                "OTHER_NAV1_109-250",
                "OTHER_NAV1_110-400",
                "OTHER_NAV1_111-950",
                "OTHER_NAV1_112-150",
                "OTHER_NAV1_113-000",
                "OTHER_NAV1_114-500",
                "OTHER_NAV1_115-750",
                "OTHER_NAV1_116-450",
                "OTHER_NAV1_117-650",
                "OTHER_NAV2_108-750",
                "OTHER_NAV2_109-650",
                "OTHER_NAV2_110-800",
                "OTHER_NAV2_111-350",
                "OTHER_NAV2_112-550",
                "OTHER_NAV2_113-400",
                "OTHER_NAV2_114-900",
                "OTHER_NAV2_115-050",
                "OTHER_NAV2_116-850",
                "OTHER_NAV2_117-950",
                "OWN_COM1_124-575",
                "OWN_COM1_125-500",
                "OWN_COM1_125-550",
                "OWN_COM1_126-450",
                "OWN_COM1_126-525",
                "OWN_COM1_127-500",
                "OWN_COM1_127-550",
                "OWN_COM1_128-475",
                "OWN_COM1_128-575",
                "OWN_COM1_129-450",
                "OWN_COM2_124-450",
                "OWN_COM2_125-500",
                "OWN_COM2_125-575",
                "OWN_COM2_126-475",
                "OWN_COM2_126-550",
                "OWN_COM2_127-500",
                "OWN_COM2_127-525",
                "OWN_COM2_128-525",
                "OWN_COM2_128-550",
                "OWN_COM2_129-575",
                "OWN_NAV1_110-650",
                "OWN_NAV1_111-500",
                "OWN_NAV1_111-600",
                "OWN_NAV1_112-450",
                "OWN_NAV1_112-550",
                "OWN_NAV1_113-500",
                "OWN_NAV1_113-600",
                "OWN_NAV1_114-450",
                "OWN_NAV1_114-650",
                "OWN_NAV1_115-400",
                "OWN_NAV2_110-500",
                "OWN_NAV2_111-400",
                "OWN_NAV2_111-650",
                "OWN_NAV2_112-450",
                "OWN_NAV2_112-600",
                "OWN_NAV2_113-500",
                "OWN_NAV2_113-550",
                "OWN_NAV2_114-450",
                "OWN_NAV2_114-600",
                "OWN_NAV2_115-650"
            };
        }

        /**
         @fn    public static double[] VoiceOverFreqListing()
        
         @brief Hard-coded voice over audio listing by frequency 
            
            - in same order as srting filename listing.
        
         @author    Ryan Beruldsen
         @date  6/10/2016
        
         @return    A double[].
         */

        public static double[] VoiceOverFreqListing() {
            return new double[] {
                    118.325,
                    120.825,
                    124.350,
                    126.175,
                    127.725,
                    128.525,
                    130.875,
                    132.950,
                    134.175,
                    135.225,
                    118.275,
                    120.775,
                    124.500,
                    126.025,
                    127.675,
                    128.475,
                    130.725,
                    132.800,
                    134.025,
                    135.175,
                    108.350,
                    109.250,
                    110.400,
                    111.950,
                    112.150,
                    113.000,
                    114.500,
                    115.750,
                    116.450,
                    117.650,
                    108.750,
                    109.650,
                    110.800,
                    111.350,
                    112.550,
                    113.400,
                    114.900,
                    115.050,
                    116.850,
                    117.950,
                    124.575,
                    125.500,
                    125.550,
                    126.450,
                    126.525,
                    127.500,
                    127.550,
                    128.475,
                    128.575,
                    129.450,
                    124.450,
                    125.500,
                    125.575,
                    126.475,
                    126.550,
                    127.500,
                    127.525,
                    128.525,
                    128.550,
                    129.575,
                    110.650,
                    111.500,
                    111.600,
                    112.450,
                    112.550,
                    113.500,
                    113.600,
                    114.450,
                    114.650,
                    115.400,
                    110.500,
                    111.400,
                    111.650,
                    112.450,
                    112.600,
                    113.500,
                    113.550,
                    114.450,
                    114.600,
                    115.650
            };
        }
    }
}
