﻿/*
 * CEDAR Operator Workload Assessment Tool
 * Copyright (C) 2023  aurizn
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using CocosSharp;
using System;

namespace CEDAR.Shared.Modules.Task.Comms {

    /**
     @class CommsViewSelectSlider
    
     @brief Modification slider of selected channel frequency
    
     @author    Ryan Beruldsen
     @date  15/09/2016
     */

    class CommsViewSelectSlider : DivisionContent, IDivisionContent {

        /** @brief Fine range indicate sprite tag constant. */
        private const int _FINE_RANGE_INDICATE_TAG = 2;

        /** @brief Warn indicate sprite tag constant. */
        private const int _WARN_INDICATE_TAG = 1;

        /** @brief Default sort order index for new sprites. */
        private const int _Z_INDEX_DEFAULT = 1;

        /** @brief Top sort order index for new sprites. */
        private const int _Z_INDEX_MAX = 2;

        /** @brief Bottom sort order index for new sprites. */
        private const int _Z_INDEX_MIN = 0;

        /** @brief The main slider draw node. */
        private readonly CCDrawNode _sliderDrawNode;

        /** @brief The frequency control minimum coordinates. */
        private CCPoint _freqControlMin = new CCPoint(-1, -1);

        /** @brief The frequency control maximum coordinates. */
        private CCPoint _freqControlMax = new CCPoint(-1, -1);

        /** @brief Width of the frequency control bar in pixels. */
        private float _freqControlWidth = -1;

        /** @brief The slider handle sprite. */
        private readonly CCSprite _slider;

        /** @brief The slider touch indicator left sprite. */
        private readonly CCSprite _sliderL;

        /** @brief The slider touch indicator right sprite. */
        private readonly CCSprite _sliderR;

        /** @brief Fine range indication colour.*/
        private readonly CCColor4B _colourRange;

        /** @brief Warning indication colour.*/
        private readonly CCColor4B _colourWarning;

        /** @brief The warning indicator sprite (indicates where the target frequency is on the slider). */
        private readonly CCSprite _warn;

        /** @brief The initial x coordinate of the slider handle. */
        private float sliderInitialX;

        private double _sliderPerc;

        /** @brief Warn indicate state. */
        private bool _warnIndicate = false;

        /** @brief User draggig slider state. */
        private readonly bool _dragging = false;

        /** @brief The x coordinate. */
        readonly float _x;

        /** @brief The y coordinate. */
        readonly float _y;

        /** @brief The width. */
        readonly float _width;

        /** @brief The height. */
        readonly float _height;

        readonly CCLabel valueText;

        /** @brief Frequency control coordinates (current slider coordinates as per user drag). */
        public CCPoint freqControl = new CCPoint(-1, -1);

        /**
         @property  public float freqControlWidth
        
         @brief Gets the width of the frequency control bar.
        
         @return    The width of the frequency control bar.
         */

        public float FreqControlWidth { get { return _freqControlWidth; } }

        /**
         @property  public CCPoint freqControlMin
        
         @brief Gets the frequency control bar minimum coordinates.
        
         @return    The frequency control minimum.
         */

        public CCPoint FreqControlMin { get { return _freqControlMin; } }

        /**
         @property  public CCPoint freqControlMax
        
         @brief Gets the frequency control bar maximum coordinates.
        
         @return    The frequency control bar maximum coordinates.
         */

        public CCPoint FreqControlMax { get { return _freqControlMax; } }

        /**
         @property  public bool dragging
        
         @brief User dragging slider state
        
         @return    true if dragging, false if not.
         */

        public bool Dragging {
            get { return _dragging; }
            set {
                _sliderR.Visible = false;
                _sliderL.Visible = false;
            }
        }

        /**
         @property  public bool touchLeft
        
         @brief Slider handle touch left state.
        
         @return    true if touch left state, false if not.
         */

        public bool TouchLeft {
            set {
                _sliderL.Visible = value;
            }
        }

        /**
         @property  public bool touchRight
        
         @brief Slider handle touch right state.
        
         @return    true if touch right state, false if not.
         */

        public bool TouchRight {
            set {
                _sliderR.Visible = value;
            }
        }

        /**
         @property  public bool warnIndicate
        
         @brief Gets or sets warning indicate state (showing where the target frequency sits on the slider bar).
        
         @return    true if warning indicate state, false if not.
         */

        public bool WarnIndicate {
            get {
                return _warnIndicate;
            }
            set {
                _warnIndicate = value;
                _warn.Visible = _warnIndicate;
            }
        }

        /**
         @fn    public CommsViewSelectSlider( Division division, float x, float y, float width, float height, CCColor4B colourActive, CCColor4B colourRange, CCColor4B colourWarning) : base(division)
        
         @brief Constructor for a modification slider for channel frequency.
        
         @author    Ryan Beruldsen
         @date  20/09/2016
        
         @param division        The division.
         @param x               Slider handle colour.
         @param y               Fine range indication colour.
         @param width           Warning indication colour.
         @param height          The height.
         @param colourActive    Active colour.
         @param colourRange     Range colour - for fine adjustment of slider to target freq.
         @param colourWarning   Warning colour.
         @param valueLabel      Whether to show a value label below the slider handle (simple percentage only).
         */

        public CommsViewSelectSlider(Division division,
                                        float x,
                                        float y,
                                        float width,
                                        float height,
                                        CCColor4B colourRange,
                                        CCColor4B colourWarning,
                                        bool valueLabel = false) : base(division) {

            _sliderDrawNode = new CCDrawNode();
            _colourRange = colourRange;
            _colourWarning = colourWarning;
            _x = x;
            _y = y;
            _width = width;
            _height = height;
            AddChild(_sliderDrawNode);
            _warn = new CCSprite();
            _slider = new CCSprite();
            _sliderL = new CCSprite();
            _sliderR = new CCSprite();

            AddChild(_warn, _Z_INDEX_MIN);
            AddChild(_slider, _Z_INDEX_DEFAULT);
            AddChild(_sliderL, _Z_INDEX_DEFAULT);
            AddChild(_sliderR, _Z_INDEX_DEFAULT);

            valueText = new CCLabel("0", "robotob", 14, CCLabelFormat.SpriteFont) {
                Color = CCColor3B.White
            };

            AddChild(valueText);
            valueText.Visible = false;
            if (valueLabel) { valueText.Visible = true; }
        }

        public double GetSliderPerc() {
            return _sliderPerc;
        }
        /**
         @fn    public void SetSliderPerc(double perc, bool warnIndicate, double targetPerc = 0)
        
         @brief Sets slider position in percentage (0-1).
        
         @author    Ryan Beruldsen
         @date  20/09/2016
        
         @param perc            The percentage of the slider to move the handle to (0 to 1 in units, eg 0.5 as 50%).
         @param warnIndicate    Warning indicate state.
         @param targetPerc      (Optional) Target percent for warning indication.
         */

        public void SetSliderPerc(double perc, bool warnIndicate, double targetPerc = 0) {
            _sliderPerc = perc;
            //calculate freqControl pixel location based on perc
            freqControl.X = _freqControlMin.X +
                Convert.ToSingle((_freqControlMax.X - _freqControlMin.X) * perc);

            //apply freqControl pixel location
            _slider.PositionX = freqControl.X - _freqControlMin.X;
            _sliderL.PositionX = _slider.PositionX;
            _sliderR.PositionX = _slider.PositionX + _freqControlWidth / 4;

            valueText.PositionX = freqControl.X + _freqControlWidth / 2;
            valueText.Text = (perc * 100).ToString("0");

            //calculate and place fine range indication if warning indicate is active
            // and the slider is over the top of the warning indication
            float currentTarget = _freqControlWidth / 2 +
                                    (_freqControlMax.X - _freqControlMin.X) *
                                    (Convert.ToSingle(targetPerc));
            float diff = freqControl.X - sliderInitialX - currentTarget;
            ((CCDrawNode)_slider.GetChildByTag(2)).Clear();
            if (diff < 0 && Math.Abs(diff) < _freqControlWidth && warnIndicate) {

                //draw fine range indication
                ((CCDrawNode)_slider.GetChildByTag(_FINE_RANGE_INDICATE_TAG)).DrawRect(
                    rect: new CCRect(sliderInitialX,
                                        _freqControlMin.Y,
                                        Math.Abs(diff),
                                        _freqControlMax.Y - _freqControlMin.Y),
                    fillColor: _colourRange,
                    borderWidth: 0,
                    borderColor: CCColor4B.White
                );

                //draw center line of slider handle (indicates the actual fine selection of the handle)
                ((CCDrawNode)_slider.GetChildByTag(_FINE_RANGE_INDICATE_TAG)).DrawLine(
                    from: new CCPoint(sliderInitialX + _freqControlWidth / 2, _freqControlMin.Y),
                    to: new CCPoint(sliderInitialX + _freqControlWidth / 2, _freqControlMax.Y),
                    lineWidth: 3,
                    color: CCColor4B.Black,
                    lineCap: CCLineCap.Square
                );
            }

        }

        /**
         @fn    public void SetWarnPerc(double perc)
        
         @brief The percentage of the slider to warning indicate to.
        
         @author    Ryan Beruldsen
         @date  20/09/2016
        
         @param perc    Target percent for warning indication (0 to 1 in units, eg 0.5 as 50%).
         */

        public void SetWarnPerc(double perc) {
            ((CCDrawNode)(_warn.GetChildByTag(_WARN_INDICATE_TAG))).Clear();
            ((CCDrawNode)(_warn.GetChildByTag(_WARN_INDICATE_TAG))).DrawRect(
                new CCRect(_freqControlMin.X + _freqControlWidth / 2,
                    _freqControlMin.Y,
                    (_freqControlMax.X - _freqControlMin.X) * (Convert.ToSingle(perc)),
                    _freqControlMax.Y - _freqControlMin.Y),
                    fillColor: _colourWarning,
                    borderWidth: 0,
                    borderColor: CCColor4B.LightGray
            );
        }

        /**
         @fn    public void Init()
        
         @brief Initialise, draw the slider bar.
        
         @author    Ryan Beruldsen
         @date  20/09/2016
         */

        public void Init() {

            float width = _width;
            float height = _height;
            float positionX = _x;
            float positionY = _y;
            _freqControlWidth = width * 0.15f;

            _freqControlMin.X = positionX;
            _freqControlMin.Y = positionY - height * 2f;
            _freqControlMax.X = positionX + width - width * 0.15f;
            _freqControlMax.Y = _freqControlMin.Y + height;
            freqControl.X = _freqControlMin.X;

            //draw base slider bar rectangle
            _sliderDrawNode.DrawRect(
                new CCRect(
                    positionX + _freqControlWidth / 2,
                    _freqControlMin.Y,
                    width - (_freqControlWidth), height
                ),
                fillColor: new CCColor4B(100, 100, 100, 100),
                borderWidth: 0,
                borderColor: CCColor4B.White
            );

            //create base warning sprite
            CCDrawNode warnDrawNode = new CCDrawNode();
            _warn.AddChild(warnDrawNode, 0, _WARN_INDICATE_TAG);
            AddChild(_warn, _Z_INDEX_MIN);


            CCDrawNode sliderHandle = new CCDrawNode();
            sliderInitialX = freqControl.X;

            //draw main handle rectangle
            sliderHandle.DrawRect(
                new CCRect(freqControl.X, _freqControlMin.Y, _freqControlWidth, height),
                fillColor: ModelData.Instance.GetColourPallete4B(
                    ModelData.COLOUR_PARAM_ACTIVE),
                borderWidth: 0,
                borderColor: CCColor4B.White
                );

            //draw handle center-line (for fine frequency selection)
            sliderHandle.DrawLine(
                new CCPoint(freqControl.X + _freqControlWidth / 2, _freqControlMin.Y),
                new CCPoint(freqControl.X + _freqControlWidth / 2, _freqControlMax.Y),
                3,
                CCColor4B.Black,
                CCLineCap.Square
            );
            valueText.PositionX = freqControl.X + _freqControlWidth / 2;
            valueText.PositionY = _freqControlMin.Y - height / 4;
            DrawTouchUpIndicators(height);

            _slider.AddChild(sliderHandle, _Z_INDEX_DEFAULT);
        }

        /**
         @fn    public void DrawTouchUpIndicators()
        
         @brief Draw slider touch up indicators - for fine movement of slider.
        
         @author    Ryan Beruldsen
         @date  5/10/2016
         */
        public void DrawTouchUpIndicators(float height) {
            //draw slider touch up indicators (appear when making touchups to slider location)
            CCDrawNode sliderLeftDrawNode = new CCDrawNode();
            CCDrawNode sliderRightDrawNode = new CCDrawNode();

            sliderLeftDrawNode.DrawRect(
                new CCRect(freqControl.X, _freqControlMin.Y, _freqControlWidth / 4, height),
                fillColor: CCColor4B.White,
                borderWidth: 0,
                borderColor: CCColor4B.White
                );

            sliderRightDrawNode.DrawRect(
                new CCRect(
                    freqControl.X + _freqControlWidth / 2,
                    _freqControlMin.Y,
                    _freqControlWidth / 4,
                    height
                ),
                fillColor: CCColor4B.White,
                borderWidth: 0,
                borderColor: CCColor4B.White
                );

            _sliderL.Visible = false;
            _sliderR.Visible = false;

            //fine range indication drawn when slider overlaps target indication
            CCDrawNode fineRangeIndicateDrawNode = new CCDrawNode();
            _slider.AddChild(fineRangeIndicateDrawNode, _Z_INDEX_MAX, _FINE_RANGE_INDICATE_TAG);

            _sliderL.AddChild(sliderLeftDrawNode);
            _sliderR.AddChild(sliderRightDrawNode);
        }
    }

}
