﻿/*
 * CEDAR Operator Workload Assessment Tool
 * Copyright (C) 2023  aurizn
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections.Generic;
using System.IO;

using UIKit;
using Newtonsoft.Json;
using CEDAR.Shared;
using Xamarin.Forms.Platform.iOS;
using Xamarin.Forms;

[assembly: ExportRenderer(typeof(CEDAR.MainPage), typeof(CEDAR.iOS.ViewController))]

/*! \namespace CEDAR.iOS
* @brief iOS platform-specific content.
*
*/
namespace CEDAR.iOS {
    /**
     @class ViewController

     @brief Main view controller for iOS, contains platform-specfic file loading and other logic.

     @author    Ryan Beruldsen
     @date  5/10/2016
     */

    public partial class ViewController : PageRenderer {

        /**
         @fn    private void FileLoad()

         @brief Loads settings, configuration files based on file type parameters (xml/json)

         @author    Ryan Beruldsen
         @date  5/10/2016
         */
        private void FileLoad(string serial = "") {
            string path = System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments);
            string settingsJSON = Path.Combine(path, "settings.json");

            if (!serial.Equals("")) {
                path = System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments) + "/SERIALS/" + serial;
            }



            //load user-specific settings - not contained in other MATB-based config or event files
            //reloading config and events files to change serial does not also require settings reload
            if (File.Exists(settingsJSON) && serial.Equals("")) {
                Dictionary<string, string> settings =
                    JsonConvert.DeserializeObject<Dictionary<string, string>>
                    (File.ReadAllText(settingsJSON));

                if (settings != null) {
                    foreach (KeyValuePair<string, string> setting in settings) {
                        //prevent file reload loop -> crash
                        if (setting.Key != ModelData.FILE_RELOAD) {
                            ModelData.Instance.ChangeSetting(setting.Key, setting.Value);
                        }

                    }
                }


            }

            //if non-default file has been selected, handle file prefix
            string prefix = ModelData.Instance.Settings[ModelData.FILE_NAME];
            ModelData.Instance.LoadedFile = prefix;
            if (prefix != "") {
                prefix += "_";
            }


            string configXML = Path.Combine(path, prefix + "CONFIG.xml");
            string configJSON = Path.Combine(path, prefix + "config.json");
            string eventsXML = Path.Combine(path, prefix + "EVENTS.xml");
            string eventsJSON = Path.Combine(path, prefix + "events.json");

            //load MATB-based config file
            if (File.Exists(configXML) &&
                ModelData.Instance.Settings[ModelData.FORMAT_INPUT] ==
                ModelData.FORMAT_TYPE_XML) {
                //xml file loaded
                ModelDataIOConfig.Instance.ParseConfigXMLFile(configXML);
                ModelDataIOConfig.Instance.ProcessTestFiles();
                //fill in any unset config items with defaults
                ModelDataIOConfig.Instance.ParseIODefault(overwrite: false);

                ModelData.CEDARConfig = true;
                ModelDataIOConfig.Instance.PrintIOConfig();
            }
            else if (File.Exists(configJSON) &&
                 ModelData.Instance.Settings[ModelData.FORMAT_INPUT] ==
                 ModelData.FORMAT_TYPE_JSON) {

                //json file loaded
                ModelData.CEDARConfig = true;
                ModelDataIOConfig.Instance.ParseJSONFile(configJSON);

                //reading prefixes noted in config
                if (ModelData.Instance.Settings[ModelData.FILE_SCAN] == ModelData.PARAM_OFF) {
                    ModelDataIOConfig.Instance.ProcessTestFiles();

                }

                //fill in any unset config items with defaults
                ModelDataIOConfig.Instance.ParseIODefault(overwrite: false);

                ModelDataIOConfig.Instance.PrintIOConfig();
            }
            else {
                //no file available, load default
                ModelDataIOConfig.Instance.ParseIODefault();
                ModelData.CEDARConfig = false;
            }

            //load MATB-based events file
            if (File.Exists(eventsXML) &&
                ModelData.Instance.Settings[ModelData.FORMAT_INPUT] ==
                ModelData.FORMAT_TYPE_XML) {
                //xml file loaded
                ModelDataIOEvents.Instance.ParseEventsXMLFile(eventsXML);
                ModelData.CEDAREvents = true;
            }
            else if (File.Exists(eventsJSON) &&
              ModelData.Instance.Settings[ModelData.FORMAT_INPUT] ==
                ModelData.FORMAT_TYPE_JSON) {
                //json file loaded
                ModelData.CEDAREvents = true;
                if (!ModelData.CEDARConfig) {
                    ModelDataIOConfig.Instance.SetConfigValue("mode", "random_mode", "false");
                    ModelData.Instance.userConfig[ModelData.RANDOM_MODE] = new List<string>() {
                        ModelData.PARAM_ON,
                        ModelData.PARAM_RANDOM_ENDLESS,
                        ModelData.PARAM_OFF
                    };
                }
                ModelDataIOEvents.Instance.ParseJSONFile(eventsJSON);
                ModelDataIOEvents.Instance.PrintIOEvents();
            }
            else {
                //no file available, no events available, force random events
                ModelData.CEDAREvents = false;
                ModelDataIOConfig.Instance.SetConfigValue("mode", "random_mode", "true");
                ModelDataIOConfig.Instance.SetConfigValue("mode", "train_mode", "true");
                ModelData.Instance.userConfig[ModelData.RANDOM_MODE] = new List<string>() {
                        ModelData.PARAM_ON,
                        ModelData.PARAM_RANDOM_ENDLESS,
                };
            }

            //if scanning, listing all prefixes available
            if (ModelData.Instance.Settings[ModelData.FILE_SCAN] == ModelData.PARAM_ON) {
                string[] files = { };
                if (ModelData.Instance.Settings[ModelData.FORMAT_INPUT] ==
                   ModelData.FORMAT_TYPE_JSON) {
                    files = Directory.GetFiles(path, "*_events.json");
                }
                else if (ModelData.Instance.Settings[ModelData.FORMAT_INPUT] ==
                    ModelData.FORMAT_TYPE_XML) {
                    files = Directory.GetFiles(path, "*_EVENTS.xml");
                }

                ModelData.Instance.userConfig[ModelData.FILE_NAME] = new List<string>() {
                        ModelData.FILE_NAME_DEFAULT
                };
                foreach (string dir in files) {
                    string dirStr = dir.Substring(dir.LastIndexOf('/') + 1, dir.Length - dir.LastIndexOf('/') - 1);
                    string prefixStr = dirStr.Substring(0, dirStr.IndexOf('_'));
                    if (!ModelData.Instance.userConfig[ModelData.FILE_NAME].Contains(prefixStr)) {
                        ModelData.Instance.userConfig[ModelData.FILE_NAME].Add(prefixStr);
                    }
                    ModelData.Instance.userConfigStr[prefixStr] = prefixStr;

                }
            }
            ModelData.Instance.ChangeSetting(ModelData.SERIAL_NAME, serial);
        }

        /**
         @fn    private void FileOutput(string filename, string fileContent)

         @brief Android file output - will output files to relevant folders determined by filename.

            - A filename of: NAME_123456 will output the file to the folder: cedar/NAME/
            - A filename of: settings_ will output the settings file: cedar/settings.json

         @author    Ryan Beruldsen
         @date  5/10/2016

         @param filename    Filename of the file.
         @param fileContent The file content.
         @param jsonContent Whether the content is JSON format.
         */
        private void FileOutput(string filename, string fileContent, bool json = false, bool csv = false) {

            //default to task output location
            string prefix = filename.Substring(0, filename.IndexOf('_'));
            string path = System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments);
            //default to plaintext for output
            string filenamePath;
            string baseDir = System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments);
            string serial = ModelData.Instance.Settings[ModelData.SERIAL_NAME];
            if (!serial.Equals("")) {
                baseDir = System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments) + "/DATA/" + ModelData.Instance.SessionStartTmeStr + "/" + serial;
            }
            //if compulstory JSON data file output
            if (prefix.Equals("settings") || prefix.Equals("analysis")) {
                filenamePath = Path.Combine(path, prefix + ".json");
                if (ModelData.Instance.Duration > 0) {
                    string fileFolder = ModelData.Instance.FileTimestamp();
                    if (!serial.Equals("")) {
                        fileFolder = ModelData.Instance.FileTimestamp(ModelData.Instance.SessionStartTime, true);
                    }
                    path = baseDir +
                        "/DATA/" + fileFolder;
                    if (!System.IO.Directory.Exists(path)) {
                        System.IO.Directory.CreateDirectory(path);
                    }
                    filenamePath = Path.Combine(path, prefix + ".json");
                }
                //if task file output
            }
            else {
                string fileFolder = filename.Substring(filename.IndexOf('_') + 1);
                path = baseDir +
                    "/DATA/" + fileFolder;
                filenamePath = Path.Combine(path, filename + ".txt");

                if (!System.IO.Directory.Exists(path)) {
                    System.IO.Directory.CreateDirectory(path);
                }

                //if output is json, change filenamePath
                if (json) {
                    filenamePath = Path.Combine(path, filename + ".json");
                }
                else if (csv) {
                    filenamePath = Path.Combine(path, filename + ".csv");
                }
            }
            File.WriteAllText(filenamePath, fileContent);
        }

        /**
         @fn    private void SettingsDataChanged(object sender, ModelDataEventArgs e)

         @brief Handler for settings data changed event - used to handle file load and output events, set by a flag change in ModelData settings

         @author    Ryan Beruldsen
         @date  5/10/2016

         @param sender  Source of the event.
         @param e       Model data event information.
         */
        private void SettingsDataChanged(object sender, ModelDataEventArgs e) {
            var fOut = ModelData.FORMAT_OUTPUT;
            bool jsonOut = ModelData.Instance.Settings[fOut] ==
                ModelData.FORMAT_TYPE_JSON;
            bool csvOut = ModelData.Instance.Settings[fOut] ==
                ModelData.FORMAT_TYPE_CSV;

            foreach (string changedSetting in e.settingsChangeList) {
                if (changedSetting.Equals(ModelData.FILE_RELOAD) &&
                    ModelData.Instance.Settings[changedSetting] == ModelData.PARAM_ON) {
                    ModelData.Instance.ChangeSetting(ModelData.FILE_RELOAD, ModelData.PARAM_OFF);
                    if (ModelDataIOConfig.Instance.GetConfigBool("mode", "test_serials")) {
                        int serialNum = int.Parse(ModelData.Instance.Settings[ModelData.SERIAL_NO]);
                        var serialList = ModelDataIOConfig.Instance.GetConfigType("test_serials");
                        int counter = 0;
                        string serialStr = "";
                        foreach (KeyValuePair<string, string> serial in serialList) {
                            if (counter++ == serialNum) {
                                serialStr = serial.Key;
                                break;
                            }
                        }
                        FileLoad(serialStr);
                    }
                    else {
                        FileLoad();
                    }


                }
                if (changedSetting.Equals(ModelData.FILE_OUTPUT) &&
                    ModelData.Instance.Settings[changedSetting] == ModelData.PARAM_ON) {

                    ModelData.Instance.ChangeSetting(ModelData.FILE_OUTPUT, ModelData.PARAM_OFF);

                    FileOutput(e.Filename, e.FileContents, json: jsonOut, csv: csvOut);
                }
            }
        }

        /**
         @fn    public override void ViewDidLoad()

         @brief View did load.

         @author    Ryan Beruldsen
         @date  5/10/2016

         @brief Called when the app is starting - setup tasks (largely CocosSharp default logic).
        */
        public override void ViewDidLoad() {

            //global iOS preset (for help info ect)
            ModelData.IsIOS = true;
            ModelData.deviceId = UIDevice.CurrentDevice.IdentifierForVendor.ToString();
            //initial config file load
            FileLoad();

            //check if dev mode is enabled, if so, show extension sample option
            if (ModelDataIOConfig.Instance.GetConfigBool("mode", "ext")) {
                ModelData.Instance.ExtensionSample();
            }

            //listen or file output trigger
            ModelData.Instance.ModelDataEventHandler += new

                ModelDataChangedEventHandler(SettingsDataChanged);

            base.ViewDidLoad();
        }

        public override void ViewWillDisappear(bool animated) {
            base.ViewWillDisappear(animated);
        }

        public override void ViewDidAppear(bool animated) {
            base.ViewDidAppear(animated);
        }

        public override void DidReceiveMemoryWarning() {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }

    }
}
