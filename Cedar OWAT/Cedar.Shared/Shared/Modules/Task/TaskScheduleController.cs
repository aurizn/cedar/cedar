﻿/*
 * CEDAR Operator Workload Assessment Tool
 * Copyright (C) 2023  aurizn
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


using System;
using System.Collections.Generic;
using CocosSharp;
using CEDAR.Shared.CedarCollection;

namespace CEDAR.Shared.Modules.Task {
    /**
     @class TaskScheduleController
    
     @brief Overall task controller (what task events happen when) for a test run.
    
     @author    Ryan Beruldsen
     @date  15/09/2016
     */

    class TaskScheduleController : CCNode {

        /** @brief Training score, out of 100. */
        public long score = 100;

        /** @brief List of managed tasks. */
        private readonly List<ITaskContent> _taskList;

        /** @brief Touch input listener to detect top-level gestures. */
        private readonly CCEventListenerTouchAllAtOnce _touchListener;

        /** @brief Touch begin point, for top-level gestures. */
        private CCPoint _touchBegin;

        /** @brief Touch begin point, for top-level gestures. */
        private CCPoint _touchEnd;

        private bool _swipeTestEnd = false;
        /** @brief Enabled state of controlled tasks 
        
            - state to be returned to on a pause event (where all tasks are disabled). */
        private readonly List<bool> _taskListEnabledState = new List<bool>();

        /** @brief The ongoing task duration. */
        private long _duration = 0;

        /** @brief The total number of IO events handled. */
        private int _eventsTrack = 0;

        /** @brief Ensure 00:00:00 is handled correctly. */
        private bool _timeZero = false;

        /** @brief The task pause duration, - 
        
            begins at max level, and decremented every second until 0, when pause mode is discontinued. */
        private long _taskPause = 0;

        /** @brief Control begin state 
        
            - this enables events to be transmitted from the task scheduler to it's controlled tasks. */
        private bool _controlBegin = false;

        /**
         @fn    public TaskScheduleController(List<ITaskContent> TaskList) : base()
        
         @brief Overall task controller (what task events happen when) for a test run.
        
         @author    Ryan Beruldsen
         @date  5/10/2016
        
         @param TaskList    List of tasks to control.
         */

        public TaskScheduleController(List<ITaskContent> TaskList) : base() {
            _taskList = TaskList;

            _touchListener = new CCEventListenerTouchAllAtOnce();
            _touchBegin = new CCPoint();
            _touchEnd = new CCPoint();

            _touchListener.OnTouchesBegan = HandleInputStart;
            _touchListener.OnTouchesEnded = HandleInputEnd;

            AddEventListener(_touchListener, this);

            ModelData.Instance.ChangeSetting(
                ModelData.OS_PAUSE,
                ModelData.PARAM_OFF
            );

            //schedule main task timer
            Schedule(TestSeconds, 1f);

            //handle settings changes
            ModelData.Instance.ModelDataEventHandler +=
                new ModelDataChangedEventHandler(SettingsDataChanged);

            //intialise added tasks
            for (int i = 0; i < TaskList.Count; i++) {
                TaskList[i].AddListener(new DivisionTaskEventHandler(DivsionTaskChanged));

                //schedule random movements of system gauges
                if (TaskList[i].GetTag() == "sysmon") {
                    Schedule(TaskList[i].Trigger, interval: 1f);
                }
                bool randomMode = ModelData.Instance.Settings[ModelData.RANDOM_MODE] != ModelData.PARAM_OFF;
                //if random mode (no config or event files required)
                if (TaskList[i] != null &&
                    randomMode) {

                    TaskList[i].SetEnabled(true);

                    //handle hardcoded tasks
                    if (TaskList[i].GetTag().Equals("comm")) {
                        Schedule(TaskList[i].Trigger, interval: 10f);
                    }
                    else if (TaskList[i].GetTag().Equals("resman")) {
                        Schedule(TaskList[i].Trigger, interval: 0.1f);

                        //handle any other tasks
                    }
                    else if (!TaskList[i].GetTag().Equals("sysmon")) {
                        Schedule(TaskList[i].Trigger, interval: 1f);
                    }
                }
            }
        }

        /**
         @fn    private void SettingsDataChanged(object sender, ModelDataEventArgs e)

         @brief Handle settings changes.

         @author    Ryan Beruldsen
         @date  16/11/2016

         @param sender  Source of the event.
         @param e       Model data event object
         */

        private void SettingsDataChanged(object sender, ModelDataEventArgs e) {
            if (e.SettingsChange) {
                foreach (string changedSetting in e.settingsChangeList) {
                    var changedSettingVal = ModelData.Instance.Settings[changedSetting];

                    if (changedSetting.Equals(ModelData.OS_PAUSE) &&
                    changedSettingVal == ModelData.PARAM_ON &&
                    ModelData.Instance.Duration > 0) {

                        ControlEventsLogAll();
                    }
                }
            }
        }

        /**
         @property  public long taskPause
        
         @brief Gets or sets the task pause.
        
         @return    The task pause.
         */

        public long TaskPause {
            get { return _taskPause; }
            set { _taskPause = value; }
        }

        /**
         @fn    private void DivsionTaskChanged(object sender, TasksControllerEventArgs e)
        
         @brief Divsion task event handler.
        
         @author    Ryan Beruldsen
         @date  5/10/2016
        
         @param sender  Source of the event.
         @param e       Tasks controller event information.
         */

        private void DivsionTaskChanged(object sender, TaskControllerEventArgs e) {
            if (e.WarningRaised) {
                if (e.Task.WarningsActive() == 1) {
                    e.Task.SetLastWarning(ModelData.Instance.Duration);
                    ModelDataIOLog.Instance.LogScore(taskTag: e.Task.GetTag());
                }
                e.Task.SetDivisionStatus(TaskStatus(e.Task));
            }
            else if (e.WarningResolved) {
                e.Task.SetDivisionStatus(TaskStatus(e.Task));
                ModelDataIOLog.Instance.LogScore(taskTag: e.Task.GetTag(), resolved: true);
            }
            else if (e.EnabledEvent) {
                e.Task.SetDivisionStatus(TaskStatus(e.Task));
            }
            else if (e.PenaltyRaised) {
                ModelDataIOLog.Instance.LogScore(taskTag: e.Task.GetTag(), resolved: true, timeout: true);
                if (score > 0) score -= e.PenaltyNum;
                if (score < 0) score = 0;
            }
        }

        /**
         @fn    public string TaskStatus(ITaskContent task)
        
         @brief Generates task status string.
        
         @author    Ryan Beruldsen
         @date  5/10/2016
        
         @param task    The task.
        
         @return    A string.
         */

        public string TaskStatus(ITaskContent task) {

            bool trainingMode = ModelData.Instance.Settings[ModelData.TRAINING_MODE] == ModelData.PARAM_ON;
            bool displayMode = ModelData.Instance.Settings[ModelData.MODE_DISPLAY] == ModelData.PARAM_ON;

            if (displayMode) {
                return "";
            }

            //if a single swipe, warn user of test end on second swipe
            if (_swipeTestEnd) {
                return "{ CONFIRM END TEST }";

                //if task is enabled and we're in training mode, display most information
            }
            else if (task.GetEnabled() && trainingMode) {
                //long seconds = ModelData.instance.duration % 60;
                //double minutes = Math.Floor(Convert.ToDouble(ModelData.instance.duration) / 60);

                //time since last warning was raised
                long secondsWarn = task.WarningsActive() > 0 ? (ModelData.Instance.Duration - task.GetLastWarning()) % 60 : 0;
                double minutesWarn = task.WarningsActive() > 0 ?
                    Math.Floor(Convert.ToDouble(ModelData.Instance.Duration - task.GetLastWarning()) / 60) : 0;
                return "{ " +
                    ModelData.TimeString(ModelData.Instance.Duration) + " | " +
                    (task.GetWarningCount()).ToString("000") + "/" + score.ToString("000") + " | " +
                    minutesWarn.ToString("00") + ":" + secondsWarn.ToString("00") + " }";

                //if task is enabled and we're not in training mode, display only enabled status and run time
            }
            else if (task.GetEnabled()) {
                //long seconds = ModelData.instance.duration % 60;
                //double minutes = Math.Floor(Convert.ToDouble(ModelData.instance.duration) / 60);
                return "{ " +
                    ModelData.TimeString(ModelData.Instance.Duration) + " | ENABLED }";

                //else task is disabled, inform user, display run time
            }
            else {
                //long seconds = ModelData.instance.duration % 60;
                //double minutes = Math.Floor(Convert.ToDouble(ModelData.instance.duration) / 60);
                return "{ " +
                    ModelData.TimeString(ModelData.Instance.Duration) + " | DISABLED }";
            }
        }

        /**
         @fn    public void TestSeconds(float time)
        
         @brief Run every second during testing - triggers events on the second, increments duration.
        
         @author    Ryan Beruldsen
         @date  6/10/2016
        
         @param time    The time.
         */

        public void TestSeconds(float time) {
            bool randomMode = ModelData.Instance.Settings[ModelData.RANDOM_MODE] != ModelData.PARAM_OFF;

            if (_timeZero) {

                DurationIncrement();
            }
            else {
                _timeZero = true;
            }

            if (randomMode) {
                RandomModeEvents();
            }
            else {

                CDRTaskEventTimeGroup events =
                    ModelDataIOEvents.Instance.GetEvent(ModelData.TimeString(_duration));
                var tempControl = _controlBegin;
                if (!_controlBegin) ControlEvents(events);
                ScoreEvents(events);
                IoEvents(events);

                if (_controlBegin && tempControl == _controlBegin) ControlEvents(events);

            }

        }

        /**
         @fn    public void RandomModeEvents()
        
         @brief Random mode events method.

            - random mode is an optional mode that does not require any events file input, events normally controlled by events IO are randomised.
        
         @author    Ryan Beruldsen
         @date  6/10/2016
         */

        public void RandomModeEvents() {

            //if duration is 01:59, pause the test and run a workload rating
            /*
            if (_duration == 119 && _taskPause == 0) {
                for (int j = 0; j < _taskList.Count; j++) {
                    _taskListEnabledState.Add(_taskList[j].getEnabled());
                }
                ModelData.currentPageStr = "rate";
                for (int j = 0; j < _taskList.Count; j++) {
                    _taskList[j].setEnabled(false);
                }
                _taskPause = ModelDataIOConfig.instance.getConfigNum("timeout", "rate");
                ModelData.instance.durationPause = _taskPause;

            }
            */
            //if duration is 02:00 and not set to endless, end test, output results
            if (_duration == 120 && ModelData.Instance.Settings[ModelData.RANDOM_MODE] != ModelData.PARAM_RANDOM_ENDLESS) {
                ControlEndEvent();
            }
            //manage random mode scoring 
            //(this is to give an indication of performance for training purposes)
            for (int i = 0; i < _taskList.Count; i++) {
                var task = _taskList[i];
                task.EventInternal(_duration);
                if (task.GetTag() == "resman") {
                    int timeout = ModelDataIOConfig.Instance.GetConfigNum("timeout", "task_resman");

                    if (task.WarningsActive() > 0 && (ModelData.Instance.Duration - task.GetLastWarning()) % timeout == 0) {
                        task.WarningOvertime(task);
                    }
                }
                /*
                if (task.warningsActive() > 0 && (_duration - task.getLastWarning()) % 10 == 0 && 
                        (_duration - task.getLastWarning()) != 0) {

                    task.warningOvertime(task);
                }
                */
                task.SetDivisionStatus(TaskStatus(task));
            }
        }

        /**
         @fn    public void DurationIncrement()
        
         @brief Increments the duration of the test if it is not currently paused, handles task pause durations.
        
         @author    Ryan Beruldsen
         @date  6/10/2016
         */

        public void DurationIncrement() {
            System.Diagnostics.Debug.WriteLine("TICK");
            Console.WriteLine("TICK");

            if (_taskPause == 0) {
                _duration++;

            }
            else if (_taskPause > 0) {
                _taskPause--;
                ModelData.Instance.DurationPause = _taskPause;
                if (ModelData.CurrentPageStr == "test_return") {
                    _taskPause = 0;
                    ModelData.Instance.DurationPause = _taskPause;
                    _duration++;
                    for (int j = 0; j < _taskListEnabledState.Count; j++) {
                        _taskList[j].SetEnabled(_taskListEnabledState[j]);
                    }
                }
                if (_taskPause == 0 && ModelData.CurrentPageStr != "test_return") {
                    var eventRow = new CDRLogRow() {
                        ModelData.Instance.TimeString24Hr(_duration),
                        (
                            ModelDataIOConfig.Instance.GetConfigNum("timeout", "rate") -
                            ModelData.Instance.DurationPause
                        ).ToString("00.0"),
                        "000",
                        "000",
                        "000",
                        "000",
                        "000",
                        "000",
                        "000.00",
                        "- event timed out",
                    };
                    ModelDataIOLog.Instance.LogTaskEvent("rate", eventRow);
                    ModelDataIOLog.Instance.LogEvent("Workload Rating", true);
                    ModelData.CurrentPageStr = "test_return";
                    _duration++;
                    for (int j = 0; j < _taskListEnabledState.Count; j++) {
                        _taskList[j].SetEnabled(_taskListEnabledState[j]);
                    }
                }
            }
            if (ModelDataIOConfig.Instance.GetConfigValue("serial", "name") == "") {
                ModelData.Instance.Duration = _duration;
            }
            else {
                string currentSerial = ModelDataIOConfig.Instance.GetConfigValue("serial", "name");
                int serialDuration = ModelDataIOConfig.Instance.GetConfigNum("test_serials", currentSerial);
                System.Diagnostics.Debug.WriteLine(ModelData.Instance.Duration + " " + serialDuration);

                if (ModelData.Instance.Duration == serialDuration) {
                    ControlEndEvent();
                }

                ModelData.Instance.Duration++;
            }
        }

        /**
         @fn    public void ScoreEvents(CDRTaskEventTimeGroup events)
        
         @brief Scores tasks, sets division status during non-random mode tests - this is for training performance metrics.
        
         @author    Ryan Beruldsen
         @date  6/10/2016
        
         @param events  The events.
         */

#pragma warning disable IDE0060 // Remove unused parameter
        public void ScoreEvents(CDRTaskEventTimeGroup events) {
            for (int i = 0; i < _taskList.Count; i++) {
                var task = _taskList[i];

                if (task.GetTag() == "resman") {
                    int timeout = ModelDataIOConfig.Instance.GetConfigNum("timeout", "task_resman");

                    if (task.WarningsActive() > 0 && (ModelData.Instance.Duration - task.GetLastWarning()) % timeout == 0) {
                        task.WarningOvertime(task);
                    }
                }

                task.SetDivisionStatus(TaskStatus(task));

            }
        }
#pragma warning restore IDE0060 // Remove unused parameter

        /**
         @fn    public void IoEvents(CDRTaskEventTimeGroup events)
        
         @brief I/O events handler - triggers internal (generally timeout/callback) and external (I/O) events to each task.
        
         @author    Ryan Beruldsen
         @date  6/10/2016
        
         @param events  The events.
         */

        public void IoEvents(CDRTaskEventTimeGroup events) {
            for (int i = 0; i < _taskList.Count; i++) {
                var task = _taskList[i];
                //if tasks are not paused
                if (TaskPause == 0) {
                    foreach (KeyValuePair<string, CDRTaskEventParamsTimeGroup> eventType in events) {

                        foreach (Dictionary<string, string> eventParameterList in events[eventType.Key]) {

                            if (_controlBegin) {
                                //send traditional "MATB XML" events directly for task to handle
                                // events may not originate from XML but retain 
                                //XML structure for compatabilty
                                task.EventExternal(eventType.Key, eventParameterList);
                            }
                        }
                    }
                    //trigger any internal events the task has registered
                    task.EventInternal(ModelData.Instance.Duration);

                }
            }
        }
        /**
         @fn    public void ControlEvents(CDRTaskEventTimeGroup events)
        
         @brief Handler schedule control events - pause task for workload rating, start/stop test.
        
         @author    Ryan Beruldsen
         @date  6/10/2016
        
         @param events  The events.
         */

        public void ControlEvents(CDRTaskEventTimeGroup events) {
            foreach (KeyValuePair<string, CDRTaskEventParamsTimeGroup> eventType in events) {
                //keep schedule level track of event numbering
                if (_taskPause == 0) {
                    _eventsTrack++;
                }

                foreach (CDRTaskEventParams eventParameterList in events[eventType.Key]) {

                    //if tasks are not paused
                    if (_taskPause == 0) {
                        int numTasks = _taskList.Count;
                        string currentSerial = ModelDataIOConfig.Instance.GetConfigValue("serial", "name");
                        int serialDuration = ModelDataIOConfig.Instance.GetConfigNum("test_serials", currentSerial);
                        //on control end event
                        if (eventType.Key.Equals("-") &&
                            eventParameterList.ContainsKey("control") &&
                            _controlBegin &&
                            eventParameterList["control"].Equals("end")) {

                            //if there is no current serial, or serial end is satisfied, end test
                            if (currentSerial.Equals("") || ModelData.Instance.Duration == serialDuration) {
                                ControlEndEvent();
                                //if this is a serial, loop test until serial specified end
                            }
                            else {
                                _duration = 0;
                            }

                        }
                        //on control start event
                        if (eventType.Key.Equals("-") &&
                            eventParameterList.ContainsKey("control") &&
                            eventParameterList["control"].Equals("start")) {
                            ModelDataIOLog.Instance.LogEvent(
                                ModelData.Instance.DurationStr24Hr,
                                "Control"
                            );
                            _controlBegin = true;
                        }

                        //start workload rating, pause test
                        if (eventType.Key.Equals("-") &&
                            _controlBegin &&
                            (eventParameterList.ContainsKey("rate") &&
                             eventParameterList["rate"].Equals("start") || eventParameterList.ContainsKey("wrs") &&
                             eventParameterList["wrs"].Equals("start"))) {

                            //save enabled state to restore later
                            for (int j = 0; j < numTasks; j++) {
                                while (_taskListEnabledState.Count < numTasks) {
                                    _taskListEnabledState.Add(true);
                                }
                                _taskListEnabledState[j] = _taskList[j].GetEnabled();
                            }

                            for (int j = 0; j < numTasks; j++) {
                                _taskList[j].SetEnabled(false);
                            }

                            //set pause duration to config param
                            _taskPause = ModelDataIOConfig.Instance.GetConfigNum("timeout", "rate");
                            ModelData.Instance.DurationPause = _taskPause;

                            ModelDataIOLog.Instance.LogEvent("Workload Rating");

                            //change page to workload rating page
                            ModelData.CurrentPageStr = "rate";

                        }
                    }
                }
            }
            //if events are handled by more than one task, the log event 
            // numbering can become out of sync with the schedule level 
            // numbering
            if (_eventsTrack < ModelDataIOLog.Instance.EventIndex) {
                _eventsTrack = ModelDataIOLog.Instance.EventIndex;
            }

            //note unhandled events in logged event numbering
            if (ModelDataIOLog.Instance.EventIndex != _eventsTrack &&
                    (
                        ModelData.CurrentPageStr.Equals("test") ||
                        ModelData.CurrentPageStr.Equals("test_return")
                    )
                ) {
                ModelDataIOLog.Instance.EventIndex = _eventsTrack;
            }

        }

        /**
         @fn    public void ControlEndEvent()

         @brief Control end event - end of CEDAR test, log all recorded user input and events.

         @author    Ryan Beruldsen
         @date  6/10/2016

         */

        public void ControlEndEvent() {
            //log end of control
            ModelDataIOLog.Instance.LogEvent(
                ModelData.Instance.DurationStr24Hr,
                "Control", true
            );

            ControlEventsLogAll();
            ModelData.Instance.Duration = 0;

            ModelData.Instance.LastScore = score;
            ModelDataIOLog.Instance.ClearLogs();

            if (ModelDataIOConfig.Instance.GetConfigBool("mode", "test_serials")) {
                ModelData.CurrentPageStr = "serial";
            }
            else {
                ModelData.CurrentPageStr = "title";
            }
        }

        /**
         @fn    public void ControlEventsLogAll()

         @brief Generates, outputs all log files for a session.

         @author    Ryan Beruldsen
         @date  6/10/2016

         */

        public void ControlEventsLogAll() {
            ModelData.Instance.Termination = ModelData.Timestamp();
            //process, output settings, analysis logs
            ModelData.Instance.SaveSettings();
            ModelDataIOLog.Instance.PrintTouchLog();
            ModelDataIOLog.Instance.PrintScoreLog();

            int numTasks = _taskList.Count;
            var fOut = ModelData.FORMAT_OUTPUT;
            bool jsonOut =
                ModelData.Instance.Settings[fOut] ==
                ModelData.FORMAT_TYPE_JSON;

            bool csvOut =
                ModelData.Instance.Settings[fOut] ==
                ModelData.FORMAT_TYPE_CSV;

            //if output all formats
            bool allOutput =
                ModelData.Instance.Settings[fOut] ==
                ModelData.FORMAT_TYPE_ALL;

            //output overall cedar log
            if (allOutput) {
                ModelData.Instance.ChangeSetting(
                    ModelData.FORMAT_OUTPUT,
                    ModelData.FORMAT_TYPE_JSON
                );
                ModelDataIOLog.Instance.OutputLog(json: true);

                ModelData.Instance.ChangeSetting(
                    ModelData.FORMAT_OUTPUT,
                    ModelData.FORMAT_TYPE_PLAINTEXT
                );
                ModelDataIOLog.Instance.OutputLog(plaintext: true);

                ModelData.Instance.ChangeSetting(
                    ModelData.FORMAT_OUTPUT,
                    ModelData.FORMAT_TYPE_CSV
                );
                ModelDataIOLog.Instance.OutputLog(csv: true);

                ModelData.Instance.ChangeSetting(
                    ModelData.FORMAT_OUTPUT,
                    ModelData.FORMAT_TYPE_ALL
                );
            }
            else if (jsonOut) {
                ModelDataIOLog.Instance.OutputLog(json: true);
            }
            else if (csvOut) {
                ModelDataIOLog.Instance.OutputLog(csv: true);
            }
            else {
                ModelDataIOLog.Instance.OutputLog(plaintext: true);
            }

            ModelDataIOLog.loggedTasksStr = new List<string> {
                "cedar"
            };
            //output all cedar task logs
            for (int j = 0; j < numTasks; j++) {
                ModelDataIOLog.loggedTasksStr.Add(_taskList[j].GetTag());
                if (allOutput) {
                    ModelData.Instance.ChangeSetting(
                        ModelData.FORMAT_OUTPUT,
                        ModelData.FORMAT_TYPE_JSON
                    );
                    ModelDataIOLog.Instance.OutputTaskEventLog(
                        _taskList[j].GetTag(),
                        json: true
                    );

                    ModelData.Instance.ChangeSetting(
                        ModelData.FORMAT_OUTPUT,
                        ModelData.FORMAT_TYPE_PLAINTEXT
                    );
                    ModelDataIOLog.Instance.OutputTaskEventLog(
                        _taskList[j].GetTag(),
                        plaintext: true
                    );

                    ModelData.Instance.ChangeSetting(
                        ModelData.FORMAT_OUTPUT,
                        ModelData.FORMAT_TYPE_CSV
                    );
                    ModelDataIOLog.Instance.OutputTaskEventLog(
                        _taskList[j].GetTag(),
                        csv: true
                    );

                    ModelData.Instance.ChangeSetting(
                        ModelData.FORMAT_OUTPUT,
                        ModelData.FORMAT_TYPE_ALL
                    );
                }
                else if (jsonOut) {
                    ModelDataIOLog.Instance.OutputTaskEventLog(
                        _taskList[j].GetTag(),
                        json: true
                    );
                }
                else if (csvOut) {
                    ModelDataIOLog.Instance.OutputTaskEventLog(
                        _taskList[j].GetTag(),
                        csv: true
                    );
                }
                else {
                    ModelDataIOLog.Instance.OutputTaskEventLog(
                       _taskList[j].GetTag(),
                       plaintext: true
                   );
                }
            }
            ModelDataIOLog.loggedAnalysisStr = new List<string>();

            //output all other non-task specific logs

            ModelDataIOLog.loggedAnalysisStr = new List<string> { "rate", "touch", "score" };

            if (ModelDataIOLog.Instance.LogTaskEventCount("rate") < 2) {
                ModelDataIOLog.loggedAnalysisStr = new List<string> { "touch", "score" };
            }

            for (int j = 0; j < ModelDataIOLog.loggedAnalysisStr.Count; j++) {
                if (allOutput) {
                    ModelData.Instance.ChangeSetting(
                        ModelData.FORMAT_OUTPUT,
                        ModelData.FORMAT_TYPE_JSON
                    );
                    ModelDataIOLog.Instance.OutputTaskEventLog(ModelDataIOLog.loggedAnalysisStr[j], json: true);

                    ModelData.Instance.ChangeSetting(
                        ModelData.FORMAT_OUTPUT,
                        ModelData.FORMAT_TYPE_PLAINTEXT
                    );
                    ModelDataIOLog.Instance.OutputTaskEventLog(ModelDataIOLog.loggedAnalysisStr[j], plaintext: true);

                    ModelData.Instance.ChangeSetting(
                        ModelData.FORMAT_OUTPUT,
                        ModelData.FORMAT_TYPE_CSV
                    );
                    ModelDataIOLog.Instance.OutputTaskEventLog(ModelDataIOLog.loggedAnalysisStr[j], csv: true);

                    ModelData.Instance.ChangeSetting(
                        ModelData.FORMAT_OUTPUT,
                        ModelData.FORMAT_TYPE_ALL
                    );
                }
                else if (jsonOut) {
                    ModelDataIOLog.Instance.OutputTaskEventLog(ModelDataIOLog.loggedAnalysisStr[j], json: true);
                }
                else if (csvOut) {
                    ModelDataIOLog.Instance.OutputTaskEventLog(ModelDataIOLog.loggedAnalysisStr[j], csv: true);
                    //plaintext
                }
                else {
                    ModelDataIOLog.Instance.OutputTaskEventLog(ModelDataIOLog.loggedAnalysisStr[j], plaintext: true);
                }
            }
            string serial = ModelData.Instance.Settings[ModelData.SERIAL_NAME];

            if (!serial.Equals("")) {
                ModelDataIOLog.loggedAnalysisStr.Add("q");
            }

            ModelDataIOLog.Instance.OutputAnalysisLog();

        }
        /**
         @fn    private void HandleInputStart(System.Collections.Generic.List<CCTouch> touches, CCEvent touchEvent)
        
         @brief Handles the touch input start.
        
         @author    Ryan Beruldsen
         @date  20/09/2016
        
         @param touches     CocosSharp injected touch list
         @param touchEvent  CocosSharp injected touch event
         */

        private void HandleInputStart(System.Collections.Generic.List<CCTouch> touches,
            CCEvent touchEvent) {
            if (touches.Count > 0) {
                _touchBegin = new CCPoint(touches[0].Location.X, touches[0].Location.Y);
            }
        }

        /**
        @fn    private void HandleInputEnd(List<CCTouch> touches, CCEvent touchEvent)

        @brief Handles the touch input end.

        @author    Ryan Beruldsen
        @date  20/09/2016

        @param touches     CocosSharp injected touch list
        @param touchEvent  CocosSharp injected touch event
        */

        private void HandleInputEnd(List<CCTouch> touches, CCEvent touchEvent) {
            if (touches.Count > 0) {
                _touchEnd = new CCPoint(touches[0].Location.X, touches[0].Location.Y);
            }
            double distX = _touchEnd.X - _touchBegin.X;
            double distY = _touchEnd.Y - _touchBegin.Y;

            bool hSwipe = -distY > ModelData.Height * 0.3f;
            bool vSwipe = Math.Abs(distX) > ModelData.Width * 0.1f;

            System.Diagnostics.Debug.WriteLine("SWIPE " + hSwipe + " " + vSwipe + " " + distY + " " + ModelData.Height * 0.3f);
            //if top down swipe
            if (hSwipe && !vSwipe) {

                if (!_swipeTestEnd) {
                    int numTasks = _taskList.Count;

                    //save enabled state to restore later
                    for (int j = 0; j < numTasks; j++) {
                        _taskListEnabledState.Add(_taskList[j].GetEnabled());
                    }

                    for (int j = 0; j < numTasks; j++) {
                        _taskList[j].SetEnabled(false);
                    }

                    _swipeTestEnd = true;

                    //pause indefinitely
                    _taskPause = -1;
                    ModelData.Instance.DurationPause = -1;
                    ModelData.Instance.Dialog("Are you sure you want to end CEDAR run?",
                    (bool response) => {
                        if (response) {
                            ControlEndEvent();
                        }
                        else {
                            //restore state
                            for (int j = 0; j < _taskList.Count; j++) {
                                _taskList[j].SetEnabled(_taskListEnabledState[j]);
                            }
                            _taskPause = 0;
                            ModelData.Instance.DurationPause = 0;
                        }
                        _swipeTestEnd = false;
                        return response;
                    });

                }
                else {

                    //controlEndEvent();
                }

            }
            else {
                //_swipeTestEnd = false;
            }
        }
    }
}
