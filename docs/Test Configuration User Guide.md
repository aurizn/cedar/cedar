# Test Configuration User Guide

![](media/cedar_ios_icons.png)

This user guide describes the operation of the Cedar operator workload assessment tool as a test master.

If you have experience with NASA MATB-II, the _Legacy MATB-II Test Subject User Guide_ document may be useful to you.

For a quick start, no [_Event_](./IO%20COMMS%20Log%20File%20Guide.md) IO Events File Guide.md) or [_Config_](./IO%20Config%20File%20Guide.md) files are required for the _Random Mode_ which simulates a moderate workload events file. For _Random Mode,_ skip to _Step 4_.


To conduct a CEDAR OWAT session:

> 1.  Create Cedar input files, according to the specifications in the _Cedar IO_ documents, that is, the [_Config File Guide_](./IO%20Config%20File%20Guide.md) and the _[Events File Guide](./IO%20Events%20File%20Guide.md)._
> 2.  Transfer Cedar input files to the required directories as per operating system specific instructions in the _File Guides_.
> 3.  Install the Cedar app.
> 4.  Run the Cedar app, ensure that the correct input format is selected in the settings (see _Input Format_ in [User Modifiable Configuration](./User Modifiable Configuration Settings.md)).
> 5.  Start the Cedar test from the main screen.
> 6.  Test will terminate on the [CONTROL END](./IO%20Events%20File%20Guide.md) event or when terminated manually by swiping down on the test screen.
> 7.  Collect the Cedar test output files according to the _Cedar IO_ documents (such as the main, [Cedar Log](./IO%20CEDAR%20Log%20File%20Guide.md)).
> 8.  Process the test output files, to ascertain any information from any test runs, for metrics such as “average response time” ect. as required, given the output format specified in the _Cedar IO_ documents.

If the _training mode_ is enabled (see [Config File Guide](./IO Config File Guide.md)), the test footer will be visible, as in the screenshots of the test screen below. For a guide on what the footer text means, refer to the _Footer Data Definition_ section.

### Random Mode

The _random mode_ will give you a score out of 100 - a perfect score of 100 should be more than acheivable with a bit of practice. For the best training experience, the _random_ and _training_ modes should be enabled at the same time for initial training.

### Pausing/Terminating a Session Early

If a CEDAR OWAT session is paused by navigating away from the CEDAR OWAT app context, this will be noted in the logs. A session can be terminated early by swiping down on the test screen and confirming in the dialog that appears. All logs are output progressively so even if the app is terminated there will be complete logging output for the session information available.
___

### **Colour Definitions**

| Colour Parameter | Default Colour | Meaning |
| --- | --- | --- |
| Active / Highlight | **Yellow** | This colour suggests a component is selected or requires ongoing attention. This may mean a system monitoring scale, which requires the attention of the operator to ensure it does not trend below or above safe levels. Alternatively it may denote selection and attention, in the case of the communications task where the NAV or COM channels can be selected and will be coloured the active colour upon selection. Following selection, the frequency slider (which is also active colour) should be attended so that it aligns with the target frequency, which will change over time, requiring the user’s ongoing attention. |
| Range Indication | **Blue** | The range indication colour is used to emphasise the safe operating levels of a given component.These can be in the form of a scale immediately to the left or right of a level component, to which a level must not trend higher or lower or it will lead to a malfunction. It can also be in the form of a range guide, such as in the case of the communication slider: a blue range indication will appear when the slider overlaps with the desired frequency target on the horizontal display. |
| Warning / Alert | **Red** | The warning colour draws the attention of the user to a current fault. This fault may or may not require attention – this is left to the user to interpret. A pump function cannot be resolved, for example and must the user must simply wait until the fault is automatically resolved. |

### **Task Divisions**

| Task Division | Screenshot | Description |
| --- | --- | --- |
| Communications | ![](media/cedar_comms.png) | The goal of the communications task is to enter target frequencies as they are declared on the voice-over. The target frequency is entered by means of a frequency slider. The slider can be operated by dragging the slider handle, or by tapping near the edges of the handle for fine control.<br><br>The target frequency is illustrated on the horizontal scale in the Warning colour. Four channels can be selected for modification, denoted by the Active colour on selection.<br><br>The actual current frequency of the current selected channel is displayed either singly below the channel label in the case the actual frequency is the same as the target frequency, of format:<br><br>**Target Frequency**<br><br>or in the case that the target frequency is different to the actual frequency, displayed of format:<br><br>**Target Frequency \[Current Frequency\]**<br><br>If training mode has been enabled:<br><br>If a channel’s current frequency is different to it’s target frequency and it is not selected, it will be Warning coloured to draw the user’s attention. In addition to the Warning colouring, a sliver of red will extend below the regular height of the frequency selection button to ensure the visibility of the warning indication even when the channel is selected. |
| System Monitoring | ![](media/cedar_sys.png) | The system monitoring task takes the form of two separate ongoing observations. The switches, to the left have a rest state of dark grey. In warning state they turn red, and can be attended by tapping them as buttons – this resolves the warning state.<br><br>The scales, to the right, will move randomly and only enter a warning state if they move outside the safe range shown on each scale. An individual scale can be attended if outside safe levels by tapping anywhere within the bounds of the scale. |
| Resource Management | ![](media/cedar_res.png) | The resource management task revolves around ensuring two main tanks (the larger two) remain within safe levels. Pumps can be activated by tapping on or near the targeted pump, the direction of each pump is labelled and appears in the subtitle of the task when activated. The speed of the pump in units per minute is also displayed when activated. If the levels of either main tank falls below the safe levels, the Active colour used to illustrate the current tank level will change to the Warning colour to indicate a tank fault.<br><br>The pumps sometimes fail, as in the screenshot to the left (the pump line is coloured the warning colour), at which point they are disabled and turned OFF if ON. If a pump is ON when it fails, it will be surrounded by a brackets in the subtitle, like so:<br><br>**\[F>B : 600\]**<br><br>The pump, in this state, will become active and start pumping again when the failure is rectified. |

### **Footer Data Definition**

Footer data is distinguishable from other division data (such as labels) by it’s enclosing braces and is formatted as follows, given the data definitions below:

| Data | Definition |
| --- | --- |
| Total Time | The total time elapsed in seconds and minutes, eg. 01:30 being one minute and thirty seconds elapsed. |
| Malfunction Time | The total time a malfunction has been left unresolved, in seconds and minutes. A unit of score will be deducted from the total score on timeout for each component in the division that is malfunctioning. |
| Total Score | The current score a user has maintained since the beginning of the test. Scores begin at 100, one unit is deducted for a timeout duration that a component has been malfunctioning without intervention (if that component requires intervention – ie a pump malfunction cannot be fixed). |
| Division Score | The number of score units that have been deducted from the total score by the division, see Total Score for a description of the score mechanism. |

| Footer Format |
| --- |
| { Total Time \| Division Score / Total Score \| Malfunction Time } |