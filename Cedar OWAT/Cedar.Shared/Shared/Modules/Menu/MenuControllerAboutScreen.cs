﻿/*
 * CEDAR Operator Workload Assessment Tool
 * Copyright (C) 2023  aurizn
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using CocosSharp;
using System;

/*! \namespace CEDAR.Shared.Modules.Menu
 * @brief Cedar UI menu content
 * 
 */

namespace CEDAR.Shared.Modules.Menu {

    /**
     @class MenuControllerAboutScreen
    
     @brief About/info screen, containing company/copyright info and marketing.
    
     @author    Ryan Beruldsen
     @date  14/09/2016
     */

    class MenuControllerAboutScreen : CCNode {

        /** @brief Internal window bounds. */
        private CCPoint _windowMax;


        /** @brief Back button. */
        private MenuViewButton _backButton;

        /** @brief Listener for button touch events. */
        private readonly CCEventListenerTouchAllAtOnce _touchListener;

        /**
         @fn    public MenuControllerAboutScreen(CCPoint windowMax) : base()
        
         @brief About screen constructor.
        
         @author    Ryan Beruldsen
         @date  16/09/2016
        
         @param windowMax   Window bounds (max X and Y coordinates)
         */

        public MenuControllerAboutScreen(CCPoint windowMax) : base() {
            this._windowMax = new CCPoint(windowMax.X, windowMax.Y);
            _touchListener = new CCEventListenerTouchAllAtOnce {
                OnTouchesBegan = HandleInput,
                OnTouchesEnded = HandleInputEnd
            };

            AddEventListener(_touchListener, this);

            Init();
        }

        /**
         @fn    public void Init()
        
         @brief Initialise about page.
        
         @author    Ryan Beruldsen
         @date  16/09/2016
         */

        private void Init() {
            //about/copyright information image
            CCSprite sprite = new CCSprite("about.png") {
                Scale = 2,
                AnchorPoint = CCPoint.AnchorMiddle
            };

            //position info image centrally
            float height = sprite.BoundingBox.MaxY - sprite.BoundingBox.MinY;
            sprite.PositionX = Math.Abs(sprite.BoundingBox.MaxX - sprite.BoundingBox.MinX);
            if (ModelData.Instance.Settings[ModelData.WIDTH_OFFSET] == ModelData.WIDTH_OFFSET_10) {
                sprite.PositionX -= ModelData.Width * 0.05f;
            }
            else if (ModelData.Instance.Settings[ModelData.WIDTH_OFFSET] == ModelData.WIDTH_OFFSET_5) {
                sprite.PositionX -= ModelData.Width * 0.025f;
            }
            sprite.PositionY = Math.Abs(height + (_windowMax.Y - height) / 4);
            AddChild(sprite);

            //back to main menu button
            _backButton = new MenuViewButton(
                label: "< Back",
                highlightColour: ModelData.Instance.GetColourPallete4B(ModelData.COLOUR_PARAM_ACTIVE),
                width: _windowMax.X * 0.175f,
                height: _windowMax.Y * 0.04f,
                x: _windowMax.X * 0.1f,
                y: _windowMax.Y * 0.95f);

            AddChild(_backButton);

            //draw version information
            CCLabel versionInfo = new CCLabel("Open Source Version " + ModelData.CEDAR_VERSION,
                                               "roboto",
                                               14,
                                               CCLabelFormat.SpriteFont
           ) {
                Position = new CCPoint(_windowMax.X / 2, _windowMax.Y - _windowMax.Y * 0.44f),
                Color = ModelData.Instance.GetColourPallete3B(ModelData.COLOUR_PARAM_ACTIVE),
                AnchorPoint = CCPoint.AnchorMiddleBottom
            };
            AddChild(versionInfo);
        }

        /**
         @fn    private void HandleInputEnd(System.Collections.Generic.List<CCTouch> touches, CCEvent touchEvent)
        
         @brief Handles touch input end
        
         @author    Ryan Beruldsen
         @date  16/09/2016
        
         @param touches     CocosSharp injected touch list
         @param touchEvent  CocosSharp injected touch event
         */

        private void HandleInputEnd(System.Collections.Generic.List<CCTouch> touches, CCEvent touchEvent) {
            if (touches.Count > 0) {
                //on back button press, return to main menu (page string: "title")
                if (_backButton.Active && MenuButtonHitTest(touches, _backButton)) {
                    ModelData.CurrentPageStr = "title";
                }

                _backButton.Active = false;
            }
        }

        /**
         @fn    private void MenuButtonHitTest(System.Collections.Generic.List<CCTouch> touches, MenuViewButton m)
        
         @brief Hit test for menu button.
        
         @author    Ryan Beruldsen
         @date  16/09/2016
        
         @param touches CocosSharp injected touch list (from touch handler)
         @param m       The MenuViewButton to test for bounding box hit.
        
         @return    True if within button bounds, otherwise false
         */

        private bool MenuButtonHitTest(System.Collections.Generic.List<CCTouch> touches, MenuViewButton m) {
            if (touches[0].Location.X > m.minButtonBounds.X && touches[0].Location.X < m.maxButtonBounds.X &&
                    touches[0].Location.Y > m.minButtonBounds.Y &&
                    touches[0].Location.Y < m.maxButtonBounds.Y) {
                return true;
            }
            return false;
        }

        /**
         @fn    private void HandleInput(System.Collections.Generic.List<CCTouch> touches, CCEvent touchEvent)
        
         @brief Handles touch input
        
         @author    Ryan Beruldsen
         @date  16/09/2016
        
         @param touches     CocosSharp injected touch list
         @param touchEvent  CocosSharp injected touch event
         */

        private void HandleInput(System.Collections.Generic.List<CCTouch> touches, CCEvent touchEvent) {
            if (touches.Count > 0) {
                //test for back buton hit
                if (MenuButtonHitTest(touches, _backButton)) {
                    _backButton.Active = true;
                }
            }
        }
    }
}

