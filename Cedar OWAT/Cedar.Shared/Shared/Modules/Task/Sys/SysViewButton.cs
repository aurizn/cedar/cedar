﻿/*
 * CEDAR Operator Workload Assessment Tool
 * Copyright (C) 2023  aurizn
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using CocosSharp;

namespace CEDAR.Shared.Modules.Task.Sys {
    /**
     @class SysViewButton
    
     @brief A button component representing a system monitoring toggle, displayed in series in SysControllerButtonPanel
    
     @author    Ryan Beruldsen
     @date  15/09/2016
     */

    class SysViewButton : DivisionContent, IDivisionContent {

        /** @brief The x coordinate. */
        private readonly float _x;

        /** @brief The y coordinate. */
        private readonly float _y;

        /** @brief The button width. */
        private readonly float _width;

        /** @brief The button height. */
        private readonly float _height;

        /** @brief Warning indication state. */
        private bool _warnIndicate;


        /** @brief The button label. */
        private readonly string _label;

        /** @brief Button warning colour */
        private readonly CCColor4B _colourWarning;

        /** @brief Gauge active text colour (used to show current levels). */
        private readonly CCColor3B _colourActiveText;

        /** @brief The main non-warning button sprite. */
        private readonly CCSprite _button;

        /** @brief The button warning sprite. */
        private readonly CCSprite _buttonWarn;

        /**
         @property  public bool warnIndicate
        
         @brief Gets or sets a value indicating warning indication state.
        
         @return    true if warning indicate, false if not.
         */

        public bool WarnIndicate {
            get {
                return _warnIndicate;
            } set {
                _warnIndicate = value;
                _buttonWarn.Visible = _warnIndicate;
                _button.Visible = !_warnIndicate;
            }
        }


        /** @brief The minimum button bounds. */
        public CCPoint minButtonBounds;


        /** @brief The maximum button bounds. */
        public CCPoint maxButtonBounds;


        /** @brief The button label. */
        CCLabel buttonLabel;

        /** @brief The warning state button label. */
        CCLabel buttonLabelWarn;

        public SysViewButton(   Division division, 
                                string label,
                                CCColor3B colourActiveText,
                                CCColor4B colourWarning,
                                float width, 
                                float height, 
                                float x, 
                                float y, 
                                bool warnIndicate = false) : base(division) {

            _colourActiveText = colourActiveText;
            _colourWarning = colourWarning;
            _warnIndicate = warnIndicate;
            _height = height;
            _width = width;
            _x = x;
            _y = y;
            _label = label;
            _button = new CCSprite();
            _buttonWarn = new CCSprite();
        }

        /**
         @fn    public void Init()
        
         @brief Initialise, draw system monitoring button.
        
         @author    Ryan Beruldsen
         @date  22/09/2016
         */

        public void Init() {
            //instantiate sprites
            CCDrawNode buttonDrawNode = new CCDrawNode();
            CCDrawNode buttonWarnDrawNode = new CCDrawNode();

            _button.AddChild(buttonDrawNode);
            _buttonWarn.AddChild(buttonWarnDrawNode);

            AddChild(_button);
            AddChild(_buttonWarn);

            _buttonWarn.Visible = _warnIndicate;

            //bounding box of button
            minButtonBounds = new CCPoint(_x, _y);
            maxButtonBounds = new CCPoint(_x+ _width, _y+ _height);

            float spacer = _width * 0.01f;

            //place labels
            buttonLabel = new CCLabel(_label, "robotob", 14, CCLabelFormat.SpriteFont) {
                Position = new CCPoint(_x + spacer, _y + spacer),
                Color = _colourActiveText,
                AnchorPoint = CCPoint.AnchorLowerLeft,
            };
            _button.AddChild(buttonLabel);

            buttonLabelWarn = new CCLabel(_label, "robotob", 14, CCLabelFormat.SpriteFont) {
                Position = new CCPoint(_x + spacer, _y + spacer),
                Color = CCColor3B.Black,
                AnchorPoint = CCPoint.AnchorLowerLeft,
            };

            _buttonWarn.AddChild(buttonLabelWarn);

            //draw button rectangles
            buttonDrawNode.DrawRect(
                new CCRect(_x, _y, _width, _height),
                fillColor: new CCColor4B(100, 100, 100, 100),
                borderWidth: 0,
                borderColor: CCColor4B.White);

            buttonWarnDrawNode.DrawRect(
                new CCRect(_x, _y, _width, _height),
                fillColor: _colourWarning,
                borderWidth: 0,
                borderColor: CCColor4B.White);

        }
    }
}
