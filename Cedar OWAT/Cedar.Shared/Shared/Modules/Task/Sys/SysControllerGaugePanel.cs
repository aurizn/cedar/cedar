﻿/*
 * CEDAR Operator Workload Assessment Tool
 * Copyright (C) 2023  aurizn
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using CocosSharp;
using System.Collections.Generic;
using CEDAR.Shared.CedarCollection;

namespace CEDAR.Shared.Modules.Task.Sys {

    /**
     @class SysControllerGaugePanel
    
     @brief Controller for panel of system gauges (SysViewGauge), representing system monitoring toggles (level variable).
    
     @author    Ryan Beruldsen
     @date  15/09/2016
     */

    class SysControllerGaugePanel : DivisionContent, IDivisionContent {

        /** @brief List of system channels (models). */
        readonly List<SysModelChannel> _sysChannelList;

        /** @brief Listener for button touch events. */
        readonly CCEventListenerTouchAllAtOnce _touchListener;

        /** @brief List of system gauges (views). */
        readonly List<SysViewGauge> _sysGaugeList;

        /** @brief Hash map of buttons (views) to respective models. */
        readonly Dictionary<SysModelChannel, SysViewGauge> _sysGaugeDict;

        /**
         @fn    public SysControllerGaugePanel(List<SysModelChannel> SysChannelList, Division division) : base(division)
        
         @brief Constructor of System Monitoring Gauge Panel.
        
         @author    Ryan Beruldsen
         @date  22/09/2016
        
         @param SysChannelList  List of system channels (models).
         @param division        The division.
         */

        public SysControllerGaugePanel(List<SysModelChannel> SysChannelList, 
                                        Division division) : base(division) {
            this._sysChannelList = SysChannelList;
            _sysGaugeList = new List<SysViewGauge>();
            _touchListener = new CCEventListenerTouchAllAtOnce {
                OnTouchesBegan = HandleInput
            };
            AddEventListener(_touchListener, this);
            _sysGaugeDict = new Dictionary<SysModelChannel, SysViewGauge>();

        }

        /**
         @fn    public void Init()
        
         @brief Initialise System Monitoring Gauge Panel.
        
         @author    Ryan Beruldsen
         @date  22/09/2016
         */

        public void Init() {
            float width = _division.Width * 0.3f;
            int warnOnlyCount = 0;

            //loop through available sys models and create gauges to match
            for (int j = 0; j < _sysChannelList.Count; j++) {
                var sysChannel = _sysChannelList[j];
                sysChannel.SysModelChannelEventHandler += SysModelChanged;

                //if channel is not indended for button (warn only) mode
                if (!sysChannel.WarnOnlyMode) {
                    float transf = (_division.Width * 0.85f - _division.Width * 0.325f) / 3;
                    float posX = _division.Width * 0.325f + transf * warnOnlyCount;

                    SysViewGauge gauge = new SysViewGauge(
                        division: _division,
                        label: sysChannel.Label,

                        colourActive: ModelData.Instance.GetColourPallete4B(ModelData.COLOUR_PARAM_ACTIVE),
                        colourRange: ModelData.Instance.GetColourPallete4B(ModelData.COLOUR_PARAM_RANGE),
                        colourWarning: ModelData.Instance.GetColourPallete4B(ModelData.COLOUR_PARAM_WARNING),
                        colourWarningText: ModelData.Instance.GetColourPallete3B(ModelData.COLOUR_PARAM_WARNING),

                        level: sysChannel.Level,
                        valueStr: sysChannel.ValueStr,
                        minSafeLevel: sysChannel.MinVal,
                        maxSafeLevel: sysChannel.MaxVal,
                        tolerance: sysChannel.Tolerance,
                        width: _division.Width * 0.1f,
                        height: width,
                        x: posX,
                        y: _division.PrevDivisionY + (_division.Height * 0.10f)
                    );

                    gauge.Init();
                    AddChild(gauge);
                    _sysGaugeDict[sysChannel] = gauge;
                    _sysGaugeList.Add(gauge);
                    warnOnlyCount++;
                }
            }
            

        }

        /**
         @fn    private void SysModelChanged(object sender, SysModelChannelEventArgs e)
        
         @brief System model changed.
        
         @author    Ryan Beruldsen
         @date  22/09/2016
        
         @param sender  Source of the event.
         @param e       System model channel event information.
         */

        private void SysModelChanged(object sender, SysModelChannelEventArgs e) {
            
            if (e.WarnIndicateChange && !e.Sys.WarnOnlyMode &&_sysGaugeDict.ContainsKey(e.Sys)) {
                _sysGaugeDict[e.Sys].WarnIndicate = e.Sys.WarnIndicate;
            } else if (e.ValueChange && !e.Sys.WarnOnlyMode) {
                _sysGaugeDict[e.Sys].SetLevel(e.Sys.Level, e.Sys.ValueStr);
            }
            
        }

        /**
         @fn    private void HandleInput(System.Collections.Generic.List<CCTouch> touches, CCEvent touchEvent)
        
         @brief Handles touch input.
        
         @author    Ryan Beruldsen
         @date  22/09/2016
        
         @param touches     CocosSharp injected touch list
         @param touchEvent  CocosSharp injected touch event
         */

        private void HandleInput(System.Collections.Generic.List<CCTouch> touches, CCEvent touchEvent) {
            if (touches.Count > 0) {
                int warnOnlyCount = 0;
                bool hit = false;

                for (int j = 0; j < _sysChannelList.Count; j++) {
                    var sysChannel = _sysChannelList[j];
                    if (!sysChannel.WarnOnlyMode) {
                        if (    touches[0].Location.X > _sysGaugeList[warnOnlyCount].minButtonBounds.X && 
                                touches[0].Location.X < _sysGaugeList[warnOnlyCount].maxButtonBounds.X &&
                                touches[0].Location.Y > _sysGaugeList[warnOnlyCount].minButtonBounds.Y && 
                                touches[0].Location.Y < _sysGaugeList[warnOnlyCount].maxButtonBounds.Y) {

                            sysChannel.IsSelected = true;
                            hit = true;

                            ModelDataIOLog.Instance.LogTouch(
                                x: touches[0].Location.X,
                                y: touches[0].Location.Y,
                                status: CDRTouchData.StatusEnum.Hit
                            );

                            if (sysChannel.WarnIndicate) {
                                sysChannel.WarnIndicate = false;                                
                            }

                        }
                        warnOnlyCount++;
                    }
                };

                if (!hit) {
                    ModelDataIOLog.Instance.LogTouch(
                        x: touches[0].Location.X,
                        y: touches[0].Location.Y,
                        status: CDRTouchData.StatusEnum.Miss
                    );
                }
            }
        }

    }
}
