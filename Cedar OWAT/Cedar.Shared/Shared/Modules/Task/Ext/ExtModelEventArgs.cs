﻿/*
 * CEDAR Operator Workload Assessment Tool
 * Copyright (C) 2023  aurizn
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;

namespace CEDAR.Shared.Modules.Task.Ext {

    /**
     @class ExtModelEventArgs
    
     @brief Ext button model event parameters.
    
     @author    Ryan Beruldsen
     @date  10/10/2016
     */

    internal class ExtModelEventArgs : EventArgs {
        /**
         @property  public bool warnIndicateChange
        
         @brief Warning indication change event (warning is raised or resolved for tank)
        
         @return    true if warning indicate change, false if not.
         */

        public bool WarnIndicateChange { get; set; }

        /**
         @property  public ExtModel ext

         @brief Gets or sets the ext button model reference (reference for model pertaining to event).

         @return    Ext button model reference.
         */
        public ExtModel Ext { get; set; }
    }
}
