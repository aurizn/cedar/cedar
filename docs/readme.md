# User Guides


Select a user guide for your Cedar app use-case. The _Test Configuration User_ Guide contains more technical information about the operation of Cedar, while the _MATB-II Legacy_ guide should be read by those moving from the NASA MATB-II system to Cedar.

| User Type | User Manual |
| --- | --- |
| Test Subject | [Cedar Test Subject User Guide](./Test%20Subject%20User%20Guide.md) |
| MATB-II Legacy User | [Legacy MATB-II Cedar User Guide](./Legacy%20MATB-II%20Cedar%20User%20Guide.md) |
| Test Configuration | [Cedar Test Configuration User Guide](./Test%20Configuration%20User%20Guide.md)<br><br>[User Modifiable Configuration Settings](./User%20Modifiable%20Configuration%20Settings.md)<br><br>[IO Events File Guide](./IO%20Events%20File%20Guide.md)  <br>[IO Config File Guide](./IO%20Config%20File%20Guide.md)<br><br>[IO CEDAR Log File Guide](./IO%20CEDAR%20Log%20File%20Guide.md)  <br>[IO COMMS Log File Guide](./IO%20COMMS%20Log%20File%20Guide.md)  <br>[IO SYSMON Log File Guide](./IO%20SYSMON%20Log%20File%20Guide.md)  <br>[IO RESMAN Log File Guide](./IO%20RESMAN%20Log%20File%20Guide.md) |

### **Download**

| File Sample | Download |
| --- | --- |
| Config and Events files in JSON and XML | [data.zip](media/data.zip) |
| Sample Log files – output from a standard 5 min MATB event run. This sample includes both MATB-II and Cedar output from the same test run. | [log.zip](media/log.zip) |
| Settings JSON output example. This file is auto-generated on any setting change within Cedar – but can be written to before the app is run. | [settings.zip](media/settings.zip) |