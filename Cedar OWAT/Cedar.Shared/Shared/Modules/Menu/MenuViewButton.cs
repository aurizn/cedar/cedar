﻿/*
 * CEDAR Operator Workload Assessment Tool
 * Copyright (C) 2023  aurizn
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using CocosSharp;
using CEDAR.Shared.Modules;
using System;
using System.Collections.Generic;
using System.Text;

namespace CEDAR.Shared.Modules.Menu {

    /**
     @class MenuViewButton
    
     @brief A simple main menu button.
    
     @author    Ryan Beruldsen
     @date  15/09/2016
     */

    class MenuViewButton : CCNode {

        /** @brief The x coordinate. */
        private readonly float _x;

        /** @brief The y coordinate. */
        private readonly float _y;

        /** @brief The width. */
        private readonly float _width;

        /** @brief The height. */
        private readonly float _height;

        /** @brief The minimum button bounds. */
        public CCPoint minButtonBounds;

        /** @brief The maximum button bounds. */
        public CCPoint maxButtonBounds;

        /** @brief Inactive button label. */
        private CCLabel _inactiveLabel;

        /** @brief Active (pressed) button label. */
        private CCLabel _activeLabel;

        /** @brief Button label string. */
        private string _labelStr;

        /** @brief button highlight (dark red, for start of test). */
        private readonly bool _highlight;

        /** @brief Inactive state sprite. */
        private readonly CCSprite _inactive;

        /** @brief Inactive state sprite. */
        private readonly bool _visible;

        /** @brief arrow direction (up/down) if an arrow type button. */
        private readonly ARROW_DIR _arrowDir;

        /** @brief arrow direction (up/down) if an arrow type button (_arrowDir > 0). */
        public static int ARROW_DIR_UP = 1;

        /** @brief arrow direction (up/down) if an arrow type button (_arrowDir > 0). */
        public static int ARROW_DIR_DOWN = 2;

        public enum ARROW_DIR {
            NA,
            UP,
            DOWN
        }

        /** @brief arrow direction (up/down) if an arrow type button. */
        public ARROW_DIR ArrowDir {
            get{
                return _arrowDir;
            }
        }

        /** @brief Active state sprite (pressed)*/
        private readonly CCSprite _active;

        /** @brief The highlight (button down) colour*/
        readonly CCColor4B _highlightColour;

        public float X {
            get {
                return _x;
            }
        }
        public float Y {
            get {
                return _y;
            }
        }

        /**
         @property  public bool active
        
         @brief Gets or sets a button active state (pressed).
        
         @return    true if active, false if not.
         */

        public bool Active {
            get {
                return _active.Visible;
            }
            set {
                if (value) {

                    _inactive.Visible = false;
                    _active.Visible = true;
                } else {
                    _inactive.Visible = true;
                    _active.Visible = false;
                }
            }
        }

        public string Label {
            get {
                return _labelStr;
            }
            set {
                _labelStr = value;
                _activeLabel.Text = _labelStr;
                _inactiveLabel.Text = _labelStr;
            }
        }

        /**
         @fn    public MenuViewButton(string label, CCColor4B highlightColour, float width, float height, float x, float y, bool highlight = false) : base()
        
         @brief Simple menu button constructor
        
         @author    Ryan Beruldsen
         @date  16/09/2016
        
         @param label           Button label.
         @param highlightColour Button highlight colour.
         @param width           The width.
         @param height          The height.
         @param x               The x coordinate.
         @param y               The y coordinate.
         @param highlight       (Optional) Button dark red highlight.
         @param visible         (Optional) If the button is visible.
         */

        public MenuViewButton(string label,
                                CCColor4B highlightColour,
                                float width,
                                float height,
                                float x,
                                float y,
                                bool highlight = false,
                                bool visible = true,
                                ARROW_DIR arrow_dir = ARROW_DIR.NA) : base() {
            
            _height = height;
            _width = width;
            _x = x;
            _y = y;
            _highlightColour = highlightColour;
            _labelStr = label;
            _highlight = highlight;
            _inactive = new CCSprite();
            _active = new CCSprite();
            _visible = visible;
            _arrowDir = arrow_dir;
            Init();
        }

        /**
         @fn    public void Init()
        
         @brief Initialise simple menu button
        
         @author    Ryan Beruldsen
         @date  16/09/2016
         */

        private void Init() {
            CCDrawNode inactiveDrawNode = new CCDrawNode();
            CCDrawNode activeDrawNode = new CCDrawNode();

            //calculate bounding box for touch hit
            minButtonBounds = new CCPoint(_x - _width / 2, _y - _height / 2);
            maxButtonBounds = new CCPoint(minButtonBounds.X + _width, minButtonBounds.Y + _height);

            _inactiveLabel = new CCLabel(_labelStr, "roboto", 14, CCLabelFormat.SpriteFont) {
                Position = new CCPoint(_x, _y),
                Color = _highlight ? CCColor3B.White : CCColor3B.White,
                AnchorPoint = CCPoint.AnchorMiddle
            };
            var rectVerts = new CCPoint[]{
                new CCPoint(_x - _width / 2, _y - _height / 2),
                new CCPoint(_x + _width / 2, _y - _height / 2),
                new CCPoint(_x + _width / 2, _y + _height / 2),
                new CCPoint(_x - _width / 2, _y + _height / 2),
            };
            if(_arrowDir == ARROW_DIR.DOWN){
                rectVerts = new CCPoint[]{
                    new CCPoint(_x, _y - _height / 2),
                    new CCPoint(_x + _width / 2, _y + _height / 2),
                    new CCPoint(_x - _width / 2, _y + _height / 2),
                    new CCPoint(_x, _y - _height / 2),
                };
            }else if (_arrowDir == ARROW_DIR.UP) {
                rectVerts = new CCPoint[]{
                    new CCPoint(_x - _width / 2, _y - _height / 2),
                    new CCPoint(_x + _width / 2, _y - _height / 2),
                    new CCPoint(_x, _y + _height / 2),
                    new CCPoint(_x - _width / 2, _y - _height / 2),
                };
            }
            //if highlight mode is selected, draw a dark red backround to the button
            // otherwise, dark grey
            if (_highlight && _visible) {
                if (_arrowDir != ARROW_DIR.DOWN) {
                    inactiveDrawNode.DrawLine(
                            from: new CCPoint(minButtonBounds.X, minButtonBounds.Y),
                            to: new CCPoint(maxButtonBounds.X, minButtonBounds.Y),
                            lineWidth: 2,
                            color: _highlightColour);
                }
                inactiveDrawNode.DrawPolygon(
                    rectVerts, 4,
                    fillColor: new CCColor4B(100, 40, 40, 40),
                    borderWidth: 0,
                    borderColor: CCColor4B.White);

            } else if (_visible) {
                if (_arrowDir != ARROW_DIR.DOWN) {
                    inactiveDrawNode.DrawLine(
                        from: new CCPoint(minButtonBounds.X, minButtonBounds.Y + 2),
                        to: new CCPoint(maxButtonBounds.X, minButtonBounds.Y + 2),
                        lineWidth: 2,
                        color: _highlightColour);
                }
                inactiveDrawNode.DrawPolygon(
                    rectVerts, 4,
                    fillColor: new CCColor4B(40, 40, 40, 40),
                    borderWidth: 0,
                    borderColor: CCColor4B.White);
            }

            _inactive.AddChild(_inactiveLabel);

            _activeLabel = new CCLabel(_labelStr, "roboto", 14, CCLabelFormat.SpriteFont) {
                Position = new CCPoint(_x, _y),
                Color = CCColor3B.Black,
                AnchorPoint = CCPoint.AnchorMiddle
            };

            if (_visible) {
                activeDrawNode.DrawPolygon(
                rectVerts, 4,
                fillColor: _highlightColour,
                borderWidth: 0,
                borderColor: CCColor4B.White);
            }

            _active.AddChild(activeDrawNode);
            _active.AddChild(_activeLabel);
            _inactive.AddChild(inactiveDrawNode);
            _inactive.AddChild(_inactiveLabel);

            AddChild(_active);
            AddChild(_inactive);
            _inactive.Visible = true;
            _active.Visible = false;

        }
    }
}
