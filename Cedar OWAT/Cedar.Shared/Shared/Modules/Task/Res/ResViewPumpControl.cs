﻿/*
 * CEDAR Operator Workload Assessment Tool
 * Copyright (C) 2023  aurizn
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


using CocosSharp;
using System.Collections.Generic;

namespace CEDAR.Shared.Modules.Task.Res {

    /**
     @class ResViewPumpControl
    
     @brief A pump control component

        The user taps on the drawn pump reprensentation to enable the pump, which is animated to visualise the pumping action.
    
     @author    Ryan Beruldsen
     @date  15/09/2016
     */

    class ResViewPumpControl : DivisionContent, IDivisionContent {

        /** @brief The minor to major tank pump draw constant. */
        public const int PCT_MINOR_TO_MAJOR = 0;

        /** @brief The major to major tank pump draw constant. */
        public const int PCT_MAJOR_TO_MAJOR = 1;

        /** @brief The minor to minor tank pump draw constant. */
        public const int PCT_MINOR_TO_MINOR = 2;

        /** @brief The minor top left. */
        const int MINOR_TOP_LEFT = 1;

        /** @brief The minor bottom right tank location constant. */
        const int MINOR_BOTTOM_RIGHT = 4;

        /** @brief The minor top right tank location constant. */
        const int MINOR_TOP_RIGHT = 5;

        /** @brief The major left tank location constant. */
        const int MAJOR_LEFT = 2;

        /** @brief The major right tank location constant. */
        const int MAJOR_RIGHT = 3;

		const int LINE_WIDTH = 5;

        /** @brief Gauge warning colour (user to show current level during warning state). */
        private readonly CCColor4B _warningColour;

        /** @brief Pump fault state - pump is not active, warning coloured in fault state. */
        private bool _pumpFault;

        /** @brief Selection state of pump - pump is active (active animated, coloured) when selected. */
        private bool _isSelected;

        /**
         @property  public bool pumpFault
        
         @brief Gets or sets pump fault state - pump is not active, warning coloured in fault state.
        
         @return    true if pump fault, false if not.
         */

        public bool PumpFault {
            get {
                return _pumpFault;
            }
            set {
                _pumpFault = value;
                DrawControl();
            }
        }

        /**
         @property  public bool isSelected
        
         @brief Gets or sets selection state of pump - pump is active (active animated, coloured) when selected
        
         @return    true if pump is selected, false if not.
         */

        public bool IsSelected {
            get {
                
                return IsSelected;
            }
            set {
                _isSelected = value;
                DrawControl();
            }
        }

        /** @brief Gauge active text colour (used to show current levels). */
        private readonly CCColor4B _activeColour;


        /** @brief Type of the pump control (PCT, see constants). */
        private readonly int _pumpControlType;

        /** @brief List of resource gauges (for positioning pump control graphics). */
        private readonly List<ResViewGauge> _resGaugeList;

        /** @brief Target A of pump (from). */
        private readonly int _targetA;

        /** @brief Target B of pump (to). */
        private readonly int _targetB;

        /** @brief The main draw node. */
        private readonly CCDrawNode _drawNode;

        /** @brief The minimum button bounds. */
        public CCPoint minButtonBounds;

        /** @brief The maximum button bounds. */
        public CCPoint maxButtonBounds;

        /** @brief Number of pumps, for animation. */
        public int pumpCount;

        /** @brief true if initialised, distinguishing initial- and re-draw. */
        private bool _initialised;

        /**
         @fn    public ResViewPumpControl( Division division, int pumpControlType, List<ResViewGauge> resGaugeList, CCColor4B activeColour, CCColor4B warningColour, int targetA, int targetB, bool pumpFault = false, bool isSelected = false) : base(division)
        
         @brief Constructor for resource tank pump control.
        
         @author    Ryan Beruldsen
         @date  21/09/2016
        
         @param division        The parent division.
         @param pumpControlType The type of the pump control PCT (see static constants).
         @param resGaugeList    List of resource gauges.
         @param activeColour    The active colour.
         @param warningColour   The warning colour.
         @param targetA         Pump tank target A (from).
         @param targetB         Pump tank target B (to).
         @param pumpFault       (Optional) True for initial pump fault state.
         @param isSelected      (Optional) True for initial pump selected state.
         */

        public ResViewPumpControl(  Division division, 
                                    int pumpControlType, 
                                    List<ResViewGauge> resGaugeList,
                                    CCColor4B activeColour,
                                    CCColor4B warningColour,
                                    int targetA, 
                                    int targetB,
                                    bool pumpFault = false,
                                    bool isSelected = false) : base(division) {

            _warningColour = warningColour;
            _activeColour = activeColour;
            _pumpFault = pumpFault;
            _isSelected = isSelected;
            _drawNode = new CCDrawNode();
            _initialised = false;
            _resGaugeList = resGaugeList;
            _pumpControlType = pumpControlType;
            _targetA = targetA;
            _targetB = targetB;
            pumpCount = 0;
        }

        /**
         @fn    public void Init()
        
         @brief Initialise, draw resource tank pump control.
        
         @author    Ryan Beruldsen
         @date  21/09/2016
         */

        public void Init() {
            DrawControl();
        }

        /**
         @fn    public void DrawControl()
        
         @brief Draw pump control based on pump control type (PCT), see static constants.
        
         @author    Ryan Beruldsen
         @date  21/09/2016
         */

        public void DrawControl() {
            if (_pumpControlType == PCT_MINOR_TO_MAJOR) MinorToMajor();
            if (_pumpControlType == PCT_MINOR_TO_MINOR) MinorToMinor();
            if (_pumpControlType == PCT_MAJOR_TO_MAJOR) MajorToMajor();
        }

        /**
         @fn    private void MinorToMajor()
        
         @brief Draw, animate minor to major pump type.
        
         @author    Ryan Beruldsen
         @date  21/09/2016
         */

        private void MinorToMajor() {

            float startX = 0;
            float startY = 0;
            float lineXTransform = 0;
            float endX = 0;
            float endY = 0;

            //if the pump is drawn to the right
            bool right = _targetA == MINOR_TOP_RIGHT || _targetA == MINOR_BOTTOM_RIGHT;

            //if the pump is stacked top or bottom vertically
            bool top = (_targetA == MINOR_TOP_RIGHT || _targetA == MINOR_TOP_LEFT);

            startX = right ? _resGaugeList[_targetA].minButtonBounds.X : 
                _resGaugeList[_targetA].maxButtonBounds.X;

            startY = _resGaugeList[_targetA].minButtonBounds.Y + (
                        _resGaugeList[_targetA].maxButtonBounds.Y - 
                        _resGaugeList[_targetA].minButtonBounds.Y
            ) / 2;

            lineXTransform = (
                (_targetB == MAJOR_LEFT ? 
                    _resGaugeList[_targetB].minButtonBounds.X : 
                    _resGaugeList[_targetB].maxButtonBounds.X) - startX
            ) / 2;

            endX = _targetB == MAJOR_LEFT ? 
                    _resGaugeList[_targetB].minButtonBounds.X : 
                    _resGaugeList[_targetB].maxButtonBounds.X;

            endY = _resGaugeList[_targetB].minButtonBounds.Y + 
                        (_resGaugeList[_targetB].maxButtonBounds.Y - 
                        _resGaugeList[_targetB].minButtonBounds.Y) / 
                        (2 + (top ? -0.2f : 0.2f));

            minButtonBounds = new CCPoint(  x: right ? endX : startX, 
                                            y: _resGaugeList[_targetA].minButtonBounds.Y);
            maxButtonBounds = new CCPoint(  x: right ? startX : endX, 
                                            y: _resGaugeList[_targetA].maxButtonBounds.Y);

            //instantiate pump direction info label if initial draw
            if (!_initialised) {
                CCLabel pumpInfoLabel = new CCLabel(_targetB == MAJOR_LEFT ? ">" : "<", "roboto", 12) {
                    Position = new CCPoint(startX + (endX - startX) * 0.7f, endY),
                    Color = CCColor3B.White,
                    AnchorPoint = CCPoint.AnchorLowerRight
                };

                AddChild(_drawNode);
                AddChild(pumpInfoLabel);
                _initialised = true;
            }

            CCPoint[] vertices = new CCPoint[] {
                        new CCPoint(startX,startY),
                        new CCPoint(startX + lineXTransform,startY),

                        new CCPoint(startX + lineXTransform, startY),
                        new CCPoint(startX + lineXTransform, endY),

                        new CCPoint(startX + lineXTransform, endY),
                        new CCPoint(endX, endY),
            };

            //draw line path with calculated verticies
            DrawPumpPath(_pumpFault, _isSelected, vertices);

        }

        /**
         @fn    private void MinorToMajor()
        
         @brief Draw, animate major to major pump type.
        
         @author    Ryan Beruldsen
         @date  21/09/2016
         */

        private void MajorToMajor() {
            float startX = 0;
            float startY = 0;
            float endX = 0;
            float endY = 0;

            //if the pump is drawn to the right
            bool right = _targetA == MAJOR_RIGHT;

            startX = right ?    _resGaugeList[MAJOR_RIGHT].minButtonBounds.X : 
                                _resGaugeList[MAJOR_LEFT].maxButtonBounds.X;
            if (right) {
                startY =    _resGaugeList[MINOR_TOP_RIGHT].minButtonBounds.Y + (
                                _resGaugeList[MINOR_TOP_RIGHT].maxButtonBounds.Y - 
                                _resGaugeList[MINOR_TOP_RIGHT].minButtonBounds.Y
                            ) / 2f;
            }
            else {
                startY = _resGaugeList[MINOR_BOTTOM_RIGHT].minButtonBounds.Y + (
                            _resGaugeList[MINOR_BOTTOM_RIGHT].maxButtonBounds.Y - 
                            _resGaugeList[MINOR_BOTTOM_RIGHT].minButtonBounds.Y
                        ) / 2f;
            }

            startX = right ? 
                        _resGaugeList[MAJOR_LEFT].maxButtonBounds.X : 
                        _resGaugeList[MAJOR_RIGHT].minButtonBounds.X;

            endY = startY;
            endX = _targetB == 
                MAJOR_RIGHT ?   _resGaugeList[MAJOR_LEFT].maxButtonBounds.X : 
                                _resGaugeList[MAJOR_RIGHT].minButtonBounds.X;

            minButtonBounds = new CCPoint(right ? startX : endX, endY - _division.Height * 0.1f);
            maxButtonBounds = new CCPoint(right ? endX : startX, endY + _division.Height * 0.2f);

            //instantiate pump direction info label if initial draw
            if (!_initialised) {

                CCLabel pumpInfoLabel = new CCLabel(_targetA == MAJOR_LEFT ? ">" : "<", "roboto", 12) {
                    Position = new CCPoint(startX + (endX - startX) * 0.5f, endY),
                    Color = CCColor3B.White,
                    AnchorPoint = CCPoint.AnchorLowerRight
                };

                AddChild(pumpInfoLabel);
                AddChild(_drawNode);
                _initialised = true;
            }

            CCPoint[] vertices = new CCPoint[] {
                        new CCPoint(startX, startY),
                        new CCPoint(endX, endY)
            };
            DrawPumpPath(_pumpFault, _isSelected, vertices);
        }

        /**
         @fn    private void MinorToMinor()
        
         @brief Draw, animate major to major pump type.
        
         @author    Ryan Beruldsen
         @date  21/09/2016
         */

        private void MinorToMinor() {
            float startX = 0;
            float startY = 0;

            float lineXTransform = 0;

            float endX = 0;
            float endY = 0;

            //if the pump is drawn to the right
            bool right = _targetB == MINOR_TOP_RIGHT || _targetB == MINOR_BOTTOM_RIGHT;

            startX = right ? 
                        _resGaugeList[_targetA].maxButtonBounds.X : 
                        _resGaugeList[_targetA].minButtonBounds.X;

            startY =    _resGaugeList[_targetA].minButtonBounds.Y + (
                            _resGaugeList[_targetA].maxButtonBounds.Y - 
                            _resGaugeList[_targetA].minButtonBounds.Y
            ) / 2;

            if (right) {
                lineXTransform = ((
                    _division.maxButtonBounds.X - 
                    _resGaugeList[_targetB].maxButtonBounds.X
                ) / 2f);
            }
            else {
                lineXTransform = _resGaugeList[_targetB].minButtonBounds.X - (
                    _resGaugeList[_targetB].minButtonBounds.X - _division.minButtonBounds.X
                ) / 2;
                lineXTransform = -lineXTransform;
            }

            endX = startX;
            endY = _resGaugeList[_targetB].minButtonBounds.Y + (
                _resGaugeList[_targetB].maxButtonBounds.Y - _resGaugeList[_targetB].minButtonBounds.Y
            ) / 2;

            float maxY = _resGaugeList[_targetA].maxButtonBounds.Y > _resGaugeList[_targetB].maxButtonBounds.Y ? 
                _resGaugeList[_targetA].maxButtonBounds.Y : _resGaugeList[_targetB].maxButtonBounds.Y;
            float minY = _resGaugeList[_targetA].minButtonBounds.Y < _resGaugeList[_targetB].minButtonBounds.Y ? 
                _resGaugeList[_targetA].minButtonBounds.Y : _resGaugeList[_targetB].maxButtonBounds.Y;

            minButtonBounds = new CCPoint(right ? endX : startX + lineXTransform * 2, minY);
            maxButtonBounds = new CCPoint(right ? startX + lineXTransform * 2 : endX, maxY);

            //instantiate pump direction info label if initial draw
            if (!_initialised) {
                CCLabel pumpInfoLabel = new CCLabel("^", "roboto", 12) {
                    Position = new CCPoint(right ? endX + 40: endX - 40, startY - (startY - endY) / 3f),
                    Color = CCColor3B.White,
                    AnchorPoint = right ? CCPoint.AnchorLowerLeft : CCPoint.AnchorLowerRight
                };
                AddChild(_drawNode);
                AddChild(pumpInfoLabel);
                _initialised = true;
            }

            CCPoint[] vertices = new CCPoint[] {
                        new CCPoint(startX, startY),
                        new CCPoint(startX + lineXTransform, startY),
                        new CCPoint(startX + lineXTransform, endY),
                        new CCPoint(endX, endY)
            };
            DrawPumpPath(_pumpFault, _isSelected, vertices);

        }

        /**
         @fn    private void DrawPumpPath(bool pumpFault, bool isSelected, CCPoint[] vertices)

         @brief Draw, animate pump path with given vertices, fault and selection state.

         @author    Ryan Beruldsen
         @date  21/09/2016

         @param pumpFault   Pump fault state.
         @param isSelected  Pump selection state.
         @param vertices    Pump line path vertices.
         */

        private void DrawPumpPath(bool pumpFault, bool isSelected, CCPoint[] vertices) {
            //selected pump draw, animate
            if (!pumpFault && isSelected) {
				int animationLength = vertices.Length;

				if (animationLength <= 2) {
					animationLength = 3;
				}

                //count is incremented to be used to direct animation frame
                pumpCount = (pumpCount + 1) % animationLength;


				_drawNode.Clear();
                for (int i = 0; i < vertices.Length; i++) {
					bool colourToggle = (pumpCount == i || pumpCount == i - 1);

					if (vertices.Length <= 2){
						colourToggle = pumpCount % 2 == 0;
					}

                    //alternate colours active/white based on pumpCount animation frame
                    if (i != 0) {
                        _drawNode.DrawLine(
                            from: vertices[i - 1], 
                            to: vertices[i], 
                            lineWidth: LINE_WIDTH, 
                            color: colourToggle ?
                                _activeColour : CCColor4B.White, 
                            lineCap: CCLineCap.Round);
                    }
                    if (i != vertices.Length - 1) {
                        _drawNode.DrawLine(
                            from: vertices[i], 
                            to: vertices[i + 1], 
                            lineWidth: LINE_WIDTH, 
                            color: colourToggle ? 
                                _activeColour : CCColor4B.White, 
                            lineCap: CCLineCap.Round
                        );
                    }
                }

            //fault or unselected static draw
            } else {
                _drawNode.Clear();
                for (int i = 0; i < vertices.Length; i++) {
                    if (i != 0) {
                        _drawNode.DrawLine(
                            from: vertices[i - 1], 
                            to: vertices[i], 
                            lineWidth: LINE_WIDTH, 
                            color: pumpFault ? 
                                _warningColour : CCColor4B.Gray, 
                            lineCap: CCLineCap.Round);
                    }
                    if (i != vertices.Length - 1) {
                        _drawNode.DrawLine(
                            from: vertices[i], 
                            to: vertices[i + 1], 
                            lineWidth: LINE_WIDTH, 
                            color: pumpFault ? 
                                _warningColour : CCColor4B.Gray, 
                            lineCap: CCLineCap.Round);
                    }
                }
            }
        }

    }
}
