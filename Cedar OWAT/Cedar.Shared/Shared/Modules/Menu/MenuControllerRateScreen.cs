﻿/*
 * CEDAR Operator Workload Assessment Tool
 * Copyright (C) 2023  aurizn
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using CocosSharp;
using System;
using System.Collections.Generic;
using System.Text;
using CEDAR.Shared;
using CEDAR.Shared.CedarCollection;

/*! \namespace CEDAR.Shared.Modules.Menu
 * @brief Cedar UI menu content
 * 
 */

namespace CEDAR.Shared.Modules.Menu {

    /**
     @class MenuControllerRateScreen
    
     @brief Workload rating screen.
        
        - Workload Rating Scale (WRD) based on the NASA-TLX (Hart & Staveland, 1988)
    
     @author    Ryan Beruldsen
     @date  14/09/2016
     */

    class MenuControllerRateScreen : CCNode {

        /** @brief Number of workload ratings metrics. */
        private const int _NUM_WR = 6;

        /** @brief Internal draw node. */
        private readonly CCDrawNode _drawNode;

        /** @brief Internal window bounds. */
        private CCPoint _windowMax;

        /** @brief Back button. */
        private MenuViewButton _backButton;

        /** @brief Listener for button touch events. */
        private readonly CCEventListenerTouchAllAtOnce _touchListener;

        /** @brief The start coordinates of a touch or drag user input. */
        private CCPoint _startTouch = new CCPoint(-1, -1);

        /** @brief The initial coordinates of the frequency control on the beginnnig of a user input. */
        private CCPoint _freqControlStart = new CCPoint(-1, -1);

        /** @brief Draging state of slider. */
        private int _freqControlDraggingIndex = -1;

        /** @brief List of select sliders to input workload rating. */
        private readonly List<Task.Comms.CommsViewSelectSlider> _sliderList;

        /** @brief List of list of ratings in percent (0.0-100.0). */
        private readonly List<Double> _rateList;

        /** @brief View initialised. */
        private bool isInit = false;

        /**
         @fn    public MenuControllerRateScreen(CCPoint windowMax) : base()
        
         @brief Workload rating screen constructor.

            - Workload Rating Scale (WRD) based on the NASA-TLX (Hart & Staveland, 1988)
        
         @author    Ryan Beruldsen
         @date  16/09/2016
        
         @param windowMax   Window bounds (max X and Y coordinates)
         */

        public MenuControllerRateScreen(CCPoint windowMax) : base() {
            this._windowMax = new CCPoint(windowMax.X, windowMax.Y);
            _drawNode = new CCDrawNode();

            //select sliders to input rating of 1-100/good-bad
            _sliderList = new List<Task.Comms.CommsViewSelectSlider>();

            //listen for touch events
            _touchListener = new CCEventListenerTouchAllAtOnce {
                OnTouchesBegan = HandleInputStart,
                OnTouchesEnded = HandleInputEnd,
                OnTouchesMoved = HandleInputDrag
            };
            AddEventListener(_touchListener, this);

            //list of ratings in percent (0.0-100.0) (in event row order, see below)
            _rateList = new List<double>();

            //setup log file header if not already existing
            if (!ModelDataIOLog.RateScreenInit) {
                int timeout = ModelDataIOConfig.Instance.GetConfigNum("timeout", "rate");
                var eventRow = new CDRLogRow() {
                    "-TIME-NOW",
                    "-TIME-",
                    "-MENL-",
                    "-PHYS-",
                    "-TEMP-",
                    "-PERF-",
                    "-EFFT-",
                    "-FRUS-",
                    "-MEAN-",
                    "-REMARKS-",
                };

                string header = "# Timeout (in seconds):  = " + timeout + "\n" +
                                "#";
                ModelDataIOLog.Instance.LogTaskHeader("rate", header);
                ModelDataIOLog.Instance.LogTaskEvent("rate", eventRow);
                ModelDataIOLog.RateScreenInit = true;
            }

            Init();
        }

        /**
         @fn    public void Init()
        
         @brief Initialise workload rating page.
        
         @author    Ryan Beruldsen
         @date  16/09/2016
         */

        private void Init() {
            for (int i = 0; i < _NUM_WR; i++) {
                if (i == 4) {
                    _rateList.Add(1);
                } else {
                    _rateList.Add(0);
                }
            }
            _drawNode.DrawRect(new CCRect(0, 0, _windowMax.X, _windowMax.Y),
                   fillColor: new CCColor4B(0, 0, 0, 220),
                   borderWidth: 0,
                   borderColor: CCColor4B.White);

            //back to main menu button
            AddChild(_drawNode);
            _backButton = new MenuViewButton(
                label: "< Submit and Return",
                highlightColour:
                    ModelData.Instance.GetColourPallete4B(ModelData.COLOUR_PARAM_ACTIVE),
                width: _windowMax.X * 0.3f,
                height: _windowMax.Y * 0.04f,
                x: _windowMax.X * 0.18f,
                y: _windowMax.Y * 0.95f);


            CCLabel title = new CCLabel("Workload Rating Scale", "robotob", 14, CCLabelFormat.SpriteFont) {
                Position = new CCPoint(_windowMax.X / 2, _windowMax.Y * 0.90f),
                Color = CCColor3B.White
            };
            AddChild(title);

            DrawWorkloadSliders();

            AddChild(_backButton);
            isInit = true;
        }

        /**
         @fn    private void DrawWorkloadSliders()
        
         @brief Draw workload rating select sliders.
        
         @author    Ryan Beruldsen
         @date  5/10/2016
         */

        private void DrawWorkloadSliders() {
            //list of workload rating input titles
            string[] wrs = {    "Mental Demand",
                                "Physical Demand",
                                "Temporal Demand",
                                "Performance",
                                "Effort",
                                "Frustration" };

            //effort input reference - effort has an inverted scale compared to other
            //  inputs, from good - bad; as opposed to min-max
            int EFFORT_WORKLOAD = 4;

            //draw the six workload rating slider scales
            for (int i = 1; i <= _NUM_WR; i++) {

                //create reference division for CommsViewSelectSlider
                var division = new Division(
                    width: _windowMax.X,
                    height: _windowMax.Y / 7,
                    x: 0,
                    y: (_windowMax.Y / 7) * i,
                    prevDivisionY: (_windowMax.Y / 7) * (i + 1),
                    windowMax: _windowMax
                );
                //draw slider labels
                CCLabel sliderTitle = new CCLabel(wrs[i - 1], "robotob", 14, CCLabelFormat.SpriteFont) {
                    Position = new CCPoint(division.Width / 2, division.Y),
                    Color = CCColor3B.White
                };
                CCLabel sliderMin = new CCLabel(i == 4 ? "Good" : "Low",
                                                "roboto",
                                                14,
                                                CCLabelFormat.SpriteFont
                ) {
                    Position = new CCPoint(division.Width * 0.05f, division.Y),
                    AnchorPoint = CCPoint.AnchorUpperLeft,
                    Color = CCColor3B.White
                };
                CCLabel sliderMax = new CCLabel(i == 4 ? "Poor" : "High",
                                                "roboto",
                                                14,
                                                CCLabelFormat.SpriteFont
                ) {
                    Position = new CCPoint(division.Width * 0.95f, division.Y),
                    AnchorPoint = CCPoint.AnchorUpperRight,
                    Color = CCColor3B.White
                };
                AddChild(sliderTitle);
                AddChild(sliderMin);
                AddChild(sliderMax);

                //instantiate CommsViewSelectSlider with custom division reference
                // and properties specific to the workload rating context
                var slider = new Task.Comms.CommsViewSelectSlider(
                    division: division,
                    x: division.Width * 0.05f,
                    y: division.PrevDivisionY - division.Height * 0.8f,
                    width: division.Width * 0.9f,
                    height: division.Height * 0.4f,
                    colourRange:
                        ModelData.Instance.GetColourPallete4B(ModelData.COLOUR_PARAM_RANGE),
                    colourWarning:
                        ModelData.Instance.GetColourPallete4B(ModelData.COLOUR_PARAM_WARNING),
                    valueLabel: true
                );
                slider.Init();

                //default 
                if (i == EFFORT_WORKLOAD) {
                    slider.SetSliderPerc(1, false);
                }
                _sliderList.Add(slider);
                AddChild(slider);
            }
        }

        /**
         @fn    private void HandleInputEnd(System.Collections.Generic.List<CCTouch> touches, CCEvent touchEvent)
        
         @brief Handles touch input end
        
         @author    Ryan Beruldsen
         @date  16/09/2016
        
         @param touches     CocosSharp injected touch list
         @param touchEvent  CocosSharp injected touch event
         */

        private void HandleInputEnd(System.Collections.Generic.List<CCTouch> touches, CCEvent touchEvent) {
            if (touches.Count > 0 && isInit) {
                //on back button press, return to main menu (page string: "title")
                if (_backButton.Active && MenuButtonHitTest(touches, _backButton)) {

                    //calculate mean of workload rating metrics
                    double mean = 0;
                    for (int i = 0; i < _sliderList.Count; i++) {
                        mean += (_sliderList[i].GetSliderPerc() * 100);
                    }
                    mean /= _sliderList.Count;
                    double tenth = ModelData.Instance.CurrentSessionPauseTenth() / 10;
                    double timeLeft = ModelData.Instance.DurationPause + tenth;
                    double timeout = ModelDataIOConfig.Instance.GetConfigNum("timeout", "rate");
                    //write workload ratings results (submit) to log
                    var eventRow = new CDRLogRow() {
                        ModelData.Instance.DurationStr24Hr,
                        (timeout - timeLeft).ToString("00.0"),
                        (_sliderList[0].GetSliderPerc()*100).ToString("000"),
                        (_sliderList[1].GetSliderPerc()*100).ToString("000"),
                        (_sliderList[2].GetSliderPerc()*100).ToString("000"),
                        (_sliderList[3].GetSliderPerc()*100).ToString("000"),
                        (_sliderList[4].GetSliderPerc()*100).ToString("000"),
                        (_sliderList[5].GetSliderPerc()*100).ToString("000"),
                        mean.ToString("000.00"),
                        "",
                    };
                    ModelDataIOLog.Instance.LogTaskEvent("rate", eventRow);
                    ModelDataIOLog.Instance.LogEvent("Workload Rating", true);

                    //return to test
                    ModelData.CurrentPageStr = "test_return";
                }

                _backButton.Active = false;
            }
        }

        /**
         @fn    private void MenuButtonHitTest(System.Collections.Generic.List<CCTouch> touches, MenuViewButton m)
        
         @brief Hit test for menu button.
        
         @author    Ryan Beruldsen
         @date  16/09/2016
        
         @param touches CocosSharp injected touch list (from touch handler)
         @param m       The MenuViewButton to test for bounding box hit.
        
         @return    True if within button bounds, otherwise false
         */

        private bool MenuButtonHitTest(System.Collections.Generic.List<CCTouch> touches,
                                        MenuViewButton m) {

            if (touches[0].Location.X > m.minButtonBounds.X &&
                touches[0].Location.X < m.maxButtonBounds.X &&
                touches[0].Location.Y > m.minButtonBounds.Y &&
                touches[0].Location.Y < m.maxButtonBounds.Y) {
                return true;
            }
            return false;
        }

        /**
         @fn    private void HandleInputStart(System.Collections.Generic.List<CCTouch> touches, CCEvent touchEvent)
        
         @brief Handles touch input
        
         @author    Ryan Beruldsen
         @date  16/09/2016
        
         @param touches     CocosSharp injected touch list
         @param touchEvent  CocosSharp injected touch event
         */

        private void HandleInputStart(System.Collections.Generic.List<CCTouch> touches,
                                        CCEvent touchEvent) {
            if (touches.Count > 0) {
                _startTouch.X = touches[0].Location.X;
                _startTouch.Y = touches[0].Location.Y;
                bool sliderDragging = false;
                for (int i = 0; i < _sliderList.Count; i++) {
                    var slider = _sliderList[i];
                    if (touches[0].Location.X > slider.freqControl.X &&
                        touches[0].Location.X < slider.freqControl.X + slider.FreqControlWidth &&
                        touches[0].Location.Y > slider.FreqControlMin.Y &&
                        touches[0].Location.Y < slider.FreqControlMax.Y) {

                        _freqControlDraggingIndex = i;
                        slider.Dragging = true;
                        sliderDragging = true;
                        _freqControlStart.X = slider.freqControl.X;
                    } else {
                        slider.Dragging = false;
                    }
                }

                if (!sliderDragging) {
                    _freqControlDraggingIndex = -1;
                }

                //test for back buton hit
                if (MenuButtonHitTest(touches, _backButton)) {
                    _backButton.Active = true;

                }
            }
        }

        /**
		@fn    private void HandleInputDrag(System.Collections.Generic.List<CCTouch> touches, CCEvent touchEvent)

		@brief Handles drag event, handles slider drag.

		@author    Ryan Beruldsen
		@date  16/09/2016

		@param touches     CocosSharp injected touch list
		@param touchEvent  CocosSharp injected touch event
		*/
        private void HandleInputDrag(System.Collections.Generic.List<CCTouch> touches,
                                        CCEvent touchEvent) {

            if (touches.Count > 0 && _freqControlDraggingIndex >= 0) {
                var slider = _sliderList[_freqControlDraggingIndex];

                //while touch input remains within dragging bounds 
                //(slightly larger than slider rectangle bounds
                // for some ease of use allowance)
                if (touches[0].Location.Y > slider.FreqControlMin.Y * 0.75 &&
                    touches[0].Location.Y < slider.FreqControlMax.Y * 1.25) {

                    //ensure sliders remain within bounds
                    slider.freqControl.X = _freqControlStart.X + (touches[0].Location.X - _startTouch.X);
                    if (slider.freqControl.X > slider.FreqControlMax.X) {
                        slider.freqControl.X = slider.FreqControlMax.X;
                    }
                    if (slider.freqControl.X < slider.FreqControlMin.X) {
                        slider.freqControl.X = slider.FreqControlMin.X;
                    }
                    //if within index bounds
                    if (_rateList.Count > _freqControlDraggingIndex) {
                        //set slider position
                        bool sliderWithinBounds = (slider.freqControl.X - slider.FreqControlMin.X) != 0;
                        float sliderPercCalc = (slider.freqControl.X - slider.FreqControlMin.X) /
                                                (slider.FreqControlMax.X - slider.FreqControlMin.X);

                        slider.SetSliderPerc(sliderWithinBounds ? sliderPercCalc : 0, false);

                        //record new user workload rating input
                        _rateList[_freqControlDraggingIndex] = slider.GetSliderPerc();
                    }
                }
            }
        }
    }
}

