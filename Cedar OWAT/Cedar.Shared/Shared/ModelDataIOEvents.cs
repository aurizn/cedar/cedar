﻿/*
 * CEDAR Operator Workload Assessment Tool
 * Copyright (C) 2023  aurizn
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using CEDAR.Shared.CedarCollection;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Xml;

namespace CEDAR.Shared {

    /**
     @class ModelDataIOEvents
    
     @brief The I/O event file data handler. 
        
        - This represents the MATB_EVENTS.xml files used by MATB.
    
     @author    Ryan Beruldsen
     @date  6/10/2016
     */

    public sealed class ModelDataIOEvents {


        /** @brief Singleton I/O event data instance. */
        internal static readonly ModelDataIOEvents _instance =
            new ModelDataIOEvents();


        /** @brief true to parse XML next tag 
        
            - this is needed to ensure multiple events can occur at the same time
            - events are stored as a list of events to a timestamp hash key. */
        private bool _parseXMLNextTag = false;

        /**
         @fn    private ModelDataIOEvents()
        
         @brief Singleton I/O event data constructor (stub). 
        
         @author    Ryan Beruldsen
         @date  6/10/2016
         */

        private ModelDataIOEvents() { }


        /** @brief Dictionary of IO event data. */
        private CDRIOEvents _ioDataDict = new CDRIOEvents();


        /**
         @property  public static ModelDataIOConfig instance
        
         @brief Gets the singleton event data instance.
        
         @return    The instance.
         */

        public static ModelDataIOEvents Instance {
            get {
                return _instance;
            }
        }

        /**
         @property  public CDRIOEvents xmlDataDict
        
         @brief Gets dictionary of IO event data.
        
         @return    Dictionary of IO event data.
         */

        public CDRIOEvents XmlDataDict {
            get {
                return _ioDataDict;
            }
        }

        /**
         @fn    public void ParseJSONFile(string filename)
        
         @brief Parse JSON events file.
        
         @author    Ryan Beruldsen
         @date  6/10/2016
        
         @param filename    Filename of the file.
         */

        public void ParseJSONFile(string filename) {
            //reset data hashmap
            _ioDataDict = new CDRIOEvents();

            //record loading of events
            ModelData.CEDAREvents = true;

            JsonTextReader reader = new JsonTextReader(
                new StringReader(File.ReadAllText(filename)));

            //maintain JSON nodes during ennumeration
            string currentNode = "";
            string currentSubNode = "";
            string currentSubSubNode = "";

            //key/value accumulate until a top level key is declared
            // - this is the last entry
            // - when the key is declare all accumulated data can be stored
            var nodeValDict = new Dictionary<string, string>();

            //this is the top level key when declate
            bool startTime = false;
            int level = 0;
            while (reader.Read()) {
                //maintain iteration level
                if (reader.TokenType == JsonToken.StartObject) {
                    level++;
                }
                else if (reader.TokenType == JsonToken.EndObject) {
                    if (level == 3) {
                        currentSubNode = "";
                    }
                    level--;

                    //set second level node
                }
                else if (reader.TokenType == JsonToken.PropertyName &&
                            level == 3 && !reader.Value.ToString().Equals("_startTime")) {
                    currentNode = reader.Value.ToString();
                    startTime = false;

                    //next value will be top level node
                }
                else if (reader.TokenType == JsonToken.PropertyName &&
                            reader.Value.ToString().Equals("_startTime")) {
                    startTime = true;

                    //set third level node
                }
                else if (reader.TokenType == JsonToken.String && level == 3 &&
                            !startTime) {
                    currentSubNode = reader.Value.ToString();

                    //set fourth level key
                }
                else if (reader.TokenType == JsonToken.PropertyName &&
                            level == 4 && !startTime) {
                    currentSubSubNode = reader.Value.ToString();
                    //set fourth level value
                }
                else if (reader.TokenType == JsonToken.String &&
                            level == 4 && !startTime && !currentSubSubNode.Equals("")) {
                    nodeValDict[currentSubSubNode] = reader.Value.ToString();
                    currentSubSubNode = "";

                    //if data is fourth level (contains 3 keys, one value)
                }
                else if (reader.TokenType == JsonToken.String &&
                            level == 3 && startTime && currentSubNode.Equals("")) {
                    string timestamp = reader.Value.ToString();
                    foreach (KeyValuePair<string, string> val in nodeValDict) {
                        string valKey = val.Key;
                        if (val.Key[0] == '_') {
                            valKey = valKey.Substring(1, valKey.Length - 1);
                        }

                        ModelDataIOEvents.Instance.ParseIOEvent(
                            timestamp, currentNode, valKey, val.Value);
                    }
                    nodeValDict = new Dictionary<string, string>();

                    //if data is third level (contains 2 keys, one value)
                }
                else if (reader.TokenType == JsonToken.String &&
                    level == 3 && startTime && !currentSubNode.Equals("")) {
                    ModelDataIOEvents.Instance.ParseIOEvent(
                        reader.Value.ToString(), "-", currentNode, currentSubNode);
                    startTime = false;
                    //ignore unknown ennumerations
                }
                else {
                    startTime = false;
                }
            }
        }

        /**
         @fn    public void ParseXMLFile(string filename)
        
         @brief Parse events XML file.
        
         @author    Ryan Beruldsen
         @date  6/10/2016
        
         @param filename    Filename of the file.
         */

        public void ParseEventsXMLFile(string filename) {
            //reset data hashmap
            _ioDataDict = new CDRIOEvents();

            //record loading of events
            ModelData.CEDAREvents = true;

            XmlTextReader reader = new XmlTextReader(filename);

            //ennumeration data
            string currentNodeValue = "";
            string currentNode = "";
            string currentSubNode = "";
            string currentSubSubNode = "";

            //XML ennumeration level
            int level = 0;

            while (reader.Read()) {
                //set current node, value
                if (reader.NodeType == XmlNodeType.Element) {
                    if (currentNode.Equals("") && level == 1 && reader.HasAttributes) {
                        currentNodeValue = reader[0];
                        currentNode = reader.Name;
                    }
                    else if (currentSubNode.Equals("") && level == 2) {
                        currentSubNode = reader.Name;
                    }
                    else {
                        currentSubSubNode = reader.Name;
                    }
                    level++;
                }
                //if data is fourth level (contains 3 keys, one value)
                if (reader.NodeType == XmlNodeType.Text &&
                    !currentNode.Equals("") && !currentSubNode.Equals("") &&
                    !currentSubSubNode.Equals("") && level == 4) {
                    ModelDataIOEvents.Instance.ParseIOEvent(
                        currentNodeValue, currentSubNode, currentSubSubNode, reader.Value);

                    //if data is third level (contains 2 keys, one value)
                }
                else if (reader.NodeType == XmlNodeType.Text && level == 3) {
                    ModelDataIOEvents.Instance.ParseIOEvent(
                        currentNodeValue, "-", currentSubNode, reader.Value);
                }

                //manage end elements and ennumeration level
                // detect if data is third or fourth level
                if (reader.NodeType == XmlNodeType.EndElement) {
                    level--;

                    if (reader.Name.Equals(currentNode)) {
                        currentNode = "";

                        //allow events at the same timestamp by forcing a next tag
                    }
                    else if (reader.Name.Equals(currentSubNode)) {
                        ModelDataIOEvents.Instance.ParseXMLNextTag();
                        currentSubNode = "";
                    }

                }
            }
        }

        /**
         @fn    public void PrintIOEvents ()
        
         @brief Print XML events to command line.
        
         @author    Ryan Beruldsen
         @date  6/10/2016
         */

        public void PrintIOEvents() {
            var events = _ioDataDict;
            foreach (KeyValuePair<string, CDRTaskEventTimeGroup> timeStamp in events) {
                foreach (KeyValuePair<string, CDRTaskEventParamsTimeGroup> eventList
                    in events[timeStamp.Key]) {
                    foreach (CDRTaskEventParams eventDataDict in
                        events[timeStamp.Key][eventList.Key]) {
                        foreach (KeyValuePair<string, string> eventData in
                            eventDataDict) {
                            System.Diagnostics.Debug.WriteLine(
                                timeStamp.Key + " " + eventList.Key +
                                " " + eventData.Key + " " + eventData.Value);
                        }
                    }
                }
            }
        }

        /**
         @fn    public CDRTaskEventTimeGroup GetEvent(string startTime)
        
         @brief Gets an CDRTaskEventTimeGroup given a start time string.
        
         @author    Ryan Beruldsen
         @date  6/10/2016
        
         @param startTime   The start time in format 00:00:00.
        
         @return    The event group.
         */

        public CDRTaskEventTimeGroup GetEvent(string startTime) {
            if (_ioDataDict.ContainsKey(startTime)) {
                return _ioDataDict[startTime];
            }
            return new CDRTaskEventTimeGroup();
        }

        /**
         @fn    public CDRTaskEventParamsTimeGroup GetEvent(string startTime, string eventType)
        
         @brief Gets an CDRTaskEventParamsTimeGroup given a start time string and an event type.
        
         @author    Ryan Beruldsen
         @date  6/10/2016
        
         @param startTime   The start time in format 00:00:00.
         @param eventType   Type of the event group.
        
         @return    The event.
         */

        public CDRTaskEventParamsTimeGroup GetEvent(string startTime, string eventType) {
            if (_ioDataDict.ContainsKey(startTime) &&
                _ioDataDict[startTime].ContainsKey(eventType)) {
                return _ioDataDict[startTime][eventType];
            }
            return new CDRTaskEventParamsTimeGroup();
        }

        /**
         @fn    public void ParseXMLNextTag()
        
         @brief Parse XML next tag.

            - this is needed to ensure multiple events can occur at the same time
            - events are stored as a list of events to a timestamp hash key. 

         @author Ryan Beruldsen
         @date  6/10/2016
         */

        public void ParseXMLNextTag() {
            _parseXMLNextTag = true;
        }

        /**
         @fn    public void ParseIOEvent(string startTime, string type, string key, string value)
        
         @brief Parse XML.
        
         @author    Ryan Beruldsen
         @date  6/10/2016
        
         @param startTime   The event start time, format 00:00:00
         @param type        The event type
         @param key         The event key
         @param value       The event value
         */

        public void ParseIOEvent(string startTime, string type, string key, string value) {

            startTime = startTime.ToLower();
            type = type.ToLower();
            key = key.ToLower();
            value = value.ToLower();

            if (!_ioDataDict.ContainsKey(startTime)) {
                _ioDataDict[startTime] = new CDRTaskEventTimeGroup();
            }

            if (!_ioDataDict[startTime].ContainsKey(type)) {
                _ioDataDict[startTime][type] = new CDRTaskEventParamsTimeGroup();
            }

            //ensure multiple events can happen at same 
            //timestamp with manual next tag call (XML)
            if (_ioDataDict[startTime][type].Count == 0 || _parseXMLNextTag) {
                _parseXMLNextTag = false;
                _ioDataDict[startTime][type].Add(new CDRTaskEventParams());

            }

            var data =
                _ioDataDict[startTime][type][_ioDataDict[startTime][type].Count - 1];

            //ensure multiple events can happen at same 
            //timestamp with no next tag call (JSON)
            if (data.ContainsKey(key)) {
                _ioDataDict[startTime][type].Add(new CDRTaskEventParams());
                data =
                    _ioDataDict[startTime][type][_ioDataDict[startTime][type].Count - 1];
            }

            data[key] = value;
        }

    }
}
