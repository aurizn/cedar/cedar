# IO Config File Guide

### Summary

The Cedar config file contains configuration information relating to the behaviour and the bounds of behaviour of Cedar tasks. MATB-II config files are fully compatible with Cedar, but Cedar config files can contain some extra information that is not compatible with MATB-II, see _MATB-II Configuration_ section for details. For information regarding the loading of configuration files, and templates containing default config setups, see the section _Config File Formats._ For a comprehensive guide of every configuration option in Cedar, see the _Standard Config_ section. The section Cedar Parse Data contains information that may assist interpreting the table layout of _Standard Config_ section.

### MATB-II Compatibility

All MATB-II cedar files are compatible with Cedar, however some Cedar config types are not backwards compatible:

* The _RESMAN_TLEVELS_ type is unique to Cedar and overrides some MATB-II Resource Management task defaults.
* Within the _MODE_ type, Cedar has defined some new modes that are not backwards compatible:
  * _RANDOM_MODE_: this allows Cedar to run without any events files, and just triggers random events for a 2 minute period.
  * _SILENT_MODE:_ this mutes any sounds and shows more visual feedback in the _Communications_ task to enable the use of the task without any sound.

### Loading of Config Files

Cedar loads one config file on installation by default. This file can be swapped at any point – but must be named and placed according to the guidelines below.

The naming guidlines are strict for each type of input available: JSON or XML. The type of input can be selected after launch and initial load, however. Event files can also be reloaded after launch.

#### Android

* The config file should be named _CEDAR_CONFIG.xml_ or _config.json_ and placed in a folder named _cedar_ in the root of the Android USB storage.

#### iOS

* The config file should be named _CEDAR_CONFIG.xml_ or _config.json_ and placed in the app shared storage for the Cedar app.
* See: [https://support.apple.com/en-au/HT201301](https://support.apple.com/en-au/HT201301)

#### Loading Alternate Config Files

Alternate config files can be loaded where an alternate events file is present – the same naming rules apply, for example “_ALT_CONFIG.xml_” would load with “_ALT_EVENTS.xml_“, see [IO Events File Guide](https://elmtek.com.au/cedar-owat/cedar-user-guide/io-events-file-guide/) for more information on loading alternate files.

### Config File Formats

### XML

The XML format of Cedar is identical to that of MATB-II. Config items should be modified from this template.

The template contains all settings in default values, note that if any config items are removed from the config file, these defaults will be loaded anyway, in this way, the original MATB config files will load in Cedar.

* The exception to this is the _RESMAN_TLEVELS_ section, which will default to MATB-II parameters if not specified.

#### XML Event File Template

        <?xml version="1.0" encoding="utf-8"?>
        <!-- Modified on 06-02-2010 -->
        <CEDAR-CONFIG>
            <!-- Mode booleans... -->
            <name type="MODE">
                <TRAIN_MODE>false</TRAIN_MODE>
                <RANDOM_MODE>true</RANDOM_MODE>
                <SILENT_MODE>false</SILENT_MODE>
                <FOLDER_PER_RUN_MODE>false</FOLDER_PER_RUN_MODE>
                <EXT>false</EXT>
            </name>
            <!-- Resource Management Pump Rates in Units per Minute... -->
            <!-- Default PUMP_1 = 800 -->
            <!-- Default PUMP_2 = 600 -->
            <!-- Default PUMP_3 = 800 -->
            <!-- Default PUMP_4 = 600 -->
            <!-- Default PUMP_5 = 600 -->
            <!-- Default PUMP_6 = 600 -->
            <!-- Default PUMP_7 = 400 -->
            <!-- Default PUMP_8 = 400 -->
            <!-- Default TANK_A = 800 -->
            <!-- Default TANK_B = 800 -->
            <name type="RESMAN_RATES">
                <PUMP_1>800</PUMP_1>
                <PUMP_2>600</PUMP_2>
                <PUMP_3>800</PUMP_3>
                <PUMP_4>600</PUMP_4>
                <PUMP_5>600</PUMP_5>
                <PUMP_6>600</PUMP_6>
                <PUMP_7>400</PUMP_7>
                <PUMP_8>400</PUMP_8>
                <TANK_A>800</TANK_A>
                <TANK_B>800</TANK_B>
            </name>
            <!-- Resource Management Tank Levels and Parameters -->
            <name type="RESMAN_TLEVELS">
                <CAPACITY>4000</CAPACITY>
                <INITIAL>2000</INITIAL>
                <MIN>1400</MIN>
                <MAX>2600</MAX>
            </name>
            <!-- Time Out in seconds... -->
            <name type="TIMEOUT">
                <RATE>30</RATE>
                <TASK_COMM>5</TASK_COMM>
                <TASK_SYSMON_SCALES>5</TASK_SYSMON_SCALES>
                <TASK_SYSMON_LIGHTS>5</TASK_SYSMON_LIGHTS>
            </name>
            <!-- Recording Interval in seconds... -->
            <name type="RECORDING_INTERVAL">
                <TASK_RESMAN>30</TASK_RESMAN>
            </name>
        </CEDAR-CONFIG>

### JSON

The template contains all settings in default values, note that if any config items are removed from the config file, these defaults will be loaded anyway, in this way, the original MATB config files will load in Cedar. The _type attribute as the final attribute in an event object is critical, this is used to anchor the end of an config object for parsing. The following online converter can be used to correctly convert MATB-II XML files to Cedar JSON files:

[http://convertjson.com/xml-to-json.htm](http://convertjson.com/xml-to-json.htm)

* The exception to this is the _RESMAN_TLEVELS_ section, which will default to MATB-II parameters if not specified.
  
        {
        "MATB-CONFIG": {
            "name": [
            {
                "TRAIN_MODE": "true",
                "RANDOM_MODE": "true",
                "SILENT_MODE": "false",
                "EXT": "false",
                "_type": "MODE"
            },
            {
                "PUMP_1": "800",
                "PUMP_2": "600",
                "PUMP_3": "800",
                "PUMP_4": "600",
                "PUMP_5": "600",
                "PUMP_6": "600",
                "PUMP_7": "400",
                "PUMP_8": "400",
                "TANK_A": "800",
                "TANK_B": "800",
                "_type": "RESMAN_RATES"
            },
            {
                "CAPACITY": "4000",
                "INITIAL": "2000",
                "MIN": "1400",
                "MAX": "2600",
                "_type": "RESMAN_TLEVELS"
            },
            {
                "RATE": "10",
                "TASK_COMM": "30",
                "TASK_SYSMON_SCALES": "5",
                "TASK_SYSMON_LIGHTS": "5",
                "TASK_EXT": "5",
                "_type": "TIMEOUT"
            },
            {
                "TASK_RESMAN": "05",
                "_type": "RECORDING_INTERVAL"
            }
            ]
        }
        }

### Cedar Parse Data

The Cedar parser, regardless of parsed format, forms a parse tree to store data in a specific way – below is an example a flat representation of the first 5 config values as they are stored in the parse tree.

| Config Type  | Config Key            | Config Value |
| ------------ | --------------------- | ------------ |
| mode         | train_mode            | true         |
| mode         | random_mode           | true         |
| mode         | silent_mode           | false        |
| mode         | folder\_per\_run_mode | false        |
| resman_rates | pump_1                | 800          |

### Standard Config

* config values are literal, except in the case of the use of parentheses or forward slashes, for example:
  * **p(1,2)** means values **p1** and **p2**
  * **one/two** values **one** and **two**
* **integer values** refer to 32-bit ints

| Config Type        | Config Key(s)                                                                                      | Config Values(s)                                                                                                                                                  | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |
| ------------------ | -------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| mode               | <ul><li>train_mode</li><li>random_mode</li><li>silent_mode</li><li>folder\_per\_run_mode</li></ul> | <ul><li>true/false</li></ul>                                                                                                                                      | Enables or disables Cedar modes:<br><br>Training mode:<br><br>* Enables visual feedback on the Communication task: this shows the target frequency and channel that the voiceover is explaining needs to be changed.<br>* A running score is shown for training purposes, out of 100, the higher the score the better.<br>* The cumulative time a warning has been raised for a task is also shown for training purposes.<br><br>Random mode:<br><br>* A test of 2 mins length is initiated on test start, with random events of all available event types, including any extension events if hooks are present.<br>* Events are logged normally, but control events (starting stopping test and enabling and disabling tasks) are not automated.<br><br>Silent mode:<br><br>* Audio is disabled and visual feedback is shown on the Communications task to enable it’s use without audio.<br>* Visual feedback for the Communication task also includes false feedback for “other” callsigns (see _Cedar IO Events Guide_), wherein the target and the current frequency is the same, but red warning feedback is still shown.<br><br>Folder per run mode:<br><br>* This outputs all log files to a single folder per test run – only for Android platform as iOS does this by default. |
| resman_rates       | pump_(1-8)                                                                                         | Any integer values.                                                                                                                                               | The pump rates in units per minute for each numbered Resource Management pump.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| resman_rates       | tank_(a,b)                                                                                         | Any integer values.                                                                                                                                               | The drain rates in units per minute for each Resource Management tank.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| resman_tlevels     | capacity                                                                                           | Any integer values.                                                                                                                                               | The total capacity in units of the Resource Management primary tanks (supply tanks have half this capacity). The integer is divided, so a sensible value to allow a divsion by two is required.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| resman_tlevels     | initial                                                                                            | Any integer values – this should begin within min and max values, however, by convention, otherwise the user will be forced to use pumps immediately on test run. | The initial Resource Management primary tank levels in units.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| resman_tlevels     | <ul><li>min</li><li>max</li></ul>                                                                  | Any integer values. These values should be between the _capacity_ config level and 0 to make sense.                                                               | The minimum and maximum safe levels in units of the Resource Management primary tanks. Warning colours will show if the tank levels are pumped above, or are allowed to drain below the min/max levels.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| timeout            | rate                                                                                               | Any integer values.                                                                                                                                               | The maximum time in seconds a user can spend on the Workload Rating form before it times out and the test begins again.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| timeout            | <ul><li>task comm</li><li>task\_sysmon\_scales</li><li>task\_sysmon\_lights</li></ul>              | Any integer values.                                                                                                                                               | The maximum time in seconds a user has to respond to a task event before it times out and the user is disallowed any further response.<br><br><ul><li>task_comm: Communications task warning raise event - note that the timeout starts 10 seconds after the start of the voiceover to allow for the voiceover to finish (this will appear to give more time than set when viewed in training mode).</li><li>task\_sysmon\_scales: Systems Monitoring Scales warning raise event</li><li>task\_sysmon\_lights: Systems Monitoring Switch/Button warning raise (Light)event</li></ul>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| recording_interval | task_resman                                                                                        | Any integer values.                                                                                                                                               | The logging frequency of Resources Management tank levels in seconds.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |