# CEDAR OWAT

CEDAR OWAT is a human factors T&E tool that has been developed to assess human workload across a range of domains. 

**aurizn** has now released CEDAR OWAT as an GPLv3-licenced open source tool for researchers.

<table>
<tr>
    <td><img src="docs/media/aurizn_black.png" width="150"/></td>
    <td><img src="docs/media/cedar_black_logo.png" width="260"/></td>
</tr>
</table>

## Workload Assessment, Portable.


![](docs/media/cedar_owat_banner.jpg)

**The Cedar Operator Workload Assessment Tool is a flexible, highly extensible product with applications across a range of domains which have human factor-related considerations. Cedar is based on NASA’s MATB-II desktop application which is widely used in human factors and operator workload research.**

Cedar OWAT is a task platform for the tablet designed to evaluate operator performance and workload. It was originally developed as an implementation of the NASA developed MATB-II for the tablet form factor with in-depth analysis of the relevant published knowledge-base informing the design. Cedar is both an accurate implementation of the workload assessment capabilities of the original software and a uniquely useful tool in its own right.

<table>
<tr>
    <td><img src="docs/media/cedar-01-110x110.jpg"/></td>
    <td>
    <b>Flight test pedigree with limitless applications</b>
    <br>
    Cedar can be used across a range of domains in support of human factors studies, and whilst the product was originally developed for the flight test community, the applications are limitless.
    </td>
</tr>
<tr>
    <td><img src="docs/media/cedar-02-110x110.jpg"/></td>
    <td>
    <b>Designed from the ground up for the tablet</b>
    <br>
    Cedar is based on NASA’s MATB-II desktop application. The principal design goal was to adapt MATB-II for the tablet platform, providing a solution that can be deployed to support human factors studies in non-office environments.
    </td>
</tr>
<tr>
    <td><img src="docs/media/cedar-03-110x110.jpg"/></td>
    <td>
    <b>Flexible configuration</b>
    <br>
    Configuration files for Cedar can be written in the modern, easy to read and compose JSON format. Cedar is compatible with the NASA MATB-II XML configuration format making it possible to use the same configuration files for both NASA MATB-II and Cedar.
    </td>
</tr>
<tr>
    <td><img src="docs/media/cedar-04_5-110x110.jpg"/></td>
    <td>
    <b>Highly extensible</b>
    <br>
    Task components for Cedar are adapted from NASA MATB-II existing components including Communications, Systems Monitoring and Resource Management tasks. Additional tasks can be developed to cater for specific needs.
    </td>
</tr>
</table>

## Build

Cedar OWAT can be built with _Visual Studio 2022_. It requires the _Xamarin_ framework, Android SDKs (for Android development), _XCode_ (for iOS development). The NuGet package manager is used for further dependencies - Visual Studio should manage this automatically.

### User Documentation

See the _docs_ folder for full user guides.

### Code Documentation

Documentation can be generated with _Doxygen_ - to generate the preset html documentation, simply open and run _docs/generated/html_docs_doxyfile_ in the Doxywizard application or use the _Doxygen_ command line tools.

## Acknowledgements

**aurizn** thanks the following organisations for their continued support in developing, adopting and helping to improve the CEDAR platform.

| NASA | University of Canberra | Dartmouth <div class="text-center" style="margin-top: 0.1em; font-size: 0.7em;">Space Medicine Innovations Lab</div>
| --- | --- | --- |
![NASA](docs/media/Logo-nasa.jpg) | ![University of Canberra](docs/media/Logo-Canberra-2.jpg) | ![Dartmouth Space Medicine Innovations Lab](docs/media/Logo-Dartmouth.jpg) | ![University of Newcastle](docs/media/newcastle-logo.png) | ![CSRC](docs/media/CSRC-Logo.png)

| University of Newcastle | CSRC
| --- | --- |
| ![University of Newcastle](docs/media/newcastle-logo.png) | ![CSRC](docs/media/CSRC-Logo.png)
