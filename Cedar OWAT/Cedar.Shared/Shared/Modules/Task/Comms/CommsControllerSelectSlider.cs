﻿/*
 * CEDAR Operator Workload Assessment Tool
 * Copyright (C) 2023  aurizn
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using CocosSharp;
using System.Collections.Generic;
using CEDAR.Shared.CedarCollection;

namespace CEDAR.Shared.Modules.Task.Comms {

    /**
     @class CommsControllerSelectSlider
    
     @brief Slider selection of selected channel frequency
    
     @author    Ryan Beruldsen
     @date  15/09/2016
     */

    class CommsControllerSelectSlider : DivisionContent, IDivisionContent {

        /** @brief The start coordinates of a touch or drag user input. */
        private CCPoint _startTouch = new CCPoint(-1, -1);

        /** Global model data singleton. */
        private readonly ModelData _modelI = ModelData.Instance;

        /** @brief The initial coordinates of the frequency control on the beginnnig of a user input. */
        private CCPoint _freqControlStart = new CCPoint(-1, -1);

        /** @brief The selected channel index. */
        private int _selectedChannelIndex = -1;

        /** @brief Draging state of slider. */
        private bool _freqControlDragging = false;

        /** @brief Touch input listener. */
        private CCEventListenerTouchAllAtOnce _touchListener;

        /** @brief List of communications channels. */
        private readonly List<CommsModelChannel> _commChannelList;

        /** @brief Slider view instance. */
        private CommsViewSelectSlider _slider;

        public CommsControllerSelectSlider(List<CommsModelChannel> CommChannelList,
                                            Division division) : base(division) {
            _commChannelList = CommChannelList;
        }

        /**
         @fn    public void Init()
        
         @brief S this object.
        
         @author    Ryan Beruldsen
         @date  20/09/2016
         */

        public void Init() {
            bool silentMode = ModelData.Instance.Settings[ModelData.SILENT_MODE] == ModelData.PARAM_ON;
            bool trainMode = ModelData.Instance.Settings[ModelData.TRAINING_MODE] == ModelData.PARAM_ON;

            bool trainSilent = silentMode || trainMode;

            _slider = new CommsViewSelectSlider(
                division: _division,
                x: _division.Width * 0.05f,
                y: _division.PrevDivisionY + _division.Height * 0.6f,
                width: _division.Width * 0.9f,
                height: _division.Height * 0.2f,
                colourRange: _modelI.GetColourPallete4B(
                    ModelData.COLOUR_PARAM_RANGE),
                colourWarning: _modelI.GetColourPallete4B(
                    ModelData.COLOUR_PARAM_WARNING)
            );
            _slider.Init();
            AddChild(_slider);

            _touchListener = new CCEventListenerTouchAllAtOnce {
                OnTouchesBegan = HandleInputStart,
                OnTouchesMoved = HandleInputDrag,
                OnTouchesEnded = HandleInputEnd
            };
            AddEventListener(_touchListener, this);

            //initialise view to represent model state
            for (int i = 0; i < _commChannelList.Count; i++) {
                var channel = _commChannelList[i];
                if (channel.WarnIndicate && channel.TargetFrequency != -1) {
                    _slider.SetWarnPerc(channel.TargetPercent);
                    _slider.WarnIndicate = true;
                }
                if (_commChannelList[i].IsSelected) {
                    _selectedChannelIndex = i;
                    //ensure visual warning only on silent or train modes
                    _slider.SetSliderPerc(
                        channel.Percent,
                        channel.WarnIndicate && trainSilent,
                        channel.TargetPercent
                    );
                }
                _commChannelList[i].CommsModelEventHandlerQueue +=
                    new CommsModelEventHandler(HandleComms);
            }
        }

        /**
         @fn    private void HandleComms(object sender, CommsModelChannelEventArgs e)
        
         @brief Handles comms model data events (channel warning raised ect).
        
         @author    Ryan Beruldsen
         @date  20/09/2016
        
         @param sender  Source of the event.
         @param e       Communications model channel event information.
         */

        private void HandleComms(object sender, CommsModelChannelEventArgs e) {
            bool silentMode = ModelData.Instance.Settings[ModelData.SILENT_MODE] == ModelData.PARAM_ON;
            bool trainMode = ModelData.Instance.Settings[ModelData.TRAINING_MODE] == ModelData.PARAM_ON;

            bool trainSilent = silentMode || trainMode;
            //if selected frequency is changed
            if (e.ValueChange) {
                if (e.Comms.IsSelected) {

                    _slider.SetSliderPerc(
                        e.Comms.Percent,
                        e.Comms.WarnIndicate && trainSilent,
                        e.Comms.TargetPercent
                    );
                }
                //if channel selection is changed
            }
            else if (e.SelectionChange) {
                if (e.Comms.IsSelected) {
                    _selectedChannelIndex = e.Comms.index;
                    _slider.SetSliderPerc(
                        e.Comms.Percent,
                        e.Comms.WarnIndicate && trainSilent,
                        e.Comms.TargetPercent
                    );
                    if (e.Comms.WarnIndicate && trainSilent) {
                        _slider.SetWarnPerc(e.Comms.TargetPercent);
                        _slider.WarnIndicate = true;
                    }
                    else {
                        _slider.WarnIndicate = false;
                    }
                }
                //if a warning is raised (a channel is not at target frequency)
            }
            else if (e.WarnIndicateChange && trainSilent) {

                if (e.Comms.WarnIndicate &&
                    e.Comms.TargetFrequency != -1 &&
                    e.Comms.IsSelected) {

                    _slider.SetWarnPerc(e.Comms.TargetPercent);
                    _slider.WarnIndicate = true;
                }
                else if (!e.Comms.WarnIndicate && e.Comms.IsSelected) {
                    _slider.WarnIndicate = false;
                }
            }
        }

        /**
         @fn    private void HandleInputStart(System.Collections.Generic.List<CCTouch> touches, CCEvent touchEvent)
        
         @brief Handles the touch input start.
        
         @author    Ryan Beruldsen
         @date  20/09/2016
        
         @param touches     CocosSharp injected touch list
         @param touchEvent  CocosSharp injected touch event
         */

        private void HandleInputStart(System.Collections.Generic.List<CCTouch> touches,
                                        CCEvent touchEvent) {
            if (touches.Count > 0) {
                //set initial tracking vars
                _startTouch.X = touches[0].Location.X;
                _startTouch.Y = touches[0].Location.Y;
                _freqControlStart.X = _slider.freqControl.X;
                bool hit = false;
                bool falsePos = !_commChannelList[_selectedChannelIndex].WarnIndicate;

                float freqDiff =
                    (_slider.freqControl.X - _slider.FreqControlWidth);
                float freqCalc1 =
                    (_slider.freqControl.X + _slider.FreqControlWidth);
                float freqCalc2 =
                    (_slider.freqControl.X + _slider.FreqControlWidth * 2);

                //if within left extended bounding box for touch up
                if (touches[0].Location.X > freqDiff &&
                    touches[0].Location.X < _slider.freqControl.X &&
                    touches[0].Location.Y > _slider.FreqControlMin.Y &&
                    touches[0].Location.Y < _slider.FreqControlMax.Y) {
                    hit = true;
                    falsePos = !_commChannelList[_selectedChannelIndex].WarnIndicate;

                    _commChannelList[_selectedChannelIndex].Frequency -=
                        _commChannelList[_selectedChannelIndex].CommIncrement;
                    _slider.TouchLeft = true;

                    //if within right extended bounding box for touch up
                }
                else if (touches[0].Location.X > freqCalc1 &&
                            touches[0].Location.X < freqCalc2 &&
                            touches[0].Location.Y > _slider.FreqControlMin.Y &&
                            touches[0].Location.Y < _slider.FreqControlMax.Y) {
                    hit = true;
                    falsePos = !_commChannelList[_selectedChannelIndex].WarnIndicate;

                    _commChannelList[_selectedChannelIndex].Frequency +=
                        _commChannelList[_selectedChannelIndex].CommIncrement;
                    _slider.TouchRight = true;
                }
                freqCalc1 = _slider.freqControl.X + _slider.FreqControlWidth;
                //if within slider bounding box for touch up
                if (touches[0].Location.X > _slider.freqControl.X &&
                    touches[0].Location.X < freqCalc1 &&
                    touches[0].Location.Y > _slider.FreqControlMin.Y &&
                    touches[0].Location.Y < _slider.FreqControlMax.Y) {

                    _freqControlDragging = true;
                    _slider.Dragging = true;
                    hit = true;
                }

                CDRTouchData.StatusEnum status;

                if (falsePos && hit) {
                    status = CDRTouchData.StatusEnum.FalsePos;
                }
                else if (hit) {
                    status = CDRTouchData.StatusEnum.Hit;
                }
                else {
                    status = CDRTouchData.StatusEnum.Miss;
                }

                ModelDataIOLog.Instance.LogTouch(
                                          x: touches[0].Location.X,
                                          y: touches[0].Location.Y,
                                          status: status
                );
            }
        }

        /**
         @fn    private void HandleInputEnd(List<CCTouch> touches, CCEvent touchEvent)
        
         @brief Handles the touch input end.
        
         @author    Ryan Beruldsen
         @date  20/09/2016
        
         @param touches     CocosSharp injected touch list
         @param touchEvent  CocosSharp injected touch event
         */

        private void HandleInputEnd(List<CCTouch> touches, CCEvent touchEvent) {
            _slider.Dragging = false;
            _freqControlDragging = false;
        }

        /**
         @fn    private void HandleInputDrag(System.Collections.Generic.List<CCTouch> touches, CCEvent touchEvent)
        
         @brief Handles the touch input drag event.
        
         @author    Ryan Beruldsen
         @date  20/09/2016
        
         @param touches     CocosSharp injected touch list
         @param touchEvent  CocosSharp injected touch event
         */

        private void HandleInputDrag(System.Collections.Generic.List<CCTouch> touches,
                                    CCEvent touchEvent) {
            if (touches.Count > 0) {

                //while touch input remains within dragging bounds 
                //(slightly larger than slider rectangle bounds
                // for some ease of use allowance)
                if (touches[0].Location.Y > _slider.FreqControlMin.Y * 0.75 &&
                        touches[0].Location.Y < _slider.FreqControlMax.Y * 1.25 &&
                        _freqControlDragging) {

                    _slider.freqControl.X = _freqControlStart.X +
                        (touches[0].Location.X - _startTouch.X);

                    //keep slider within bounds
                    if (_slider.freqControl.X > _slider.FreqControlMax.X) {
                        _slider.freqControl.X = _slider.FreqControlMax.X;
                    }
                    if (_slider.freqControl.X < _slider.FreqControlMin.X) {
                        _slider.freqControl.X = _slider.FreqControlMin.X;
                    }

                    bool sliderWithinBounds =
                        (_slider.freqControl.X - _slider.FreqControlMin.X) != 0;

                    float sliderCalc =
                        (_slider.freqControl.X - _slider.FreqControlMin.X) /
                        (_slider.FreqControlMax.X - _slider.FreqControlMin.X);

                    _commChannelList[_selectedChannelIndex].Percent =
                        sliderWithinBounds ? sliderCalc : 0;
                }
            }
        }
    }
}
