﻿/*
 * CEDAR Operator Workload Assessment Tool
 * Copyright (C) 2023  aurizn
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


using System;

namespace CEDAR.Shared.Modules.Task.Res {
    internal delegate void ResModelTankEventHandler(object sender, ResModelTankEventArgs e);

    /**
     @class ResModelTank
    
     @brief Model representing a fuel tank with a capacity managed by a number of pumps (ResModelPump)
    
     @author    Ryan Beruldsen
     @date  15/09/2016
     */

    class ResModelTank {


        /** @brief Enabled status of model. */
        private bool _enabled = false;

        /**
         @property  public bool enabled
        
         @brief Gets or sets a value indicating whether the model is enabled.
        
         @return    true if enabled, false if not.
         */

        public bool Enabled {
            get { return _enabled; }
            set {
                _enabled = value;
            }
        }

        /** @brief Tank level warning state, if levels are outside prescribed min/max range. */
        private bool _warnIndicate = false;

        /** @brief The percentage value of the tank level (0 empty, 0.5 half full). */
        private double _value;

        private readonly bool _unlimited;

        /** @brief The minimum safe tank level range (in percentage). */
        private readonly double _minRange;

        /** @brief The maximum safe tank level range (in percentage). */
        private readonly double _maxRange;

        /** @brief The total tank capacity in units (empty capacity is 0). */
        private readonly int _capacity;

        /** @brief Critical state of tank, a critical tank will raise a warning if level is outside safe range. */
        private readonly bool _critical;

        /** @brief Tank label string. */
        private readonly string _label;

        public bool Unlimited {
            get { return _unlimited; }
        }

        /** @brief Event queue for all listeners interested in resModelTankChanged events. */
        public event ResModelTankEventHandler ResModelTankEventHandler;

        /**
         @property  public bool critical
        
         @brief Gets a value indicating critical property of tank, a critical tank will raise a warning if level is outside safe range.
        
         @return    true if critical property, false if not.
         */

        public bool Critical { get { return _critical; } }

        /**
         @property  public string label
        
         @brief Gets the tank label string.
        
         @return    The tank label string.
         */

        public string Label { get { return _label; } }

        /**
         @property  public string levelUnits
        
         @brief Gets a display string of level in units (as per tank capacity).
        
         @return    Tank level in units.
         */

        public string LevelUnitsStr {
            get {
                return ((int)(_value * _capacity)).ToString();
            }
        }

        public double LevelUnits {
            get {
                return _value * _capacity;
            }
            set {
                Level = value / _capacity;
            }
        }
        /**
         @property  public bool warnIndicate
        
         @brief Gets or sets a value indicating tank level warning state, if levels are outside prescribed min/max range.

        @return    true if tank level warning state, false if not.
         */

        public bool WarnIndicate {
            get { return _warnIndicate; }
            set {
                if (_enabled) {
                    ResModelTankEventArgs e = new ResModelTankEventArgs {
                        WarnIndicateChange = true,
                        Res = this
                    };
                    bool tempVal = _warnIndicate;
                    _warnIndicate = value;

                    if (tempVal != value && _critical) {
                        ResModelTankChanged(e);
                    }
                }

            }
        }

        /**
         @property  public double maxLevel
        
         @brief Gets the maximum safe level of tank in percentage (0 for empty, 0.5 half full).
        
         @return    The maximum safe level of tank.
         */

        public double MaxLevel { get { return _maxRange; } }

        /**
         @property  public double minLevel
        
         @brief Gets the minimum safe level of tank in percentage (0 for empty, 0.5 half full).
        
         @return    The minimum safe level of tank.
         */

        public double MinLevel { get { return _minRange; } }

        /**
         @property  public double level
        
         @brief Gets or sets the level of the tank in percentage (0 for empty, 0.5 half full).
        
         @return    The tank level.
         */

        public double Level {
            get { return _value; }
            set {
                if (!_unlimited && _enabled) {
                    ResModelTankEventArgs e = new ResModelTankEventArgs();
                    if (value >= 0 && value <= 1) this._value = value;
                    if (value < 0) this._value = 0;
                    if (value > 1) this._value = 1;

                    if (this._value < _minRange && Math.Abs(this._value - _minRange) > 0.001 && !WarnIndicate) {

                        WarnIndicate = true;
                    }
                    else if (this._value > _maxRange && Math.Abs(_value - _maxRange) > 0.001 && !WarnIndicate) {
                        WarnIndicate = true;
                    }
                    else if (this._value >= _minRange && this._value <= _maxRange && WarnIndicate) {
                        WarnIndicate = false;
                    }

                    e.ValueChange = true;
                    e.Res = this;
                    ResModelTankChanged(e);
                }

            }
        }

        /**
         @fn    protected virtual void ResModelTankChanged(ResModelTankEventArgs e)
        
         @brief Raises resource model tank events.
        
         @author    Ryan Beruldsen
         @date  21/09/2016
        
         @param e   Resource model tank event information.
         */

        protected virtual void ResModelTankChanged(ResModelTankEventArgs e) {
            ResModelTankEventHandler?.Invoke(this, e);
        }

        /**
         @fn    public ResModelTank(string label, bool critical, int capacity, double level, double minRange, double maxRange)
        
         @brief Constructor for resource tank model.
        
         @author    Ryan Beruldsen
         @date  21/09/2016
        
         @param label       The label.
         @param critical    true to critical.
         @param capacity    The capacity.
         @param level       The initial level in units.
         @param minRange    The minimum range in units.
         @param maxRange    The maximum range in units.
         @param unlimited   (Optional) If the tank is unlimited.
         */

        public ResModelTank(string label,
                                bool critical,
                                int capacity,
                                double level,
                                double minRange,
                                double maxRange,
                                bool unlimited = false) {

            _value = level / capacity;
            _label = label;
            _minRange = minRange / capacity;
            _maxRange = maxRange / capacity;
            _capacity = capacity;
            _critical = critical;
            _unlimited = unlimited;

        }
    }
}
