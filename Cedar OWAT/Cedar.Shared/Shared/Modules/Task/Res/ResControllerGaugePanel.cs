﻿/*
 * CEDAR Operator Workload Assessment Tool
 * Copyright (C) 2023  aurizn
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


using CEDAR.Shared.CedarCollection;
using CocosSharp;
using System;
using System.Collections.Generic;

namespace CEDAR.Shared.Modules.Task.Res {

    /**
     @class ResControllerGaugePanel
    
     @brief Controller for panel of gauges (ResViewGauge), representing tank levels, also controls pumps and pump controls.
    
     @author    Ryan Beruldsen
     @date  15/09/2016
     */

    class ResControllerGaugePanel : DivisionContent, IDivisionContent {

        /** @brief List of resource tanks. */
        private readonly List<ResModelTank> _resTankList;

        /** @brief List of resource pumps. */
        private readonly List<ResModelPump> _resPumpList;

        /** @brief Listener for button touch events. */
        private readonly CCEventListenerTouchAllAtOnce _touchListener;

        /** @brief List of resource tank gauges. */
        private readonly List<ResViewGauge> _resGaugeList;

        /** @brief List of resource pump controls. */
        private readonly List<ResViewPumpControl> _resPumpControlList;

        /** @brief Pump status text, displays active, faulted pumps. */
        private CCLabel _pumpStatus;

        /** @brief Hash map of tank gauges (views) to respective models. */
        private readonly Dictionary<ResModelTank, ResViewGauge> _resGaugeDict;

        /** @brief Hash map of pump controls (views) to respective models. */
        private readonly Dictionary<ResModelPump, ResViewPumpControl> _resPumpDict;

        /** @brief Tank location helper constant. */
        private const int LOOP_MINOR_LEFT = 0;

        /** @brief Tank location helper constant. */
        private const int LOOP_MINOR_RIGHT = 3;

        /** @brief Tank location helper constant. */
        private const int LOOP_MAJOR_LEFT = 1;

        /** @brief Tank location helper constant. */
        private const int LOOP_MAJOR_RIGHT = 2;

        //controller tank gauge identification constants (based on location)
        // minor being a support tank, major, a main, critical tank

        /** @brief Support tank location constant. */
        public const int TL_MINOR_BOTTOM_LEFT = 0;
        /** @brief Support tank location constant. */
        public const int TL_MINOR_TOP_LEFT = 1;
        /** @brief Support tank location constant. */
        public const int TL_MINOR_BOTTOM_RIGHT = 4;
        /** @brief Support tank location constant. */
        public const int TL_MINOR_TOP_RIGHT = 5;

        /** @brief Critical tank location constant. */
        public const int TL_MAJOR_LEFT = 2;
        /** @brief Critical tank location constant. */
        public const int TL_MAJOR_RIGHT = 3;

        /** @brief Tank letter labels, in order of location (see constants). */
        public static String[] tankLabels = { "E", "C", "A", "B", "F", "D" };

        /** @brief Pump descriptions (and prescription, as the string are interpreted here to the model), in order of location. */
        public static String[] pumpDescriptions = {   "C>A (1)",
                                                        "E>A (2)",
                                                        "D>B (3)",
                                                        "F>B (4)",
                                                        "E>C (5)",
                                                        "F>D (6)",
                                                        "A>B (7)",
                                                        "B>A (8)"
        };

        /**
         @fn    public ResControllerGaugePanel(List<ResModelTank> ResTankList, List<ResModelPump> ResPumpList, Division division) : base(division)
        
         @brief Constructor of Resource Management Gauge Panel.
        
         @author    Ryan Beruldsen
         @date  21/09/2016
        
         @param ResTankList List of resource tanks.
         @param ResPumpList List of resource pumps.
         @param division        Parent division.
         */

        public ResControllerGaugePanel(List<ResModelTank> ResTankList,
                                        List<ResModelPump> ResPumpList,
                                        Division division) : base(division) {
            _resTankList = ResTankList;
            _resGaugeList = new List<ResViewGauge>();
            _resPumpControlList = new List<ResViewPumpControl>();
            _resPumpList = ResPumpList;
            _touchListener = new CCEventListenerTouchAllAtOnce {
                OnTouchesBegan = HandleInput
            };
            AddEventListener(_touchListener, this);
            _resGaugeDict = new Dictionary<ResModelTank, ResViewGauge>();
            _resPumpDict = new Dictionary<ResModelPump, ResViewPumpControl>();
        }

        /**
         @fn    public void Init()
        
         @brief Initialise resources management guage panel.
        
         @author    Ryan Beruldsen
         @date  21/09/2016
         */

        public void Init() {
            //view colour helper variables
            var cActive = ModelData.COLOUR_PARAM_ACTIVE;
            var cWarning = ModelData.COLOUR_PARAM_WARNING;
            var cRange = ModelData.COLOUR_PARAM_RANGE;
            var gaugeActiveColour = ModelData.Instance.GetColourPallete4B(cActive);
            var gaugeRangeColour = ModelData.Instance.GetColourPallete4B(cRange);
            var gaugeWarningColour = ModelData.Instance.GetColourPallete4B(cWarning);
            var gaugeWarningColourText = ModelData.Instance.GetColourPallete3B(cWarning);

            float width = _division.Width * 0.3f;
            int tankCount = 0;

            _pumpStatus = new CCLabel("NO PUMPS ACTIVE", "roboto", 12) {
                Position = new CCPoint(_division.Width / 2, _division.Y - _division.Height * 0.17f),
                Color = CCColor3B.White,
                AnchorPoint = CCPoint.AnchorMiddle
            };

            AddChild(_pumpStatus);
            bool displayMode = ModelData.Instance.Settings[ModelData.MODE_DISPLAY] == ModelData.PARAM_ON;
            if (displayMode) {
                _pumpStatus.Visible = false;
            }
            //instantiate tank gauges based on location (see constants)
            for (int j = 0; j < 4 && _resTankList.Count >= 6; j++) {

                float gradient = (_division.Width - _division.Width * 0.2f) / 3;
                float initialX = _division.Width * 0.05f + gradient * j;

                //major tanks, larger in size, critical - 
                // will raise warning if levels outside acceptable range
                if (j == LOOP_MAJOR_LEFT || j == LOOP_MAJOR_RIGHT) {
                    var tank = _resTankList[tankCount++];
                    tank.ResModelTankEventHandler += ResModelTankChanged;

                    ResViewGauge gauge = new ResViewGauge(
                        label: tank.Label,
                        division: _division,
                        critical: tank.Critical,
                        level: tank.Level,
                        levelUnits: tank.LevelUnitsStr,
                        colourWarning: gaugeWarningColour,
                        colourWarningText: gaugeWarningColourText,
                        colourRange: gaugeRangeColour,
                        colourActive: gaugeActiveColour,
                        maxLevel: tank.MaxLevel,
                        minLevel: tank.MinLevel,
                        width: _division.Width * 0.1f,
                        height: width,
                        x: initialX,
                        y: _division.PrevDivisionY + (_division.Height * 0.10f)
                    );
                    gauge.Init();

                    _resGaugeList.Add(gauge);
                    _resGaugeDict[tank] = gauge;
                    AddChild(gauge, 10);
                }

                //minor tanks supporting major tanks, two supporting tanks per major tank
                // smaller than major tank, stacked vertically
                if (j == LOOP_MINOR_LEFT || j == LOOP_MINOR_RIGHT) {
                    //first vertically stacked minor tank
                    var tank = _resTankList[tankCount++];
                    tank.ResModelTankEventHandler += ResModelTankChanged;
                    ResViewGauge gauge = new ResViewGauge(
                        label: tank.Label,
                        division: _division,
                        critical: tank.Critical,
                        level: tank.Level,
                        levelUnits: tank.LevelUnitsStr,
                        colourWarning: gaugeWarningColour,
                        colourWarningText: gaugeWarningColourText,
                        colourRange: gaugeRangeColour,
                        colourActive: gaugeActiveColour,
                        width: _division.Width * 0.05f,
                        height: width * 0.45f,
                        x: initialX + _division.Width * 0.025f,
                        y: _division.PrevDivisionY + (_division.Height * 0.10f)
                    );
                    gauge.Init();

                    _resGaugeList.Add(gauge);
                    _resGaugeDict[tank] = gauge;
                    AddChild(gauge, 10);

                    //second vertically stacked minor tank
                    var tank2 = _resTankList[tankCount++];
                    tank2.ResModelTankEventHandler += ResModelTankChanged;
                    ResViewGauge gauge2 = new ResViewGauge(
                        label: tank2.Label,
                        division: _division,
                        critical: tank2.Critical,
                        level: tank2.Level,
                        levelUnits: tank2.LevelUnitsStr,
                        colourWarning: gaugeWarningColour,
                        colourWarningText: gaugeWarningColourText,
                        colourRange: gaugeRangeColour,
                        colourActive: gaugeActiveColour,
                        width: _division.Width * 0.05f,
                        height: width * 0.45f,
                        x: initialX + _division.Width * 0.025f,
                        y: _division.PrevDivisionY + (_division.Height * 0.10f) + width * 0.5f
                    );
                    gauge2.Init();

                    _resGaugeList.Add(gauge2);
                    _resGaugeDict[tank2] = gauge2;
                    AddChild(gauge2, 10);
                }
            }

            //instantiate pump controls, based on location order (see constants)
            for (int i = 0; i < _resPumpList.Count; i++) {

                //interpret pump description for from/to config for pumps
                int from = Array.IndexOf(tankLabels,
                        (_resPumpList[i].PumpDescription.ToCharArray()[0]).ToString());
                int to = Array.IndexOf(tankLabels,
                    (_resPumpList[i].PumpDescription.ToCharArray()[2]).ToString());

                _resPumpList[i].ResPumpEventHandler += new
                    ResModelPumpEventHandler(ResModelPumpChanged);

                //add pump controls based on location
                if (from == TL_MINOR_TOP_LEFT && to == TL_MAJOR_LEFT) {
                    AddPumpControl(ResViewPumpControl.PCT_MINOR_TO_MAJOR, _resPumpList[i], from, to);
                }
                if (from == TL_MINOR_BOTTOM_LEFT && to == TL_MAJOR_LEFT) {
                    AddPumpControl(ResViewPumpControl.PCT_MINOR_TO_MAJOR, _resPumpList[i], from, to);
                }
                if (from == TL_MINOR_BOTTOM_RIGHT && to == TL_MAJOR_RIGHT) {
                    AddPumpControl(ResViewPumpControl.PCT_MINOR_TO_MAJOR, _resPumpList[i], from, to);
                }
                if (from == TL_MINOR_TOP_RIGHT && to == TL_MAJOR_RIGHT) {
                    AddPumpControl(ResViewPumpControl.PCT_MINOR_TO_MAJOR, _resPumpList[i], from, to);
                }
                if (from == TL_MINOR_BOTTOM_RIGHT && to == TL_MINOR_TOP_RIGHT) {
                    AddPumpControl(ResViewPumpControl.PCT_MINOR_TO_MINOR, _resPumpList[i], from, to);
                }
                if (from == TL_MINOR_BOTTOM_LEFT && to == TL_MINOR_TOP_LEFT) {
                    AddPumpControl(ResViewPumpControl.PCT_MINOR_TO_MINOR, _resPumpList[i], from, to);
                }
                if (from == TL_MAJOR_RIGHT && to == TL_MAJOR_LEFT) {
                    AddPumpControl(ResViewPumpControl.PCT_MAJOR_TO_MAJOR, _resPumpList[i], from, to);
                }
                if (from == TL_MAJOR_LEFT && to == TL_MAJOR_RIGHT) {
                    AddPumpControl(ResViewPumpControl.PCT_MAJOR_TO_MAJOR, _resPumpList[i], from, to);
                }
            }
        }

        /**
         @fn    private void AddPumpControl(int type, ResModelPump pump, int targetA, int targetB)
        
         @brief Adds a pump control to specified location as per tank location (TL) constants.
        
         @author    Ryan Beruldsen
         @date  21/09/2016
        
         @param type    Tank location (TL) constant.
         @param pump    The pump model.
         @param targetA Target a (from).
         @param targetB Target b (to).
         */

        private void AddPumpControl(
            int type,
            ResModelPump pump,
            int targetA,
            int targetB) {
            var cActive = ModelData.COLOUR_PARAM_ACTIVE;
            var cWarning = ModelData.COLOUR_PARAM_WARNING;

            ResViewPumpControl pumpControl = new ResViewPumpControl(
                division: _division,
                pumpControlType: type,
                activeColour: ModelData.Instance.GetColourPallete4B(cActive),
                warningColour: ModelData.Instance.GetColourPallete4B(cWarning),
                resGaugeList: _resGaugeList,
                targetA: targetA,
                targetB: targetB
            );

            pumpControl.Init();
            AddChild(pumpControl);
            _resPumpDict[pump] = pumpControl;
            _resPumpControlList.Add(pumpControl);
        }

        /**
         @fn    private void ResModelTankChanged(object sender, ResModelTankEventArgs e)
        
         @brief Handle tank model events.
        
         @author    Ryan Beruldsen
         @date  21/09/2016
        
         @param sender  Source of the event.
         @param e       Resource model tank event information.
         */

        private void ResModelTankChanged(object sender, ResModelTankEventArgs e) {
            //if tank level has changed
            if (e.ValueChange && _resGaugeDict.ContainsKey(e.Res)) {
                _resGaugeDict[e.Res].SetLevel(e.Res.Level, e.Res.LevelUnitsStr);

                //if warning has been raised
            }
            else if (e.WarnIndicateChange && _resGaugeDict.ContainsKey(e.Res)) {
                _resGaugeDict[e.Res].WarnIndicate = e.Res.WarnIndicate;
            }
        }

        /**
         @fn    private void ResModelPumpChanged(object sender, ResModelPumpEventArgs e)
        
         @brief Resource model pump changed.
        
         @author    Ryan Beruldsen
         @date  21/09/2016
        
         @param sender  Source of the event.
         @param e       Resource model pump event information.
         */

        private void ResModelPumpChanged(object sender, ResModelPumpEventArgs e) {
            //on a pump event (pump has pumped x units)
            if (e.Pump && _resPumpDict.ContainsKey(e.Pumpref)) {
                _resPumpDict[e.Pumpref].DrawControl();

                //pump is selected/deselected
            }
            else if (e.SelectionChange && _resPumpDict.ContainsKey(e.Pumpref)) {
                _resPumpDict[e.Pumpref].IsSelected = e.Pumpref.IsSelected;

                //pump fault has been raised (this is independent of user input - not a warning)
            }
            else if (e.Fault && _resPumpDict.ContainsKey(e.Pumpref)) {
                _resPumpDict[e.Pumpref].PumpFault = e.Pumpref.PumpFault;
            }

            //on selection change or fault, display info to user
            if (e.SelectionChange || e.Fault) {

                _pumpStatus.Text = "";
                int selectedNo = 0;
                for (int i = 0; i < _resPumpList.Count; i++) {
                    if (_resPumpList[i].IsSelected) selectedNo++;
                }
                int curSelectedNo = 0;
                for (int i = 0; i < _resPumpList.Count; i++) {
                    var pump = _resPumpList[i];
                    if (pump.IsSelected && pump.PumpFault) {
                        _pumpStatus.Text += "[" + pump.PumpDescription + " : " +
                            (pump.FlowRate * 600).ToString("000") + "]" +
                            (++curSelectedNo != selectedNo ? " | " : "");
                    }
                    else if (_resPumpList[i].IsSelected) {
                        _pumpStatus.Text += "" + _resPumpList[i].PumpDescription + " : " +
                            (pump.FlowRate * 600).ToString("000") + "" +
                            (++curSelectedNo != selectedNo ? " | " : "");
                    }
                }
                if (selectedNo == 0) _pumpStatus.Text = "NO PUMPS ACTIVE";

            }
        }

        /**
         @fn    private void HandleInput(System.Collections.Generic.List<CCTouch> touches, CCEvent touchEvent)
        
         @brief Handles touch input
        
         @author    Ryan Beruldsen
         @date  21/09/2016
        
         @param touches     CocosSharp injected touch list
         @param touchEvent  CocosSharp injected touch event
         */

        private void HandleInput(System.Collections.Generic.List<CCTouch> touches, CCEvent touchEvent) {
            if (touches.Count > 0) {
                bool hit = false;
                for (int j = 0; j < _resPumpControlList.Count; j++) {
                    if (!_resPumpControlList[j].PumpFault) {
                        if (touches[0].Location.X > _resPumpControlList[j].minButtonBounds.X &&
                                touches[0].Location.X < _resPumpControlList[j].maxButtonBounds.X &&
                                touches[0].Location.Y > _resPumpControlList[j].minButtonBounds.Y &&
                                touches[0].Location.Y < _resPumpControlList[j].maxButtonBounds.Y) {
                            _resPumpList[j].IsSelected = !_resPumpList[j].IsSelected;

                            hit = true;
                            ModelDataIOLog.Instance.LogTouch(
                                           x: touches[0].Location.X,
                                           y: touches[0].Location.Y,
                                           status: CDRTouchData.StatusEnum.Hit
                            );
                        }
                    }
                };

                if (!hit) {
                    ModelDataIOLog.Instance.LogTouch(
                                           x: touches[0].Location.X,
                                           y: touches[0].Location.Y,
                                           status: CDRTouchData.StatusEnum.Miss
                    );
                }

            }
        }
    }
}
