﻿/*
 * CEDAR Operator Workload Assessment Tool
 * Copyright (C) 2023  aurizn
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using CocosSharp;
using System;
using System.Collections.Generic;
using CEDAR.Shared.CedarCollection;

namespace CEDAR.Shared.Modules.Task.Comms {

    /**
     @class CommsControllerSimpleSelectBarFreq
    
     @brief Communications task simple select bar, 
        communications channels are selected from a series of buttons (CommsViewSelectButton) on a horizontal bar.
    
     @author    Ryan Beruldsen
     @date  15/09/2016
     */

    class CommsControllerSimpleSelectBarFreq : DivisionContent, IDivisionContent {

        /** Selection bar location enum, normal Comms select bar is lower. */
        public enum SelectBarFreqLocation {
            Upper,
            Lower
        }

        /** Selection bar location. */
        private readonly SelectBarFreqLocation _location;

        /** main internal draw node. */
        private readonly CCDrawNode _drawNode;

        /** Global model data singleton. */
        private readonly ModelData _modelI = ModelData.Instance;

        /** The currently selected comms channel. */
        private int _selectedChannel = -1;

        /** If the bar has a fixed channel, in SC1 and SC2 modes. */
        private readonly bool _fixedChannel = false;

        /** The button reflecting the current selected freq. */
        private double _selectedFreq = 1;

        /** The button reflecting the current selected freq. */
        private double _targetFreq = 1;

        /** List of communications channel models. */
        private readonly List<CommsModelChannel> _commsChannelList;

        /** @brief Listener for button touch events. */
        private readonly CCEventListenerTouchAllAtOnce _touchListener;

        /** @brief List of communications channel select buttons. */
        private readonly List<CommsViewSelectButton> _commsButtonList;

        /** @brief Random number generator for random freq. */
        private readonly Random _rnd = new Random();


        /**
         @fn    publicCommsControllerSimpleSelectBarFreq(  
            List<CommsModelChannel> CommChannelList,
            Division division, 
            int channel = -1, 
            SelectBarFreqLocation location = SelectBarFreqLocation.Lower
        )
        
         @brief Constructor of communications channel select bar.
        
         @author    Ryan Beruldsen
         @date  20/09/2016
        
         @param CommChannelList List of communications channels.
         @param division       Parent division.
		 @param channel        Channel index, > 0 if the bar has a fixed channel, as in SC1 and SC2 modes.
		 @param location       Location, whether upper or lower bar location, default is lower.
		 
         */

        public CommsControllerSimpleSelectBarFreq(
            List<CommsModelChannel> CommChannelList,
            Division division,
            int channel = -1,
            SelectBarFreqLocation location = SelectBarFreqLocation.Lower
        ) : base(division) {

            _commsChannelList = CommChannelList;
            _commsButtonList = new List<CommsViewSelectButton>();
            _drawNode = new CCDrawNode();
            _touchListener = new CCEventListenerTouchAllAtOnce {
                OnTouchesBegan = HandleInput
            };
            AddEventListener(_touchListener, this);
            if (channel > -1) {
                _selectedChannel = channel;
                _fixedChannel = true;
            }
            _location = location;
        }

        /**
         @fn    public void Init()
        
         @brief Initialise communications channel select bar.
        
         @author    Ryan Beruldsen
         @date  20/09/2016
         */

        public void Init() {

            AddChild(_drawNode);

            //draw select bar backing rectangle
            float width = _division.Width * 0.9f;
            float height = _division.Height * 0.2f;
            float positionY = _division.PrevDivisionY + (_division.Height * 0.2f);
            if (_location == SelectBarFreqLocation.Upper) {
                positionY = _division.PrevDivisionY + (_division.Height * 0.6f);
            }
            float positionX = _division.Width * 0.05f;

            _drawNode.DrawRect(
                new CCRect(positionX, positionY, width, height),
                fillColor: new CCColor4B(100, 100, 100, 100),
                borderWidth: 0,
                borderColor: CCColor4B.White);

            _selectedFreq = Math.Round(_rnd.NextDouble() * 4);

            for (int j = 0; j < _commsChannelList.Count; j++) {
                if (_commsChannelList[j].IsSelected && !_fixedChannel) _selectedChannel = j;
                _commsChannelList[j].CommsModelEventHandlerQueue += new
                    CommsModelEventHandler(HandleComms);
            }

            //create buttons for each selectable channel model
            for (int j = 0; j < 4; j++) {
                CommsViewSelectButton button = new CommsViewSelectButton(
                    label: "",
                    index: j,
                    selected: false,
                    warnIndicate: false,
                    displayValue: "",
                    targetValue: "",
                    colourActive:
                        _modelI.GetColourPallete4B(ModelData.COLOUR_PARAM_ACTIVE),
                    colourActiveText:
                        _modelI.GetColourPallete3B(ModelData.COLOUR_PARAM_ACTIVE),
                    colourWarning:
                       _modelI.GetColourPallete4B(ModelData.COLOUR_PARAM_WARNING),
                    width: width,
                    barHeight: height,
                    position: new CCPoint(positionX, positionY),
                    division: _division
                );

                button.Init();

                AddChild(button);
                _commsButtonList.Add(button);


            }
            ResetSelectBarFreq();

        }

        /**
		@fn    public void ResetSelectBarFreq()

		@brief Set/reset frequencies and Comms model linkage to each button.

		@author    Ryan Beruldsen
		@date  20/09/201  */
        private void ResetSelectBarFreq() {

            bool silentMode = ModelData.Instance.Settings[ModelData.SILENT_MODE] == ModelData.PARAM_ON;
            bool trainMode = ModelData.Instance.Settings[ModelData.TRAINING_MODE] == ModelData.PARAM_ON;

            //currently selected button index, representing the current frequency for the channel
            var selectedButton = 0;

            //determine the selected channel
            for (int j = 0; j < _commsChannelList.Count && !_fixedChannel; j++) {
                if (_commsChannelList[j].IsSelected) _selectedChannel = j;
            }

            //determine the selected button
            for (int j = 0; j < _commsButtonList.Count; j++) {
                if (_commsButtonList[j].IsSelected) selectedButton = j;
            }

            //selected channel model
            var channel = _commsChannelList[_selectedChannel];

            //if a channel switch has not occured, and user is selecting from frequency buttons
            if (_commsButtonList[selectedButton].SecondaryLabelStr.Equals(channel.ValueStr)) {
                _selectedFreq = selectedButton;
                //if a channel switch has occured and the user has not selected a frequency, randomise
                //location of selected frequency button
            }
            else {
                _selectedFreq = Math.Round(_rnd.NextDouble() * 3);
            }

            if (!channel.TargetStr.Equals(channel.ValueStr)) {
                _targetFreq = Math.Round(_rnd.NextDouble() * 3);
                while (_targetFreq == _selectedFreq) _targetFreq = Math.Round(_rnd.NextDouble() * 3);
            }
            else {
                _targetFreq = _selectedFreq;
            }

            //create buttons for each selectable channel model
            for (int j = 0; j < 4; j++) {

                string[] voiceover = ModelDataIOConfig.VoiceOverListing();
                double[] voiceoverFreq = ModelDataIOConfig.VoiceOverFreqListing();
                int index = Convert.ToInt32(_rnd.NextDouble() * (voiceover.Length - 1));
                var freq = voiceoverFreq[index];
                if (j == _selectedFreq || (j == _targetFreq && _targetFreq == _selectedFreq)) {
                    freq = Convert.ToDouble(channel.ValueStr);
                    _commsButtonList[j].IsSelected = true;
                }
                else {
                    _commsButtonList[j].IsSelected = false;
                }
                if (j == _targetFreq && _targetFreq != _selectedFreq) {
                    if ((silentMode || trainMode) && !channel.TargetStr.Equals(channel.ValueStr)
                        && !_commsButtonList[j].IsSelected) {
                        _commsButtonList[j].WarnIndicate = true;
                    }
                    freq = Convert.ToDouble(channel.TargetStr);
                }
                else {
                    _commsButtonList[j].WarnIndicate = false;
                }
                while ((j != _selectedFreq && j != _targetFreq &&
                    (freq == channel.Frequency || freq == channel.TargetFrequency)) ||
                    (freq < channel.Minimum || freq > channel.Maximum)) {

                    index = Convert.ToInt32(_rnd.NextDouble() * (voiceover.Length - 1));
                    freq = voiceoverFreq[index];
                }
                string formatStr = "0.0";
                for (int i = 0; i < channel.Precision - 1; i++) {
                    formatStr += "0";
                }
                var freqStr = freq.ToString(formatStr) + channel.Suffix;
                _commsButtonList[j].SetDisplayStr(displayValue: freqStr, primaryLabel: channel.LabelStr);
            }
        }

        /**
         @fn    private void HandleComms(object sender, CommsModelChannelEventArgs e)
        
         @brief Handles for comms channel events (new frequency target ect).
        
         @author    Ryan Beruldsen
         @date  20/09/2016
        
         @param sender  Source of the event.
         @param e       Communications model channel event information.
         */

        private void HandleComms(object sender, CommsModelChannelEventArgs e) {

            if (e.WarnIndicateChange && _commsChannelList[_selectedChannel].WarnIndicate && _selectedChannel == e.Comms.index) {
                ResetSelectBarFreq();
            }
            else if (e.WarnIndicateChange && !_commsChannelList[_selectedChannel].WarnIndicate && _selectedChannel == e.Comms.index) {
                for (int k = 0; k < _commsButtonList.Count; k++) {
                    _commsButtonList[k].WarnIndicate = false;
                }
            }

            if (e.SelectionChange && !_fixedChannel) {
                ResetSelectBarFreq();
            }
        }

        /**
         @fn    private void HandleInput(System.Collections.Generic.List<CCTouch> touches, CCEvent touchEvent)
        
         @brief Handles touch input
        
         @author    Ryan Beruldsen
         @date  20/09/2016
        
         @param touches     CocosSharp injected touch list
         @param touchEvent  CocosSharp injected touch event
         */

        private void HandleInput(System.Collections.Generic.List<CCTouch> touches,
                                    CCEvent touchEvent) {

            if (touches.Count > 0) {
                bool hit = false;
                for (int j = 0; j < _commsButtonList.Count; j++) {

                    if (touches[0].Location.X > _commsButtonList[j].MinButtonBounds.X &&
                            touches[0].Location.X < _commsButtonList[j].MaxButtonBounds.X &&
                            touches[0].Location.Y > _commsButtonList[j].MinButtonBounds.Y &&
                            touches[0].Location.Y < _commsButtonList[j].MaxButtonBounds.Y) {

                        hit = true;
                        CDRTouchData.StatusEnum statusTouch;

                        //if a valid user input
                        if (_commsButtonList[j].WarnIndicate &&
                            !_commsButtonList[j].IsSelected &&
                            !_commsChannelList[_selectedChannel].other &&
                            _commsChannelList[_selectedChannel].Enabled) {
                            statusTouch = CDRTouchData.StatusEnum.Hit;

                            //else an unnecessary user input
                        }
                        else {
                            _commsChannelList[_selectedChannel].other = true;
                            statusTouch = CDRTouchData.StatusEnum.FalsePos;
                        }

                        //select hit button, clearing any warnings, deselect all others
                        for (int k = 0; k < _commsButtonList.Count; k++) {
                            if (k != j) {
                                _commsButtonList[k].IsSelected = false;
                            }
                            else if (_commsChannelList[_selectedChannel].Enabled) {
                                _commsButtonList[k].IsSelected = true;
                                _commsButtonList[k].WarnIndicate = false;
                            }
                        }

                        //set channel frequency to selected frequency
                        if (_commsChannelList[_selectedChannel].Enabled) {
                            _commsChannelList[_selectedChannel].Frequency =
                                Convert.ToDouble(_commsButtonList[j].SecondaryLabelStr);
                        }

                        ModelDataIOLog.Instance.LogTouch(
                                          x: touches[0].Location.X,
                                          y: touches[0].Location.Y,
                                          status: statusTouch);

                    }
                };

                if (!hit) {

                    ModelDataIOLog.Instance.LogTouch(
                        x: touches[0].Location.X,
                        y: touches[0].Location.Y,
                        status: CDRTouchData.StatusEnum.Miss
                    );

                }

            }
        }

    }
}
