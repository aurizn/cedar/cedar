﻿using System.Reflection;
using Android.App;

[assembly: AssemblyTitle ("Cedar.Droid")]
[assembly: AssemblyDescription ("")]
[assembly: AssemblyConfiguration ("")]
[assembly: AssemblyCompany ("aurizn")]
[assembly: AssemblyProduct ("Cedar OWAT")]
[assembly: AssemblyCopyright ("aurizn © 2023")]
[assembly: AssemblyTrademark ("")]
[assembly: AssemblyCulture ("")]


[assembly: AssemblyVersion ("1.0.0")]

[assembly: UsesPermission(Android.Manifest.Permission.Internet)]
[assembly: UsesPermission(Android.Manifest.Permission.WriteExternalStorage)]