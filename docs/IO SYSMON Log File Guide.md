# IO SYSMON Log File Guide

The SYSMON log file is the System Monitoring task log, providing detailed logging information on user interaction with the task. The output file is nearly identical to the MATB-II equivalent.

### MATB-II Compatibility

Complete compatibility with the caveat of some limited whitespace differences.

### Example Log File Snippet

        # 10-11-2016 17:51:00 SYSMON_2016_10111751.txt
        #
        # Events Filename: CEDAR_EVENTS.xml
        #
        # Timeout (in seconds): Lights = 10 Scales = 10
        #
        # RT = Response Time (in seconds)
        # SYS_OK = An event for the system selected is active
        #
        #-TIME-      -RT-  -SYSTEM-   -LIGHT/SCALE- -SYS_OK- -REMARKS- 
        #-------------------------------------------------------------------------------
        00:00:05.9 00.9   scale      one           true 
        00:00:08.8 00.8   light      green         true 
        00:00:19.0 -10.0  light      red                    - event timed out

### Example JSON Log File

        {
        "rows": [
            {
            "-TIME-": "00:00:06.2",
            "-RT-": "01.2",
            "-SYSTEM-": "scale",
            "-LIGHT/SCALE-": "one",
            "-SYS_OK-": "true",
            "-REMARKS-": ""
            },
            {
            "-TIME-": "00:00:08.8",
            "-RT-": "00.8",
            "-SYSTEM-": "light",
            "-LIGHT/SCALE-": "green",
            "-SYS_OK-": "true",
            "-REMARKS-": ""
            },
            {
            "-TIME-": "00:00:19.0",
            "-RT-": "-10.0",
            "-SYSTEM-": "light",
            "-LIGHT/SCALE-": "red",
            "-SYS_OK-": "",
            "-REMARKS-": "- event timed out"
            }
        ]
        }

### Example Log File Snippet Explanation

The log file begins with a header detailing the timestamp and filename of the log file, then a plain text formatted table of log entries. The timeout in seconds of the lights (in Cedar these are switches/buttons) and the scales.

The JSON format variant just lists the columns and rows of the plaintext file. Column titles are not listed in the JSON variant.

| Column | Format | Meaning |
| --- | --- | --- |
| -TIME- | 00:00:00.4 (hour:minute:second:tenth of second) | The timestamp of the event in question. |
| -RT- | Floating point, format (-)00.0 | The response time of the user to the event in seconds, accurate to the tenth of a second, negative if the event timed out. |
| -SYSTEM- | scale / light | Whether the event pertains to a System Monitoring Task scale or light. |
| -SYS_OK- | true (or empty if false) | Whether the user responded withing the specified period and the event did not timeout. |
| -REMARKS- | String | Any additional remarks. |