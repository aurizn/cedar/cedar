﻿/*
 * CEDAR Operator Workload Assessment Tool
 * Copyright (C) 2023  aurizn
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using Android.App;
using Android.Content.PM;
using Android.OS;
using CEDAR.Shared;
using System.IO;
using System.Collections.Generic;
using Newtonsoft.Json;
using Android.Media;

namespace CEDAR.Droid {

    [Activity (Label = "Cedar.Droid", Icon = "@drawable/icon", Theme = "@style/MainTheme", MainLauncher = false, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
	
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity {

        /**
         @fn    private void FileLoad()
        
         @brief Loads settings, configuration files based on file type parameters (xml/json)
        
         @author    Ryan Beruldsen
         @date  5/10/2016
         */

        private void FileLoad(string serial = "") {
            string path = Android.OS.Environment.ExternalStorageDirectory.Path + "/CEDAR";
            string settingsJSON = Path.Combine(path, "settings.json");
            if (!serial.Equals("")) {
                path = Android.OS.Environment.ExternalStorageDirectory.Path + "/CEDAR/SERIALS/" + serial;
            }

            var inputParam = ModelData.FORMAT_INPUT;
            var xmlParam = ModelData.FORMAT_TYPE_XML;
            var jsonParam = ModelData.FORMAT_TYPE_JSON;
            if (!System.IO.Directory.Exists(path)) {
                System.IO.Directory.CreateDirectory(path);
            }

            //load user-specific settings - not contained in other MATB-based config or event files
            if (File.Exists(settingsJSON) && serial.Equals("")) {
                Dictionary<string, string> settings =
                    JsonConvert.DeserializeObject<Dictionary<string, string>>
                        (File.ReadAllText(settingsJSON));

                foreach (KeyValuePair<string, string> setting in settings) {
                    ModelData.Instance.ChangeSetting(setting.Key, setting.Value);
                }
            }
            var inputParamSettings = ModelData.Instance.Settings[inputParam];

            string prefix = ModelData.Instance.Settings[ModelData.FILE_NAME];
            ModelData.Instance.LoadedFile = prefix;

            if (prefix != "") {
                prefix += "_";
            }
            string configXML = Path.Combine(path, prefix + "CONFIG.xml");
            string configJSON = Path.Combine(path, prefix + "config.json");
            string eventsXML = Path.Combine(path, prefix + "EVENTS.xml");
            string eventsJSON = Path.Combine(path, prefix + "events.json");

            //load MATB-based config file
            if (File.Exists(configXML) &&
                inputParamSettings == xmlParam) {

                //xml file loaded
                ModelDataIOConfig.Instance.ParseConfigXMLFile(configXML);
                ModelDataIOConfig.Instance.PrintIOConfig();
                //fill in any unset config items with defaults
                ModelDataIOConfig.Instance.ParseIODefault(overwrite: false);

                ModelData.CEDARConfig = true;
            } else if (File.Exists(configJSON) &&
                inputParamSettings == jsonParam) {

                //json file loaded
                ModelDataIOConfig.Instance.ParseJSONFile(configJSON);
                //fill in any unset config items with defaults
                ModelDataIOConfig.Instance.ParseIODefault(overwrite: false);

                ModelData.CEDARConfig = true;
            } else {
                //no file available, load default
                ModelDataIOConfig.Instance.ParseIODefault();

                ModelData.Instance.ChangeSetting(
                    ModelData.FILE_NAME,
                    ModelData.FILE_NAME_DEFAULT
                );
            }

            //load MATB-based events file
            if (File.Exists(eventsXML) && inputParamSettings == xmlParam) {
                //xml file loaded
                ModelDataIOEvents.Instance.ParseEventsXMLFile(eventsXML);
                ModelData.Instance.userConfig[ModelData.RANDOM_MODE] = new List<string>() {
                        ModelData.PARAM_ON,
                        ModelData.PARAM_RANDOM_ENDLESS,
                        ModelData.PARAM_OFF
                };
                ModelData.CEDAREvents = true;
            } else if (File.Exists(eventsJSON)) {
                //json file loaded
                ModelDataIOEvents.Instance.ParseJSONFile(eventsJSON);
                ModelData.Instance.userConfig[ModelData.RANDOM_MODE] = new List<string>() {
                        ModelData.PARAM_ON,
                        ModelData.PARAM_RANDOM_ENDLESS,
                };
                ModelData.CEDAREvents = true;

            } else {
                //no file available, no events available, force random events
                ModelDataIOConfig.Instance.SetConfigValue("mode", "random_mode", "true");
                ModelDataIOConfig.Instance.SetConfigValue("mode", "train_mode", "true");
                ModelData.Instance.userConfig[ModelData.RANDOM_MODE] = new List<string>() {
                        ModelData.PARAM_ON,
                        ModelData.PARAM_RANDOM_ENDLESS,
                };
                ModelData.Instance.ChangeSetting(
                    ModelData.FILE_NAME,
                    ModelData.FILE_NAME_DEFAULT
                );
            }

            //handle file scanning
            if (ModelData.Instance.Settings[ModelData.FILE_SCAN] == ModelData.PARAM_OFF) {
                ModelDataIOConfig.Instance.ProcessTestFiles();
            } else if (inputParamSettings == jsonParam) {
                string[] files = Directory.GetFiles(path, "*_events.json");
                ModelData.Instance.userConfig[ModelData.FILE_NAME] = new List<string>() {
                        ModelData.FILE_NAME_DEFAULT
                };
                foreach (string dir in files) {
                    string dirStr = dir.Substring(dir.LastIndexOf('/') + 1, dir.Length - dir.LastIndexOf('/') - 1);
                    string prefixStr = dirStr[..dirStr.IndexOf('_')];
                    if (!ModelData.Instance.userConfig[ModelData.FILE_NAME].Contains(prefixStr)) {
                        ModelData.Instance.userConfig[ModelData.FILE_NAME].Add(prefixStr);
                    }
                    ModelData.Instance.userConfigStr[prefixStr] = prefixStr;

                }
            } else if (inputParamSettings == xmlParam) {
                string[] files = Directory.GetFiles(path, "*_EVENTS.xml");
                ModelData.Instance.userConfig[ModelData.FILE_NAME] = new List<string>() {
                        ModelData.FILE_NAME_DEFAULT
                };
                foreach (string dir in files) {
                    string dirStr = dir.Substring(dir.LastIndexOf('/') + 1, dir.Length - dir.LastIndexOf('/') - 1);
                    string prefixStr = dirStr[..dirStr.IndexOf('_')];
                    if (!ModelData.Instance.userConfig[ModelData.FILE_NAME].Contains(prefixStr)) {
                        ModelData.Instance.userConfig[ModelData.FILE_NAME].Add(prefixStr);
                    }
                    ModelData.Instance.userConfigStr[prefixStr] = prefixStr;

                }
            }

            ModelData.Instance.ChangeSetting(ModelData.SERIAL_NAME, serial);

        }

        /**
         @fn    private void FileOutputFolderPerRun(string filename, 
                                            string fileContent, 
                                            bool plaintext = true,
                                            bool json = false, bool csv = false)

         @brief Android file output - will output files to a timestamp folder: cedar/data/ for each test run

         @author    Ryan Beruldsen
         @date  5/10/2016

         @param filename    Filename of the file.
         @param fileContent The file content.
         @param json Whether the content is JSON format.
         @param csv Whether the content is CSV format.
         */

        private void FileOutputFolderPerRun(string filename,
                                            string fileContent,
                                            bool plaintext = true,
                                            bool json = false, bool csv = false) {
            if (json || csv) { plaintext = false; }

            //default to task output location
            string fileFolder = filename[(filename.IndexOf('_') + 1)..];
            string path = Android.OS.Environment.ExternalStorageDirectory.Path + "/CEDAR/DATA/" + fileFolder;

            string serial = ModelData.Instance.Settings[ModelData.SERIAL_NAME];
            if (!serial.Equals("")) {
                path = Android.OS.Environment.ExternalStorageDirectory.Path +
                    "/CEDAR/DATA/" + ModelData.Instance.SessionStartTmeStr + "/" + serial + "/" + fileFolder;
            }

            //default to plaintext for output
            string filenamePath = Path.Combine(path, filename + ".txt");

            if (!System.IO.Directory.Exists(path)) {
                System.IO.Directory.CreateDirectory(path);
            }

            //if output is json, change filenamePath
            if (json) {
                filenamePath = Path.Combine(path, filename + ".json");
                File.WriteAllText(filenamePath, fileContent);

                //default plaintext output
            } else if (plaintext) {
                File.WriteAllText(filenamePath, fileContent);
            } else if (csv) {
                filenamePath = Path.Combine(path, filename + ".csv");
                File.WriteAllText(filenamePath, fileContent);
            }
            // initiate media scan and put the new things into the path array to
            // make the scanner aware of the location and the files you want to see
            MediaScannerConnection.ScanFile(this, new String[] { filenamePath }, null, null);
        }

        /**
         @fn    private void FileOutput(string filename, string fileContent, bool plaintext = true, bool json = false, bool csv = false)
        
         @brief Android file output - will output files to relevant folders determined by filename.

            - A filename of: NAME_123456 will output the file to the folder: cedar/data/NAME/
            - A filename of: settings_ will output the settings file: cedar/settings.json
        
         @author    Ryan Beruldsen
         @date  5/10/2016
        
         @param filename    Filename of the file.
         @param fileContent The file content.
         @param json Whether the content is JSON format.
         @param csv Whether the content is CSV format.
         */

        private void FileOutput(string filename, string fileContent, bool plaintext = true, bool json = false, bool csv = false) {
            if (json || csv) { plaintext = false; }
            //if one folder per test run
            bool folderPerRunMode =
                ModelDataIOConfig.Instance.GetConfigBool("mode", "folder_per_run_mode");

            string fileFolder = filename[..filename.IndexOf('_')];

            //whether the data must always be JSON and has no plaintext version
            bool jsonDataFile = fileFolder.Equals("settings") || fileFolder.Equals("analysis");

            string baseDir = "/CEDAR/DATA/";
            string serial = ModelData.Instance.Settings[ModelData.SERIAL_NAME];
            if (!serial.Equals("")) {
                baseDir = "/CEDAR/DATA/" + ModelData.Instance.SessionStartTmeStr + "/" + serial + "/";
            }
            //test output sorted into output folder structure
            if (!folderPerRunMode || jsonDataFile) {
                //default to task output location
                string path = Android.OS.Environment.ExternalStorageDirectory.Path +
                        baseDir + fileFolder;


                //if setting file output
                if (jsonDataFile && ModelData.Instance.Duration == 0) {
                    path = Android.OS.Environment.ExternalStorageDirectory.Path + "/CEDAR/";
                } else if (jsonDataFile) {
                    path = Android.OS.Environment.ExternalStorageDirectory.Path +
                        baseDir + ModelData.Instance.FileTimestamp();
                    if (!serial.Equals("")) {
                        path = Android.OS.Environment.ExternalStorageDirectory.Path +
                        baseDir + ModelData.Instance.FileTimestamp(ModelData.Instance.SessionStartTime, true);
                    }
                }

                if (!System.IO.Directory.Exists(path)) {
                    System.IO.Directory.CreateDirectory(path);
                }
                string filenamePath = Path.Combine(path, filename + ".txt");

                if (plaintext) {
                    //default to plaintext for output
                    File.WriteAllText(filenamePath, fileContent);
                }

                //if output is not plaintext (and not settings), change filenamePath
                if (json) {
                    filenamePath = Path.Combine(path, filename + ".json");
                    File.WriteAllText(filenamePath, fileContent);
                }
                if (csv) {
                    filenamePath = Path.Combine(path, filename + ".csv");
                    File.WriteAllText(filenamePath, fileContent);
                }

                //if output is settings file, change path
                if (jsonDataFile) {
                    filenamePath = Path.Combine(path, fileFolder + ".json");
                    File.WriteAllText(filenamePath, fileContent);
                }

                // initiate media scan and put the new things into the path array to
                // make the scanner aware of the location and the files you want to see
                MediaScannerConnection.ScanFile(this, new String[] { filenamePath }, null, null);
                //one folder per test run
            } else {
                FileOutputFolderPerRun(filename, fileContent, json: json, csv: csv);
            }

        }

        /**
         @fn    private void SettingsDataChanged(object sender, ModelDataEventArgs e)
        
         @brief Handler for settings data changed event - used to handle file load and output events, set by a flag change in ModelData settings
        
         @author    Ryan Beruldsen
         @date  5/10/2016
        
         @param sender  Source of the event.
         @param e       Model data event information.
         */

        private void SettingsDataChanged(object sender, ModelDataEventArgs e) {
            bool jsonOut = ModelData.Instance.Settings[ModelData.FORMAT_OUTPUT] ==
                                ModelData.FORMAT_TYPE_JSON;

            bool csvOut = ModelData.Instance.Settings[ModelData.FORMAT_OUTPUT] ==
                                ModelData.FORMAT_TYPE_CSV;

            foreach (string changedSetting in e.settingsChangeList) {
                var changedSettingVal = ModelData.Instance.Settings[changedSetting];
                if (changedSetting.Equals(ModelData.FILE_RELOAD) &&
                    changedSettingVal == ModelData.PARAM_ON) {

                    ModelData.Instance.ChangeSetting(
                        ModelData.FILE_RELOAD,
                        ModelData.PARAM_OFF
                    );
                    if (ModelDataIOConfig.Instance.GetConfigBool("mode", "test_serials")) {
                        int serialNum = int.Parse(ModelData.Instance.Settings[ModelData.SERIAL_NO]);
                        var serialList = ModelDataIOConfig.Instance.GetConfigType("test_serials");
                        int counter = 0;
                        string serialStr = "";
                        foreach (KeyValuePair<string, string> serial in serialList) {
                            if (counter++ == serialNum) {
                                serialStr = serial.Key;
                                break;
                            }
                        }
                        FileLoad(serialStr);
                    } else {
                        FileLoad();
                    }

                }
                if (changedSetting.Equals(ModelData.FILE_OUTPUT) &&
                    changedSettingVal == ModelData.PARAM_ON) {

                    ModelData.Instance.ChangeSetting(
                        ModelData.FILE_OUTPUT,
                        ModelData.PARAM_OFF
                    );

                    FileOutput(e.Filename, e.FileContents, json: jsonOut, csv: csvOut);

                }
            }
        }

        /**
         @fn    protected override void OnCreate(Bundle bundle)
        
         @brief Called when the activity is starting - setup tasks (largely CocosSharp default logic).
        
         @author    Ryan Beruldsen
         @date  5/10/2016
         */
        protected override void OnCreate(Bundle bundle) {
            //initial config file load
            FileLoad();
            System.Diagnostics.Debug.WriteLine("LOAD");
            //check if dev mode is enabled, if so, show extension sample option
            if (ModelDataIOConfig.Instance.GetConfigBool("mode", "ext")) {
                ModelData.Instance.ExtensionSample();
            }
            //listen for file output trigger
            ModelData.Instance.ModelDataEventHandler +=
                new ModelDataChangedEventHandler(SettingsDataChanged);

            base.OnCreate(bundle);
            global::Xamarin.Forms.Forms.Init(this, bundle);

            LoadApplication(new App());
        }
    }
}

