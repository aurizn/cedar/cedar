# IO RESMAN Log File Guide

The RESMAN log file is the Resource Management task log, providing detailed logging information on user interaction with the task. The output file is nearly identical to the MATB-II equivalent.

### MATB-II Compatibility

Near compatibility with the caveat of some limited whitespace differences.

The new configuration options unique to Cedar are listed additionally in the log file header, the Tank Levels in Units section. This refers to some initial primary tank levels and the minimum and maximum for the primary tanks.

### Example Log File Snippet

      # 10-11-2016 18:16:00 RESMAN_2016_10111816.txt
      #
      # Events Filename: CEDAR_EVENTS.xml
      #
      # Pump Flow Rates in Units Per Minute
      # Pump 1 = 800 Pump 2 = 600 Pump 3 = 800 Pump 4 = 600
      # Pump 5 = 600 Pump 6 = 600 Pump 7 = 400 Pump 8 = 400
      #
      # Tank Consumption Rates in Units Per Minute
      # Tank A = 800 Tank B = 800
      #
      # Tank Levels in Units
      # Initial = 2000 Capacity = 4000 Minimum = 1400 Maximum = 2600
      #
      # Recording Interval is 5 seconds
      #
      # N = Recording trigged by a pump action 
      # Y = Recording trigged by a timer, and not a pump action 
      #
      #-ELAPSED TIME -PUMP # -PUMP ACTION -TANK UPDATE -TANK A -TANK B -TANK C -TANK D 
      #----------------------------------------------------------------------------------------------
      00:00:02.7    3       on          n             1978    1978    1000    1000 
      00:00:03.1    4       on          n             1972    1978    1000    993 
      00:00:04.3                        y             1957    1989    1000    978 
      00:00:05.0    1       fail        n             1955    1996    1000    969

### Example JSON Log File

      {
         "rows":[
            {
               "-ELAPSED TIME":"00:00:02.9",
               "-PUMP #":"4",
               "-PUMP ACTION":"on",
               "-TANK UPDATE":"n",
               "-TANK A":"1976",
               "-TANK B":"1976",
               "-TANK C":"1000",
               "-TANK D":"1000"
            },
            {
               "-ELAPSED TIME":"00:00:04.5",
               "-PUMP #":"6",
               "-PUMP ACTION":"on",
               "-TANK UPDATE":"n",
               "-TANK A":"1954",
               "-TANK B":"1970",
               "-TANK C":"1000",
               "-TANK D":"1000"
            }
         ]
      }

### Example Log File Snippet Explanation

The log file begins with a header detailing the timestamp and filename of the log file, then a plain text formatted table of log entries. Initial tank parameters are also listed in the header (see _Config File Guide_ for more information on setting these parameters).

The JSON format variant just lists the columns and rows of the plaintext file. Column titles are not listed in the JSON variant.

| Column | Format | Meaning |
| --- | --- | --- |
| -ELAPSED TIME- | 00:00:00.4 (hour:minute:second:tenth of second) | The timestamp of the event in question. |
| -PUMP ACTION | Integer | The pump number to which the log row data pertains. |
| -TANK UPDATE | y/n | Whether the log row is part of a scheduled tank update – designed to track the levels of the tanks over time to give an indication of the users performance in the task. |
| -TANK (A-D) | Integer | Tank levels at that moment in time. |