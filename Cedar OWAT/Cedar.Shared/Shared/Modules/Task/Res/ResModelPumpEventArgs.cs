﻿/*
 * CEDAR Operator Workload Assessment Tool
 * Copyright (C) 2023  aurizn
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


using System;
using System.Collections.Generic;
using System.Text;

namespace CEDAR.Shared.Modules.Task.Res
{
    /**
     @class ResModelPumpEventArgs
    
     @brief Resources pump event parameters
    
     @author    Ryan Beruldsen
     @date  15/09/2016
     */

    internal class ResModelPumpEventArgs : EventArgs {

        /**
         @property  public bool valueChange
        
         @brief Pump rate value change event
        
         @return    true if value change, false if not.
         */

        public bool ValueChange { get; set; }

        /**
         @property  public bool pump
        
         @brief Pump flow event, represents flow of X units by active tank.
        
         @return    true if pump flow event, false if not.
         */

        public bool Pump { get; set; }

        /**
         @property  public bool fault
        
         @brief Pump fault state event raise: while faulted pump cannot operate.
        
         @return    true if fault, false if not.
         */

        public bool Fault { get; set; }

        /**
         @property  public ResModelPump pumpref
        
         @brief Gets or sets the pump model reference.
        
         @return    The pumpref.
         */

        public ResModelPump Pumpref { get; set; }

        /**
         @property  public bool selectionChange
        
         @brief Pump selection change event, (toggles whether pump is active)
        
         @return    true if selection change, false if not.
         */

        public bool SelectionChange { get; set; }
    }
}
