﻿/*
 * CEDAR Operator Workload Assessment Tool
 * Copyright (C) 2023  aurizn
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;

namespace CEDAR.Shared.Modules.Task.Comms {
    internal delegate void CommsModelEventHandler(object sender, CommsModelChannelEventArgs e);

    /**
     @class CommsModelChannel
    
     @brief Communications channel model, represents a virtual radio. 
        
     The radio must always be set to a proscribed frequency or will transmit a warning event. Proscribed frequency for the radio can be changed (this will raise a warning if the current frequency does not match the new proscribed frequency).
    
     @author    Ryan Beruldsen
     @date  15/09/2016
     */

    class CommsModelChannel
    {
        /** @brief Enabled status of model. */
        private bool _enabled = false;

        /**
         @property  public bool enabled
        
         @brief Gets or sets a value indicating whether the model is enabled.
        
         @return    true if enabled, false if not.
         */

        public bool Enabled {
            get { return _enabled; }
            set {
                _enabled = value;
            }
        }

        /** @brief The current frequency value of the channel. */
        private double _frequency;

        /** @brief The frequency suffix for display purposes (eg Hz). */
        private readonly string _suffix;

        /** @brief The minimum frequency range. */
        private readonly double _min;

        /** @brief The maximum frequency range. */
        private readonly double _max;

        /** @brief Frequency increment/division. */
        private readonly double _increment;

        /** @brief The precision (number of decimals) of the frequency. */
        private readonly int _precision;

        /** @brief Selection state of comms channel. */
        private bool _selected;

        /** @brief The label of the comms channel. */
        private readonly string _label;

        /** @brief The (unique) index of the comms channel. */
        public int index;

        /** @brief Warning state of the comms channel, warning if target frequency does not equal actual frequency. */
        private bool _warn;

        /** @brief The target frequency for the channel. */
        private double _target = -1;

        /** @brief Event queue for all listeners interested in commsModelChanged events (any comms channel related events). */
        public event CommsModelEventHandler CommsModelEventHandlerQueue;
        public bool other = false;
        /**
         @property  public int eventIndex
        
         @brief Gets or sets the event index of the event - for logging metrics.
        
         @return    The event index.
         */

        public int EventIndex { get; set; }

        /**
         @property  public double commIncrement
        
         @brief Gets the communications frequency increment.
        
         @return    The communications increment.
         */

        public double CommIncrement {
            get { return _increment; }
        }

        /**
         @property  public double precision
        
         @brief Gets the communications frequency precision.
        
         @return    The communications frequency precision.
         */

        public double Precision {
            get { return _precision; }
        }

        /**
         @property  public string suffix

         @brief Gets the communications frequency suffix.

         @return    The communications frequency suffix.
         */

        public string Suffix {
            get { return _suffix; }
        }

        /**
         @property  public double minimum
        
         @brief Gets the minimum frequency range.
        
         @return    The minimum value.
         */

        public double Minimum {
            get { return _min; }
        }

        /**
         @property  public double maximum
        
         @brief Gets the maximum frequency range.
        
         @return    The maximum value.
         */

        public double Maximum {
            get { return _max; }
        }

        /**
         @property  public double targetPercent
        
         @brief Gets or sets target frequency percent within min, max range.
        
         @return    The target percent.
         */

        public double TargetPercent {
            get {
                
                return (_target - Minimum) > 0 ? (_target - Minimum)/(Maximum-Minimum) : 0;
            }
            set {
                double tempVal = Math.Round(
                    Minimum + (Maximum - Minimum) * value, _precision);

                if (tempVal % _increment != 0) tempVal -= (tempVal % _increment);
                if (tempVal < 0) tempVal = 0;
                TargetFrequency = tempVal;
            }
        }

        /**
         @property  public double percent
        
         @brief Gets or sets the frequency percent percent within min, max range.
        
         @return    The percent.
         */

        public double Percent {
            get { return (_frequency - Minimum) != 0 ? (_frequency- Minimum) / (Maximum-Minimum) : 0; }
            set {
                if (_enabled) {
                    this.Frequency = Minimum + (Maximum - Minimum) * value;
                    CommsModelChannelEventArgs e = new CommsModelChannelEventArgs {
                        ValueChange = true,
                        Comms = this
                    };
                    CommsModelChannelChanged(e);
                }
            }
        }

        /**
         @property  public bool warnIndicate
        
         @brief Gets or sets the warning indication for the channel.
        
         @return    true if warning indicated, false if not.
         */

        public bool WarnIndicate {
            get { return _warn; }
            set {
                if (_enabled || !(_enabled && _warn && !value)) {
                    var e = new CommsModelChannelEventArgs {
                        WarnIndicateChange = true,
                        Comms = this
                    };
                    bool tempVal = _warn;
                    _warn = value;
                    if (tempVal != value) CommsModelChannelChanged(e);
                }
            }
        }

        /**
         @property  public double targetFrequency
        
         @brief Gets or sets target frequency.
        
         @return    The target frequency.
         */

        public double TargetFrequency {
            get { return _target; }
            set {
                if (_enabled) {
                    _target = value;

                    if (_target < Minimum) _target = Minimum;
                    if (_target > Maximum) _target = Maximum;
                    var e = new CommsModelChannelEventArgs();
                    if (_frequency != _target) {
                        e.WarnIndicateChange = true;
                        _warn = true;
                    }
                    e.Comms = this;
                    CommsModelChannelChanged(e);
                }
            }
        }

        /**
         @property  public bool isSelected
        
         @brief Gets or sets a value indicating whether this object is selected.
        
         @return    true if this object is selected, false if not.
         */

        public bool IsSelected {
            get { return _selected; }
            set {
                if (_enabled) {
                    var e = new CommsModelChannelEventArgs {
                        SelectionChange = true
                    };
                    _selected = value;
                    e.Comms = this;
                    CommsModelChannelChanged(e);
                }
            }
        }

        /**
         @property  public double frequency
        
         @brief Gets or sets the frequency value (truncated to min, max if necessary).
        
         @return    The frequency.
         */

        public double Frequency {
            get { return _frequency; }
            set {
                if (_enabled) {
                    if (value >= 0 && value >= Minimum && value <= Maximum) {
                        _frequency = value;
                    }

                    if (value > Maximum) _frequency = Maximum;
                    if (value < Minimum) _frequency = Minimum;

                    double tempVal = Convert.ToDouble(
                        ValueStr.Substring(0, ValueStr.Length - _suffix.Length));
                    double tempValTarget = Convert.ToDouble(
                        TargetStr.Substring(0, TargetStr.Length - _suffix.Length));
                    if (tempVal == tempValTarget) {
                        WarnIndicate = false;
                    } else if (!_warn) {
                        WarnIndicate = true;
                    }
                    var e = new CommsModelChannelEventArgs {
                        ValueChange = true,
                        Comms = this
                    };
                    CommsModelChannelChanged(e);
                }
                
            }
        }

        /**
         @property  public string labelStr
        
         @brief Gets the channel label string.
        
         @return    The channel label string.
         */

        public string LabelStr {
            get { return _label;  }
        }

        /**
         @fn    protected virtual void CommsModelChannelChanged(CommsModelChannelEventArgs e)
        
         @brief Raises comms channel events
        
         @author    Ryan Beruldsen
         @date  20/09/2016
        
         @param e   Communications model channel event information.
         */

        protected virtual void CommsModelChannelChanged(CommsModelChannelEventArgs e) {
            CommsModelEventHandlerQueue?.Invoke(this, e);
        }

        /**
         @property  public string targetStr
        
         @brief Gets the formatted frequency target string (truncated to increment if required)

         @return    The formatted frequency target string.
         */

        public string TargetStr {
            get {
                double freqTemp = Math.Round(_target, _precision);
                double dec = Math.Round(_target - Math.Floor(_target), _precision);

                if (dec % _increment < _increment / 2) freqTemp -= (dec % _increment);
                if (dec % _increment >= _increment / 2) freqTemp += _increment - (dec % _increment);

                freqTemp = Math.Round(freqTemp, _precision);
                string formatStr = "0.0";
                for(int i = 0; i < _precision - 1; i++) {
                    formatStr += "0";
                }
                if (_target == Minimum) freqTemp = Minimum;
                if (_target == Maximum) freqTemp = Maximum;
                return freqTemp.ToString(formatStr) + _suffix;
            }
        }

        /**
         @property  public string valueStr
        
         @brief Gets the formatted frequency string (truncated to increment if required)
        
         @return    The formatted frequency string.
         */

        public string ValueStr {
            get {
                double freqTemp = Math.Round(_frequency, _precision);
                double dec = Math.Round(_frequency - Math.Floor(_frequency), _precision);

                if (dec % _increment < _increment/2) freqTemp -= (dec % _increment);
                if (dec % _increment >= _increment / 2) freqTemp += _increment - (dec % _increment);

                freqTemp = Math.Round(freqTemp, _precision);

                if (_frequency == Minimum) freqTemp = Minimum;
                if (_frequency == Maximum) freqTemp = Maximum;
                string formatStr = "0.0";
                for (int i = 0; i < _precision - 1; i++) {
                    formatStr += "0";
                }
                return freqTemp.ToString(formatStr) + _suffix;
            }
        }

        /**
         @fn    public CommsModelChannel(string label, double startPerc, string suffix, double min, double max, double increment, int precision, int index, bool isSelected)
        
         @brief Constructor for the communications channel model (representing a discrete radio module with one variable channel)
        
         @author    Ryan Beruldsen
         @date  20/09/2016
        
         @exception ArgumentException   Thrown when one or more arguments have unsupported or illegal
                                        values.
        
         @param label       The label of the channel
         @param startPerc   The starting percent of the channel (between min and max ranges), value 0-1
         @param suffix      The suffix for display purposes (eg Hz)
         @param min         The minimum frequency range.
         @param max         The maximum frequency range.
         @param increment   The increment of the frequency (divisions in a unit, eg 0.5 for two divisions to a unit)
         @param precision   The precision (number of decimal places) of the channel.
         @param index       The unique index of the channel for id.
         @param isSelected  The starting selection state of the channel.
         */

        public CommsModelChannel(   string label, 
                                    double startPerc, 
                                    string suffix, 
                                    double min, 
                                    double max, 
                                    double increment, 
                                    int precision, 
                                    int index, 
                                    bool isSelected) {

            _frequency = min + (max-min)* startPerc;
		    _target = _frequency;
            this._suffix = suffix;
            this._min = min;
            this._label = label;
            this._max = max;
            this.index = index;
            this._selected = isSelected;
            if (increment > 1) throw new System.ArgumentException("increment must be < 1.0", "original");
            if (startPerc > 1) throw new System.ArgumentException("startPerc must be < 1.0", "original");
            this._increment = increment;
            this._precision = precision;
        }


    }
}
