﻿/*
 * CEDAR Operator Workload Assessment Tool
 * Copyright (C) 2023  aurizn
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using CocosSharp;
using CEDAR.Shared.CedarCollection;
using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace CEDAR.Shared {

    /**
     @fn    internal delegate void ModelDataChangedEventHandler(object sender, ModelDataEventArgs e);
    
     @brief Delegate for handling ModelDataChanged events.
    
     @author    Ryan Beruldsen
     @date  14/09/2016
    
     @param sender  Source of the event.
     @param e       Model data event information.
     */

    public delegate void ModelDataChangedEventHandler(object sender, ModelDataEventArgs e);

    /**
     @class ModelData
    
     @brief Singleton class for whole of app settings and test run configuration
    
     @author    Ryan Beruldsen
     @date  15/09/2016
     */

    public sealed class ModelData {

        /** @brief Cedar app version number. */
        public static string CEDAR_VERSION = "1.8.0";

        /** @brief Cedar app unique device id. */
        public static string deviceId = "";

        /** @brief Configuration file loaded state. */
        private static bool _config = false;

        /** @brief Events file loaded state. */
        private static bool _events = false;

        /** @brief The screen width. */
        private static int _width = 768;

        /** @brief The screen height. */
        private static int _height = 1024;

        /** @brief Screen page index. */
        private static string _pageIndex = "title";

        /** @brief The last score. */
        private long _lastScore = -1;

        /** @brief The best score. */
        private long _bestScore = -1;

        /** @brief Current Test Serial step. */
        private int _serialStep = 1;

        /** @brief Current Test Serial step. */
        private HashSet<string> _serialComplete = new HashSet<string>();

        /** @brief The current test session duration. */
        private long _duration = 0;

        /** @brief Timestamp of second tick. */
        private long _durationStamp = 0;

        /** @brief  The start time timestamp of the current test session.*/
        private long _sessionStartTime = 0;

        /** @brief  The timestamp of the last time the OS initiated a pause on the app.*/
        private long _pauseTimeStamp = 0;

        /** @brief  current string identifier of the loaded config and events files.*/
        private string _loadedFile = "";

        /** @brief  The filename string generated from the start time of the current test session. */
        private string _sessionStartTimeFileStr;

        /** @brief Timestamp of second tick on pause state. */
        private long _durationPauseStamp = 0;

        /** @brief The tenth of a second the duration pause incremented a second. */
        private long _durationPause = 0;

        /** @brief The timestamp the app was backgrounded (and paused). */
        private long _systemPause = 0;

        /** @brief iOS state - whether to enable iOS specific info/behaviour. */
        private static bool _ios = false;

        /** @brief Event queue for all listeners interested in modelDataEventHandler events. */
        public event ModelDataChangedEventHandler ModelDataEventHandler;

        /** @brief Current questionnaire in progres. */
        private string _currentQ = "";

        /** @brief Timestamp of test termination. */
        private long _termination = 0;

        /** @brief The singleton data model instance. */
        internal static readonly ModelData _instance = new ModelData();

        /** @brief Number of vertical divisions on the test session screen. */
        public const int NUM_DIVISIONS = 3;

        //settings hashmap keys and values
        public const string TASK_COMMUNICATIONS = "TASK_COMMUNICATIONS";
        public const string TASK_COMMUNICATIONS_SIMPLE_1 = "TASK_COMMUNICATIONS_SIMPLE_1";
        public const string TASK_COMMUNICATIONS_SIMPLE_2 = "TASK_COMMUNICATIONS_SIMPLE_2";
        public const string TASK_COMMUNICATIONS_SIMPLE_3 = "TASK_COMMUNICATIONS_SIMPLE_3";
        public const string TASK_RESOURCE_MANAGEMENT = "TASK_RESOURCE_MANAGEMENT";
        public const string TASK_SYSTEM_MONITORING = "TASK_SYSTEM_MONITORING";
        public const string TASK_EXTENSION_SAMPLE = "TANK_EXTENSION_SAMPLE";

        public const string TASK_DIVISION = "TASK_DIVISION";
        public const string FILE_RELOAD = "FILE_RELOAD";
        public const string FILE_OUTPUT = "FILE_OUTPUT";

        public const string FORMAT_INPUT = "FORMAT_INPUT";
        public const string FORMAT_OUTPUT = "FORMAT_OUTPUT";

        public const string REGISTRATION = "REGISTRATION";
        public const string REGISTERED = "REGISTERED";
        public const string REGISTERED_OFFLINE = "REGISTERED_OFFLINE";

        public const string REGISTRATION_DEFAULT = "000000";

        public const string SERIAL_NO = "SERIAL_NO";
        public const string SERIAL_NAME = "SERIAL_NAME";

        public const string FILE_NAME = "FILE_NAME";
        public const string FILE_SCAN = "FILE_SCAN";

        public const string FILE_NAME_DEFAULT = "";

        public const string FORMAT_TYPE_JSON = "FORMAT_TYPE_JSON";
        public const string FORMAT_TYPE_ALL = "FORMAT_TYPE_ALL";
        public const string FORMAT_TYPE_XML = "FORMAT_TYPE_XML";
        public const string FORMAT_TYPE_PLAINTEXT = "FORMAT_TYPE_PLAINTEXT";
        public const string FORMAT_TYPE_CSV = "FORMAT_TYPE_CSV";

        public const string TIMING_UNIX = "TIMING_UNIX";
        public const string RANDOM_MODE = "RANDOM_MODE";
        public const string SILENT_MODE = "SILENT_MODE";
        public const string TRAINING_MODE = "TRAINING_MODE";

        public const string OS_PAUSE = "OS_PAUSE";
        public const string OS_DROID_BACK = "BUTTON_DROID_BACK";

        public const string PARAM_HIGH_CONTRAST_COLOUR = "COLOUR_PARAM_HIGH_CONTRAST";
        public const string PARAM_ON = "PARAM_ON";
        public const string PARAM_OFF = "PARAM_OFF";
        public const string PARAM_RANDOM_ENDLESS = "PARAM_RANDOM_ENDLESS";

        public const string TASK_DIVISION_1 = "TASK_DIVISION_1";
        public const string TASK_DIVISION_2 = "TASK_DIVISION_2";
        public const string TASK_DIVISION_3 = "TASK_DIVISION_3";

        public const string COLOUR_PARAM_WARNING = "COLOUR_WARNING";
        public const string COLOUR_PARAM_RANGE = "COLOUR_RANGE";
        public const string COLOUR_PARAM_NEUTRAL = "COLOUR_NEUTRAL";
        public const string COLOUR_PARAM_ACTIVE = "COLOUR_ACTIVE";
        public const string COLOUR_PARAM_INACTIVE = "COLOUR_INACTIVE";

        public const string COLOUR_WHITE = "COLOUR_WHITE";
        public const string COLOUR_GREY = "COLOUR_GREY";
        public const string COLOUR_YELLOW = "COLOUR_YELLOW";
        public const string COLOUR_RED = "COLOUR_RED";
        public const string COLOUR_BLUE = "COLOUR_BLUE";
        public const string COLOUR_GREEN = "COLOUR_GREEN";

        public const string WIDTH_OFFSET = "WIDTH_OFFSET";

        public const string WIDTH_OFFSET_0 = "WIDTH_OFFSET_0";
        public const string WIDTH_OFFSET_5 = "WIDTH_OFFSET_5";
        public const string WIDTH_OFFSET_10 = "WIDTH_OFFSET_10";

        public const string DESIGNATOR_1 = "DESIGNATOR_1";
        public const string DESIGNATOR_2 = "DESIGNATOR_2";
        public const string DESIG_A = "A";
        public const string DESIG_B = "B";
        public const string DESIG_C = "C";
        public const string DESIG_D = "D";
        public const string DESIG_E = "E";
        public const string DESIG_F = "F";

        public const string CEDAR_SETTINGS = "CEDAR_SETTINGS";

        public const string MODE_DISPLAY = "MODE_DISPLAY";
        public const string MODE_CLEAR_ANALYSIS_ON_PAUSE = "MODE_CLEAR_ANALYSIS_ON_PAUSE";

        public const string NONE = "NONE";


        /**
         @property  public static string currentQ

         @brief Gets or sets the current string identifier of the loaded config and events files.
         - this represents the prefix of the loaded file, ie: test for the file "test_config.json"

        @return The current string identifier of the loaded config and events files.  */
        public string LoadedFile {
            get {
                return _loadedFile;
            }
            set {
                _loadedFile = value;
            }
        }

        /**
         @property  public static string currentQ
        
         @brief Gets or sets the current questionnaire key string.
        
         @return    The current questionnaire key string string.
         */
        public string CurrentQ {
            get {
                return _currentQ;
            }
            set {
                _currentQ = value;
            }
        }




        /**
         @property  public long termination

         @brief Gets or sets the the session termination timestamp.

         @return    The current session termination timestamp.  */
        public long Termination {
            get {
                return _termination;
            }
            set {
                _termination = value;
            }

        }

        /**
         @property  public double durationDouble

         @brief Gets the current session duration in seconds and tenths

        @return The current session duration in seconds and tenths.  */
        public double DurationDouble {
            get {
                return _duration + (CurrentTenth() / 10.0);
            }

        }

        /**
         @property  public double durationDouble

         @brief Gets or sets the current test serial sequential number.

         @return The current test serial sequential number.  */
        public int SerialStep {
            get {
                return _serialStep;
            }
            set {
                _serialStep = value;
            }

        }

        /**
         @property  public double durationDouble

         @brief Gets or sets a string Set of serial identifiers representing completed serials.

         @return A string Set of serial identifiers representing completed serials.  */
        public HashSet<string> SerialComplete {
            get {
                return _serialComplete;
            }
            set {
                _serialComplete = value;
            }

        }

        /**
         @property  public static string currentPageStr
        
         @brief Gets or sets the current screen page string.

            - on change, a page navigate event will be raised to change the page if the string is valid
        
         @return    The current screen page string.
         */

        public static string CurrentPageStr {
            get {
                return _pageIndex;
            }
            set {
                ModelDataEventArgs e = new ModelDataEventArgs {
                    PageNavigate = true
                };
                _pageIndex = value;
                _instance.ModelDataChanged(e);
            }
        }

        /**
         @property  public long duration
        
         @brief Gets or sets the current test session duration.
        
         @return    The duration.
         */

        public long Duration {
            get { return _duration; }
            set {
                _duration = value;
                _durationStamp = Timestamp();
            }
        }

        /**
         @property  public long durationPause
        
         @brief Gets or sets the test session duration pause length.
        
         @return    The duration pause.
         */

        public long SystemPause {
            get {
                return _systemPause;
            }
            set {
                _systemPause = value;
            }
        }

        /**
         @property  public long durationPause
        
         @brief Gets or sets the test session duration pause length.
        
         @return    The duration pause.
         */

        public long DurationPause {
            get {
                return _durationPause;
            }
            set {
                _durationPauseStamp = Timestamp();
                _durationPause = value;

            }
        }

        /**
         @property  public string durationStr
        
         @brief Gets the current test session duration string in format 0:00:00.
        
         @return    The duration string.
         */

        public string DurationStr {
            get { return TimeString(_duration); }
        }

        /**
         @property  public string durationStr
        
         @brief Gets the current test session duration string in format 00:00:00.
        
         @return    The duration string.
         */
        public string DurationStr24Hr {
            get { return TimeString24Hr(_duration); }
        }

        /**
         @property  public long lastScore
        
         @brief Gets or sets the last score.
        
         @return    The last score.
         */

        public long LastScore {
            get {
                return _lastScore;
            }
            set {
                _lastScore = value;
                if (_lastScore > _bestScore) _bestScore = _lastScore;
            }
        }

        /**
         @property  public long bestScore
        
         @brief Gets the best score.
        
         @return    The best score.
         */

        public long BestScore {
            get {
                return _bestScore;
            }
        }


        public void Dialog(string message, Func<bool, bool> callback) {
            ModelDataEventArgs e = new ModelDataEventArgs {
                Dialog = true,
                DialogMessage = message,
                DialogCallback = callback
            };
            _instance.ModelDataChanged(e);
        }

        /**
         @fn    public static string TimestampStr(long timestamp)

         @brief Formats a string of format 00:00:00.0 from a timestamp (milliseconds)
            
            - the timestamp should not be formatted from 1974, but a shorter duration of under 24hrs 

         @author    Ryan Beruldsen
         @date  6/10/2016

         @return    String of format 00:00:00.0 from a timestamp (milliseconds)
         */

        public static string TimestampStr(long timestamp) {
            long durationMills = timestamp;
            long durationSecs = durationMills / 1000;
            String timeSecondsStr = ModelData.TimeString(durationSecs);

            while (durationMills > 1000) {
                durationMills -= 1000;
            }

            int tenth = Convert.ToInt32(((double)durationMills / 1000.0) * 10.0);
            if (tenth > 9) {
                tenth = 0;
            }

            return timeSecondsStr + "." + tenth;
        }

        /**
         @fn    public static long Timestamp()

         @brief Gets the current system timestamp (milliseconds since 1/1/1970).

         @author    Ryan Beruldsen
         @date  6/10/2016

         @return    A long.
         */

        public static long Timestamp() {
            return Convert.ToInt64(
                            DateTime.Now.ToUniversalTime().Subtract(
                            new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)
                            ).TotalMilliseconds
                        );
        }

        /**
         @fn    public static DateTime UnixTimeStampToDateTime(long unixTimeStamp)

         @brief Converts unix timestamp to DateTime object.

         @author    Ryan Beruldsen
         @date  6/10/2016

         @return    A DateTime object wrapper for the input unix timestamp.
         */
        public static DateTime UnixTimeStampToDateTime(long unixTimeStamp) {
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddMilliseconds(unixTimeStamp);
            return dtDateTime;
        }

        /**
         @fn    public string TimeString(long duration)
        
         @brief Gets a current test session duration (or other) time string in format 0:00:00.
        
         @author    Ryan Beruldsen
         @date  6/10/2016
        
         @param duration    The current test session (or other) duration.
        
         @return    A time string in format 0:00:00.
         */

        public static string TimeString(long durationSeconds) {
            long seconds = durationSeconds % 60;
            double minutes = Math.Floor(Convert.ToDouble(durationSeconds) / 60);
            double hours = Math.Floor(Convert.ToDouble(minutes) / 60);
            return hours.ToString("0") + ":" + (minutes % 60).ToString("00") + ":" + seconds.ToString("00");
        }

        /**
         @fn    public string UnixOutput()
        
         @brief Gets the additional unix timestemp if enabled in user settings.
        
         @author    Ryan Beruldsen
         @date  27/07/2018
        
         @return    A time string in format 1532654053 or an empty string.
         */
        public string UnixOutput() {
            string output = "";
            if (_instance.Settings[TIMING_UNIX] == PARAM_ON) {
                output = ", " + Timestamp();
            }
            return output;
        }

        /**
         @fn    public string timeString24Hr(long duration)
        
         @brief Gets a current test session duration time string in format 00:00:00.
        
         @author    Ryan Beruldsen
         @date  6/10/2016
        
         @param duration    The current test session (or other) duration.
        
         @return    A time string in format 0:00:00.
         */
        public string TimeString24Hr(long durationSeconds) {
            long seconds = durationSeconds % 60;
            double minutes = Math.Floor(Convert.ToDouble(durationSeconds) / 60);
            double hours = Math.Floor(Convert.ToDouble(minutes) / 60);

            return hours.ToString("00") + ":" + (minutes % 60).ToString("00") + ":" + seconds.ToString("00") + "." +
                    CurrentTenth() + UnixOutput();
        }

        /**
         @fn    public int CurrentTenth()
         @return    The current tenth of a millisecond
         */
        public int CurrentTenth() {
            long durationMills = _durationStamp != 0 ?
                ModelData.Timestamp() - _durationStamp : 0;

            int tenth = Convert.ToInt32(((double)durationMills / 1000.0) * 10.0);


            while (tenth > 9) {
                tenth -= 10;
            }

            return tenth;
        }

        /**
         @fn    public int CurrentSessionPauseTenth()
         @return    The current tenth of a millisecond, relative to session pause time
         */
        public int CurrentSessionPauseTenth() {
            long durationMills = _durationPauseStamp != 0 ?
                ModelData.Timestamp() - _durationPauseStamp : 0;

            int tenth = Convert.ToInt32(((double)durationMills / 1000.0) * 10.0);

            if (tenth > 9) {
                tenth = 9;
            }

            return tenth;
        }

        /**
         @fn    public CCColor4B getColourPallete4B(string component, byte alpha = 255)
        
         @brief Gets google material colour pallete given certain COLOUR_ constants 
            
            - 4 byte colour (with optional alpha, set to 255 if not specified)
        
         @author    Ryan Beruldsen
         @date  6/10/2016
        
         @param component   The COLOUR_ constant to return material colour for.
         @param alpha       (Optional) The alpha component of the colour.
        
         @return    The colour pallete 4 b.
         */

        public CCColor4B GetColourPallete4B(string component, byte alpha = 255) {
            if (settingsInternal[PARAM_HIGH_CONTRAST_COLOUR] == PARAM_OFF &&
                settingsInternal.ContainsKey(component)) {

                if (settingsInternal[component] == COLOUR_YELLOW) {
                    return new CCColor4B(205, 220, 57, alpha); //google material lime 500 #FFEB3B
                }
                else if (settingsInternal[component] == COLOUR_BLUE) {
                    return new CCColor4B(33, 150, 243, alpha); //google material blue 500 #2196F3
                }
                else if (settingsInternal[component] == COLOUR_GREEN) {
                    return new CCColor4B(76, 175, 80, alpha); //google material green 500 #4CAF50
                }
                else if (settingsInternal[component] == COLOUR_RED) {
                    return new CCColor4B(244, 67, 54, alpha); //google material red 500 #F44336
                }
                else if (settingsInternal[component] == COLOUR_GREY) {
                    return CCColor4B.Gray;
                }
                else if (settingsInternal[component] == COLOUR_WHITE) {
                    return CCColor4B.White;
                }
                else {
                    return CCColor4B.Black;
                }
            }
            else if (settingsInternal.ContainsKey(component)) {
                if (settingsInternal[component] == COLOUR_YELLOW) {
                    return CCColor4B.Yellow;
                }
                else if (settingsInternal[component] == COLOUR_BLUE) {
                    return CCColor4B.Blue;
                }
                else if (settingsInternal[component] == COLOUR_GREEN) {
                    return CCColor4B.Green;
                }
                else if (settingsInternal[component] == COLOUR_RED) {
                    return CCColor4B.Red;
                }
                else if (settingsInternal[component] == COLOUR_GREY) {
                    return CCColor4B.Gray;
                }
                else if (settingsInternal[component] == COLOUR_WHITE) {
                    return CCColor4B.White;
                }
                else {
                    return CCColor4B.Black;
                }
            }
            else {
                return CCColor4B.Black;
            }
        }

        /**
         @fn    public CCColor3B getColourPallete3B(string component)
        
         @brief Gets google material colour pallete given certain COLOUR_ constants 
            
            - 3 byte colour (generally for text colours)
        
         @author    Ryan Beruldsen
         @date  6/10/2016
        
         @param component   The COLOUR_ constant to return material colour for.
         */
        public CCColor3B GetColourPallete3B(string component) {

            if (settingsInternal[PARAM_HIGH_CONTRAST_COLOUR] == PARAM_OFF &&
                settingsInternal.ContainsKey(component)) {

                if (settingsInternal[component] == COLOUR_YELLOW) {
                    return new CCColor3B(205, 220, 57);
                }
                else if (settingsInternal[component] == COLOUR_BLUE) {
                    return new CCColor3B(33, 150, 243);
                }
                else if (settingsInternal[component] == COLOUR_GREEN) {
                    return new CCColor3B(76, 175, 80);
                }
                else if (settingsInternal[component] == COLOUR_RED) {
                    return new CCColor3B(244, 67, 54);
                }
                else if (settingsInternal[component] == COLOUR_GREY) {
                    return CCColor3B.Gray;
                }
                else if (settingsInternal[component] == COLOUR_WHITE) {
                    return CCColor3B.White;
                }
                else {
                    return CCColor3B.Black;
                }
            }
            else if (settingsInternal.ContainsKey(component)) {
                if (settingsInternal[component] == COLOUR_YELLOW) {
                    return CCColor3B.Yellow;
                }
                else if (settingsInternal[component] == COLOUR_BLUE) {
                    return CCColor3B.Blue;
                }
                else if (settingsInternal[component] == COLOUR_GREEN) {
                    return CCColor3B.Green;
                }
                else if (settingsInternal[component] == COLOUR_RED) {
                    return CCColor3B.Red;
                }
                else if (settingsInternal[component] == COLOUR_GREY) {
                    return CCColor3B.Gray;
                }
                else if (settingsInternal[component] == COLOUR_WHITE) {
                    return CCColor3B.White;
                }
                else {
                    return CCColor3B.Black;
                }
            }
            else {
                return CCColor3B.Black;
            }

        }


        /** @brief Cedar internal settings 
        
            - not set by external event or config file.
            - can be loaded from settings.json if existing
            - saves to new settings.json if not existing
        */
        private readonly Dictionary<string, string> settingsInternal =
        new Dictionary<string, string>() {
            { REGISTRATION, REGISTRATION_DEFAULT },
            { REGISTERED, PARAM_OFF},
            { REGISTERED_OFFLINE, PARAM_OFF},

            { FILE_NAME, FILE_NAME_DEFAULT },
            { TASK_DIVISION_1, TASK_COMMUNICATIONS },
            { TASK_DIVISION_2, TASK_SYSTEM_MONITORING },
            { TASK_DIVISION_3, TASK_RESOURCE_MANAGEMENT },
            { COLOUR_PARAM_ACTIVE, COLOUR_YELLOW },
            { COLOUR_PARAM_WARNING, COLOUR_RED },
            { COLOUR_PARAM_RANGE, COLOUR_BLUE },
            { PARAM_HIGH_CONTRAST_COLOUR, PARAM_OFF },
            { FILE_RELOAD, PARAM_OFF},
            { OS_DROID_BACK, PARAM_OFF},
            { OS_PAUSE, PARAM_OFF},
            { FILE_OUTPUT, PARAM_OFF},
            { FORMAT_INPUT, FORMAT_TYPE_XML },
            { FORMAT_OUTPUT, FORMAT_TYPE_PLAINTEXT },
            { DESIGNATOR_1, DESIG_C },
            { DESIGNATOR_2, DESIG_D },
            { FILE_SCAN, PARAM_ON },
            { SERIAL_NAME, "" },
            { SERIAL_NO, "-1" },
            { WIDTH_OFFSET, WIDTH_OFFSET_0 },
            { MODE_DISPLAY, PARAM_OFF },
            { MODE_CLEAR_ANALYSIS_ON_PAUSE, PARAM_OFF },
            { TIMING_UNIX, PARAM_OFF},
            { RANDOM_MODE, PARAM_OFF},
            { TRAINING_MODE, PARAM_OFF},
            { SILENT_MODE, PARAM_OFF},
        };

        /** @brief Cedar default internal settings  */
        private readonly Dictionary<string, string> settingsInternalDefault =
        new Dictionary<string, string>() {
            { REGISTRATION, REGISTRATION_DEFAULT },
            { REGISTERED, PARAM_OFF},
            { REGISTERED_OFFLINE, PARAM_OFF},
            { FILE_NAME, FILE_NAME_DEFAULT },
            { TASK_DIVISION_1, TASK_COMMUNICATIONS },
            { TASK_DIVISION_2, TASK_SYSTEM_MONITORING },
            { TASK_DIVISION_3, TASK_RESOURCE_MANAGEMENT },
            { COLOUR_PARAM_ACTIVE, COLOUR_YELLOW },
            { COLOUR_PARAM_WARNING, COLOUR_RED },
            { COLOUR_PARAM_RANGE, COLOUR_BLUE },
            { PARAM_HIGH_CONTRAST_COLOUR, PARAM_OFF },
            { FILE_RELOAD, PARAM_OFF},
            { OS_DROID_BACK, PARAM_OFF},
            { OS_PAUSE, PARAM_OFF},
            { FILE_OUTPUT, PARAM_OFF},
            { FORMAT_INPUT, FORMAT_TYPE_XML },
            { FORMAT_OUTPUT, FORMAT_TYPE_PLAINTEXT },
            { DESIGNATOR_1, DESIG_C },
            { DESIGNATOR_2, DESIG_D },
            { FILE_SCAN, PARAM_ON },
            { SERIAL_NAME, "" },
            { SERIAL_NO, "-1" },
            { WIDTH_OFFSET, WIDTH_OFFSET_0 },
            { MODE_DISPLAY, PARAM_OFF },
            { MODE_CLEAR_ANALYSIS_ON_PAUSE, PARAM_OFF },
            { TIMING_UNIX, PARAM_OFF},
            { RANDOM_MODE, PARAM_OFF},
            { TRAINING_MODE, PARAM_OFF},
            { SILENT_MODE, PARAM_OFF},
        };

        /**
         @property  public static ModelData instance
        
         @brief Gets the singleton Cedar data instance.
        
         @return    The singleton Cedar data instance.
         */

        public static ModelData Instance {
            get {
                return _instance;
            }
        }

        /**
         @property  public static bool isIOS
        
         @brief Gets or sets a value indicating whether the current platform is iOS.
        
         @return    true if current platform is iOS, false if not.
         */

        public static bool IsIOS {
            get {
                return _ios;
            }
            set {
                _ios = value;
            }
        }

        /**
         @property  public static bool CEDARConfig
        
         @brief Gets or sets configuration file loaded state
        
         @return    true if configuration file loaded state, false if not.
         */
        public static bool CEDARConfig {
            get {
                return _config;
            }
            set {
                _config = value;
            }
        }

        /**
         @property  public static bool CEDAREvents
        
         @brief Gets or sets events file loaded state
        
         @return    true if configuration file loaded state, false if not.
         */
        public static bool CEDAREvents {
            get {
                return _events;
            }
            set {
                _events = value;
            }
        }
        /**
         @property  public static int Height
        
         @brief Gets or sets the screen height.
        
         @return    The screen height.
         */

        public static int Height {
            get {
                return _height;
            }
            set {
                _height = value;
            }
        }

        public static int WidthOR {
            get {
                return _width;
            }
        }

        public static int XOffset {
            get {
                if (Device.RuntimePlatform == Device.iOS) {
                    return 0;
                }
                int xOffset = 0;
                if (Instance.Settings[ModelData.WIDTH_OFFSET] == ModelData.WIDTH_OFFSET_5) {
                    xOffset = (int)(ModelData.WidthOR * 0.025);
                }
                else if (Instance.Settings[ModelData.WIDTH_OFFSET] == ModelData.WIDTH_OFFSET_10) {
                    xOffset = (int)(ModelData.WidthOR * 0.05);
                }
                return xOffset;
                //return 0;
            }
        }

        /**
         @property  public static int width
        
         @brief Gets or sets the screen height.
        
         @return    The screen width.
         */
        public static int Width {
            get {

                if (_instance.settingsInternal[WIDTH_OFFSET] == WIDTH_OFFSET_5) {
                    return Convert.ToInt32(_width * 0.95);
                }
                else if (_instance.settingsInternal[WIDTH_OFFSET] == WIDTH_OFFSET_10) {
                    return Convert.ToInt32(_width * 0.875);
                }

                return _width;

            }
            set {
                _width = value;
            }
        }

        /**
         @property  public long SessionStartTime

         @brief Gets or sets the start timestamp of the current test session.

        @return Start timestamp of the current test session.  */
        public long SessionStartTime {
            get {
                return _sessionStartTime;
            }
            set {
                _sessionStartTimeFileStr = FileTimestamp(timeNow: true);
                _sessionStartTime = value;
            }
        }

        public long PauseTimeStamp {
            get {
                return _pauseTimeStamp;
            }
            set {
                _pauseTimeStamp = value;
            }
        }

        public string SessionStartTmeStr {
            get {
                return _sessionStartTimeFileStr;
            }
        }

        /**
         @property  public Dictionary<string, string> settings
        
         @brief Cedar internal settings .
        
            - not set by external event or config file.
            - can be loaded from settings.json if existing
            - saves to new settings.json if not existing
        
         @return    Cedar internal settings.
         */

        public Dictionary<string, string> Settings {
            get {
                return settingsInternal;
            }
        }

        /**
         @fn    public void OutputFile(string filename, string fileContents)
        
         @brief Output text file 
            
            - raises file output event for platform specific code to handle.
        
         @author    Ryan Beruldsen
         @date  6/10/2016
        
         @param filename        Filename of the file.
         @param fileContents    The file contents.
         */

        public void OutputFile(string filename, string fileContents) {
            settingsInternal[FILE_OUTPUT] = PARAM_ON;
            ModelDataEventArgs e = new ModelDataEventArgs();
            e.settingsChangeList.Add(FILE_OUTPUT);
            e.SettingsChange = true;
            e.FileContents = fileContents;
            e.Filename = filename;
            _instance.ModelDataChanged(e);
        }

        /**
         @fn    public void DefaultSettings()

         @brief Reset internal Cedar settings to default.

         @author    Ryan Beruldsen
         @date  3/07/23
         */

        public void DefaultSettings() {
            foreach (var keyValue in settingsInternalDefault) {
                if (!Instance.userConfig.ContainsKey(keyValue.Key) || Instance.userConfig[keyValue.Key].Count > 1) {
                    ChangeSetting(keyValue.Key, keyValue.Value);
                }
            }
        }

        /**
         @fn    public void ChangeSetting(string key, string value)
        
         @brief Change internal Cedar setting.
        
         @author    Ryan Beruldsen
         @date  6/10/2016
        
         @param key     The setting key.
         @param value   The setting value.
         */

        public void ChangeSetting(string key, string value, bool notify = true) {
            //if key and value can be changed (are specified in settingsInternal and userConfig
            // - 
            //settings that are not user modifiable can be saved and loaded
            //but if a user modifiable setting is loaded, it must be specified in userConfig

            //retain case-sensitivity for filenames, otherwise uppercase
            if (key != FILE_NAME) {
                key = key.ToUpper();
                value = value.ToUpper();
            }

            bool userConfigContains = userConfig.ContainsKey(key);
            bool userConfigValueContains = false;
            if (userConfigContains) {
                var test = userConfig[key];
                userConfigValueContains = test.Contains(value);
                if (!userConfigValueContains) {
                    userConfig[key].Add(value);
                    if (!userConfigStr.ContainsKey(value)) {
                        userConfigStr.Add(value, value);
                    }
                }
                userConfigValueContains = true;

            }

            settingsInternal[key] = value;


            if (notify && settingsInternal.ContainsKey(key) && (!userConfigContains || userConfigValueContains)) {
                ModelDataEventArgs e = new ModelDataEventArgs();
                e.settingsChangeList.Add(key);
                e.SettingsChange = true;
                _instance.ModelDataChanged(e);
            }
        }


        /**
         @fn    public string FileTimestamp()
        
         @brief Formats a timestamp for output file use.
        
         @author    Ryan Beruldsen
         @date  6/10/2016
         */

        public string FileTimestamp(long customTimestamp = 0, bool serial = false, bool timeNow = false) {
            DateTime d = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

            if (customTimestamp != 0) {
                d = d.AddMilliseconds(customTimestamp).ToLocalTime(); ;
            }
            else if (!timeNow) {
                d = d.AddMilliseconds(_termination).ToLocalTime(); ;
            }
            else {
                d = d.AddMilliseconds(Timestamp()).ToLocalTime(); ;
            }
            bool partial = Settings[OS_PAUSE].Equals(PARAM_ON);
            string prefix = partial ? "PARTIAL_" : "";
            string year = d.Year.ToString();
            string month = d.Month.ToString("00");
            string day = d.Day.ToString("00");
            string minute = d.Minute.ToString("00");
            string hour = d.Hour.ToString("00");
            string designator = ModelData.Instance.Settings[ModelData.DESIGNATOR_1] +
                                ModelData.Instance.Settings[ModelData.DESIGNATOR_2];

            string filename = prefix + year + "_" + month + day +
                                hour + minute + "_" + designator;
            if (serial) {
                filename = filename + "_" + ModelData.Instance.Settings[ModelData.SERIAL_NAME];
            }
            return filename;
        }

        /**
         @fn    public void SaveSettings()
        
         @brief Saves the settings by serialising, outputting to settings.json file.
        
         @author    Ryan Beruldsen
         @date  6/10/2016
         */

        public void SaveSettings() {
            string output = "{\n";
            int numSettings = settingsInternal.Count;
            int settingsCount = 0;
            foreach (KeyValuePair<string, string> setting in settingsInternal) {
                settingsCount++;
                output += settingsCount == numSettings ?
                    "\t\"" + setting.Key + "\": \"" + setting.Value + "\"\n" :
                    "\t\"" + setting.Key + "\": \"" + setting.Value + "\",\n";
            }
            output += "}";
            ModelData.Instance.OutputFile("settings_", output);
        }

        /** @brief The user configuration information string 
        
            - essentially provides human readable descriptions to internal settings hash map keys. */
        public Dictionary<string, string> userConfigStr =
            new Dictionary<string, string>() {
                { TASK_DIVISION_1, "Division 1 Module"},
                { TASK_DIVISION_2, "Division 2 Module"},
                { FILE_SCAN, "File Scan Mode" },
                { TASK_DIVISION_3, "Division 3 Module"},
                { TASK_COMMUNICATIONS, "Communications"},
                { TASK_COMMUNICATIONS_SIMPLE_1, "Communications SC1"},
                { TASK_COMMUNICATIONS_SIMPLE_2, "Communications SC2"},
                { TASK_COMMUNICATIONS_SIMPLE_3, "Communications SC3"},
                { TASK_SYSTEM_MONITORING, "System Monitoring"},
                { TASK_RESOURCE_MANAGEMENT, "Resource Management"},
                { TASK_EXTENSION_SAMPLE, "Extension Task Sample"},
                { COLOUR_PARAM_ACTIVE, "Active/Highlight Colour"},
                { COLOUR_PARAM_RANGE, "Range Indicate Colour"},
                { COLOUR_PARAM_WARNING, "Warning/Alert Colour"},
                { DESIGNATOR_1, "Designator Part 1"},
                { DESIGNATOR_2, "Designator Part 2"},
                { COLOUR_RED, "Red"},
                { COLOUR_YELLOW, "Yellow"},
                { COLOUR_GREY, "Grey"},
                { COLOUR_BLUE, "Blue"},
                { COLOUR_GREEN, "Green"},
                { COLOUR_WHITE, "White"},
                { DESIG_A, "Alpha"},
                { DESIG_B, "Bravo"},
                { DESIG_C, "Charlie"},
                { DESIG_D, "Delta"},
                { DESIG_E, "Echo"},
                { DESIG_F, "Foxtrot"},
                { PARAM_ON, "On" },
                { PARAM_OFF, "Off" },
                { PARAM_RANDOM_ENDLESS, "Endless" },
                { PARAM_HIGH_CONTRAST_COLOUR, "High Contrast Colours" },
                { FORMAT_INPUT, "Input Format" },
                { FORMAT_OUTPUT, "Output Format" },
                { FORMAT_TYPE_JSON, "JSON" },
                { FORMAT_TYPE_XML, "XML" },
                { FORMAT_TYPE_PLAINTEXT, "Plaintext" },
                { FORMAT_TYPE_CSV, "CSV"},
                { FORMAT_TYPE_ALL, "All Output Formats" },
                { WIDTH_OFFSET, "Width Offset" },
                { WIDTH_OFFSET_0, "0%" },
                { WIDTH_OFFSET_5, "5%" },
                { WIDTH_OFFSET_10, "10%" },
                { FILE_NAME, "File" },
                { FILE_NAME_DEFAULT, "Default" },
                { NONE, "None"},
                { MODE_DISPLAY, "Display Mode" },
                { MODE_CLEAR_ANALYSIS_ON_PAUSE, "CAOP Mode" },
                { TIMING_UNIX, "Output Unix Time" },
                { TRAINING_MODE, "Training Mode" },
                { SILENT_MODE, "Silent Mode" },
                { RANDOM_MODE, "Random Mode" },
        };

        /** @brief User-modifiable configuration options. */
        public Dictionary<string, List<string>> userConfig =
            new Dictionary<string, List<string>>() {
                { FILE_NAME,

                    new List<string> {
                        FILE_NAME_DEFAULT
                    }
                },{ FORMAT_INPUT,

                    new List<string> {
                        FORMAT_TYPE_JSON,
                        FORMAT_TYPE_XML
                    }
                },{ FORMAT_OUTPUT,

                    new List<string> {
                        FORMAT_TYPE_JSON,
                        FORMAT_TYPE_CSV,
                        FORMAT_TYPE_PLAINTEXT,
                        FORMAT_TYPE_ALL
                    }
                },{   TASK_DIVISION_1,

                    new List<string> {
                            TASK_COMMUNICATIONS,
                            TASK_COMMUNICATIONS_SIMPLE_1,
                            TASK_COMMUNICATIONS_SIMPLE_2,
                            TASK_COMMUNICATIONS_SIMPLE_3,
                            TASK_SYSTEM_MONITORING,
                            TASK_RESOURCE_MANAGEMENT,
                            NONE
                    }
                },{ TASK_DIVISION_2,

                    new List<string> {
                        TASK_COMMUNICATIONS,
                        TASK_COMMUNICATIONS_SIMPLE_1,
                        TASK_COMMUNICATIONS_SIMPLE_2,
                        TASK_COMMUNICATIONS_SIMPLE_3,
                        TASK_SYSTEM_MONITORING,
                        TASK_RESOURCE_MANAGEMENT,
                        NONE
                    }
                },{ TASK_DIVISION_3,

                    new List<string> {
                        TASK_COMMUNICATIONS,
                        TASK_COMMUNICATIONS_SIMPLE_1,
                        TASK_COMMUNICATIONS_SIMPLE_2,
                        TASK_COMMUNICATIONS_SIMPLE_3,
                        TASK_SYSTEM_MONITORING,
                        TASK_RESOURCE_MANAGEMENT,
                        NONE
                    }
                },{ COLOUR_PARAM_ACTIVE,

                    new List<string> {
                        COLOUR_YELLOW,
                        COLOUR_BLUE,
                        COLOUR_RED,
                        COLOUR_GREEN,
                        COLOUR_GREY,
                        COLOUR_WHITE
                    }
                },{ COLOUR_PARAM_WARNING,

                    new List<string> {
                        COLOUR_YELLOW,
                        COLOUR_BLUE,
                        COLOUR_RED,
                        COLOUR_GREEN,
                        COLOUR_GREY,
                        COLOUR_WHITE
                    }
                },{ COLOUR_PARAM_RANGE,

                    new List<string> {
                        COLOUR_YELLOW,
                        COLOUR_BLUE,
                        COLOUR_RED,
                        COLOUR_GREEN,
                        COLOUR_GREY,
                        COLOUR_WHITE
                    }
                }, { PARAM_HIGH_CONTRAST_COLOUR,

                    new List<string> {
                        PARAM_ON,
                        PARAM_OFF
                    }
                }, { DESIGNATOR_1,

                    new List<string> {
                        DESIG_A,
                        DESIG_B,
                        DESIG_C,
                        DESIG_D,
                        DESIG_E,
                        DESIG_F,
                    }
                },{ DESIGNATOR_2,

                    new List<string> {
                        DESIG_A,
                        DESIG_B,
                        DESIG_C,
                        DESIG_D,
                        DESIG_E,
                        DESIG_F,
                    }
                },{ WIDTH_OFFSET,

                    new List<string> {
                        WIDTH_OFFSET_0,
                        WIDTH_OFFSET_5,
                        WIDTH_OFFSET_10,
                    }
                },{ MODE_CLEAR_ANALYSIS_ON_PAUSE,

                    new List<string> {
                        PARAM_ON,
                        PARAM_OFF
                    }
            },{ TIMING_UNIX,

                    new List<string> {
                        PARAM_ON,
                        PARAM_OFF
                    }
            },{ RANDOM_MODE,

                new List<string> {
                    PARAM_ON,
                    PARAM_RANDOM_ENDLESS,
                    PARAM_OFF
                }
            },{ TRAINING_MODE,

                new List<string> {
                    PARAM_ON,
                    PARAM_OFF
                }
            },{ SILENT_MODE,

                new List<string> {
                    PARAM_ON,
                    PARAM_OFF
                }
            },
        };

        /**
         @fn    public void ExtensionSample()
        
         @brief Extension sample logic - defaults division one to the extension task.
        
         @author    Ryan Beruldsen
         @date  10/10/2016
         */

        public void ExtensionSample() {
            userConfig[TASK_DIVISION_1] =
                new List<string> {
                    TASK_COMMUNICATIONS,
                    TASK_SYSTEM_MONITORING,
                    TASK_RESOURCE_MANAGEMENT,
                    TASK_EXTENSION_SAMPLE,
                    NONE
            };
            userConfig[TASK_DIVISION_2] =
                new List<string> {
                    TASK_COMMUNICATIONS,
                    TASK_SYSTEM_MONITORING,
                    TASK_RESOURCE_MANAGEMENT,
                    TASK_EXTENSION_SAMPLE,
                    NONE
            };
            userConfig[TASK_DIVISION_3] =
                new List<string> {
                    TASK_COMMUNICATIONS,
                    TASK_SYSTEM_MONITORING,
                    TASK_RESOURCE_MANAGEMENT,
                    TASK_EXTENSION_SAMPLE,
                    NONE
            };

            Settings[TASK_DIVISION_1] = TASK_EXTENSION_SAMPLE;
        }

        /**
         @fn    public Dictionary<string, string> userOption (string userConfigKey)
        
         @brief Gets user-modifiable options, given a settings key.
        
         @author    Ryan Beruldsen
         @date  6/10/2016
        
         @param userConfigKey   The user-modifiable setting key.
        
         @return    Avaible options given a user-modifiable setting key
         */

        public CDRKeyValData UserOption(string userConfigKey) {
            var d = new CDRKeyValData();
            for (int i = 0; i < userConfig[userConfigKey].Count; i++) {
                string currentKey = userConfig[userConfigKey][i];
                d[currentKey] = userConfigStr[currentKey];
            }
            return d;
        }

        /**
         @fn    internal void ModelDataChanged(ModelDataEventArgs e)
        
         @brief Raises model data changed events for listeners.

            - this is used for file loading and other flags and for settings changes
        
         @author    Ryan Beruldsen
         @date  6/10/2016
        
         @param e   Model data event information.
         */

        internal void ModelDataChanged(ModelDataEventArgs e) {
            ModelDataEventHandler?.Invoke(this, e);
        }

        /**
         @fn    private ModelData()
        
         @brief Stub constructor that prevents a default instance of this class from being created.
        
         @author    Ryan Beruldsen
         @date  10/10/2016
         */

        private ModelData() { }

    }
}
