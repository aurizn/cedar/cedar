﻿/*
 * CEDAR Operator Workload Assessment Tool
 * Copyright (C) 2023  aurizn
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using CocosSharp;
using System;
using System.Collections.Generic;
using System.Text;
using CEDAR.Shared;

namespace CEDAR.Shared.Modules.Menu {

    /**
     @class MenuControllerSettingsScreen
    
     @brief Settings screen, drop down options for configuring what tests are available, coloursets.
    
     @author    Ryan Beruldsen
     @date  14/09/2016
     */

    class MenuControllerSettingsScreen : CCNode {

        /** @brief Drop down settings per page. */
        private readonly int _SETTINGS_DD_PAGE = 9;

        /** @brief Internal draw node. */
        private readonly CCDrawNode _drawNode;

        /** @brief Splash screen sprite, contains Cedar logo.*/
        private CCSprite _splashScreen;

        /** @brief The window maximum. */
        private CCPoint _windowMax;

        /** @brief Back button, return to main menu or back a settings page. */
        private MenuViewButton _back;

        /** @brief Next button, next settings page if available. */
        private MenuViewButton _next;

        /** @brief Settings page, 9 drop-down settings displayed per page. */
        private int _page = 0;

        /** @brief Default settings button. */
        MenuViewButton _defaults;

        /** @brief Config files setup (XML). */
        MenuViewButton _config;

        /** @brief List of drop downs buttons (setting options, dictionary with reference to settings data). */
        readonly Dictionary<string, MenuViewButtonDropDown> _dropDownDictionary;

        /** @brief List of drop downs buttons (setting options, dictionary with reference to settings data). */
        List<MenuViewButtonDropDown> _dropDownList;

        /** @brief Listener for button touch events. */
        readonly CCEventListenerTouchAllAtOnce _touchListener;

        /**
         @fn    public MenuControllerSettingsScreen(CCPoint windowMax) : base()
        
         @brief Settings screen constructor.
        
         @author    Ryan Beruldsen
         @date  16/09/2016
        
         @param windowMax   Window bounds (max X and Y coordinates)
         */

        public MenuControllerSettingsScreen(CCPoint windowMax) : base() {
            _windowMax = new CCPoint(windowMax.X, windowMax.Y);

            _drawNode = new CCDrawNode();
            _dropDownDictionary = new Dictionary<string, MenuViewButtonDropDown>();
            _dropDownList = new List<MenuViewButtonDropDown>();

            _touchListener = new CCEventListenerTouchAllAtOnce {
                OnTouchesBegan = HandleInput,
                OnTouchesEnded = HandleInputEnd
            };
            AddEventListener(_touchListener, this);

            Init();
        }

        /**
         @fn    public void Init()
        
         @brief Initialise settings page.
        
         @author    Ryan Beruldsen
         @date  16/09/2016
         */

        private void Init() {
            var data = ModelData.Instance;
            AddChild(_drawNode);
            _splashScreen = new CCSprite();
            AddChild(_splashScreen);

            //draw divider line for top buttons and settings drop down
            _drawNode.DrawLine(
                        from: new CCPoint(0, _windowMax.Y * 0.65f),
                        to: new CCPoint(_windowMax.X, _windowMax.Y * 0.65f),
                        lineWidth: 2,
                        color: data.GetColourPallete4B(ModelData.COLOUR_PARAM_ACTIVE));

            //add, position logo
            _splashScreen.AddChild(new MenuViewCedarLogo(
                windowMax: _windowMax,
                highlightColour: data.GetColourPallete4B(ModelData.COLOUR_PARAM_ACTIVE)));
            _splashScreen.PositionY = _windowMax.Y * 0.30f;

            //add top buttons (defaults, back)
            _config = new MenuViewButton(
               label: "Reload Configuration Files",
               highlight: false,
               highlightColour: data.GetColourPallete4B(ModelData.COLOUR_PARAM_ACTIVE),
               width: _windowMax.X * 0.4f,
               height: _windowMax.Y * 0.04f,
               x: _windowMax.X / 2,
               y: _windowMax.Y * 0.80f);

            _back = new MenuViewButton(
               label: "< Back",
               highlightColour: data.GetColourPallete4B(ModelData.COLOUR_PARAM_ACTIVE),
               width: _windowMax.X * 0.175f,
               height: _windowMax.Y * 0.04f,
               x: _windowMax.X * 0.1f,
               y: _windowMax.Y * 0.95f);

            _next = new MenuViewButton(
               label: "Next >",
               highlightColour: data.GetColourPallete4B(ModelData.COLOUR_PARAM_ACTIVE),
               width: _windowMax.X * 0.175f,
               height: _windowMax.Y * 0.04f,
               x: _windowMax.X * 0.9f,
               y: _windowMax.Y * 0.95f);

            _defaults = new MenuViewButton(
               label: "Defaults",
               highlightColour: data.GetColourPallete4B(ModelData.COLOUR_PARAM_ACTIVE),
               width: _windowMax.X * 0.4f,
               height: _windowMax.Y * 0.04f,
               x: _windowMax.X / 2,
               y: _windowMax.Y * 0.73f);

            AddChild(_back);
            AddChild(_next);
            AddChild(_defaults);
            AddChild(_config);

            //handle settings changes
            ModelData.Instance.ModelDataEventHandler +=
                new ModelDataChangedEventHandler(SettingsDataChanged);

            //add settings drop down buttons, page 0 to start
            SettingsDropDownPage(page: 0);

            if (ModelData.Instance.userConfig.Count > _SETTINGS_DD_PAGE) {
                _next.Visible = true;
            } else {
                _next.Visible = false;
            }
        }

        /**
        @fn    private void SettingsDropDownPage(int page)
       
        @brief Instantiate a drop down setting page, clear existing page.
       
        @author    Ryan Beruldsen
        @date  16/11/2016
       
        @param page  Page number.
        */
        private void SettingsDropDownPage(int page) {
            foreach (MenuViewButtonDropDown button in _dropDownList) {
                RemoveChild(button);
            }
            _dropDownList = new List<MenuViewButtonDropDown>();

            //button spacer variable
            int count = 0;

            int settingIndex = (page * _SETTINGS_DD_PAGE);
            int settingIndexMax = (page + 1) * _SETTINGS_DD_PAGE;

            int loopIndex = 0;

            //add settings drop down buttons
            foreach (KeyValuePair<string, List<string>> entry in ModelData.Instance.userConfig) {
                if (loopIndex >= settingIndex && loopIndex < settingIndexMax) {
                    float spacer = 0.06f;

                    MenuViewButtonDropDown dropDownButton = new MenuViewButtonDropDown(
                        titleKey: entry.Key,
                        titleValue: ModelData.Instance.userConfigStr[entry.Key],
                        options: ModelData.Instance.UserOption(entry.Key),
                        selectedKey: ModelData.Instance.Settings[entry.Key],
                        windowMax: _windowMax,
                        width: _windowMax.X * 0.4f,
                        height: _windowMax.Y * 0.04f,
                        highlightColour:
                            ModelData.Instance.GetColourPallete4B(ModelData.COLOUR_PARAM_ACTIVE),
                        highlightColourText:
                            ModelData.Instance.GetColourPallete3B(ModelData.COLOUR_PARAM_ACTIVE),
                        x: _windowMax.X * 0.65f,
                        y: _windowMax.Y * 0.57f - count++ * _windowMax.Y * spacer);

                    _dropDownDictionary[entry.Key] = dropDownButton;
                    _dropDownList.Add(dropDownButton);
                    AddChild(dropDownButton);
                }
                loopIndex++;

            }
        }

        /**
         @fn    private void SettingsDataChanged(object sender, ModelDataEventArgs e)
        
         @brief Handle settings changes
        
         @author    Ryan Beruldsen
         @date  16/09/2016
        
         @param sender  Source of the event.
         @param e       Model data event object
         */

        private void SettingsDataChanged(object sender, ModelDataEventArgs e) {
            if (e.SettingsChange && ModelData.CurrentPageStr == "settings") {
                foreach (string changedSetting in e.settingsChangeList) {

                    if (_dropDownDictionary.ContainsKey(changedSetting) &&
                            _dropDownDictionary[changedSetting].SelectedKey !=
                            ModelData.Instance.Settings[changedSetting]) {

                        _dropDownDictionary[changedSetting].SelectedKey = ModelData.Instance.Settings[changedSetting];
                        ModelData.Instance.SaveSettings();
                    }
                    //reload drop down options on file reload in case changed (new/removed file)
                    if (changedSetting == ModelData.FILE_RELOAD && ModelData.Instance.Settings[ModelData.FILE_RELOAD] == ModelData.PARAM_OFF) {
                        SettingsDropDownPage(_page);
                    }
                }
            }
        }

        /**
         @fn    private void HandleInputEnd(System.Collections.Generic.List<CCTouch> touches, CCEvent touchEvent)
        
         @brief Handles touch input end
        
         @author    Ryan Beruldsen
         @date  16/09/2016
        
         @param touches     CocosSharp injected touch list
         @param touchEvent  CocosSharp injected touch event
         */

        private void HandleInputEnd(System.Collections.Generic.List<CCTouch> touches, CCEvent touchEvent) {
            if (touches.Count > 0) {

                if (_back.Active && MenuButtonHitTest(touches, _back)) {
                    _back.Active = false;

                    if (_page == 0) {
                        ModelData.CurrentPageStr = "title";
                    } else {
                        _page--;
                        SettingsDropDownPage(_page);
                        System.Diagnostics.Debug.WriteLine("PAGE " + _page);
                        if (ModelData.Instance.userConfig.Count > (_page + 1) * _SETTINGS_DD_PAGE) {
                            _next.Visible = true;
                        }
                    }
                }

                if (_next.Active && MenuButtonHitTest(touches, _next)) {
                    _next.Active = false;
                    if (ModelData.Instance.userConfig.Count > (_page + 1) * _SETTINGS_DD_PAGE) {
                        _page++;
                        System.Diagnostics.Debug.WriteLine("PAGE " + _page);

                        if (ModelData.Instance.userConfig.Count <= (_page + 1) * _SETTINGS_DD_PAGE) {
                            _next.Visible = false;
                        }
                        SettingsDropDownPage(_page);
                    }
                }

                if (_defaults.Active && MenuButtonHitTest(touches, _defaults)) {
                    _defaults.Active = false;
                    ModelData.Instance.DefaultSettings();
                    ModelData.Instance.SaveSettings();
                }

                if (_config.Active && MenuButtonHitTest(touches, _config)) {
                    ModelData.Instance.ChangeSetting(ModelData.FILE_RELOAD, ModelData.PARAM_ON);
                    _config.Active = false;

                }

                _config.Active = false;
                _back.Active = false;
                _defaults.Active = false;
            }
        }

        /**
         @fn    private void MenuButtonHitTest(System.Collections.Generic.List<CCTouch> touches, MenuViewButton m)
        
         @brief Hit test for menu button, shorthand function.
        
         @author    Ryan Beruldsen
         @date  16/09/2016
        
         @param touches CocosSharp injected touch list (from touch handler)
         @param m       The MenuViewButton to test for bounding box hit.
        
         @return    True if within button/drop down bounds, otherwise false
         */

        private bool MenuButtonHitTest(System.Collections.Generic.List<CCTouch> touches,
                                        MenuViewButton m) {

            if (touches[0].Location.X > m.minButtonBounds.X &&
                    touches[0].Location.X < m.maxButtonBounds.X &&
                    touches[0].Location.Y > m.minButtonBounds.Y &&
                    touches[0].Location.Y < m.maxButtonBounds.Y) {
                return true;
            }
            return false;
        }

        /**
         @fn    private bool MenuDropHitTest(System.Collections.Generic.List<CCTouch> touches, MenuViewButtonDropDown m)
        
         @brief Hit test for drop down menu button
        
         @author    Ryan Beruldsen
         @date  16/09/2016
        
         @param touches CocosSharp injected touch list (from touch handler)
         @param m       The MenuViewButtonDropDown to process.
        
         @return    True if within button bounds, otherwise false
         */

        private bool MenuDropHitTest(System.Collections.Generic.List<CCTouch> touches, MenuViewButtonDropDown m) {

            //hit test buton drop down options if visible
            if (!m.Disabled && m.Active) {
                foreach (KeyValuePair<string, string> option in m.Options) {
                    //if option is selected

                    if (m.minButtonBoundsOptions.ContainsKey(option.Key) &&
                        touches[0].Location.X > m.minButtonBoundsOptions[option.Key].X &&
                        touches[0].Location.X < m.maxButtonBoundsOptions[option.Key].X &&
                        touches[0].Location.Y > m.minButtonBoundsOptions[option.Key].Y &&
                        touches[0].Location.Y < m.maxButtonBoundsOptions[option.Key].Y) {

                        //change related setting with view held parameters
                        ModelData.Instance.ChangeSetting(m.TitleKey, option.Key);
                        ModelData.Instance.SaveSettings();
                        return true;
                    }
                }
            }

            //test regular button bounds
            if (touches[0].Location.X > m.minButtonBounds.X &&
                    touches[0].Location.X < m.maxButtonBounds.X &&
                    touches[0].Location.Y > m.minButtonBounds.Y &&
                    touches[0].Location.Y < m.maxButtonBounds.Y) {
                return true;
            }

            return false;
        }

        /**
         @fn    private void HandleInput(System.Collections.Generic.List<CCTouch> touches, CCEvent touchEvent)
        
         @brief Handles touch input
        
         @author    Ryan Beruldsen
         @date  16/09/2016
        
         @param touches     CocosSharp injected touch list
         @param touchEvent  CocosSharp injected touch event
         */

        private void HandleInput(System.Collections.Generic.List<CCTouch> touches, CCEvent touchEvent) {
            if (touches.Count > 0) {
                if (MenuButtonHitTest(touches, _back)) {
                    _back.Active = true;
                } else if (MenuButtonHitTest(touches, _next) && _next.Visible) {
                    _next.Active = true;
                } else if (MenuButtonHitTest(touches, _defaults)) {
                    _defaults.Active = true;
                } else if (MenuButtonHitTest(touches, _config)) {
                    _config.Active = true;
                }

                bool breakLoop = false;
                for (int i = 0; i < _dropDownList.Count && !breakLoop; i++) {
                    if (!_dropDownList[i].Disabled && MenuDropHitTest(touches, _dropDownList[i])) {
                        _dropDownList[i].Active = !_dropDownList[i].Active;

                        for (int j = 0; j < _dropDownList.Count && _dropDownList[i].Active; j++) {
                            if (j != i) _dropDownList[j].Disabled = true;
                        }
                        for (int j = 0; j < _dropDownList.Count && !_dropDownList[i].Active; j++) {
                            if (j != i) _dropDownList[j].Disabled = false;
                            breakLoop = true;
                        }
                    }
                }
            }
        }
    }
}

