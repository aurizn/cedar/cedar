# Legacy MATB-II Test Subject User Guide

![](media/cedar_ios_icons.png)

This brief user guide describes the use of the (cross-platform) Tablet OS based Cedar operator workload assessment tool in the context of transitioning from the use of the (Windows) PC OS based MATB-II tool. The _Task Divisions_ table outlines MATB-II tasks of which Cedar is an implementation for the tablet form factor. Refer to the MATB-II documentation for information on the goals and specifics of each task, as the behaviour of Cedar is functionally equivalent, except where noted.

To aid usability, colours have distinct meaning in Cedar, see the _Colour Definitions_ table for an outline of each distinct colour and it’s meaning. The default colours can be customised in the settings menu.

A dense informational footer is provided to enable the user to prioritise tasks and to give general information relating to the total elapsed time of a given test, see _Footer Data Definition_.

### **Colour Definitions**

| Colour Parameter | Default Colour | Meaning |
| --- | --- | --- |
| Active / Highlight | **Yellow** | This colour denotes a component is selected or requires ongoing attention. This may mean a system monitoring scale, which requires the attention of the operator to ensure it does not trend below or above safe levels. Alternatively it may denote selection and attention, in the case of the communications task where the NAV or COM channels can be selected and will be coloured the active colour upon selection. Following selection, the frequency slider (which is also active colour) should be attended so that it aligns with the target frequency, which will change over time, requiring the user’s ongoing attention. |
| Range Indication | **Blue** | The range indication colour is used to emphasise the safe operating levels of a given component.These can be in the form of a scale immediately to the left or right of a level component, to which a level must not trend higher or lower or it will lead to a malfunction. It can also be in the form of a range guide, such as in the case of the communication slider: a blue range indication will appear when the slider overlaps with the desired frequency target on the horizontal display. |
| Warning / Alert | **Red** | The warning colour draws the attention of the user to a current fault. This fault may or may not require attention – this is left to the user to interpret. A pump function cannot be resolved, for example and must the user must simply wait until the fault is automatically resolved. |

### **Task Divisions**

| Task Division | Screenshot | Description |
| --- | --- | --- |
| Communications | ![](media/cedar_comms.png) | The communications task has been adapted to the tablet form factor by removing the requirement to type a desired frequency manually. A target frequency is entered on Cedar by means of a frequency slider. The slider can be operated by dragging the slider handle, or by tapping near the edges of the handle for fine control.<br><br>The target frequency is illustrated on the horizontal scale in the Warning colour. Four channels can be selected for modification, denoted by the Active colour on selection. If a channel’s current frequency is different to it’s target frequency and it is not selected, it will be Warning coloured to draw the user’s attention. In addition to the Warning colouring, a sliver of red will extend below the regular height of the frequency selection button to ensure the visibility of the warning indication even when the channel is selected.<br><br>The actual current frequency of the current selected channel is displayed either singly below the channel label in the case the actual frequency is the same as the target frequency, of format:<br><br>**Target Frequency**<br><br>or in the case that the target frequency is different to the actual frequency, displayed of format:<br><br>**Target Frequency \[Current Frequency\]** |
| System Monitoring | ![](media/cedar_sys.png) | The system monitoring task is fairly analogous to it’s MATB-II equivalent. The two warning lights can be attended by tapping them as buttons, as opposed to clicking or pressing the F5/F6 key as in MATB-II. The scales can be attended if outside safe levels (which are now visually displayed in the Range colour: in the MATB-II the safe level range was less clear) by tapping anywhere within the bounds of the scale in question. |
| Resource Management | ![](media/cedar_res.png) | The resource management task is of greatest direct equivalence to the relevant MATB-II task of any of the other Cedar task divisions. The only difference is in the presentation of the information, which has been condensed and reconfigured for the tablet platform. Pumps can be activated by tapping on or near the targeted pump and while not numerically labelled as in MATB-II, the direction of each pump is similarly labelled. The goal of the resource management task in Cedar as in MATB-II is to ensure the levels of the two main (larger) tanks remain within the safe levels (indicated in Cedar with the Range colour). If the levels of either main tank falls below the safe levels, the Active colour used to illustrate the current tank level will change to the Warning colour to indicate a tank fault. |

### **Footer Data Definition**

Footer data is distinguishable from other division data (such as labels) by it’s enclosing braces and is formatted as follows, given the data definitions below:

| Data | Definition |
| --- | --- |
| Total Time | The total time elapsed in seconds and minutes, eg. 01:30 being one minute and thirty seconds elapsed. |
| Malfunction Time | The total time a malfunction has been left unresolved, in seconds and minutes. A unit of score will be deducted from the total score on timeout for each component in the division that is malfunctioning. |
| Total Score | The current score a user has maintained since the beginning of the test. Scores begin at 100, one unit is deducted for a timeout duration that a component has been malfunctioning without intervention (if that component requires intervention – ie a pump malfunction cannot be fixed). |
| Division Score | The number of score units that have been deducted from the total score by the division, see Total Score for a description of the score mechanism. |

| Footer Format |
| --- |
| { Total Time \| Division Score / Total Score \| Malfunction Time } |