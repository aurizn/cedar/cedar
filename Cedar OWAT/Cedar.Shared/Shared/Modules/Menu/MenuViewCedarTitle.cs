﻿/*
 * CEDAR Operator Workload Assessment Tool
 * Copyright (C) 2023  aurizn
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using CocosSharp;
using CEDAR.Shared.Modules;
using System;
using System.Collections.Generic;
using System.Text;

namespace CEDAR.Shared.Modules.Menu {
    /**
     @class MenuViewCedarTitle
    
     @brief The Cedar formatted title, positioned for title screen display.
    
     @author    Ryan Beruldsen
     @date  15/09/2016
     */

    class MenuViewCedarTitle : CCNode {
        private CCPoint windowMax;
        private CCLabel titleLabelSub;
        private CCLabel titleLabelMain;
        public MenuViewCedarTitle(CCPoint windowMax) : base() {
            this.windowMax = windowMax;
            Init();
        }

        /**
         @fn    public void Init()
        
         @brief Initialise/draw Cedar title
        
         @author    Ryan Beruldsen
         @date  16/09/2016
         */

        public void Init() {
            //text padding
            int padding = 225;

            //draw cedar title
            titleLabelSub = new CCLabel(
                " Operator Workload Assessment Tool",
                "robotob",
                25,
                CCLabelFormat.SpriteFont
            ) {

                Position = new CCPoint((windowMax.X / 2) - padding, (windowMax.Y / 2)),
                Color = CCColor3B.White,
                AnchorPoint = CCPoint.AnchorMiddleLeft
            };
            titleLabelMain = new CCLabel("CEDAR", "roboto", 25, CCLabelFormat.SpriteFont) {
                Position = new CCPoint((windowMax.X / 2) - padding, (windowMax.Y / 2)),
                Color = CCColor3B.White,
                AnchorPoint = CCPoint.AnchorMiddleRight
            };

            AddChild(titleLabelSub);
            AddChild(titleLabelMain);

        }
    }
}
