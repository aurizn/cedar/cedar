﻿/*
 * CEDAR Operator Workload Assessment Tool
 * Copyright (C) 2023  aurizn
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using CocosSharp;

namespace CEDAR.Shared.Modules.Task.Ext {
    /**
     @class ExtViewText
    
     @brief A ext text view component
    
     @author    Ryan Beruldsen
     @date  10/10/2016
     */

    class ExtViewText : DivisionContent, IDivisionContent {

        /** @brief The text label. */
        CCLabel textLabel;

        /**
         @property  public string text
        
         @brief Gets or sets the main text.
        
         @return    The main text.
         */

        public string Text {
            get {
                return textLabel.Text;
            }set {
                textLabel.Text = value;
            }
        }

        /**
         @fn    public ExtViewText(Division division): base(division)
        
         @brief Ext text view component constructor.
        
         @author    Ryan Beruldsen
         @date  10/10/2016
        
         @param division    The division.
         */

        public ExtViewText(Division division): base(division) { }

        /**
         @fn    public void Init()
        
         @brief Initialise text view component.
        
         @author    Ryan Beruldsen
         @date  10/10/2016
         */

        public void Init() {
            textLabel = new CCLabel("TAP BUTTON TO RESOLVE WARNING WHEN RAISED", "robotob", 14) {
                Position = new CCPoint(_division.Width / 2, _division.Y - _division.Height * 0.2f),
                Color = CCColor3B.White,
                AnchorPoint = CCPoint.AnchorMiddle
            };
            AddChild(textLabel);
        }
    }
}
