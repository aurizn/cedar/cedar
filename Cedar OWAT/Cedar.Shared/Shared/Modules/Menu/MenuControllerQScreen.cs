﻿/*
 * CEDAR Operator Workload Assessment Tool
 * Copyright (C) 2023  aurizn
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using CocosSharp;
using System;
using System.Collections.Generic;
using System.Text;
using CEDAR.Shared;
using CEDAR.Shared.CedarCollection;

namespace CEDAR.Shared.Modules.Menu {

    /**
     @class MenuControllerQScreen
    
     @brief The serial questionnaire screen.
    
     @author    Ryan Beruldsen
     @date  15/09/2016
     */

    class MenuControllerQScreen : CCNode {

        /** @brief Internal draw node. */
        private readonly CCDrawNode _drawNode;

        /** @brief Internal draw node. */
        private readonly CCDrawNode _drawNodeOption;

        /** @brief Serial title label. */
        private CCLabel serialTitle;

        /** @brief Serial task description label. */
        private CCLabel serialTask;

        /** @brief Internal window bounds. */
        private CCPoint _windowMax;

        /** @brief Back button, return to main menu or back a settings page. */
        private MenuViewButton _back;

        /** @brief Next button, next settings page if available. */
        private MenuViewButton _next;

        /** @brief Back button, return to main menu or back a settings page. */
        private readonly MenuViewButton _cedar;

        /** @brief Index of selected rating option. */
        private int _selectedIndex = -1;

        /** @brief Back button, return to main menu or back a settings page. */
        private readonly MenuViewButton _end;

        /** @brief Listener for button touch events. */
        private readonly CCEventListenerTouchAllAtOnce _touchListener;

        /** @brief Invisible, but touch sensitive stand-in buttons for the user to select a score. */
        private readonly List<MenuViewButton> _optionButtonList;

        private readonly List<List<CCLabel>> _stepLabelList;

        /**
         @fn    public MenuControllerQScreen(CCPoint windowMax) : base()
        
         @brief Questionnaire screen for qualitative user response.
        
         @author    Ryan Beruldsen
         @date  16/09/2016
        
         @param windowMax   Window bounds (max X and Y coordinates)
         */

        public MenuControllerQScreen(CCPoint windowMax) : base() {
            _windowMax = new CCPoint(windowMax.X, windowMax.Y);
            _drawNode = new CCDrawNode();
            _drawNodeOption = new CCDrawNode();

            _touchListener = new CCEventListenerTouchAllAtOnce {
                OnTouchesBegan = HandleInput,
                OnTouchesEnded = HandleInputEnd,
                OnTouchesMoved = HandleInputDrag
            };

            _optionButtonList = new List<MenuViewButton>();
            _stepLabelList = new List<List<CCLabel>>();

            AddEventListener(_touchListener, this);

            Init();
        }

        /**
         @fn    public void Init()
        
         @brief Initialise questionnaire page.
        
         @author    Ryan Beruldsen
         @date  20/01/2017
         */

        private void Init() {

            var title = ModelDataIOConfig.Instance.GetConfigValue(ModelData.Instance.CurrentQ, "descr");

            var data = ModelData.Instance;
            serialTitle = new CCLabel(
                title,
                "robotob",
                30,
                CCLabelFormat.SpriteFont
            ) {
                Position = new CCPoint(ModelData.Width * 0.1f, ModelData.Height * 0.875f),
                Color = CCColor3B.White,
                AnchorPoint = CCPoint.AnchorMiddleLeft
            };
            AddChild(serialTitle);

            serialTask = new CCLabel(
                "Tap to Select a Rating Described Below",
                "roboto",
                14,
                CCLabelFormat.SpriteFont
            ) {
                Position = new CCPoint(ModelData.Width * 0.1f, ModelData.Height * 0.825f),
                Color = CCColor3B.White,
                AnchorPoint = CCPoint.AnchorMiddleLeft
            };
            AddChild(serialTask);

            _next = new MenuViewButton(
               label: "Submit and Return >",
               highlightColour: data.GetColourPallete4B(ModelData.COLOUR_PARAM_ACTIVE),
                width: _windowMax.X * 0.3f,
               height: _windowMax.Y * 0.04f,
               x: _windowMax.X * 0.825f,
               y: _windowMax.Y * 0.95f);

            //back to main menu button

            AddChild(_drawNode);
            AddChild(_drawNodeOption);
            _back = new MenuViewButton(
                label: "< Submit and Return",
                highlightColour:
                    ModelData.Instance.GetColourPallete4B(ModelData.COLOUR_PARAM_ACTIVE),
                width: _windowMax.X * 0.3f,
                height: _windowMax.Y * 0.04f,
                x: _windowMax.X * 0.18f,
                y: _windowMax.Y * 0.95f);

            AddChild(_back);
            AddChild(_next);
            _next.Visible = false;
            AddChild(_drawNode);
            DrawQuestionnaire();

            _back.Visible = false;
        }

        /**
		@fn    public void DrawQuestionnaire(bool redraw = false)

         @brief Draw questionnaire category labels and option buttons.
         
		 @param redraw     If the questionnaire draw is a re-draw on selection of an option, or an initial draw.

         @author    Ryan Beruldsen
         @date  20/01/2017
         */
        private void DrawQuestionnaire(bool redraw = false) {
            _drawNode.Clear();
            //list of step categories and steps
            var steps = new List<List<string>>();

            var yTop = ModelData.Height * 0.8f;
            var space = ModelData.Height * 0.03f;
            var textSpace = ModelData.Height * 0.04f;
            var stepCount = 0;
            float startY = 0;
            var qKey = ModelData.Instance.CurrentQ;
            var colorList = new List<CCColor4B>();

            //retreive list of step and step categories from config
            var qList = ModelDataIOConfig.Instance.GetConfigType(qKey);
            var stepsIndex = -1;

            //convert config mapping of step and step categories to 
            //ennumerable list of lists
            foreach (KeyValuePair<string, string> q in qList) {
                if (int.TryParse(q.Key, out int i)) {
                    if (q.Value.Substring(0, 2).Equals(">>")) {
                        steps.Add(new List<string>());
                        stepsIndex++;
                        var titleStr = q.Value.Replace(">>", "");
                        steps[stepsIndex].Add(titleStr);
                    } else if (stepsIndex > -1) {
                        steps[stepsIndex].Add(q.Value);
                    }
                }
            }

            //draw questionaire score steps 
            for (int i = 0; i < steps.Count; i++) {
                int gradient = Convert.ToInt32(255.0 * ((double)i / (double)steps.Count));
                for (int j = 0; j < steps[i].Count; j++) {

                    if (j == 0) {
                        if (!redraw) {
                            _stepLabelList.Add(new List<CCLabel>());
                        }

                        startY = yTop - space * stepCount;

                        colorList.Add(new CCColor4B(255, (byte)255 - gradient, (byte)255 - gradient, 255));
                        if (i != _selectedIndex) {
                            _drawNode.DrawLine(
                                from: new CCPoint(ModelData.Width * 0.1f, yTop - textSpace - space * stepCount),
                                to: new CCPoint(ModelData.Width * 0.6f, yTop - textSpace - space * stepCount),
                                lineWidth: 2,
                                color: new CCColor4B(255, Convert.ToByte(255 - gradient), Convert.ToByte(255 - gradient), 255));
                        }
                        stepCount++;
                    }

                    if (!redraw) {
                        var stepLabel = new CCLabel(
                        steps[i][j],
                        "robotob",
                        14,
                        CCLabelFormat.SpriteFont
                    ) {
                            Position = new CCPoint(ModelData.Width * 0.1f, yTop - space * stepCount),
                            Color = CCColor3B.White,
                            AnchorPoint = CCPoint.AnchorMiddleLeft
                        };
                        _stepLabelList[i].Add(stepLabel);
                        AddChild(stepLabel);
                    }


                    stepCount++;
                }
                if (!redraw) {
                    //draw main dialog rectangle
                    float optionHeight = Math.Abs((yTop - space * stepCount) - startY);
                    float optionWidth = ModelData.Width;

                    var x = new MenuViewButton(
                        label: "",
                        highlightColour: CCColor4B.Black,
                        width: optionWidth,
                        height: optionHeight,
                        x: (ModelData.Width * 0.1f) + optionWidth / 2,
                        y: startY - optionHeight + optionHeight / 2,
                        visible: false);
                    _optionButtonList.Add(x);
                }

            }
            if (!redraw) {
                _optionButtonList[0].Active = true;
                _next.Visible = true;
                OptionInputEnd();
            }

        }

        /**
         @fn    private void HandleInputDrag(System.Collections.Generic.List<CCTouch> touches, CCEvent touchEvent)

         @brief Handles the touch input drag event.

         @author    Ryan Beruldsen
         @date  20/09/2016

         @param touches     CocosSharp injected touch list
         @param touchEvent  CocosSharp injected touch event
         */

        private void HandleInputDrag(System.Collections.Generic.List<CCTouch> touches,
                                    CCEvent touchEvent) {
            for (var i = 0; i < _optionButtonList.Count; i++) {
                if (MenuButtonHitTest(touches, _optionButtonList[i])) {
                    _optionButtonList[i].Active = true;
                    _selectedIndex = i;
                }
            }

            //if score option button is selected
            for (var i = 0; i < _optionButtonList.Count; i++) {
                if (_optionButtonList[i].Active) {
                    _optionButtonList[i].Active = false;
                    float optionWidth = _optionButtonList[i].maxButtonBounds.X - _optionButtonList[i].minButtonBounds.X;
                    float optionHeightOr = _optionButtonList[i].maxButtonBounds.Y - _optionButtonList[i].minButtonBounds.Y;

                    float optionHeight = ModelData.Height * 0.0425f;
                    _drawNodeOption.Clear();

                    //draw highlight rectangle to indicate selection
                    _drawNodeOption.DrawRect(
                       new CCRect(_optionButtonList[i].X - optionWidth / 2, _optionButtonList[i].Y + optionHeightOr / 2 - optionHeight, optionWidth, optionHeight),
                       fillColor: ModelData.Instance.GetColourPallete4B(ModelData.COLOUR_PARAM_ACTIVE),
                       borderWidth: 0,
                       borderColor: CCColor4B.White);
                    _next.Visible = true;
                    _selectedIndex = i;

                    for (var j = 0; j < _optionButtonList.Count; j++) {
                        _stepLabelList[j][0].Color = CCColor3B.White;
                    }

                    _stepLabelList[i][0].Color = CCColor3B.Black;
                    DrawQuestionnaire(redraw: true);
                }
            }
        }

        private void OptionInputEnd() {
            //if score option button is selected
            for (var i = 0; i < _optionButtonList.Count; i++) {
                if (_optionButtonList[i].Active && _selectedIndex != i) {
                    _selectedIndex = i;
                    _optionButtonList[i].Active = false;
                    float optionWidth = _optionButtonList[i].maxButtonBounds.X - _optionButtonList[i].minButtonBounds.X;
                    float optionHeightOr = _optionButtonList[i].maxButtonBounds.Y - _optionButtonList[i].minButtonBounds.Y;

                    float optionHeight = ModelData.Height * 0.0425f;
                    _drawNodeOption.Clear();

                    //draw highlight rectangle to indicate selection
                    _drawNodeOption.DrawRect(
                       new CCRect(_optionButtonList[i].X - optionWidth / 2, _optionButtonList[i].Y + optionHeightOr / 2 - optionHeight, optionWidth, optionHeight),
                       fillColor: ModelData.Instance.GetColourPallete4B(ModelData.COLOUR_PARAM_ACTIVE),
                       borderWidth: 0,
                       borderColor: CCColor4B.White);
                    _next.Visible = true;
                    _selectedIndex = i;

                    for (var j = 0; j < _optionButtonList.Count; j++) {
                        _stepLabelList[j][0].Color = CCColor3B.White;
                    }

                    _stepLabelList[i][0].Color = CCColor3B.Black;
                    DrawQuestionnaire(redraw: true);

                }
            }
        }

        /**
         @fn    private void HandleInputEnd(System.Collections.Generic.List<CCTouch> touches, CCEvent touchEvent)
        
         @brief Handles touch input end
        
         @author    Ryan Beruldsen
         @date  16/09/2016
        
         @param touches     CocosSharp injected touch list
         @param touchEvent  CocosSharp injected touch event
         */
        private void HandleInputEnd(System.Collections.Generic.List<CCTouch> touches, CCEvent touchEvent) {
            OptionInputEnd();

            //if submit/back button is selected
            if (_next.Active && MenuButtonHitTest(touches, _next)) {
                //determine score from config
                var scoreBasis = ModelDataIOConfig.Instance.GetConfigNum(ModelData.Instance.CurrentQ, "value");
                string sanitisedQ = ModelData.Instance.CurrentQ;
                if (sanitisedQ.IndexOf("_") > -1) {
                    sanitisedQ = sanitisedQ.Substring(0, sanitisedQ.IndexOf("_"));
                }
                int score = scoreBasis + _selectedIndex;
                long serialSecondsElapsed = (ModelData.Timestamp() - ModelData.Instance.SessionStartTime) / 1000;

                //log score for output
                var eventRow = new CDRLogRow() {
                        ModelData.TimeString(serialSecondsElapsed),
                        sanitisedQ,
                        score+"",
                };
                ModelDataIOLog.Instance.LogTaskEvent("q", eventRow);

                ModelData.CurrentPageStr = "serial";
                _next.Active = false;
            }
        }

        /**
         @fn    private void MenuButtonHitTest(System.Collections.Generic.List<CCTouch> touches, MenuViewButton m)
        
         @brief Hit test for menu button, shorthand function.
        
         @author    Ryan Beruldsen
         @date  16/09/2016
        
         @param touches CocosSharp injected touch list (from touch handler)
         @param m       The MenuViewButton to test for bounding box hit.
        
         @return    True if within button/drop down bounds, otherwise false
         */

        private bool MenuButtonHitTest(System.Collections.Generic.List<CCTouch> touches, MenuViewButton m) {
            if (m != null && touches[0].Location.X > m.minButtonBounds.X &&
                     touches[0].Location.X < m.maxButtonBounds.X &&
                     touches[0].Location.Y > m.minButtonBounds.Y &&
                     touches[0].Location.Y < m.maxButtonBounds.Y &&
                     m.Visible) {
                return true;
            }
            return false;
        }

        /**
         @fn    private void HandleInput(System.Collections.Generic.List<CCTouch> touches, CCEvent touchEvent)

         @brief Handles touch input end

         @author    Ryan Beruldsen
         @date  16/09/2016

         @param touches     CocosSharp injected touch list
         @param touchEvent  CocosSharp injected touch event
         */
        private void HandleInput(System.Collections.Generic.List<CCTouch> touches, CCEvent touchEvent) {
            if (touches.Count > 0) {
                for (var i = 0; i < _optionButtonList.Count; i++) {
                    if (MenuButtonHitTest(touches, _optionButtonList[i])) {
                        _optionButtonList[i].Active = true;
                    }
                }
                if (MenuButtonHitTest(touches, _next) && _next.Visible == true) {
                    _next.Active = true;

                } else if (MenuButtonHitTest(touches, _next)) {
                    _next.Active = true;

                } else if (MenuButtonHitTest(touches, _cedar)) {
                    _cedar.Active = true;

                } else if (MenuButtonHitTest(touches, _end)) {
                    _end.Active = true;

                }
            }
        }
    }
}

