﻿/*
 * CEDAR Operator Workload Assessment Tool
 * Copyright (C) 2023  aurizn
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;

namespace CEDAR.Shared {
    /**
     @class ModelDataEventArgs
    
     @brief Whole of app model data event parameters
    
     @author    Ryan Beruldsen
     @date  15/09/2016
     */

    public class ModelDataEventArgs : EventArgs {

        /**
         @property  public bool pageNavigate
        
         @brief Gets or sets a value indicating a page navigate event.

         @return    true if page navigate event, false if not.
         */

        public bool PageNavigate { get; set; }

        /**
         @property  public bool dialog

         @brief Gets or sets a value indicating a dialog event.

         @return    true if dialog event, false if not.
         */

        public bool Dialog { get; set; }

        /**
         @property  public string dialogMessage

         @brief Gets or sets a value indicating the dialog string.

         @return    dialog string.
         */

        public string DialogMessage { get; set; }

        /**
         @property  public Func<bool, bool> dialogCallback

         @brief Gets or sets the dialog callback function.

         @return    dialog callback function.
         */

        public Func<bool, bool> DialogCallback { get; set; }


        /**
         @property  public bool zip
        
         @brief Zip.
        
         @return    Zip.
         */

        public bool Zip { get; set; }

        /**
         @property  public bool settingsChange
        
         @brief Gets or sets a value indicating whether the settings change event.
        
         @return    true if settings change event, false if not.
         */

        public bool SettingsChange { get; set; }

        /**
         @property  public string filename
        
         @brief Gets or sets the output filename, raised with file settings change event.
        
         @return    The filename.
         */

        public string Filename { get; set; }

        /**
         @property  public string fileContents
        
         @brief Gets or sets the output file contents, raised with file settings change event.
        
         @return    The file contents change event.
         */

        public string FileContents { get; set; }

        /** @brief List of settings changes raised with settings changed event. */
        public List<string> settingsChangeList = new List<string>();
    }
}
