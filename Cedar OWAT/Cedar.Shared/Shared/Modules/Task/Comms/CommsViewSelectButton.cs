﻿/*
 * CEDAR Operator Workload Assessment Tool
 * Copyright (C) 2023  aurizn
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using CocosSharp;
using System;

namespace CEDAR.Shared.Modules.Task.Comms {
    /**
     @class CommsViewSelectButton
    
     @brief Communications channel select button, placed in series in a horizonal selection bar (CommsControllerSelectBar)
    
     @author    Ryan Beruldsen
     @date  15/09/2016
     */

    class CommsViewSelectButton : DivisionContent, IDivisionContent {

        /** @brief The button width. */
        private readonly float _width;

        /** @brief The button height. */
        private readonly float _height;

        /** @brief The button position x coordinate. */
        private readonly float _positionX;

        /** @brief The button position y coordinate. */
        private readonly float _positionY;

        /** @brief Button selected state. */
        private bool _selected;

        /** @brief The unique button position index. */
        private readonly int _index;

        /** @brief Warning indication state. */
        private bool _warnIndicate;


        /** @brief The button text highlight colour. */
        private readonly CCColor3B _colourActiveText;

        /** @brief The button warning colour. */
        private readonly CCColor4B _colourWarning;

        /** @brief The button highlight colour. */
        private readonly CCColor4B _colourActive;

        /** @brief The button highlight sprite. */
        private CCSprite _highlightSprite;

        /** @brief The button warn indicate sprite. */
        private CCSprite _warnSprite;

        /** @brief The ok (unselected) sprite. */
        private CCSprite _okSprite;

        /** @brief The primary label string. */
        private string _plabelStr;

        /** @brief The secondary string. */
        private string _slabelStr;

        /** @brief The minimum button bounds. */
        private CCPoint _minButtonBounds;

        /** @brief The maximum button bounds. */
        private CCPoint _maxButtonBounds;

        /** @brief The primary (title) button label. */
        private CCLabel _primaryLabel;
        
        /** @brief The primary (subtitle) button label. */
        private CCLabel _secondaryLabel;

        /**
         @property  public CCPoint minButtonBounds
        
         @brief Gets the minimum button bounds.
        
         @return    The minimum button bounds.
         */

        public CCPoint MinButtonBounds {
            get { return _minButtonBounds;  }
        }

        /**
         @property  public CCPoint maxButtonBounds
        
         @brief Gets the maximum button bounds.
        
         @return    The maximum button bounds.
         */

        public CCPoint MaxButtonBounds {
            get { return _maxButtonBounds; }
        }

        /**
         @property  public string primaryLabelStr
        
         @brief Gets or sets the primary label string.
        
         @return    The primary label string.
         */

        public string PrimaryLabelStr {
            get { return _plabelStr; }
            set {
                _plabelStr = value;
                _primaryLabel.Text = _plabelStr;
            }
        }

        /**
         @property  public string secondaryLabelStr
        
         @brief Gets or sets the secondary label string.
        
         @return    The secondary label string.
         */

        public string SecondaryLabelStr {
            get { return _slabelStr; }
            set {
                _slabelStr = value;
                _secondaryLabel.Text = _slabelStr;
            }
        }

        /**
         @property  public bool isSelected
        
         @brief Gets or sets a value indicating whether the button is selected.
        
         @return    true if button is selected, false if not.
         */

        public bool IsSelected {
            get { return _selected; }
            set {
                if (!WarnIndicate) {
                    _primaryLabel.Color = _secondaryLabel.Color = 
                        value ? CCColor3B.Black : _colourActiveText;
                }
                _highlightSprite.Visible = value;
                _selected = value;
            }
        }

        /**
         @property  public bool warnIndicate
        
         @brief Gets or sets the warning indicate state of the button.
        
         @return    true if warning indicate, false if not.
         */

        public bool WarnIndicate {
            get { return _warnIndicate; }
            set {
                _warnIndicate = value;
                _warnSprite.Visible = _warnIndicate;
                _okSprite.Visible = !_warnIndicate;

                if (_warnIndicate) {
                    _primaryLabel.Color = _secondaryLabel.Color = CCColor3B.Black;
                } else {
                    _primaryLabel.Color = _secondaryLabel.Color = 
                        IsSelected ? CCColor3B.Black : _colourActiveText;
                }
            }
        }

        /**
		 @fn    public void SetDisplayStr(string displayValue, string targetValue = "")

		 @brief Sets primary, secondary display string.
			Sets label values, to be used after initialisation, variables alone are set in displayStr

		 @author    Ryan Beruldsen
		 @date  20/09/2016

		 @param displayValue    The display (secondary) value string.
		 @param targetValue     (Optional) Target (secondary) value string.
		 @param primaryLabel     (Optional) Bolded label (primary) value string.
		 */

        public void SetDisplayStr(string displayValue, string targetValue = "", string primaryLabel = "") {
			DisplayStr(displayValue, targetValue);
			if (!primaryLabel.Equals("")) {
				_primaryLabel.Text = primaryLabel;
				_plabelStr = primaryLabel;
			}
			if (targetValue.Equals("")) {
				_primaryLabel.Text = _plabelStr;
			}
			_secondaryLabel.Text = _slabelStr;
		}

        /**
		 @fn    private void DisplayStr(string displayValue, string targetValue = "")

		 @brief Sets primary, secondary display string.

		 @author    Ryan Beruldsen
		 @date  20/09/2016

		 @param displayValue    The display (primary) value string.
		 @param targetValue     (Optional) Target (secondary) value string.
		 */

        private void DisplayStr(string displayValue, string targetValue = "") {
			_slabelStr = displayValue;

			if(!targetValue.Equals("")) {
				//_slabelStr = targetValue + " [" + displayValue + "]";
				_primaryLabel.Text = _plabelStr + " [" + targetValue + "]";
			}
		}

        /**
         @fn    public CommsViewSelectButton(int index, string label, bool selected, bool warnIndicate, string displayValue, CCColor4B barHighlight, CCColor3B barTextHighlight, float barWidth, float barHeight, CCPoint barPosition, ControllerDivision division, string targetValue = "") : base(division)
        
         @brief Constructor of communications channel select button.
        
         @author    Ryan Beruldsen
         @date  20/09/2016
        
         @param index               Unique position index of the button (with reference to button bar).
         @param label               Label value - button primary label.
         @param selected            Initial selection status.
         @param warnIndicate        Initial warning indicate status.
         @param displayValue        Initial frequency value.
         @param colourActive        Button highlight colour (expected to be consistent across a button bar)
         @param colourActiveText    Button text highlight colour (expected to be consistent across a button bar)
         @param colourWarning       Button warn colour (expected to be consistent across a button bar)
         @param width               Width of the button.
         @param barHeight           Height of the button (and button bar).
         @param position            Coordinates of the button.
         @param division            The parent division.
         @param targetValue         (Optional) Target value - button secondary label.
         */

        public CommsViewSelectButton(   int index, 
                                        string label, 
                                        bool selected, 
                                        bool warnIndicate, 
                                        string displayValue, 
                                        CCColor4B colourActive, 
                                        CCColor3B colourActiveText, 
                                        CCColor4B colourWarning, 
                                        float width, 
                                        float barHeight, 
                                        CCPoint position, 
                                        Division division, 
                                        string targetValue = "") : base(division) {
            this._plabelStr = label;
            DisplayStr(displayValue, targetValue);
            _colourActive = colourActive;
            _colourActiveText = colourActiveText;
            _colourWarning = colourWarning;
            _width = width;
            _height = barHeight;
            _positionX = position.X;
            _positionY = position.Y;
            _index = index;
            _selected = selected;
            _warnIndicate = warnIndicate;
        }

        /**
         @fn    public void Init()
        
         @brief Initialises, draws communications channel select button.
        
         @author    Ryan Beruldsen
         @date  20/09/2016
         */

        public void Init() {
            //base instances/config
            CCDrawNode highlightDrawNode = new CCDrawNode();

            _minButtonBounds = new CCPoint(
                x: _positionX + _index * _width * 0.25f, 
                y: _positionY
            );
            _maxButtonBounds = new CCPoint(
                x: _minButtonBounds.X + _width * 0.25f, 
                y: _minButtonBounds.Y + _height
            );

            //draw selected state sprite
            _highlightSprite = new CCSprite();
            _highlightSprite.AddChild(highlightDrawNode);
            highlightDrawNode.DrawRect(
                new CCRect( _minButtonBounds.X, 
                            _minButtonBounds.Y, 
                            _width * 0.25f, 
                            _height
                ),
                fillColor: _colourActive,
                borderWidth: 0,
                borderColor: CCColor4B.White);
            //sprite visible on selected state
            _highlightSprite.Visible = _selected;

            //draw warning indicate state sprite
            CCDrawNode warningDrawNode = new CCDrawNode();
            _warnSprite = new CCSprite();
            _warnSprite.AddChild(warningDrawNode);
            warningDrawNode.DrawRect(
               new CCRect(  _minButtonBounds.X, 
                            _minButtonBounds.Y - _height*0.2f, 
                            _width * 0.25f, _height*1.2f
                ),
               fillColor: _colourWarning,
               borderWidth: 0,
               borderColor: CCColor4B.White);
            //sprite visible on warning indicate state
            _warnSprite.Visible = _warnIndicate;

            //draw resting (unselected) state sprite
            CCDrawNode okDrawNode = new CCDrawNode();
            _okSprite = new CCSprite();
            _okSprite.AddChild(okDrawNode);
            okDrawNode.DrawRect(
               new CCRect(  _minButtonBounds.X, 
                            _minButtonBounds.Y - _height * 0.2f, 
                            _width * 0.25f, 
                            _height * 0.2f
                ),
               fillColor: CCColor4B.LightGray,
               borderWidth: 0,
               borderColor: CCColor4B.White);
            //ok sprite visible on non-warning state
            _okSprite.Visible = !_warnIndicate;

            //scaled offsets
            float spacer = _width * 0.01f;
            float spacer2 = 25;

            //draw labels
            _primaryLabel = new CCLabel(_plabelStr, "robotob", 14, CCLabelFormat.SpriteFont) {
                Position = new CCPoint(
                    x: _positionX + _index * _width * 0.25f + spacer, 
                    y: _positionY + spacer2 + spacer
                ),
                Color = _selected ? CCColor3B.Black : _colourActiveText,
                AnchorPoint = CCPoint.AnchorLowerLeft,
            };

            _secondaryLabel = new CCLabel(_slabelStr, "roboto", 14, CCLabelFormat.SpriteFont) {
                Position = new CCPoint(
                    x: _positionX + _index * _width * 0.25f + spacer, 
                    y: _positionY + spacer
                ),
                Color = _selected ? CCColor3B.Black : _colourActiveText,
                AnchorPoint = CCPoint.AnchorLowerLeft,
            };
            if (_warnIndicate) _primaryLabel.Color = _secondaryLabel.Color = CCColor3B.Black;

            AddChild(_warnSprite);
            AddChild(_highlightSprite);
            AddChild(_primaryLabel);
            AddChild(_secondaryLabel);

        }
    }
}
