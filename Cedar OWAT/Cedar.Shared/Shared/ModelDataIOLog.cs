﻿/*
 * CEDAR Operator Workload Assessment Tool
 * Copyright (C) 2023  aurizn
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using CEDAR.Shared.CedarCollection;

namespace CEDAR.Shared {

    /**
     @class ModelDataIOLog
    
     @brief The I/O log file data handler. 
        
        - This represents the plaintext output data produced by MATB.
    
     @author    Ryan Beruldsen
     @date  6/10/2016
     */

    public sealed class ModelDataIOLog {

        /** @brief The singleton I/O config log instance. */
        internal static readonly ModelDataIOLog _instance = new ModelDataIOLog();

        /**
         @fn    private ModelDataIOLog()
        
         @brief Singleton I/O log data constructor (stub). 
        
         @author    Ryan Beruldsen
         @date  6/10/2016
         */

        private ModelDataIOLog() { }


        /** @brief The central cedar log hashmap - contains general index of task and schedule level events. */
        private CDRKeyValLogData _cedarLog = new CDRKeyValLogData();

        /** @brief Hold touch data in a hash map of locations to touch data. */
        private CDRTouchLogData _touchLogData = new CDRTouchLogData();

        /** @brief Hold touch data in a list sorted by timestamp. */
        private CDRTouchLog _touchLog = new CDRTouchLog();

        /** @brief Score log - user test performance. */
        private CDRScoreLogData _cedarScoreLogData = new CDRScoreLogData();

        /** @brief Score log - user test performance. */
        private CDRScoreLog _cedarScoreLog = new CDRScoreLog();

        /** @brief A hashmap of task tag to it's plaintext output header. */
        private CDRKeyValData _outputHeader = new CDRKeyValData();

        /** @brief The cedar task level log - specific task-level information. */
        private CDRTasksLog _cedarTasksLog = new CDRTasksLog();

        /** @brief Central event index (for overall and task-specific data linking). */
        private int _eventIndex = 0;

        /** @brief Counter for total correct, scored user actions. */
        private int _scoreResolved = 0;

        /** @brief Counter for total timed out tasks the user did not complete. */
        private int _scoreTimeout = 0;

        /** @brief Counter for total user reaction time for tasks. */
        private long _scoreReactionTime = 0;

        /** @brief Counter for total correct touch hits. */
        private int _touchHits = 0;

        /** @brief Counter for total touch misses. */
        private int _touchMisses = 0;

        /** @brief Counter for total touch hits where the touch was not needed. */
        private int _touchFalsePos = 0;

        /** @brief Whether the rate screen log header has been initialised. */
        public static bool RateScreenInit { get; set; }

        /** @brief List of logged tasks. */
        public static List<string> loggedTasksStr = new List<string>();

        /** @brief List of logged analysis files. */
        public static List<string> loggedAnalysisStr = new List<string>();

        /** @brief Central event index (for overall and task-specific data linking). */
        public int EventIndex {
            get { return _eventIndex; }
            set { _eventIndex = value; }
        }

        /**
         @property  public long scoreReactionTime

         @brief Cumulative milliseconds of reaction time.

            - this total is divided by the total amount of 
            events to gain an average reaction time metic

         @return    Cumulative milliseconds of reaction time.
         */
        public long ScoreReactionTime {
            get {
                if (_scoreResolved > 0) {
                    return (long)((double)_scoreReactionTime / (_scoreTimeout + _scoreResolved));
                }
                else {
                    return 0;
                }
            }
        }

        /**
         @property  public double scorePerformance

         @brief Fraction of total events the user has responded correctly to.

            - any event that raises a score penalty is considered an event here, 
            not just individual XML events

         @return    Fraction of total events not timed out.
        */
        public double ScorePerformance {
            get {
                double total = _scoreTimeout + _scoreResolved;

                if (total > 0) {
                    return (double)_scoreResolved / total;
                }
                else {
                    return 0;
                }
            }
        }

        /**
         @property  public double scorePerformance

         @brief Fraction of total touches that have been accurate.

            - this means the touch was aimed correctly at a component that needed attention

         @return    Fraction of total touches that have been accurate.
        */
        public double TouchAccuracy {
            get {
                double total = _touchMisses + _touchFalsePos + _touchHits;

                if (total > 0) {
                    return (double)_touchHits / total;
                }
                else {
                    return 0;
                }
            }
        }

        /**
         @property  public double touchPrecision

         @brief Fraction of total touches that have been precise.

            - this means the touch was aimed correctly at a component
            - distinguishable from scorePerformance as this metric includes
            false positives, where a component was touched that did not need attention

         @return    Fraction of total touches that have been precise.
        */
        public double TouchPrecision {
            get {
                double total = _touchMisses + _touchFalsePos + _touchHits;

                if (total > 0) {
                    return (double)(_touchHits + _touchFalsePos) / total;
                }
                else {
                    return 0;
                }
            }
        }

        /**
         @fn    public void LogScore(string taskTag, bool resolved = false, bool timeout = false)

         @brief Logs a scoring event for metrics - this is a raised, resolved or timeout (penalty) event.

         @param taskTag             X coordinate.
         @param resolved            (Optional) Whether the event is a resolved event.
         @param timeout             (Optional) Whether the event is a timeout event.


         @author    Ryan Beruldsen
         @date  21/11/2016
         */

        public void LogScore(string taskTag, bool resolved = false, bool timeout = false) {
            var score = new CDRScoreData();
            if (resolved && _cedarScoreLogData.ContainsKey(taskTag) && _cedarScoreLogData[taskTag].Peek().ReactionTime == 0) {
                score.TimeResolved = Convert.ToInt64(ModelData.Instance.DurationDouble * 1000);
                //score.timeResolved = ModelData.timestamp();
                var reactionTime = Convert.ToInt64(ModelData.Instance.DurationDouble * 1000) - _cedarScoreLogData[taskTag].Peek().TimeRaised;
                _scoreReactionTime += reactionTime;
                var resolvedStr = ModelData.Instance.DurationStr24Hr;
                score.ReactionTimeStr = ModelData.TimestampStr(reactionTime);
                score.ReactionTime = reactionTime;
                score.TimeRaisedStr = _cedarScoreLogData[taskTag].Peek().TimeRaisedStr;
                score.TimeResolvedStr = resolvedStr;
                score.Tag = taskTag;
                score.Timeout = timeout;

                if (timeout) {
                    _scoreTimeout++;
                }
                else {
                    _scoreResolved++;
                }

                if (!_cedarScoreLog.ContainsKey(resolvedStr)) {
                    _cedarScoreLog[resolvedStr] = new List<CDRScoreData>();
                }
                _cedarScoreLog[resolvedStr].Add(score);
                //printScoreLog();

            }
            else {
                score.TimeRaised = Convert.ToInt64(ModelData.Instance.DurationDouble * 1000);
                //score.timeRaised = ModelData.timestamp();
                score.TimeRaisedStr = ModelData.Instance.DurationStr24Hr;
                score.ReactionTime = 0;
                score.Tag = taskTag;
                if (!_cedarScoreLogData.ContainsKey(taskTag)) {
                    _cedarScoreLogData[taskTag] = new Stack<CDRScoreData>();
                }
                _cedarScoreLogData[taskTag].Push(score);
            }
        }

        /**
         @fn    public void PrintScoreLog()

         @brief Prints and logs to task event format recorded scoring metrics.

         @author    Ryan Beruldsen
         @date  21/11/2016
         */

        public void PrintScoreLog() {

            var eventRowHeader = new CDRLogRow() {
                    "-RAISED-",
                    "-RESOLVED-",
                    "-TAG-",
                    "-REACTION_TIME-",
                    "-TIMEOUT-",
            };
            string header = "# Performance = " + (ScorePerformance * 100).ToString("0.00") + "%" + "\r\n" +
                            "# Average Reaction Time = " + ModelData.TimestampStr(ScoreReactionTime) + "\r\n" +
                            "#";
            var set = ModelDataIOLog.Instance.LogTaskHeader("score", header);
            if (!set) {
                ModelDataIOLog.Instance.LogTaskEvent("score", eventRowHeader);
            }

            //System.Diagnostics.Debug.WriteLine("");
            foreach (List<CDRScoreData> scoreList in _cedarScoreLog.Values) {
                foreach (CDRScoreData score in scoreList) {
                    //System.Diagnostics.Debug.WriteLine(score.timeRaisedStr + "-" + score.timeResolvedStr + " " + score.tag);

                    if (score.Timeout) {
                        //System.Diagnostics.Debug.WriteLine("TIMEOUT: "+score.reactionTimeStr);
                    }
                    else {
                        //System.Diagnostics.Debug.WriteLine("REACTION TIME: "+score.reactionTimeStr);
                    }
                    //System.Diagnostics.Debug.WriteLine("");

                    var eventRow = new CDRLogRow() {
                        score.TimeRaisedStr,
                        score.TimeResolvedStr,
                        score.Tag,
                        score.ReactionTimeStr,
                        score.Timeout ? "Y" : "N"
                    };
                    ModelDataIOLog.Instance.LogTaskEvent("score", eventRow);
                }
            }
            //System.Diagnostics.Debug.WriteLine("PERFORMANCE:\t" + (scorePerformance*100)+"%");
            //System.Diagnostics.Debug.WriteLine("REACTION TIME:\t" + ModelData.timestampStr(scoreReactionTime));


        }

        /**
         @fn    public void LogTouch(float x, float y, CDRTouchData.statusEnum status)

         @brief Logs a touch event for metrics.

            - Touches are logged in a stack per cartesian coordinate, 
            this means that a new touch at exactly the same location as another will be overridden
         @param x           X coordinate.
         @param y           Y coordinate.
         @param status      The status of the touch, whether it was a hit, miss or false positive.


         @author    Ryan Beruldsen
         @date  21/11/2016
         */

        public void LogTouch(float x, float y, CDRTouchData.StatusEnum status) {
            ClearTaskEventLogs("touch");
            var touchData = new CDRTouchData() {
                X = x,
                Y = y,
                Status = status,
                TimestampStr = ModelData.Instance.DurationStr24Hr,
                Timestamp = ModelData.Timestamp()
            };

            var coord = x + "," + y;

            if (!_touchLogData.ContainsKey(coord)) {
                _touchLogData[coord] = new Stack<CDRTouchData>();
                _touchLogData[coord].Push(touchData);
                _touchLog[touchData.TimestampStr] = _touchLogData[coord];

            }
            else if (status != CDRTouchData.StatusEnum.Miss) {
                long deBounceTimestamp = _touchLogData[coord].Peek().Timestamp;
                string deBounceTimeKey = _touchLogData[coord].Peek().TimestampStr;
                long bounceMils = touchData.Timestamp - deBounceTimestamp;
                bool bounce = bounceMils <= 300;

                _touchLogData[coord].Push(touchData);

                //if a miss has already been recorded within 300ms, this is a 
                //bounced (junk) touch, override with hit
                if (bounce && (
                    status == CDRTouchData.StatusEnum.FalsePos ||
                    status == CDRTouchData.StatusEnum.Hit) &&
                    _touchLog.ContainsKey(deBounceTimeKey)) {
                    _touchLog.Remove(deBounceTimeKey);
                    _touchLog[touchData.TimestampStr] = _touchLogData[coord];
                }

            }

        }

        /**
         @fn    public void PrintTouchLog()

         @brief Prints and logs to task event format recorded touch metrics.

         @author    Ryan Beruldsen
         @date  21/11/2016
         */

        public void PrintTouchLog() {
            ClearTaskEventLogs("touch");
            _touchHits = 0;
            _touchMisses = 0;
            _touchFalsePos = 0;

            var eventRowHeader = new CDRLogRow() {
                    "-TIME-",
                    "-X-",
                    "-Y-",
                    "-STATUS-",
            };
            ModelDataIOLog.Instance.LogTaskEvent("touch", eventRowHeader);
            foreach (Stack<CDRTouchData> touchDataStack in _touchLog.Values) {
                if (touchDataStack.Count > 0) {
                    var touchData = touchDataStack.Peek();

                    var eventRow = new CDRLogRow() {
                        touchData.TimestampStr,
                        touchData.X.ToString("0.0"),
                        touchData.Y.ToString("0.0"),
                        touchData.Status.ToString(),
                    };
                    ModelDataIOLog.Instance.LogTaskEvent("touch", eventRow);
                    /*
                    System.Diagnostics.Debug.WriteLine(touchData.timestampStr +
                        " [" + touchData.x + ", " + touchData.y + "] - " +
                        touchData.status.ToString());
                        */
                    if (touchData.Status == CDRTouchData.StatusEnum.FalsePos) _touchFalsePos++;
                    if (touchData.Status == CDRTouchData.StatusEnum.Hit) _touchHits++;
                    if (touchData.Status == CDRTouchData.StatusEnum.Miss) {
                        _touchMisses++;
                    }

                }

            }

            string header = "# Hits = " + _touchHits + "\r\n" +
                "# Misses = " + _touchMisses + "\r\n" +
                "# False Positives = " + _touchFalsePos + "\r\n" +
                "# Accuracy = " + (TouchAccuracy * 100).ToString("0.00") + "%" + "\r\n" +
                "# Precision = " + (TouchPrecision * 100).ToString("0.00") + "%" + "\r\n" +
                "#";
            ModelDataIOLog.Instance.LogTaskHeader("touch", header);
            /*
            System.Diagnostics.Debug.WriteLine("MISSES:\t\t\t" + _touchMisses);
            System.Diagnostics.Debug.WriteLine("HITS:\t\t\t\t" + _touchHits);
            System.Diagnostics.Debug.WriteLine("FALSE POSITIVES:\t\t" + _touchFalsePos);

            

            System.Diagnostics.Debug.WriteLine("ACCURACY:\t\t\t" + (touchAccuracy* 100) + "%");
            System.Diagnostics.Debug.WriteLine("PRECISION:\t\t\t" + (touchPrecision * 100)+"%");
            */


        }

        /**
         @fn    public int LogEvent(string eventDescription, bool terminated = false)
        
         @brief Logs an indexed cedar central event at current test timing.
        
         @author    Ryan Beruldsen
         @date  6/10/2016
        
         @param eventDescription    Information describing the event.
         @param terminated          (Optional) True if termination event.
        
         @return    The new event index of the event.
         */

        public int LogEvent(string eventDescription, bool terminated = false) {
            string time = ModelData.Instance.DurationStr24Hr;
            if (!_cedarLog.ContainsKey(time)) {
                _cedarLog[time] = new HashSet<CDRLogData>();
            }
            _cedarLog[time].Add(
                new CDRLogEvent(
                        ++_eventIndex,
                        terminated ? "Event Terminated: " + eventDescription :
                            "Event Initiated: " + eventDescription
                    )
                );
            return _eventIndex;
        }

        /**
         @fn    public int LogEvent(string time, string eventDescription, bool terminated = false, int indexInc = 0)
        
         @brief Logs an indexed cedar central event, overload to allow custom time.
        
         @author    Ryan Beruldsen
         @date  6/10/2016
        
         @param time                The time.
         @param eventDescription    Information describing the event.
         @param terminated          (Optional) True if terminated.
         @param indexInc            (Optional) Number of extra increments to event index.
        
         @return    An int.
         */

        public int LogEvent(string time,
                            string eventDescription,
                            bool terminated = false,
                            int indexInc = 0) {

            if (!_cedarLog.ContainsKey(time)) {
                _cedarLog[time] = new HashSet<CDRLogData>();
            }
            _eventIndex += indexInc;

            var logEvent = new CDRLogEvent(
                ++_eventIndex,
                terminated ? "Event Terminated: " + eventDescription :
                    "Event Initiated: " + eventDescription);

            _cedarLog[time].Add(logEvent);
            return _eventIndex;
        }

        /**
         @fn    public void LogEventBrief(string time, string eventDescription, bool terminated = false)
        
         @brief Logs an non-indexed cedar central brief event 
            
            - this is a short indication of an event that is detailed in task-specific logs.
            - overload to allow custom timing
        
         @author    Ryan Beruldsen
         @date  6/10/2016
        
         @param time                The time.
         @param eventDescription    Information describing the event.
         @param terminated          (Optional) True if terminated.
         */

        public void LogEventBrief(string time,
                                    string eventDescription,
                                    bool terminated = false) {

            if (!_cedarLog.ContainsKey(time)) {
                _cedarLog[time] = new HashSet<CDRLogData>();
            }
            _cedarLog[time].Add(new CDRLogData(terminated ? "Event Terminated: " +
                eventDescription : eventDescription));
        }

        /**
         @fn    public void LogEventBrief(string eventDescription, bool terminated = false)
        
         @brief Logs an non-indexed cedar central brief event at current test timing.
            
            - this is a short indication of an event that is detailed in task-specific logs.
        
         @author    Ryan Beruldsen
         @date  6/10/2016
        
         @param eventDescription    Information describing the event.
         @param terminated          (Optional) True if terminated.
         */

        public void LogEventBrief(string eventDescription, bool terminated = false) {
            string time = ModelData.Instance.DurationStr24Hr;
            if (!_cedarLog.ContainsKey(time)) {
                _cedarLog[time] = new HashSet<CDRLogData>();
            }
            _cedarLog[time].Add(new CDRLogData(terminated ?
                "Event Terminated: " + eventDescription : eventDescription));
        }

        /**
         @fn    public void ClearLogs()
        
         @brief Clears the central cedar log.
        
         @author    Ryan Beruldsen
         @date  6/10/2016
         */

        public void ClearLogs() {
            _cedarLog = new CDRKeyValLogData();
            _outputHeader = new CDRKeyValData();
            _cedarTasksLog = new CDRTasksLog();
            _touchLog = new CDRTouchLog();
            _touchLogData = new CDRTouchLogData();
            _cedarScoreLog = new CDRScoreLog();
            _cedarScoreLogData = new CDRScoreLogData();

            _scoreReactionTime = 0;
            _scoreResolved = 0;
            _scoreTimeout = 0;
            _eventIndex = 0;
            _touchHits = 0;
            _touchMisses = 0;
            _touchFalsePos = 0;
        }

        public void ClearAnalysis() {
            _scoreReactionTime = 0;
            _scoreResolved = 0;
            _scoreTimeout = 0;
            _touchHits = 0;
            _touchMisses = 0;
            _touchFalsePos = 0;
        }

        public void ClearTaskEventLogs(string task) {
            _cedarTasksLog[task] = new CDRLogColumns();
        }

        /**
         @fn        public void LogTaskEvent(string task, CDRLogRow eventRow)
        
         @brief     Logs task-specific event.
        
         @author    Ryan Beruldsen
         @date      6/10/2016

         @param task        The task tag.
         @return    Number of event logs.
         */

        public int LogTaskEventCount(string task) {
            if (_cedarTasksLog.ContainsKey(task)) {
                return _cedarTasksLog[task].Count;
            }

            return 0;
        }

        /**
         @fn    public void LogTaskEvent(string task, CDRLogRow eventRow)
        
         @brief Logs task-specific event.
        
         @author    Ryan Beruldsen
         @date  6/10/2016
        
         @param task        The task tag.
         @param eventRow    The event row.
         */

        public void LogTaskEvent(string task, CDRLogRow eventRow) {
            if (!_cedarTasksLog.ContainsKey(task)) {
                _cedarTasksLog[task] = new CDRLogColumns();
            }
            if (_cedarTasksLog[task].Count > 0) {
                var titleColumn = _cedarTasksLog[task][0];
                if (eventRow.Count == titleColumn.Count) {
                    _cedarTasksLog[task].Add(eventRow);
                }
            }
            else {
                _cedarTasksLog[task].Add(eventRow);
            }

        }

        /**
         @fn    private string Filler(int length, char fill = ' ')
        
         @brief Generates string whitespace or other character fillers for plaintext logs.
        
         @author    Ryan Beruldsen
         @date  6/10/2016
        
         @param length  The length.
         @param fill    Filler character.
        
         @return    A string.
         */

        private string Filler(int length, char fill = ' ') {
            string output = "  ";
            if (fill != ' ') {
                output = "";
            }
            for (int i = 0; i < length; i++) {
                output += fill;
            }
            return output;
        }

        /**
         @fn    public void LogTaskHeader(string task, string header)
        
         @brief Sets a plaintext task header.
        
         @author    Ryan Beruldsen
         @date  6/10/2016
        
         @param task    The task tag.
         @param header  The header.
         @returns whether the header has been set previously
         */

        public bool LogTaskHeader(string task, string header) {
            var set = _outputHeader.ContainsKey(task);
            _outputHeader[task] = header;
            return set;
        }

        /**
         @fn    public void OutputAnalysisLog()

         @brief Outputs a JSON containing analysis of test session data.

         @author    Ryan Beruldsen
         @date  6/10/2016

         */

        public void OutputAnalysisLog() {
            bool formatJSON = ModelData.Instance.Settings[ModelData.FORMAT_OUTPUT].
                Equals(ModelData.FORMAT_TYPE_JSON) ||
                ModelData.Instance.Settings[ModelData.FORMAT_OUTPUT].
                Equals(ModelData.FORMAT_TYPE_ALL);

            if (ModelData.Instance.Settings[ModelData.MODE_CLEAR_ANALYSIS_ON_PAUSE] == ModelData.PARAM_ON &&
                ModelData.Instance.PauseTimeStamp != 0) {
            }

            bool trainMode = ModelData.Instance.Settings[ModelData.TRAINING_MODE] == ModelData.PARAM_ON;
            bool randomMode = ModelData.Instance.Settings[ModelData.RANDOM_MODE] != ModelData.PARAM_OFF;
            bool silentMode = ModelData.Instance.Settings[ModelData.SILENT_MODE] == ModelData.PARAM_ON;

            string output = "{\n";
            output += "\t\"initiated\": \"" + ModelData.Instance.SessionStartTime + "\",\n";
            output += "\t\"terminated\": \"" + ModelData.Timestamp() + "\",\n";
            output += "\t\"trainingMode\": \"" + trainMode + "\",\n";
            output += "\t\"silentMode\": \"" + silentMode + "\",\n";
            output += "\t\"randomMode\": \"" + randomMode + "\",\n";
            output += "\t\"jsonMode\": \"" + formatJSON + "\",\n";
            output += "\t\"performance\": \"" + ScorePerformance + "%\",\n";
            output += "\t\"scoreTimeout\": \"" + _scoreTimeout + "\",\n";
            output += "\t\"scoreResolved\": \"" + _scoreResolved + "\",\n";
            output += "\t\"scoreTotalReactionTime\": \"" + _scoreReactionTime + "\",\n";
            output += "\t\"touchFalsePos\": \"" + _touchFalsePos + "\",\n";
            output += "\t\"touchHits\": \"" + _touchHits + "\",\n";
            output += "\t\"touchMisses\": \"" + _touchMisses + "\",\n";
            output += "\t\"loggedTasks\": [\"" + string.Join("\",\"", loggedTasksStr) + "\"],\n";
            output += "\t\"serial\": \"" + string.Join("\",\"", ModelData.Instance.Settings[ModelData.SERIAL_NAME]) + "\",\n";
            output += "\t\"loggedAnalysis\": [\"" + string.Join("\",\"", loggedAnalysisStr) + "\"],\n";
            output += "\t\"partial\": \"" + ModelData.Instance.Settings[ModelData.OS_PAUSE].Equals(ModelData.PARAM_ON) + "\",\n";
            output += "\t\"duration\": \"" + ModelData.Instance.DurationDouble * 1000 + "\"\n";
            output += "}";
            ModelData.Instance.OutputFile("analysis_", output);
        }

        /**
         @fn    public void OutputTaskEventLog(string task, bool json = false)
        
         @brief Print task event log. Output can alternaltely be in JSON or CSV format.
        
         @author    Ryan Beruldsen
         @date  6/10/2016
        
         @param task    The task.
         @param json    (Optional) True if JSON output.
         @param json    (Optional) True if CSV output.
         */

        public void OutputTaskEventLog(string task, bool plaintext = true, bool json = false, bool csv = false) {
            if (json || csv) { plaintext = false; }
            //handle plaintext header
            string year = DateTime.Now.Date.Year.ToString();
            string month = DateTime.Now.Date.Month.ToString("00");
            string day = DateTime.Now.Date.Day.ToString("00");
            string minute = DateTime.Now.Minute.ToString("00");
            string hour = DateTime.Now.Hour.ToString("00");
            string second = DateTime.Now.Second.ToString("00");
            string serial = ModelData.Instance.Settings[ModelData.SERIAL_NAME];
            string filename = task.ToUpper() + "_" + ModelData.Instance.FileTimestamp();

            var serialTime = ModelData.UnixTimeStampToDateTime(ModelData.Instance.SessionStartTime);
            serialTime = serialTime.ToLocalTime();
            string serialMinute = serialTime.Minute.ToString("00");
            string serialHour = serialTime.Hour.ToString("00");
            string serialSecond = serialTime.Second.ToString("00");

            if (!serial.Equals("")) {
                filename = task.ToUpper() + "_" + ModelData.Instance.FileTimestamp(ModelData.Instance.SessionStartTime, true);
            }

            string output = "";
            if (plaintext) {
                output += "# " + month + "-" + day + "-" + year
                + "     " + hour + ":" + minute + ":" + second + "     " +
                filename + ".txt\r\n";
                string prefix = ModelData.Instance.Settings[ModelData.FILE_NAME];
                if (prefix != "") {
                    prefix += "_";
                }
                string eventsFilename = "CEDAR_EVENTS.xml";
                if (ModelData.Instance.Settings[ModelData.FORMAT_INPUT] == ModelData.FORMAT_TYPE_JSON) {
                    eventsFilename = "events.json";
                }
                output += "#\r\n# Events Filename: " + prefix + eventsFilename + "\r\n#\r\n";
                output += "#\r\n# Test Commenced: " + serialHour + ":" + serialMinute + ":" + serialSecond + "\r\n#\r\n";
            }
            //ennumerate task-specific log, format table of data
            if (_cedarTasksLog.ContainsKey(task)) {
                //if output is plaintext
                if (plaintext) {
                    //add plaintext header
                    if (_outputHeader.ContainsKey(task)) {
                        output += _outputHeader[task];
                    }
                    List<int> maxRowLength = new List<int>();
                    int totalRowLength = 0;
                    var taskLog = _cedarTasksLog[task];

                    //first pass - calculate max column widths for spacing
                    for (int i = 0; i < taskLog.Count; i++) {
                        for (int j = 0; j < taskLog[i].Count; j++) {
                            if (maxRowLength.Count <= j) maxRowLength.Add(0);
                            if (maxRowLength[j] < taskLog[i][j].Length) {
                                maxRowLength[j] = taskLog[i][j].Length;
                            }
                        }
                    }

                    //calculate title filler length
                    for (int i = 0; i < maxRowLength.Count; i++) {
                        totalRowLength += maxRowLength[i];
                    }

                    output += "\r\n";

                    //second pass - output data
                    for (int i = 0; i < taskLog.Count; i++) {
                        //format column title
                        output += i == 0 ? "#" : " ";

                        //add row formatted with filler to allow homogenous columns
                        for (int j = 0; j < taskLog[i].Count; j++) {
                            output += taskLog[i][j] +
                                Filler(maxRowLength[j] - taskLog[i][j].Length);
                        }
                        //separate column titles with line
                        if (i == 0) {
                            var rowDispLength = Convert.ToInt32(totalRowLength * 1.3);
                            output += "\r\n#" + Filler(rowDispLength, '-') + "\r\n";
                        }
                        else {
                            output += "\r\n";
                        }
                    }
                    //json output
                }
                else if (json) {
                    output = "{\n\t\"rows\" : [{\n";
                    var taskLog = _cedarTasksLog[task];
                    int numRows = taskLog.Count;
                    int currentRow = 1;

                    for (int i = 1; i < taskLog.Count; i++) {
                        currentRow++;
                        int numColumns = taskLog[i].Count;
                        int currentColumn = 0;


                        for (int j = 0; j < taskLog[i].Count; j++) {
                            currentColumn++;
                            output += "\t\t\"" + taskLog[0][j] + "\": \"" + taskLog[i][j] +
                                (currentColumn == numColumns ? "\"\n" : "\",\n");
                        }

                        if (currentRow == numRows) {
                            output += "\t}]\n";
                        }
                        else if (currentRow != 1) {
                            output += "\t}, {\n";
                        }
                    }
                    if (taskLog.Count == 1) {
                        output += "\t}]\n";
                    }
                    output += "}";
                }//csv output
                else if (csv) {
                    var taskLog = _cedarTasksLog[task];
                    int currentRow = 0;

                    for (int i = 0; i < taskLog.Count; i++) {
                        currentRow++;
                        int numColumns = taskLog[i].Count;
                        int currentColumn = 0;
                        string unixColumn = "";


                        for (int j = 0; j < taskLog[i].Count; j++) {
                            if (currentRow == 1 && currentColumn == 1) {
                                if (ModelData.Instance.Settings[ModelData.TIMING_UNIX] == ModelData.PARAM_ON) {
                                    unixColumn = "UNIX,";
                                }
                            }
                            else {
                                unixColumn = "";
                            }
                            currentColumn++;
                            output += unixColumn + taskLog[i][j] + (currentColumn == numColumns ? "\n" : ",");
                        }
                    }
                }
            }
            ModelData.Instance.OutputFile(filename, output);
        }

        /**
         @fn    public void OutputLog(bool json = false)
        
         @brief Output cedar central log. Output can alternaltely be in JSON or CSV format.
        
         @author    Ryan Beruldsen
         @date  6/10/2016
        
         @param json    (Optional) True if JSON output.
         @param csv    (Optional) True if CSV output.
         */

        public void OutputLog(bool plaintext = true, bool json = false, bool csv = false) {
            //ensure default doesn't override
            if (json || csv) { plaintext = false; }

            string output = "";

            string month = DateTime.Now.Date.Month.ToString("00");
            string day = DateTime.Now.Date.Day.ToString("00");
            string minute = DateTime.Now.Minute.ToString("00");
            string second = DateTime.Now.Second.ToString("00");
            string hour = DateTime.Now.Hour.ToString("00");
            string year = DateTime.Now.Date.Year.ToString();
            string serial = ModelData.Instance.Settings[ModelData.SERIAL_NAME];

            var serialTime = ModelData.UnixTimeStampToDateTime(ModelData.Instance.SessionStartTime);
            serialTime = serialTime.ToLocalTime();
            string serialMinute = serialTime.Minute.ToString("00");
            string serialHour = serialTime.Hour.ToString("00");
            string serialSecond = serialTime.Second.ToString("00");

            string filename = "CEDAR_" + ModelData.Instance.FileTimestamp();
            if (!serial.Equals("")) {
                filename = "CEDAR_" + ModelData.Instance.FileTimestamp(ModelData.Instance.SessionStartTime, true);
            }

            //if output is plaintext
            if (plaintext) {
                //add header
                output += "# " + month + "-" + day + "-" + year + "     " +
                hour + ":" + minute + ":" + second + "     " + filename + ".txt\r\n";

                string prefix = ModelData.Instance.Settings[ModelData.FILE_NAME];
                if (prefix != "") {
                    prefix += "_";
                }
                string eventsFilename = "CEDAR_EVENTS.xml";
                if (ModelData.Instance.Settings[ModelData.FORMAT_INPUT] == ModelData.FORMAT_TYPE_JSON) {
                    eventsFilename = "events.json";
                }
                output += "#\r\n# Events Filename: " + prefix + eventsFilename + "\r\n#\r\n";
                output += "#\r\n# Test Commenced: " + serialHour + ":" + serialMinute + ":" + serialSecond + "\r\n#\r\n";
                int[] maxRowLength = new int[] { 0, 0, 0 };
                int totalRowLength = 0;
                //first pass - calculate max column widths for spacing
                foreach (KeyValuePair<string, HashSet<CDRLogData>> logEntry in _cedarLog) {
                    foreach (CDRLogData logData in _cedarLog[logEntry.Key]) {
                        if (logData.GetType() == typeof(CDRLogEvent)) {
                            if (maxRowLength[0] < logEntry.Key.Length) {
                                maxRowLength[0] = logEntry.Key.Length;
                            }
                            if (maxRowLength[1] < (((CDRLogEvent)logData).Index +
                                "").Length) {
                                maxRowLength[1] =
                                    (((CDRLogEvent)logData).Index + "").Length;
                            }
                            if (maxRowLength[2] < logData.Value.Length) {
                                maxRowLength[2] = logData.Value.Length;
                            }
                        }
                        else {
                            if (maxRowLength[0] < logEntry.Key.Length) {
                                maxRowLength[0] = logEntry.Key.Length;
                            }
                            if (maxRowLength[2] < logData.Value.Length) {
                                maxRowLength[2] = logData.Value.Length;
                            }
                        }
                    }
                }

                //calculate title filler length
                for (int i = 0; i < maxRowLength.Length; i++) {
                    totalRowLength += maxRowLength[i];
                }

                //second pass - output data
                int rowCount = 0;
                foreach (KeyValuePair<string, HashSet<CDRLogData>> logEntry in _cedarLog) {
                    foreach (CDRLogData logData in _cedarLog[logEntry.Key]) {
                        //format column titles
                        if (rowCount++ == 0) {
                            string time = "# TIME";
                            string eventL = "EVENT";
                            string task = "TASK";

                            if (maxRowLength[0] < time.Length) {
                                maxRowLength[0] = time.Length;
                            }
                            if (maxRowLength[1] < eventL.Length) {
                                maxRowLength[1] = eventL.Length;
                            }
                            if (maxRowLength[2] < task.Length) {
                                maxRowLength[2] = task.Length;
                            }

                            string col1 = time + Filler(maxRowLength[0] - time.Length);
                            string col2 = eventL + Filler(maxRowLength[1] - eventL.Length);
                            string col3 = task + Filler(maxRowLength[2] - task.Length);
                            output += col1 + col2 + col3;
                            var rowDispLength = Convert.ToInt32(totalRowLength * 1.3);
                            output += "\r\n#" + Filler(rowDispLength, '-') + "\r\n";
                        }
                        //if full event with event index, col2 is event index
                        if (logData.GetType() == typeof(CDRLogEvent)) {
                            string line = logEntry.Key + "\t" +
                                ((CDRLogEvent)logData).Index + "\t" + logData.Value;
                            string col1 = logEntry.Key +
                                Filler(maxRowLength[0] - logEntry.Key.Length);
                            string col2 = ((CDRLogEvent)logData).Index +
                                Filler(maxRowLength[1] - (((CDRLogEvent)logData).Index + "").Length);
                            string col3 = logData.Value +
                                Filler(maxRowLength[2] - logData.Value.Length);
                            output += " " + col1 + col2 + col3 + "\r\n";

                            //if brief event with no event index, col2 is empty
                        }
                        else {
                            string line = logEntry.Key + "\t\t" + logData.Value;
                            string col1 = logEntry.Key +
                                Filler(maxRowLength[0] - logEntry.Key.Length);
                            string col2 = Filler(maxRowLength[1]);
                            string col3 = logData.Value +
                                Filler(maxRowLength[2] - logData.Value.Length);
                            output += " " + col1 + col2 + col3 + "\r\n";
                        }
                    }
                }
                //json output
            }
            else if (json) {
                output = "{\n\t\"rows\" : [{\n";
                int numRows = _cedarLog.Count;
                int currentRow = 0;
                foreach (KeyValuePair<string, HashSet<CDRLogData>> logEntry in _cedarLog) {
                    currentRow++;
                    int currentColumn = 0;
                    int numColumns = _cedarLog[logEntry.Key].Count;
                    foreach (CDRLogData logData in _cedarLog[logEntry.Key]) {
                        if (logData.GetType() == typeof(CDRLogEvent)) {
                            output += "\t\t\"TIME\": \"" + logEntry.Key + "\",\n";
                            output += "\t\t\"EVENT\": \"" + ((CDRLogEvent)logData).Index + "\",\n";
                            output += "\t\t\"TASK\": \"" + logData.Value + "\"\n";
                        }
                        else {
                            output += "\t\t\"TIME\": \"" + logEntry.Key + "\",\n";
                            output += "\t\t\"TASK\": \"" + logData.Value + "\"\n";
                        }

                        if (!(currentRow == numRows && ++currentColumn == numColumns)) {
                            output += "\t}, {\n";
                        }
                    }
                    if (currentRow == numRows) {
                        output += "\t}]\n";
                    }
                }
                output += "}";

            }
            else if (csv) {
                output = "TIME, EVENT, TASK\n";
                if (ModelData.Instance.Settings[ModelData.TIMING_UNIX] == ModelData.PARAM_ON) {
                    output = "TIME, UNIX, EVENT, TASK\n";
                }
                int numRows = _cedarLog.Count;
                int currentRow = 0;
                foreach (KeyValuePair<string, HashSet<CDRLogData>> logEntry in _cedarLog) {
                    currentRow++;
                    int currentColumn = 0;
                    int numColumns = _cedarLog[logEntry.Key].Count;
                    foreach (CDRLogData logData in _cedarLog[logEntry.Key]) {

                        if (logData.GetType() == typeof(CDRLogEvent)) {
                            output += logEntry.Key + ",";
                            output += ((CDRLogEvent)logData).Index + ",";
                            output += logData.Value + "\n";
                        }
                        else {
                            output += logEntry.Key + ",-,";
                            output += logData.Value + "\n";
                        }

                    }
                    if (currentRow == numRows) {
                        output += "\n";
                    }
                }


            }
            ModelData.Instance.OutputFile(filename, output);
        }

        /**
         @property  public static ModelDataIOConfig instance
        
         @brief Gets the singleton log data instance.
        
         @return    The instance.
         */
        public static ModelDataIOLog Instance {
            get {
                return _instance;
            }
        }
    }
}
