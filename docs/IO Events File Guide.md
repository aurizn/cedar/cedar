# IO Events File Guide

Cedar, as a direct implementation of MATB-II for the tablet form factor, is fully compatible with all MATB-II events files, for full compatability information see the _MATB-II Compatability_ section. Cedar recognises a few additional events that can augment traditional MATB-II session content. As Cedar is also designed as a platform for additional, custom tasks beyond the basic task-set of MATB-II, the example extension module demonstrates the extensibility of the platform. See _Extension Events_ for information on the example events implemented for the extension module.

Events can be input in the XML or JSON formats, for information on the differences of these formats, see the _Event File Formats_ section. For guidance as to how to load events files and the naming conventions of the events files, see the _Events Formats_ section. The way events are stored after parsing is format independent, so additional formats may be supported in the future.

For information on all standard events on the implemented MATB-II tasks, see _Standard Events_.

### MATB-II Compatibility

All MATB-II events files are compatible with Cedar, however some Cedar events are not backwards compatible:

* All timeout events can be manually triggered in Cedar events files (see _Standard Events_ section for a list of timeout events). Timeout events were all  
    automatic and had no relevant events file entry equivalent in MATB-II.
* Extension sample events will not work with MATB-II.

The Cedar events file parser behaves differently to the MATB-II events file parser, in that events are logged in the order that they are processed by individual tasks rather than the strict tag order of the events file. This is relevant for logging, but does not effect program behaviour in any way.

As the MATB-II Tracking task is not implemented in Cedar, all tracking events are ignored. Tracking events are of the XML format:

        <!-- Sched task: TRACK AUTO --> 
        <event startTime="0:02:00">
        <sched >
            <task>TRACK</task>
            <action>AUTO</action>
            <update>NULL</update>
            <response>NULL</response>
        </sched> 
        </event>

A _sched_ Event Type relating to tracking is also ignored:

        <!-- Sched task: TRACK START --> 
        <event startTime="0:00:02">
        <sched>
            <task>TRACK</task>
            <action>MANUAL</action>
            <update>HIGH</update>
            <response>MEDIUM</response>
        </sched> 
        </event>

### Loading of Events Files

Cedar loads one events file on installation by default. This events file can be swapped at any point – but must be named and placed according to the guidelines below.

The naming guidlines are strict for each type of input available: JSON or XML. The type of input can be selected after launch and initial load, however. Event files can also be reloaded after launch.

#### Android

* The events file should be named _EVENTS.xml_ or _events.json_ and placed in a folder named _cedar_ in the root of the Android USB storage.

#### iOS

* The events file should be named _EVENTS.xml_ or _events.json_ placed in the app shared storage for the Cedar app.
* See: [https://support.apple.com/en-au/HT201301](https://support.apple.com/en-au/HT201301)

#### Loading Alternate Events Files

In addition to the default events file, alternate files can be selected and loaded. These must be placed in the same way as the default file, as per platform instructions, but must conform the following naming convention:

* _ALT_EVENTS.xml_ or
* _alt_events.json_

for the file selection of “ALT” – a file selection of “MATB” would result in the JSON file “_matb_events.json”._

Alternate events files can be selected in Settings, note that configuration files should be reloaded on the changing of a file (press the Reload Configuration Files button or restart Cedar). The name of the loaded file will be indicated on the title screen. Note that in _Training Mode_ a score text will replace the file indication after the first training run.

#### “ALT” file loaded

![](media/alt_file_loaded.png)

#### Alternate file selection

![](media/file-select.png)

### Event File Formats

### **XML**

The XML format of Cedar is identical to that of MATB-II. Events should be placed in the body of the below general template for XML files as per MATB-II formatting.

#### XML Event File Template

&lt;?xml version="1.0" encoding="UTF-8" ?&gt;
 &lt;CEDAR-EVENTS&gt;
 &lt;!\-\- events go here --&gt;
 &lt;CEDAR-EVENTS&gt;

It should be noted that the Cedar parser is case insensitive.

Individual events contain **Start Time**, **Event Type, Event Key(s), Event Value(s)**. Below is an example of tabulated form and literal form of an event.

#### XML Example 1

| Start Time | Event Type | Event Key(s)                                                          | Event Value(s)                                                   |
| ---------- | ---------- | --------------------------------------------------------------------- | ---------------------------------------------------------------- |
| 0:00:01    | ressys     | <ul><li>task</li><li>action</li><li>update</li><li>response</li></ul> | <ul><li>ressys</li><li>start</li><li>null</li><li>null</li></ul> |

        <?xml version="1.0" encoding="UTF-8" ?>
        <CEDAR-EVENTS>
        <!-- events go here -->
        <CEDAR-EVENTS>

Some events, however, for the purpose of the Cedar parser, do not contain an event type, in which case the event type is recorded as **–**.

#### XML Example 2

| Start Time | Event Type | Event Key(s)              | Event Value(s)          |
| ---------- | ---------- | ------------------------- | ----------------------- |
| 0:00:00    | –          | <ul><li>control</li></ul> | <ul><li>start</li></ul> |

        <event startTime="0:00:00">
        <control>START</control>
        </event>

### **JSON**

The JSON format event input option is offered for convenience. The event formatting in JSON is equivalent to XML with some stylistic changes occuring during conversion. Any XML to JSON converter should produce this format converting to/from. Below is the JSON format of the XML examples.

Note that the control events in the _JSON File Template_ are only to show the formatting of events (JSON does not allow comments), and should not be included in this exact form in any event file (any test run would end instantly). The _startTime attribute as the final attribute in an event object is critical, this is used to anchor the end of an event object for parsing. The following online converter can be used to correctly convert MATB-II XML files to Cedar JSON files:

[http://convertjson.com/xml-to-json.htm](http://convertjson.com/xml-to-json.htm)

#### JSON Event File Template

        {
        "MATB-EVENTS":{
            "event":[
                {
                    "control":"START",
                    "_startTime":"0:00:00"
                },
                {
                    "control":"END",
                    "_startTime":"0:00:00"
                }
            ]
        }
        }

**JSON Example 1**

        {
        "control": "START",
        "_startTime": "0:00:00"
        },

**JSON Example 2**

        {
        "sched":{
            "task":"RESSYS",
            "action":"START",
            "update":"NULL",
            "response":"NULL"
        },
        "_startTime":"0:00:04"
        },

### Standard Events

Notes:

* The Cedar parser is case-insensitive.
* Timeout events do not need to be included in a normal events file, but can be included if a custom timeout timing is required.
* A **warning raise event** creates an automatic timeout event that can be overriden by a custom timeout in an events file if it occurs before the automatic timeout event.
* Event values are literal, except in the case of the user of parentheses or forward slashes, for example:
  * **p(1-2)** means values **p1** and **p2**
  * **one/two** values **one** and **two  
      **
* Refer to the _Event Formats_ section for information on how **Event Type,** **Event Key(s)**, **Event Value(s)** are formatted in XML and JSON.
* For information on what constitutes a **Full Event** is for logging purposes, review the _Cedar IO Log Guide._
  * In summary: a Full Event generally only exists in the CEDAR Log, wheras a Brief Event occurs in a Task Log in addition to the main log (in a brief form).
* **Event Key(s)** with the value: **null** are ignored by the Cedar parser, and so are not included in the _Standard Events_ table, but should be included in MATB-II events files as per the MATB-II guides.

| Event Type | Event Key(s)                                                                             | Event Value(s)                                                                                                                    | Event Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           | Full Event | CEDAR Log Example                                                                                                     | Task Log Example                                        |
| ---------- | ---------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------- | --------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------- |
| –          | <ul><li>control</li></ul>                                                                | <ul><li>start</li></ul>                                                                                                           | Starts Cedar test.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          | Y          | 00:00:00.4 1 Event Initiated: Control                                                                                 |                                                         |
| –          | <ul><li>control</li></ul>                                                                | <ul><li>end</li></ul>                                                                                                             | Stops Cedar test.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           | Y          | 00:05:00.1 48 Event Terminated: Control                                                                               |                                                         |
| sched      | <ul><li>task</li><li>action</li></ul>                                                    | <ul><li>ressys</li><li>start</li></ul>                                                                                            | Start _Resources_ and _System Monitoring_ Tasks. This is two events in Cedar, as any event can be intercepted by any task, at an architectural level, it’s more important to know what tasks interpreted what events, thus each use of an event tag will log a new entry. This is distinct from MATB-II, which has an ad-hoc approach to logging from a task perspective, but strict event numbering (see _Cedar IO Log Guide_) for event tags. Cedar numbers each use of an event, rather than each instance of a tag. Unused event tags are included in numbering, however.                                                                                                               | Y          | 00:00:01.0 2 Event Initiated: Scheduling - RESMAN Active <br>00:00:01.0 3 Event Initiated: Scheduling - SYSMON Active | N/A                                                     |
| sched      | <ul><li>task</li><li>action</li></ul>                                                    | <ul><li>ressys</li><li>stop</li></ul>                                                                                             | Stop Resources and System Monitoring Tasks.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 | Y          |                                                                                                                       | N/A                                                     |
| sched      | <ul><li>task</li><li>action</li></ul>                                                    | <ul><li>comm</li><li>start</li></ul>                                                                                              | Start(enable) _Communications_ task.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |            | 00:00:10.1 7 Event Initiated: Scheduling - COMM Session Started                                                       | N/A                                                     |
| sched      | <ul><li>task</li><li>action</li></ul>                                                    | <ul><li>comm</li><li>stop</li></ul>                                                                                               | Stop(disable) _Communications_ task.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |            | 00:02:30.1 26 Event Initiated: Scheduling - COMM Session Ended                                                        | N/A                                                     |
| resman     | <ul><li>fail</li></ul>                                                                   | <ul><li>p(1-8)</li></ul>                                                                                                          | Fail Resource Management task pumps 1-8 (single pump as specified).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         | Y          | 00:00:05.1 5 Event Initiated: Resource Management                                                                     | 00:00:05.1 1 fail n 2447 2447 1000 1000                 |
| resman     | <ul><li>fix</li></ul>                                                                    | <ul><li>p(1-8)</li></ul>                                                                                                          | Fix Resource Management task pumps 1-8 (single pump as specified).                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |            | 00:00:35.1 10 Event Initiated: Resource Management                                                                    | 00:00:35.1 1 fix n 2047 2047 1000 1000                  |
| sysmon     | <ul><li>monitoringScaleNumber</li><li>monitoringScaleDirection</li></ul>                 | <ul><li>one/two/three/four</li><li>up/down</li></ul>                                                                              | Move monitoring scales (scale number as specified), up or down (direction as specified). This is a warning raise event, with a relevant timeout event, event type _monitoringScaleNumber_timeout_.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          | Y          | 00:00:38.1 11 Event Initiated: Monitoring                                                                             | N/A                                                     |
| sysmon     | <ul><li>monitoringScaleNumber_timeout</li><li>timeout_originator</li><li>index</li></ul> | <ul><li>one/two/three/four</li><li>(timing of warning raise)</li><li>(event index of warning raise event)</li></ul>               | Timeout monitoring scale event – this is raised as a callback from a _sysmon monitoringScale_ event.<br><br>The timing of the warning raise event must be passed in the _timeout_originator_ valuein the format _0.0_ in seconds.<br><br>The event index of the warning raise event must be passed in the _index_ value – if this index does not match the index stored in the relevant model, the timeout will fail.                                                                                                                                                                                                                                                                       | N          | 00:00:48.1 Event Terminated: Monitoring                                                                               | 00:00:48.0 -10.0 scale one - event timed out            |
| sysmon     | <ul><li>monitoringLightType</li></ul>                                                    | <ul><li>green/red</li></ul>                                                                                                       | Raise warning for _System Monitoring_ SW1/SW2 (aligning to green/red in event values respectively). The colours are maintained in the events file for MATB-II compatibility. The colours are not used in the Cedar UI however, due to the colour design in the Cedar specifications. This is a warning raise event, with a relevant timeout event key _sysmon_ _monitoringLightType_timeout_.                                                                                                                                                                                                                                                                                               | Y          | 00:00:20.1 9 Event Initiated: Monitoring                                                                              | N/A                                                     |
| sysmon     | <ul><li>monitoringLightType_timeout</li><li>timeout_originator</li><li>index</li></ul>   | <ul><li>green/red</li><li>(timing of warning raise)</li><li>(event index of warning raise event)</li></ul>                        | Timeout monitoring scale event – this is raised as a callback from a _sysmon monitoringLightType_ event.<br><br>The timing of the warning raise event must be passed in the _timeout_originator_ valuein the format _0.0_ in seconds. This is part of a metric recorded in the _-RT-_ column of the _SYSMON_ task log file.<br><br>The event index of the warning raise event must be passed in the _index_ value – if this index does not match the index stored in the relevant model, the timeout will fail.                                                                                                                                                                             | N          | 00:01:41.1 Event Terminated: Monitoring                                                                               | 00:00:23.0 -15.0 light green - event timed out          |
| comm       | <ul><li>ship</li><li>radio</li><li>freq</li></ul>                                        | <ul><li>own/other</li><li>COM1/COM2/NAV1/NAV2</li><li>(specified frequency, if voiceover see _MATB-II Voiceover Guide_)</li></ul> | Starts a voiceover calling for a change of (specified) radio frequency for a particular (specified) radio channel in the _Communications_ task. If in _silent mode_ or _training mode_, this is a visual indicator of a frequency target on the specified channel, otherwise there is no visual indication of the channel and frequency to be change and changed to respectively: the user must listen to the voiceover to complete the objective. This is a warning raise event, with a relevant timeout event, event key _ship_timeout_.                                                                                                                                                  | Y          | 00:00:40.1 12 Event Initiated: Communications                                                                         | N/A                                                     |
| comm       | <ul><li>radio_timeout</li><li>index</li><li>freq</li></ul>                               | <ul><li>own/other</li><li>COM1/COM2/NAV1/NAV2</li><li>(specified frequency, if voiceover see _MATB-II Voiceover Guide_)</li></ul> | Timeout a _Communications_ event – this is raised as a callback from a _comm_ event.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        | N          | 00:01:10.1 Event Terminated: Communications                                                                           | 00:00:45.0 7 other COM2 130.850 COM2 130.850 false true |
| –          | <ul><li>rate</li></ul>                                                                   | <ul><li>start</li></ul>                                                                                                           | Starts a workload rating form. The workload rating is stopped when it times out (see _Cedar IO Config Guide_ for information on this timeout) or the user submits the form. This timeout event does not have a relevant callback event though, as the workload rating pauses the processing of new events completely until timeout or submit has occurred.<br><br>There is a slight difference in the output from Cedar than in the equivalent MATB-II. Wheras MATB-II only raises a _Full Event_ for workload termination (this is formatted the same as the Cedar termination event), Cedar raises a _Full Event_ for the workload rating initiation and a _Brief Event_ for termination. | Y          | 00:02:25.0 24 Event Initiated: Workload Rating<br>00:02:25.0 Event Terminated: Workload Rating                        | N/A                                                     |

### Extension Events

| Event Type | Event Key(s)             | Event Value(s)               | Event Description                                                                                                                     | Full Event | CEDAR Log Example                        | Task Log Example      |
| ---------- | ------------------------ | ---------------------------- | ------------------------------------------------------------------------------------------------------------------------------------- | ---------- | ---------------------------------------- | --------------------- |
| sched      | <ul><li>ext</li></ul>    | <ul><li>start</li></ul>      | Starts(enables) extension task.                                                                                                       | Y          | 00:00:00.4 1 Event Initiated: Extension  | N/A                   |
| sched      | <ul><li>ext</li></ul>    | <ul><li>stop</li></ul>       | Stops(disables) extension task.                                                                                                       | Y          | 00:00:00.4 1 Event Terminated: Extension | N/A                   |
| ext        | <ul><li>button</li></ul> | <ul><li>1/2</li></ul>        | Raises warning for (specified) button one or two.                                                                                     | N          | 00:00:00.4 Event Initiated: Extension    | 00:00:04.0 5 01.0 1 n |
| ext        | <ul><li>text</li></ul>   | <ul><li>(new text)</li></ul> | Changes the extension subtitle text to the (new text) value.<br><br>Note that this is a _Brief Event_ that also does not log a value. | N          | 00:00:17.5 Event Initiated: Extension    | N/A                   |