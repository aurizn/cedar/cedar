﻿/*
 * CEDAR Operator Workload Assessment Tool
 * Copyright (C) 2023  aurizn
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using System;

namespace CEDAR.Shared.Modules.Task.Res {
    /**
     @class ResModelTankEventArgs
    
     @brief Resources fuel tank event parameters
    
     @author    Ryan Beruldsen
     @date  15/09/2016
     */

    internal class ResModelTankEventArgs : EventArgs {

        /**
         @property  public bool valueChange
        
         @brief Tank level change event type
        
         @return    true if value tank level change event type.
         */

        public bool ValueChange { get; set; }

        /**
         @property  public bool warnIndicateChange
        
         @brief Warning indication change event (warning is raised or resolved for tank)
        
         @return    true if warning indicate change, false if not.
         */

        public bool WarnIndicateChange { get; set; }

        /**
         @property  public ResModelTank res
        
         @brief Gets or sets the resource tank model reference (reference for model pertaining to event).
        
         @return    Resource tank model reference.
         */

        public ResModelTank Res { get; set; }
    }
}
