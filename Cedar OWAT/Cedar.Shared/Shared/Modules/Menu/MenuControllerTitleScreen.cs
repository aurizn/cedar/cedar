﻿/*
 * CEDAR Operator Workload Assessment Tool
 * Copyright (C) 2023  aurizn
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using CocosSharp;
using System;
using System.Collections.Generic;
using System.Text;
using CEDAR.Shared;

namespace CEDAR.Shared.Modules.Menu {

    /**
     @class MenuControllerTitleScreen
    
     @brief The title screen, main menu.
    
     @author    Ryan Beruldsen
     @date  15/09/2016
     */

    class MenuControllerTitleScreen : CCNode {

        /** @brief Internal draw node. */
        private readonly CCDrawNode _drawNode;

        /** @brief Splash screen sprite, contains Cedar logo, title so that these can be positioned as a group. */
        private CCSprite _splashScreen;

        /** @brief Internal window bounds. */
        private CCPoint _windowMax;

        /** @brief test start page button. */
        private MenuViewButton _start;

        /** @brief settings page button. */
        private MenuViewButton _settings;

        /** @brief about page button. */
        private MenuViewButton _about;

        /** @brief Listener for button touch events. */
        private readonly CCEventListenerTouchAllAtOnce _touchListener;

        /**
         @fn    public MenuControllerTitleScreen(CCPoint windowMax) : base()
        
         @brief Title (initial) screen constructor.
        
         @author    Ryan Beruldsen
         @date  16/09/2016
        
         @param windowMax   Window bounds (max X and Y coordinates)
         */

        public MenuControllerTitleScreen(CCPoint windowMax) : base() {
            _windowMax = new CCPoint(windowMax.X, windowMax.Y);
            _drawNode = new CCDrawNode();

            _touchListener = new CCEventListenerTouchAllAtOnce {
                OnTouchesBegan = HandleInput,
                OnTouchesEnded = HandleInputEnd
            };
            AddEventListener(_touchListener, this);

            Init();
        }

        /**
         @fn    public void Init()
        
         @brief Initialise title page.
        
         @author    Ryan Beruldsen
         @date  16/09/2016
         */

        private void Init() {
            AddChild(_drawNode);

            /** @brief Splash screen sprite, contains Cedar logo, logo text and basic app info; positioning container.*/
            _splashScreen = new CCSprite();
            AddChild(_splashScreen);

            _splashScreen.AddChild(new MenuViewCedarLogo(
                windowMax: _windowMax,
                highlightColour: ModelData.Instance.GetColourPallete4B(ModelData.COLOUR_PARAM_ACTIVE))
            );

            _splashScreen.AddChild(new MenuViewCedarTitle(_windowMax));
            _splashScreen.AddChild(new MenuViewCedarInfo(_windowMax));

            //button layout
            float baseY = 0.46f;
            float spacer = 0.065f;
            float baseWidth = 0.4f;

            _start = new MenuViewButton(
              label: "Start OWAT",
              highlight: true,
              highlightColour: ModelData.Instance.GetColourPallete4B(ModelData.COLOUR_PARAM_ACTIVE),
              width: _windowMax.X * baseWidth,
              height: _windowMax.Y * 0.04f,
              x: _windowMax.X / 2,
              y: _windowMax.Y * (baseY - 0 * spacer));

            _settings = new MenuViewButton(
              label: "Settings",
              highlightColour: ModelData.Instance.GetColourPallete4B(ModelData.COLOUR_PARAM_ACTIVE),
              width: _windowMax.X * baseWidth,
              height: _windowMax.Y * 0.04f,
              x: _windowMax.X / 2,
              y: _windowMax.Y * (baseY - 1 * spacer));

            _about = new MenuViewButton(
              label: "About",
              highlightColour: ModelData.Instance.GetColourPallete4B(ModelData.COLOUR_PARAM_ACTIVE),
              width: _windowMax.X * baseWidth,
              height: _windowMax.Y * 0.04f,
              x: _windowMax.X / 2,
              y: _windowMax.Y * (baseY - 2 * spacer));

            _splashScreen.PositionY = _windowMax.Y * 0.05f;

            AddChild(_start);
            AddChild(_settings);
            AddChild(_about);
        }

        /**
         @fn    private void HandleInputEnd(System.Collections.Generic.List<CCTouch> touches, CCEvent touchEvent)
        
         @brief Handles touch input end
        
         @author    Ryan Beruldsen
         @date  16/09/2016
        
         @param touches     CocosSharp injected touch list
         @param touchEvent  CocosSharp injected touch event
         */

        private void HandleInputEnd(System.Collections.Generic.List<CCTouch> touches, CCEvent touchEvent) {
            if (touches.Count > 0) {
                //Data.currentPageStr = "test";
                if (_start.Active && MenuButtonHitTest(touches, _start)) {
                    //enforce no file loading if in unregistered demo mode

                    if (ModelDataIOConfig.Instance.GetConfigBool("mode", "test_serials")) {
                        int serialNum = int.Parse(ModelData.Instance.Settings[ModelData.SERIAL_NO]);
                        if (serialNum < 0) {
                            ModelData.Instance.ChangeSetting(ModelData.SERIAL_NO, "0");
                            ModelData.Instance.SaveSettings();
                        }

                        ModelData.CurrentPageStr = "serial";
                    }
                    else {
                        ModelData.CurrentPageStr = "test";
                    }
                    ModelData.Instance.SessionStartTime = ModelData.Timestamp();

                }
                if (_settings.Active && MenuButtonHitTest(touches, _settings)) {
                    ModelData.CurrentPageStr = "settings";
                }
                if (_about.Active && MenuButtonHitTest(touches, _about)) {
                    ModelData.CurrentPageStr = "about";
                }
                _settings.Active = false;
                _start.Active = false;
                _about.Active = false;
            }
        }

        /**
         @fn    private void MenuButtonHitTest(System.Collections.Generic.List<CCTouch> touches, MenuViewButton m)
        
         @brief Hit test for menu button, shorthand function.
        
         @author    Ryan Beruldsen
         @date  16/09/2016
        
         @param touches CocosSharp injected touch list (from touch handler)
         @param m       The MenuViewButton to test for bounding box hit.
        
         @return    True if within button/drop down bounds, otherwise false
         */

        private bool MenuButtonHitTest(System.Collections.Generic.List<CCTouch> touches, MenuViewButton m) {
            if (touches[0].Location.X > m.minButtonBounds.X &&
                    touches[0].Location.X < m.maxButtonBounds.X &&
                    touches[0].Location.Y > m.minButtonBounds.Y &&
                    touches[0].Location.Y < m.maxButtonBounds.Y) {
                return true;
            }
            return false;
        }

        /**
         @fn    private void HandleInput(System.Collections.Generic.List<CCTouch> touches, CCEvent touchEvent)

         @brief Handles touch input end

         @author    Ryan Beruldsen
         @date  16/09/2016

         @param touches     CocosSharp injected touch list
         @param touchEvent  CocosSharp injected touch event
         */
        private void HandleInput(System.Collections.Generic.List<CCTouch> touches, CCEvent touchEvent) {
            if (touches.Count > 0) {
                if (MenuButtonHitTest(touches, _start)) {
                    _start.Active = true;

                }
                else if (MenuButtonHitTest(touches, _settings)) {
                    _settings.Active = true;

                }
                else if (MenuButtonHitTest(touches, _about)) {
                    _about.Active = true;

                }
                //Data.currentPageStr = "test";

            }
        }
    }
}

