﻿/*
 * CEDAR Operator Workload Assessment Tool
 * Copyright (C) 2023  aurizn
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

namespace CEDAR.Shared.Modules.Task.Ext {
    internal delegate void ExtModelEventHandler(object sender, ExtModelEventArgs e);

    /**
     @class ExtModel
    
     @brief The data Model for the extension button component.
    
     @author    Ryan Beruldsen
     @date  10/10/2016
     */

    class ExtModel {
        /** @brief Event queue for all listeners interested in resPumpChanged events. */
        public event ExtModelEventHandler ExtModelEventHandler;

        /** @brief Enabled status of model. */
        private bool _enabled = false;

        /** @brief Warn indication state of model. */
        private bool _warnIndicate = false;

        /** @brief The timestamp of the last event trigger. */
        public double Trigger { get; set; }

        /** @brief The current unique event index (to prevent phantom timeouts).*/
        public int Index { get; set; }

        /**
         @property  public bool enabled

         @brief Gets or sets a value indicating whether the model is enabled.

         @return    true if enabled, false if not.
         */
        public bool Enabled {
            get { return _enabled; }
            set { _enabled = value; }
        }

        /**
         @fn    protected virtual void ExtModelChanged(ExtModelEventArgs e)
        
         @brief Raises ext model events.
        
         @author    Ryan Beruldsen
         @date  21/09/2016
        
         @param e   Event information to send to registered event handlers.
         */

        protected virtual void ExtModelChanged(ExtModelEventArgs e) {
            ExtModelEventHandler?.Invoke(this, e);
        }

        /**
         @property  public bool warnIndicate
        
         @brief Gets or sets warn indicate status of the model.
        
         @return    The warn indicate status.
        */
        public bool WarnIndicate {
            get { return _warnIndicate; }
            set {
                if (_enabled) {
                    ExtModelEventArgs e = new ExtModelEventArgs {
                        WarnIndicateChange = true
                    };
                    _warnIndicate = value;
                    e.Ext = this;
                    ExtModelChanged(e);
                }
            }
        }

    }
}
