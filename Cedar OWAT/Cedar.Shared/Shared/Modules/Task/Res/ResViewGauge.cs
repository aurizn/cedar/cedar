﻿/*
 * CEDAR Operator Workload Assessment Tool
 * Copyright (C) 2023  aurizn
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


using CocosSharp;
using CEDAR.Shared.Modules;
using System;
using System.Collections.Generic;
using System.Text;

namespace CEDAR.Shared.Modules.Task.Res {
    /**
     @class ResViewGauge
    
     @brief A gauge component for tank levels, presented in a series in a gauge panel (ResControllerGaugePanel).

     Sized with reference to the critical or noncritical nature of the tank (main or supply tanks).
    
     @author    Ryan Beruldsen
     @date  15/09/2016
     */

    class ResViewGauge : DivisionContent, IDivisionContent {
        /** @brief Level indicate sprite tag constant. */
        private const int _LEVEL_INDICATE_TAG = 0;

        /** @brief The x coordinate. */
        private readonly float _x;

        /** @brief The y coordinate. */
        private readonly float _y;

        /** @brief The gauge width. */
        private readonly float _width;

        /** @brief The height. */
        private readonly float _height;

        /** @brief Critical status of gauge, whether to display range, warnings raised if levels outside range. */
        private readonly bool _critical;

        /** @brief The gauge label. */
        private readonly string _label;

        /** @brief The current gauge level. */
        private double _level;

        /** @brief The current displayed gauge level in units. */
        private string _levelUnits;

        /** @brief The minimum gauge level. */
        private readonly double _minLevel;

        /** @brief The maximum gauge level. */
        private readonly double _maxLevel;

        /** @brief Gauge warning text colour. */
        private readonly CCColor3B _colourWarningText;

        /** @brief Gauge warning colour (user to show current level during warning state). */
        private readonly CCColor4B _colourWarning;

        /** @brief Gauge active text colour (used to show current levels). */
        private readonly CCColor4B _colourActive;

        /** @brief Gauge active text colour (used to show current levels). */
        private readonly CCColor4B _colourRange;

        /** @brief The safe range of the gauge translated to display pixels. */
        private readonly float _range;

        /** @brief The main draw node. */
        private readonly CCDrawNode _drawNode;

        /** @brief Warning indicate state. */
        private bool _warnIndicate;

        /** @brief The normal level indication sprite. */
        private readonly CCSprite _levelIndicate;

        /** @brief The warning state level indication sprite. */
        private readonly CCSprite _levelWarnIndicate;

        /** @brief The minimum bounds of gauge. */
        public CCPoint minButtonBounds;

        /** @brief The maximum bounds of gauge. */
        public CCPoint maxButtonBounds;

        /**
         @property  public float width
        
         @brief Gets the gauge width.
        
         @return    The gauge width.
         */

        public float Width { get { return _width; } }

        /**
         @property  public float height
        
         @brief Gets the height.
        
         @return    The height.
         */

        public float Height { get { return _height; } }
        /**
         @property  public bool warnIndicate
        
         @brief Gets or sets warning indication state.
        
         @return    Warning indication state.
         */

        public bool WarnIndicate {
            get {
                return _warnIndicate;
            }
            set {
                _warnIndicate = value;

                if (_critical) {
                    _levelWarnIndicate.Visible = _warnIndicate;
                    _levelIndicate.Visible = !_warnIndicate;
                }
            }
        }

        /**
         @fn    public void SetLevel (double level, string levelUnits)
        
         @brief Sets gauge level, display level in units.
        
         @author    Ryan Beruldsen
         @date  21/09/2016
        
         @param level       The level.
         @param levelUnits  The level string in units.
         */

        public void SetLevel(
            double level, string levelUnits) {
            _level = level;
            _levelUnits = levelUnits;

            //draw normal level indication
            ((CCDrawNode)_levelIndicate.GetChildByTag(_LEVEL_INDICATE_TAG)).Clear();
            ((CCDrawNode)_levelIndicate.GetChildByTag(_LEVEL_INDICATE_TAG)).DrawRect(
                new CCRect(_x, _y, _width, _height * Convert.ToSingle(_level)),
                fillColor: _colourActive,
                borderWidth: 0,
                borderColor: CCColor4B.White);

            //draw warning level indication
            ((CCDrawNode)_levelWarnIndicate.GetChildByTag(_LEVEL_INDICATE_TAG)).Clear();
            ((CCDrawNode)_levelWarnIndicate.GetChildByTag(_LEVEL_INDICATE_TAG)).DrawRect(
                new CCRect(_x, _y, _width, _height * Convert.ToSingle(_level)),
                fillColor: _colourWarning,
                borderWidth: 0,
                borderColor: CCColor4B.White);

            //set level and warning info texts
            ((CCLabel)_levelWarnIndicate.GetChildByTag(1)).Text = _levelUnits;
            ((CCLabel)_levelIndicate.GetChildByTag(1)).Text = _levelUnits;

            //draw range if gauge is critical
            if (_critical) {
                DrawCriticalRange((CCDrawNode)_levelIndicate.GetChildByTag(_LEVEL_INDICATE_TAG));
                DrawCriticalRange((CCDrawNode)_levelWarnIndicate.GetChildByTag(_LEVEL_INDICATE_TAG));
            }
        }

        /**
         @fn    public ResViewGauge( Division division, string label, bool critical, double level, string levelUnits, CCColor4B rangeColour, CCColor4B activeColour, CCColor4B warningColour, CCColor3B warningColourText, double maxLevel, double minLevel, float width, float height, float x, float y ) : base(division)
        
         @brief Constructor.
        
         @author    Ryan Beruldsen
         @date  21/09/2016
        
         @param division            The parent division.
         @param label               The gauge label.
         @param critical            Critical state of gauge (show range).
         @param level               The initial gauge level.
         @param levelUnits          The level units.
         @param colourRange         The range colour.
         @param colourActive        The active colour.
         @param colourWarning       The warning colour.
         @param colourWarningText   The warning text colour.
         @param maxLevel            (Optional) The maximum level (must be set if gauge is critical).
         @param minLevel            (Optional) The minimum level (must be set if gauge is critical).
         @param width               The width.
         @param height              The height.
         @param x                   The x coordinate.
         @param y                   The y coordinate.
         */

        public ResViewGauge(Division division,
                                string label,
                                bool critical,
                                double level,
                                string levelUnits,
                                CCColor4B colourRange,
                                CCColor4B colourActive,
                                CCColor4B colourWarning,
                                CCColor3B colourWarningText,
                                float width,
                                float height,
                                float x,
                                float y,
                                double maxLevel = 0,
                                double minLevel = 0) : base(division) {

            _colourWarning = colourWarning;
            _colourWarningText = colourWarningText;
            _colourActive = colourActive;
            _colourRange = colourRange;

            _height = height;
            _width = width;
            _label = label;
            _critical = critical;

            //set initial warn state based on min/max levels
            _warnIndicate = (level < minLevel || level > maxLevel) && critical;

            _level = level;
            _levelUnits = levelUnits;
            _x = x;
            _y = y;
            _drawNode = new CCDrawNode();
            _levelIndicate = new CCSprite();
            _levelWarnIndicate = new CCSprite();
            _maxLevel = maxLevel;
            _minLevel = minLevel;

            //model range converted to pixels to draw range sprites
            _range = _height * Convert.ToSingle(maxLevel - minLevel);
        }

        /**
         @fn    public void DrawCriticalRange(CCDrawNode drawNode)
        
         @brief Draw critical range of gauge.
        
         @author    Ryan Beruldsen
         @date  21/09/2016
        
         @param drawNode    The main draw node.
         */

        public void DrawCriticalRange(CCDrawNode drawNode) {
            drawNode.DrawRect(
                    new CCRect(
                        _x - _division.Width * 0.01f, 
                        _y + Convert.ToSingle(_minLevel) * _height,
                        _division.Width * 0.01f, _range
                    ),
                    fillColor: _colourRange,
                    borderWidth: 0,
                    borderColor: CCColor4B.White);

            drawNode.DrawRect(
                new CCRect(
                    _x + _width, 
                    _y + Convert.ToSingle(_minLevel) * _height, 
                    _division.Width * 0.01f, 
                    _range
                ),
                fillColor: _colourRange,
                borderWidth: 0,
                borderColor: CCColor4B.White);
        }

        /**
         @fn    public void Init()
        
         @brief Initialises, draws the tank gauge view.
        
         @author    Ryan Beruldsen
         @date  21/09/2016
         */

        public void Init() {
            AddChild(_drawNode);

            DrawLevelIndicator();

            //label padding
            float padding = Convert.ToInt32((10.0 / 768.0) * _division.Width);

            //draw capacity labels
            CCLabel tankCapacity = new CCLabel(_levelUnits, "roboto", 12) {
                Position = new CCPoint(_x + _width + padding, _y),
                Color = CCColor3B.White,
                AnchorPoint = CCPoint.AnchorLowerLeft
            };

            CCLabel tankCapacityWarn = new CCLabel(_levelUnits, "roboto", 12) {
                Position = new CCPoint(_x + _width + padding, _y),
                Color = _colourWarningText,
                AnchorPoint = CCPoint.AnchorLowerLeft
            };

            _levelIndicate.AddChild(tankCapacity, 1, 1);
            _levelWarnIndicate.AddChild(tankCapacityWarn, 1, 1);

            //draw tank label: eg 'A'
            CCLabel tankLabel = new CCLabel(_label, "roboto", 12) {
                Position = new CCPoint(_x - padding, _y),
                Color = CCColor3B.White,
                AnchorPoint = CCPoint.AnchorLowerRight
            };

            AddChild(tankLabel);

            minButtonBounds = new CCPoint(_x, _y);
            maxButtonBounds = new CCPoint(_x + _width, _y + _height);

            //draw black backing for level text label
            _drawNode.DrawRect(
                   new CCRect(_x + _width + padding, _y + padding / 4, 50, 16),
                   fillColor: CCColor4B.Black,
                   borderWidth: 0,
                   borderColor: CCColor4B.White);

            //draw light great backing of gauge
            _drawNode.DrawRect(
                   new CCRect(_x, _y, _width, _height),
                   fillColor: new CCColor4B(100, 100, 100, 255),
                   borderWidth: 0,
                   borderColor: CCColor4B.White);
        }

        /**
         @fn    private void DrawLevelIndicator()
        
         @brief Draw tank gauge level indicator.
        
         @author    Ryan Beruldsen
         @date  5/10/2016
         */

        private void DrawLevelIndicator() {
            //normal gauge level indication draw node
            CCDrawNode levelIndicateDrawNode = new CCDrawNode();

            //warning gauge level indication draw node
            CCDrawNode levelWarnIndicateDrawNode = new CCDrawNode();

            _levelIndicate.AddChild(levelIndicateDrawNode, 0, _LEVEL_INDICATE_TAG);
            _levelWarnIndicate.AddChild(levelWarnIndicateDrawNode, 0, _LEVEL_INDICATE_TAG);

            AddChild(_levelIndicate);
            AddChild(_levelWarnIndicate);

            //draw active colour level indication
            levelIndicateDrawNode.DrawRect(
                new CCRect(_x, _y, _width, _height * Convert.ToSingle(_level)),
                    fillColor: _colourActive,
                    borderWidth: 0,
                    borderColor: CCColor4B.White);

            //draw warning colour level indication
            levelWarnIndicateDrawNode.DrawRect(
                new CCRect(_x, _y, _width, _height * Convert.ToSingle(_level)),
                fillColor: _colourWarning,
                borderWidth: 0,
                borderColor: CCColor4B.White);

            _levelWarnIndicate.Visible = _warnIndicate;

            //draw range indication if gauge is critical
            if (_critical) {
                DrawCriticalRange(levelIndicateDrawNode);
                DrawCriticalRange(levelWarnIndicateDrawNode);
            }
        }

        public override bool Equals(object obj) {
            return obj is ResViewGauge gauge &&
                   EqualityComparer<CCSprite>.Default.Equals(_levelIndicate, gauge._levelIndicate);
        }
    }
}
