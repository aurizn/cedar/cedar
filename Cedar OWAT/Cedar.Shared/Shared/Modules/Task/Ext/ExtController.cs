﻿/*
 * CEDAR Operator Workload Assessment Tool
 * Copyright (C) 2023  aurizn
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using CocosSharp;
using CEDAR.Shared.CedarCollection;
using System;
using System.Collections.Generic;

/*! \r\namespace CEDAR.Shared.Modules.Task.Ext
 * @brief Extention sample task content
 * 
 * - a simplified end-to-end example of an extension in the form of a new task.
 * 
 */

namespace CEDAR.Shared.Modules.Task.Ext {
    /**
     @class ExtController
    
     @brief Extention sample controller, handles IO, instantiating component Controller/Views.
    
     @author    Ryan Beruldsen
     @date  10/10/2016
     */

    class ExtController : DivisionContent, IDivisionContent, ITaskContent {

        /** @brief Enabled status of task (sched start/stop event).*/
        private bool _enabled = false;

        /** @brief Button 1 index constant.*/
        private const int _BUTTON_1 = 0;

        /** @brief Button 2 index constant.*/
        private const int _BUTTON_2 = 1;

        /** @brief Global log singleton.*/
        private readonly ModelDataIOLog _log = ModelDataIOLog.Instance;

        /** @brief Brief event log description.*/
        private const string EVENT_BRIEF = "Extension";

        /** @brief Unique tag of Extension sample task. */
        private const string EXT_TAG = "ext";

        /** @brief Division status label string - contains test related information for division (see user guide).*/
        private CCLabel _divStatus;

        /** @brief List of button models. */
        private readonly List<ExtModel> _modelList;

        /** @brief Timeout value warnings raised. */
        private readonly int _timeout = 5;

        /** @brief The view text. */
        private ExtViewText viewText;

        /** @brief IO event to function register. */
        private readonly CDRIOEventFn _ioEventDict;

        /**
         @fn    public ExtController(Division division) : base(division)
        
         @brief Extension task controller constructor.

            - task log headers declared here
        
         @author    Ryan Beruldsen
         @date  10/10/2016
        
         @param division    The parent division.
         */

        public ExtController(Division division) : base(division) {

            _modelList = new List<ExtModel>();

            _ioEventDict = new CDRIOEventFn {
                ["sched"] = SchedTrigger,
                [EXT_TAG] = ExtTrigger
            };

            if (ModelDataIOConfig.Instance.GetConfigNum("timeout", "task_ext") > 0) {
                _timeout = ModelDataIOConfig.Instance.GetConfigNum("timeout", "task_ext");
            }

            //add plaintext log file header
            var eventRow = new CDRLogRow() {
                    "-TIME-",
                    "-EVENT-",
                    "-RT-",
                    "-BUTTON-",
                    "-EXT_OK-"
            };
            string header = "# Timeout (in seconds) = " + _timeout + "\r\n" +
                            "#\r\n" +
                            "# RT = Response Time (in seconds)\r\n" +
                            "# EXT_OK = If the user responded before timeout.\r\n" +
                            "#";
            ModelDataIOLog.Instance.LogTaskHeader(EXT_TAG, header);
            ModelDataIOLog.Instance.LogTaskEvent(EXT_TAG, eventRow);
        }

        public void EventExternal(string type, Dictionary<string, string> parameters) {
            if (_ioEventDict.ContainsKey(type)) {
                _ioEventDict[type](parameters);
            }
        }

        public bool GetEnabled() {
            return _enabled;
        }

        public string GetTag() {
            return EXT_TAG;
        }

        public void Init() {
            //default status string
            _divStatus = new CCLabel("", "roboto", 12) {
                Position = new CCPoint(_division.Width / 2, _division.Y - _division.Height * 0.95f),
                Color = CCColor3B.White,
                AnchorPoint = CCPoint.AnchorMiddle
            };
            AddChild(_divStatus);


            for (int i = 0; i < 2; i++) {
                _modelList.Add(new ExtModel());
                _modelList[i].ExtModelEventHandler += ExtModelChanged;
            }
            _division.AddContent(new ExtControllerButtonPanel(_modelList, _division));
            viewText = new ExtViewText(_division);
            _division.AddContent(viewText);
        }

        /**
          @fn    public void SetDivisionStatus(string statusText)
 
          @brief Sets the division status string (_divStatus).
 
          @author    Ryan Beruldsen
          @date  21/09/2016
 
          @param statusText  The new status text string.
        */

        public void SetDivisionStatus(string statusText) {
            _divStatus.Text = statusText;
        }

        /**
         @fn    void SetEnabled(bool enabled);
        
         @brief Enabled status of task.
        
         @param enabled    Enabled status of task (sched start/stop event).
         */
        public void SetEnabled(bool enabled) {
            _enabled = enabled;
            if (enabled) {
                for (int i = 0; i < _modelList.Count; i++) {
                    _modelList[i].Enabled = true;
                }
            }
            var t = new TaskControllerEventArgs {
                EnabledEvent = true,
                Task = this
            };
            TasksControllerChanged(t);
        }

        /**
         @fn    private void ExtModelChanged(object sender, ExtModelEventArgs e)

         @brief Handle ext model events.

         @author    Ryan Beruldsen
         @date  21/09/2016

         @param sender  Source of the event.
         @param e       Resource model tank event information.
        */
        private void ExtModelChanged(object sender, ExtModelEventArgs e) {
            //if warning has been resolved and not timed out
            if (e.WarnIndicateChange && !e.Ext.WarnIndicate && e.Ext.Trigger != 0) {

                int buttonRef = _modelList.IndexOf(e.Ext);
                var eventRow = new CDRLogRow() {
                            ModelData.Instance.DurationStr24Hr,
                            _modelList[buttonRef].Index+"",
                            (ModelData.Instance.DurationDouble-e.Ext.Trigger).ToString("00.0"),
                            buttonRef == _BUTTON_1 ? "1" : "2",
                            "y"
                };
                e.Ext.Trigger = 0;
                ModelDataIOLog.Instance.LogTaskEvent("ext", eventRow);
                ModelDataIOLog.Instance.LogEventBrief(EVENT_BRIEF);

                var t = new TaskControllerEventArgs {
                    WarningResolved = true,
                    Task = this
                };
                TasksControllerChanged(t);
            }

            if (e.WarnIndicateChange && e.Ext.WarnIndicate) {
                _warnings++;
                var t = new TaskControllerEventArgs {
                    WarningRaised = true,
                    Task = this
                };
                TasksControllerChanged(t);
            }else if (e.WarnIndicateChange && !e.Ext.WarnIndicate) {
                _warnings--;
                
            }
        }

        /**
         @fn    private int SchedTrigger(Dictionary<string, string> parameters)

         @brief Trigger function for the "sched" IO event tag - starts and stops ext task.

         @author    Ryan Beruldsen
         @date  5/10/2016

         @param parameters  IO Event parameters hash map.

         @return    An int - this result is ignored.
         */

        private int SchedTrigger(Dictionary<string, string> parameters) {
            //initialise event parameter strings
            string task = "NULL", action = "NULL", update = "NULL", response = "NULL";

            //set event parameter strings
            if (parameters.ContainsKey("task")) {
                task = parameters["task"];
            }
            if (parameters.ContainsKey("action")) action = parameters["action"];
            if (parameters.ContainsKey("update")) update = parameters["update"];
            if (parameters.ContainsKey("response")) response = parameters["response"];

            //switch task parameter string
            switch (task) {
                case "ext":
                    switch (action) {
                        case "start":
                            _log.LogEvent(   ModelData.Instance.DurationStr24Hr, 
                                            "Scheduling - EXT Session Started");
                            SetEnabled(true);
                            break;
                        default:
                            _log.LogEvent(  ModelData.Instance.DurationStr24Hr, 
                                            "Scheduling - EXT Session Ended");
                            SetEnabled(false);
                            break;
                    }
                    break;
            }

            return 1;
        }

        /**
         @fn    private int extTrigger(Dictionary<string, string> parameters)

         @brief Trigger function for the "ext" IO event tag - starts and stops ext button task.

         @author    Ryan Beruldsen
         @date  5/10/2016

         @param parameters  IO Event parameters hash map.

         @return    An int - this result is ignored.
         */

        private int ExtTrigger(Dictionary<string, string> parameters) {
            bool randomMode = ModelData.Instance.Settings[ModelData.RANDOM_MODE] != ModelData.PARAM_OFF;

            //initialise event parameter strings
            string buttonWarning = "";
            bool eventsPresent = false;

            //if button trigger
            if (parameters.ContainsKey("button")) {
                buttonWarning = parameters["button"];
                eventsPresent = true;

            //if text view update trigger
            }else if (parameters.ContainsKey("text")) {
                viewText.Text = parameters["text"].ToUpper();
            }

            //if button (component controller) events are present
            if (eventsPresent) {
                //more event parameter variables
                bool button1 = buttonWarning.Contains("1");
                bool button2 = buttonWarning.Contains("2");

                bool timeout = buttonWarning.Contains("timeout");

                bool button = (button1 || button2) && !timeout;

                bool button_timeout = (button1 || button2) && timeout;

                int buttonModelRef = button1 ? _BUTTON_1 : _BUTTON_2;
                var buttonModel = _modelList[buttonModelRef];

                //if valid button warning raise event
                if (button && !buttonModel.WarnIndicate) {
                    //log the raised event in central log
                    int eventIndex = ModelDataIOLog.Instance.LogEvent(
                        ModelData.Instance.DurationStr24Hr, 
                        "Extension"
                    );

                    //index to ensure bind with timeout trigger
                    buttonModel.Index = eventIndex;

                    buttonModel.WarnIndicate = true;

                    //record timing of warning for log metrics
                    buttonModel.Trigger = ModelData.Instance.DurationDouble;

                    //trigger a timeout for raised warning
                    TriggerTimeout(_timeout,
                                    callback: ExtTrigger,
                                    parameters: new CDRKeyValData() {
                                                        { "button", button1 ? "1_timeout" : "2_timeout"},
                                                        { "index", eventIndex+""}
                    });

                //if valid button warning timeout resolve event
                } else if (button_timeout && buttonModel.Trigger != 0 && buttonModel.WarnIndicate) {
                    bool phantomTimeout = buttonModel.Index != Convert.ToInt32(parameters["index"]);

                    //if timeout indexes match, prevent phantom timeout
                    if (!phantomTimeout) {
                        //log timout event in task log
                        var eventRow = new CDRLogRow() {
                            ModelData.Instance.DurationStr24Hr,
                            buttonModel.Index+"",
                            (ModelData.Instance.DurationDouble-buttonModel.Trigger).ToString("00.0"),
                            button1 ? "1" : "2",
                            "n"
                        };
                        ModelDataIOLog.Instance.LogTaskEvent(EXT_TAG, eventRow);
                        ModelDataIOLog.Instance.LogEventBrief(EVENT_BRIEF);

                        //trigger score penalty if in random mode 
                        //(loaded events mode penalty handled elsewhere)
                        if(!randomMode) {
                            WarningOvertime(this);
                        }
                       
                        //reset trigger time to ensure warning resolution is not handled elsewhere
                        buttonModel.Trigger = 0;

                        //reset warning indication
                        buttonModel.WarnIndicate = false;
                    }
                    

                }

            }
            return 1;
        }

        /**
         @fn    public void Trigger(float time)

         @brief Random task trigger for user - used in random mode.

            - sends random event parameters to event trigger functions.

         @author    Ryan Beruldsen
         @date  21/09/2016

         @param time    Time elapsed in seconds (not used).
         */

        public void Trigger(float time) {
            var rnd = new Random();
            if (rnd.NextDouble() <= 0.2 && !_modelList[_BUTTON_1].WarnIndicate) {
                ExtTrigger(new Dictionary<string, string>(){
                    { "button", "1"},
                });
            } else if (rnd.NextDouble() >= 0.8 && !_modelList[_BUTTON_2].WarnIndicate) {
                ExtTrigger(new Dictionary<string, string>(){
                    { "button", "2"},
                });
            }
        }
    }
}
