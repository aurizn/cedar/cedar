﻿/*
 * CEDAR Operator Workload Assessment Tool
 * Copyright (C) 2023  aurizn
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


using System;

namespace CEDAR.Shared.Modules.Task.Res {
    internal delegate void ResModelPumpEventHandler(object sender, ResModelPumpEventArgs e);

    /**
     @class ResModelPump
    
     @brief Model representing a pump between two tanks (ResModelTank)
    
     @author    Ryan Beruldsen
     @date  15/09/2016
     */

    class ResModelPump {

        /** @brief Enabled status of model. */
        private bool _enabled = false;

        /**
         @property  public bool enabled
        
         @brief Gets or sets a value indicating whether the model is enabled.
        
         @return    true if enabled, false if not.
         */
        public bool Enabled {
            get { return _enabled; }
            set {
                _enabled = value;
            }
        }

        /** @brief The flow rate of the pump. */
        private double _rate;

        /** @brief Pump fault status. */
        private bool _fault;

        /** @brief Pump description (contains tanks from and to, eg "A>B"). */
        private readonly string description;

        /** @brief The channel to pump from. */
        private readonly ResModelTank _channelFrom;

        /** @brief The channel to pump to. */
        private readonly ResModelTank _channelTo;

        /** @brief Pump selected state (pump is active when selected). */
        private bool _selected;

        /** @brief Event queue for all listeners interested in resPumpChanged events. */
        public event ResModelPumpEventHandler ResPumpEventHandler;

        /**
         @property  public ResModelTank channelTo
        
         @brief Gets the to channel for the pump.
        
         @return    To channel for pump.
         */

        public ResModelTank GetchannelTo() { return _channelTo; }
        /**
         @property  public ResModelTank channelFrom
        
         @brief Gets the channel from.
        
         @return    The channel from.
         */

        public ResModelTank ChannelFrom { get { return _channelFrom; } }

        /**
         @property  public string pumpDescription
        
         @brief Gets pump description (contains tanks from and to, eg "A>B").
        
         @return    Pump description (contains tanks from and to, eg "A>B").
         */

        public string PumpDescription { get { return description; } }

        /**
         @property  public double flowRate
        
         @brief Gets or sets the pump flow rate.
        
         @return    The pump flow rate.
         */

        public double FlowRate {
            get { return _rate; }
            set {
                if (_enabled) {
                    ResModelPumpEventArgs e = new ResModelPumpEventArgs {
                        ValueChange = true
                    };
                    _rate = value;
                    e.Pumpref = this;
                    ResModelPumpChanged(e);
                }
            }
        }

        /**
         @property  public bool isSelected
        
         @brief Gets or sets a pump selected state.
        
         @return    Pump selected state (pump is active when selected).
         */

        public bool IsSelected {
            get { return _selected; }
            set {
                if (_enabled) {
                    _selected = value;
                    ResModelPumpEventArgs e = new ResModelPumpEventArgs {
                        SelectionChange = true,
                        Pumpref = this
                    };
                    ResModelPumpChanged(e);
                }
            }
        }

        /**
         @property  public bool pumpFault
        
         @brief Gets or sets pump fault state.
        
         @return    Pump fault state boolean (pump cannot activate on fault state)
         */

        public bool PumpFault {
            get { return _fault; }
            set {
                if (_enabled) {
                    ResModelPumpEventArgs e = new ResModelPumpEventArgs();
                    _fault = value;
                    e.Fault = true;
                    e.Pumpref = this;
                    ResModelPumpChanged(e);
                }
            }
        }

        /**
         @fn    protected virtual void ResModelPumpChanged(ResModelPumpEventArgs e)
        
         @brief Raises resource model pump events.
        
         @author    Ryan Beruldsen
         @date  21/09/2016
        
         @param e   Event information to send to registered event handlers.
         */

        protected virtual void ResModelPumpChanged(ResModelPumpEventArgs e) {
            ResPumpEventHandler?.Invoke(this, e);
        }

        /**
         @fn    public ResModelPump(string description, double rate, bool fault, ResModelTank channelFrom, ResModelTank channelTo)
        
         @brief Constructor for resource pump model.
        
         @author    Ryan Beruldsen
         @date  21/09/2016
        
         @param description The pump description (contains tanks from and to, eg "A>B").
         @param rate        The pump rate.
         @param fault       true to fault.
         @param channelFrom The channel to pump from.
         @param channelTo   The channel to pump to.
         */

        public ResModelPump(    string description, 
                                double rate, 
                                bool fault, 
                                ResModelTank channelFrom, 
                                ResModelTank channelTo) {

            this.description = description;
            this._rate = rate;
            this._fault = fault;
            this._channelTo = channelTo;
            this._channelFrom = channelFrom;
            this._selected = false;
        }

        /**
         @fn    public void Pump()
        
         @brief Triggers a pump event.
        
         @author    Ryan Beruldsen
         @date  21/09/2016
         */

        public void Pump() {
            if (_enabled) {
                ResModelPumpEventArgs e = new ResModelPumpEventArgs {
                    Pump = true,
                    Pumpref = this
                };

                double diff = _channelFrom.LevelUnits;
                _channelFrom.LevelUnits -= FlowRate;
                diff -= _channelFrom.LevelUnits;
                if (ChannelFrom.Unlimited) {
                    diff = FlowRate;
                }
                _channelTo.LevelUnits += diff;

                if (diff > 0) {
                    ResModelPumpChanged(e);
                }
            }
        }
    }
}
