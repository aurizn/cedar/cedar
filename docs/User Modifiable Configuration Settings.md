# User Modifiable Configuration Settings

### Summary

While all test procedure related configuration is contained in the _config_ file, some modifications can be made to the test environment, I/O, file loading mechanisms and file selection. Settings are persisted to the external _settings.json_ once changed, this file is loaded on launch.

### User Modifiable Configuration

| Title | Options | Description |
| --- | --- | --- |
| File | (Files available) | Selects the file to load from a list of files as specified by either the loaded config file or scanned in the root CEDAR app folder. Note that _Reload Configuration Files_ must be pressed for a new file load event to occur. The loaded filename is listed on the title page below the CEDAR title if _Training Mode_ is not enabled (in which case the training score will replace it if a score is available). |
| Input Format | (Input formats available) | Input format to load for the test files – note that _Reload Configuration Files_ must be pressed for a new file load event to occur. |
| Output Format | (Output formats available) | Output format of log files. |
| Division 1 Module | (Modules available for division 1 position) | Selects the first (top) division module, by default this is the Communications task. |
| Division 2 Module | (Modules available for division 2 position) | Selects the second (middle) division module, by default this is the System Monitoring task. |
| Division 3 Module | (Modules available for division 3 position) | Selects the third (bottom) division module, by default this is the Resource Management task. |
| (Colours) | (Colours available) | Modifiable colour options, for more information on the meaning of each colour type, see the _User Guides._ |
| High Contrast Colours | On/Off | Enables/Disables high contrast colour mode – this is not recommended for extended viewing as the bright colours will irritate the eye. |
| Designator Part 1, 2 | (A-F) | Two part device designator, noted in the name and content of log files to allow multiple CEDAR devices to coexist in a test environment. |
| Width Offset | (Width offset % available) | Width offset for test session – this is to allow convenient interaction with the test screen even with a case obscuring the periphery of the tablet screen. APP MUST BE RESTARTED WHEN CHANGING. |
| CAOP Mode | On/Off | Clear Analysis On Pause (CAOP) Mode clears the analysis logs (output as _analysis.json_) on OS Pause. This is useful if the OS pause function is used to delimit test serials. All other logs will still continue uncleared to allow overall analysis of the entire session – pauses are noted in the CEDAR log, so each pause event can be tracked in the log. |
| Output Unix Time | On/Off | Whether to additionally output unix timestamps in additional to relative test timing. |