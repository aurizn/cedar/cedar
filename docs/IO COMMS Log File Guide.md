# IO COMMS Log File Guide

The COMMS log file is the Communications task log, providing detailed logging information on user interaction with the task. The output file is nearly identical to the MATB-II equivalent.

### MATB-II Compatibility

Complete compatibility with the caveat of some limited whitespace and case differences.

The referenced event numbering does differ slightly from MATB-II, see the _CEDAR Log File_ document and the _Events File Guide_ for more information.

Example Log File Snippet

      # 10-11-2016 18:16:00 COMM_2016_10111816.txt
      #
      # Events Filename: CEDAR_EVENTS.xml
      #
      # Timeout (in seconds) = 10
      #
      # _T = Target, _S = Selected
      # R_ = Radio, F_ = Frequency
      #
      #-TIME-     -EVENT- -SHIP- -RADIO_T- -FREQ_T- -RADIO_S- -FREQ_S_ -FREQ_S_- -F_OK- 
      #----------------------------------------------------------------------------------------------
      00:00:11.0 5       own    COM2      124.450  COM2      123.950  true      false 
      00:00:20.0 13      other  COM1      124.700  COM1      124.700  false     true

### Example JSON Log File

        [
        {
            "-TIME-": "00:00:02.0",
            "-EVENT-": "5",
            "-SHIP-": "own",
            "-RADIO_T-": "COM2",
            "-FREQ_T-": "124.450",
            "-RADIO_S-": "COM2",
            "-FREQ_S_": "129.500",
            "-FREQ_S_-": "false",
            "-F_OK-": "false"
        },
        {
            "-TIME-": "00:00:11.0",
            "-EVENT-": "13",
            "-SHIP-": "other",
            "-RADIO_T-": "COM1",
            "-FREQ_T-": "123.475",
            "-RADIO_S-": "COM1",
            "-FREQ_S_": "123.475",
            "-FREQ_S_-": "false",
            "-F_OK-": "true"
        }
        ]

### Example Log File Snippet Explanation

The log file begins with a header detailing the timestamp and filename of the log file, then a plain text formatted table of log entries, followed by the timeout parameter of communication events (see _Config File Guide_ for more information on setting this).

Each log row represents a Communications task event timeout or user response.

The JSON format variant just lists the columns and rows of the plaintext file. Column titles are not listed in the JSON variant.

| Column | Format | Meaning |
| --- | --- | --- |
| -TIME- | 00:00:00.4 (hour:minute:second:tenth of second) | The timestamp of the event in question. |
| -EVENT- | Integer | The cumulative Cedar processing event number pertaining to the row, referenced from the central CEDAR log file (see the _CEDAR Log File_ document for more information). |
| -SHIP- | own/other | Whether the voiceover pertained to the correct (own), or other callsign. If in silent mode, this is whether the warning raised is a false warning or not. |
| -RADIO_T- | NAV1NAV2/COM1/COM2 | The target radio channel of the event. |
| -FREQ_T- | Floating point (000.000). | The target frequency of the event. |
| -RADIO_S- | NAV1NAV2/COM1/COM2 | The radio selected at the time of the log. |
| -FREQ\_S\_- | Floating point (000.000). | The frequency of the radio selected at the time of the log. |
| -FREQ\_S\_ | true/false | Whether the correct frequency was selected at the time of the log. |
| -F_OK- | true/false | Whether the user reacted the event correctly or not. |