﻿using System;
using System.Collections.Generic;
using System.Text;

/*! \namespace CEDAR.Shared.CedarCollection
 * @brief Cedar conventional collection aliases and custom collection subclasses.
 * 
 */

namespace CEDAR.Shared.CedarCollection {

    /**
     @class CDRLogData
    
     @brief Cedar brief event log data.
    
     @author    Ryan Beruldsen
     @date  4/10/2016
     */

    public class CDRLogData {
        public CDRLogData(string value) {
            this.Value = value;
        }

        public string Value { get; set; }
    }

    /**
     @class CDRLogEvent
    
     @brief Cedar event log data, contains event index.
    
     @author    Ryan Beruldsen
     @date  4/10/2016
     */

    public class CDRLogEvent : CDRLogData {
        public CDRLogEvent(int index, string value) : base(value) {
            this.Index = index;
        }
        public int Index { get; set; }
    }

    /**
     @class CDRKeyValLogData
    
     @brief Cedar overall log summarising task events 
        
        - hashpmap of time in seconds to a set of log data items.
    
     @author    Ryan Beruldsen
     @date  5/10/2016
     */

    public class CDRKeyValLogData : Dictionary<string, HashSet<CDRLogData>> { }

    /**
     @class CDRLogRow
    
     @brief Cedar log row data.
    
     @author    Ryan Beruldsen
     @date  5/10/2016
     */

    public class CDRLogRow : List<string> {
        public CDRLogRow() : base() {
        }
    }

    /**
     @class CDRLogColumns
    
     @brief Cedar log column data - list of row data.
    
     @author    Ryan Beruldsen
     @date  5/10/2016
     */
    public class CDRLogColumns : List<CDRLogRow> { }

    /**
     @class CDRTasksLog
    
     @brief Cedar task event log 
        
        - hashpmap of task tag to relevant task data log columns 
    
     @author    Ryan Beruldsen
     @date  5/10/2016
     */

    public class CDRTasksLog : Dictionary<string, CDRLogColumns> { }

    /**
     @class CDRTaskEventParams
    
     @brief Cedar task event parameters

        - hashmap of event key to parameter
    
     @author    Ryan Beruldsen
     @date  5/10/2016
     */

    public class CDRTaskEventParams : Dictionary<string, string> { }

    /**
     @class CDRTaskEventParamsTimeGroup
    
     @brief Cedar task event parameters time group

        - a list of task event parameters, representing all of the events occurring at a given second (the "time group")
    
     @author    Ryan Beruldsen
     @date  5/10/2016
     */

    public class CDRTaskEventParamsTimeGroup : List<CDRTaskEventParams> { }

    /**
     @class CDRTaskEventTimeGroup
    
     @brief Cedar task event time group

        - at a given moment in (time), a hashmap of the type of event to the relevant (group of) task event parameters that occur at that time (CDRTaskEventParamsTimeGroup)
    
     @author    Ryan Beruldsenevent
     @date  5/10/2016
     */

    public class CDRTaskEventTimeGroup : Dictionary<string, CDRTaskEventParamsTimeGroup> { }

    /**
     @class CDRIOEvents
    
     @brief Cedar top level input events collection

        - hashmap of time in seconds to the (group of) events set to occur at that time
    
     @author    Ryan Beruldsen
     @date  5/10/2016
     */

    public class CDRIOEvents : Dictionary<string, CDRTaskEventTimeGroup> { }

    /**
     @class CDRConfigParams
    
     @brief Cedar configuration parameters

        - hashmap of config parameter to value
    
     @author    Ryan Beruldsen
     @date  5/10/2016
     */

    public class CDRConfigParams : SortedDictionary<string, string> { }

    /**
     @class CDRIOConfig
    
     @brief Cedar top level input config collection

        - hashmap of top level config parameter to group of child parameters and values (CDRConfigParams)
    
     @author    Ryan Beruldsen
     @date  5/10/2016
     */

    public class CDRIOConfig : SortedDictionary<string, CDRConfigParams> { }

    /**
     @class CDRKeyValData
    
     @brief Cedar wrapper for simple key, value hashmap.
    
     @author    Ryan Beruldsen
     @date  5/10/2016
     */

    public class CDRKeyValData : Dictionary<string, string> { }

    /**
     @class CDRTouchLogData
    
     @brief Cedar test session touch log data

        - hashmap of a coordinates of touch to a stack of touch data
        - touch data is recorded in a stack to allow quick filtering of
        multiple listeners firing the same touch event for a single touch
    
     @author    Ryan Beruldsen
     @date  17/11/2016
     */
    public class CDRTouchLogData : Dictionary<string, Stack<CDRTouchData>> { }

    /**
     @class CDRTouchLog
    
     @brief Cedar test session ordered touch log list

        - sorted list of timestamp to touch data
    
     @author    Ryan Beruldsen
     @date  17/11/2016
     */
    public class CDRTouchLog : SortedList<string, Stack<CDRTouchData>> { }

    /**
     @class CDRTouchData
    
     @brief Cedar touch data - for accuracy metrics
        - long timestamp held to debounce touches to 300ms
    
     @author    Ryan Beruldsen
     @date  17/11/2016
     */
    public class CDRTouchData {
        //hit, miss or false positive status
        public enum StatusEnum { Hit, Miss, FalsePos}
        public StatusEnum Status { get; set; }

        public float X { get; set; }
        public float Y { get; set; }
        public string TimestampStr { get; set; }
        public long Timestamp { get; set; }

    }

    /**
     @class CDRScoreLogData

     @brief Cedar test session score data collection

        - map of task tag to score data

     @author    Ryan Beruldsen
     @date  17/11/2016
     */
    public class CDRScoreLogData : Dictionary<string, Stack<CDRScoreData>> { }

    /**
     @class CDRScoreLog

     @brief Cedar test session score data collection - chronologically ordered list 

     @author    Ryan Beruldsen
     @date  17/11/2016
     */
    public class CDRScoreLog : SortedList<string, List<CDRScoreData>> { }

    /**
     @class CDRScoreData
    
     @brief Cedar test session score data
        - records performance of user in completing required tasks (pass/timeout-fail)
    
     @author    Ryan Beruldsen
     @date  18/11/2016
     */
    public class CDRScoreData {
        //hit, miss or false positive status
        public long TimeRaised { get; set; }
        public long TimeResolved { get; set; }
        public bool Timeout { get; set; }
        public string TimeResolvedStr { get; set; }
        public string TimeRaisedStr { get; set; }
        public string ReactionTimeStr { get; set; }
        public long ReactionTime { get; set; }
        public string Tag { get; set; }

    }

    /**
     @class CDREventFnParam
    
     @brief Cedar event callback function and parameter collection.

        - contains required data for callback functions, used to timeout events
        - hashmap of a specified function to relevant event data to be used in the callback
    
     @author    Ryan Beruldsen
     @date  5/10/2016
     */
    public class CDREventFnParam : Dictionary<Func<Dictionary<string, string>, int>, CDRKeyValData> { }

    /**
     @class CDRIOEventFn
    
     @brief Cedar IO event to function register.

        - hashmap from event parameter to relevant local handler function
    
     @author    Ryan Beruldsen
     @date  5/10/2016
     */
    public class CDRIOEventFn : Dictionary<string, Func<Dictionary<string, string>, int>> { }

}
