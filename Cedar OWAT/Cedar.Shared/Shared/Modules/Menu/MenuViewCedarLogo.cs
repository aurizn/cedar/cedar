﻿/*
 * CEDAR Operator Workload Assessment Tool
 * Copyright (C) 2023  aurizn
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using CocosSharp;
using CEDAR.Shared.Modules;
using System;
using System.Collections.Generic;
using System.Text;

namespace CEDAR.Shared.Modules.Menu {

    /**
     @class MenuViewCedarLogo
    
     @brief The Cedar logo, positioned for display on the title screen.
    
     @author    Ryan Beruldsen
     @date  15/09/2016
     */

    class MenuViewCedarLogo : CCNode {

        /** @brief The minimum bounds of the logo */
        public CCPoint minButtonBounds;

        /** @brief The maximum bounds of the logo */
        public CCPoint maxButtonBounds;

        /** @brief Maximum window bounds */
        private CCPoint windowMax;

        private readonly CCColor4B _highlightColour;
        /**
         @fn    public MenuViewCedarLogo(CCPoint windowMax): base()
        
         @brief Title Cedar logo draw constructor.
        
         @author    Ryan Beruldsen
         @date  16/09/2016
        
         @param windowMax   The window maximum.
         @param highlightColour Logo highlight colour.
         */

        public MenuViewCedarLogo(CCPoint windowMax, CCColor4B highlightColour) : base() {
            _highlightColour = highlightColour;
            this.windowMax = windowMax;
            Init();
        }

        /**
         @fn    public void Init()
        
         @brief Initialise/draw Cedar logo
        
         @author    Ryan Beruldsen
         @date  16/09/2016
         */

        public void Init() {
            CCDrawNode logoDrawNode = new CCDrawNode();
            minButtonBounds = new CCPoint(windowMax.X / 2f, windowMax.Y / 2);
            maxButtonBounds = new CCPoint(
                windowMax.X / 2f + windowMax.Y * 0.095f,
                windowMax.Y / 2 + windowMax.X * 0.073f
            );

            //draw outer white band
            logoDrawNode.DrawSolidCircle(new CCPoint((windowMax.X / 2),
                                            (windowMax.Y / 2) + (windowMax.Y * 0.095f)),
                                            (windowMax.X * 0.073f), CCColor4B.White);
            logoDrawNode.DrawSolidCircle(new CCPoint((windowMax.X / 2f),
                                            (windowMax.Y / 2) + (windowMax.Y * 0.095f)),
                                            (windowMax.X * 0.06f), CCColor4B.Black);

            float yGaugePos = 0.06f;
            float gaugeHeight = 0.0085f;

            //draw first logo gauge
            logoDrawNode.DrawRect(
                    new CCRect(windowMax.X * 0.5f - windowMax.X * 0.05f,
                                windowMax.Y * 0.5f + (windowMax.Y * yGaugePos) * 1.75f,
                                windowMax.X * 0.1f, windowMax.Y * gaugeHeight),
                    fillColor: new CCColor4B(100, 100, 100, 100),
                    borderWidth: 0,
                    borderColor: CCColor4B.White
            );

            logoDrawNode.DrawRect(
                    new CCRect(windowMax.X * 0.5f - windowMax.X * 0.05f,
                                windowMax.Y * 0.5f + (windowMax.Y * yGaugePos) * 1.75f,
                                windowMax.X * 0.1f * 0.7f, windowMax.Y * gaugeHeight),
                    fillColor: _highlightColour,
                    borderWidth: 0,
                    borderColor: CCColor4B.White
            );

            //draw second logo gauge
            logoDrawNode.DrawRect(
                    new CCRect(windowMax.X * 0.5f - windowMax.X * 0.05f,
                                windowMax.Y * 0.5f + (windowMax.Y * yGaugePos) * 1.50f,
                                windowMax.X * 0.1f, windowMax.Y * gaugeHeight),
                    fillColor: new CCColor4B(100, 100, 100, 100),
                    borderWidth: 0,
                    borderColor: CCColor4B.White
            );

            logoDrawNode.DrawRect(
                    new CCRect(windowMax.X * 0.5f - windowMax.X * 0.05f,
                                windowMax.Y * 0.5f + (windowMax.Y * yGaugePos) * 1.50f,
                                windowMax.X * 0.1f * 0.95f, windowMax.Y * gaugeHeight),
                    fillColor: _highlightColour,
                    borderWidth: 0,
                    borderColor: CCColor4B.White
            );

            //draw third logo gauge
            logoDrawNode.DrawRect(
                    new CCRect(windowMax.X * 0.5f - windowMax.X * 0.05f,
                                windowMax.Y * 0.5f + (windowMax.Y * yGaugePos) * 1.25f,
                                windowMax.X * 0.1f, windowMax.Y * gaugeHeight),
                    fillColor: new CCColor4B(100, 100, 100, 100),
                    borderWidth: 0,
                    borderColor: CCColor4B.White
            );

            logoDrawNode.DrawRect(
                    new CCRect(windowMax.X * 0.5f - windowMax.X * 0.05f,
                                windowMax.Y * 0.5f + (windowMax.Y * yGaugePos) * 1.25f,
                                windowMax.X * 0.1f * 0.25f,
                                windowMax.Y * gaugeHeight),
                    fillColor: _highlightColour,
                    borderWidth: 0,
                    borderColor: CCColor4B.White
            );
            AddChild(logoDrawNode);
        }
    }
}
