﻿/*
 * CEDAR Operator Workload Assessment Tool
 * Copyright (C) 2023  aurizn
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using CocosSharp;
using CEDAR.Shared.CedarCollection;
using System;
using System.Collections.Generic;

/*! \r\namespace CEDAR.Shared.Modules.Task.Sys
 * @brief Systems Monitoring task content
 * 
 */

namespace CEDAR.Shared.Modules.Task.Sys {

    /**
     @class SysController
    
     @brief Systems monitoring task content controller - handles instantiating instances of child Sys models and controllers
    
     @author    Ryan Beruldsen
     @date  15/09/2016
     */

    class SysController : DivisionContent, IDivisionContent, ITaskContent {

        /** @brief Button one (vertically) index constant 
        
            - referred to in MATB event files as "green". */
        private const int _BUTTON_1_MATB_GREEN = 0;

        /** @brief Button two index constant 
        
            - referred to in MATB event files as "red". */
        private const int _BUTTON_2_MATB_RED = 1;

        /** @brief Scale one (horizontally) index constant. */
        private const int _SYSMON_SCALE_ONE = 2;

        /** @brief Unique tag of System monitoring task. */
        private const string SYS_TAG = "sysmon";

        /** @brief Increment to move scale by on an "UP" or "DOWN" IO event. */
        private const double _SYSMON_SCALE_INCREMENT = 0.75;

        /** @brief The event brief description in task log file. */
        private static readonly string EVENT_BRIEF = "Monitoring";

        /** @brief List of system channels (sys task model). */
        private readonly List<SysModelChannel> _sysChannelList;

        /** @brief IO event to function register. */
        private readonly CDRIOEventFn _ioEventDict;

        /** @brief Random number generator for random events. */
        private readonly Random _rnd;

        /** @brief Division status label string - contains test related information for division (see user guide).*/
        private CCLabel _divStatus;

        /** @brief Enabled status of task (sched start/stop event).*/
        private bool _enabled = false;

        /** @brief Index to prevent phantom (invalid) timeouts.*/
        private int _phantomTimeoutIndex = 0;
        /**
         @fn    public SysController(Division division) : base(division)
        
         @brief Constructor for System Monitoring task.
        
         @author    Ryan Beruldsen
         @date  22/09/2016
        
         @param division    The division.
         */

        public SysController(Division division) : base(division) {
            _sysChannelList = new List<SysModelChannel>();
            _rnd = new Random();

            //register IO event functions
            _ioEventDict = new CDRIOEventFn {
                ["sched"] = SchedTrigger,
                [SYS_TAG] = SysmonTrigger
            };

            //add log file header
            int lTimeout = ModelDataIOConfig.Instance.GetConfigNum("timeout", "task_sysmon_lights");
            int sTimeout = ModelDataIOConfig.Instance.GetConfigNum("timeout", "task_sysmon_scales");
            var eventRow = new CDRLogRow() {
                    "-TIME-",
                    "-RT-",
                    "-SYSTEM-",
                    "-LIGHT/SCALE-",
                    "-SYS_OK-",
                    "-REMARKS-",
            };

            string header = "# Timeout (in seconds):  Lights = " + lTimeout + "   Scales = " + sTimeout + "\r\n#\r\n" +
                            "# RT = Response Time (in seconds)\r\n" +
                            "# SYS_OK = An event for the system selected is active\r\n" +
                            "#";
            ModelDataIOLog.Instance.LogTaskHeader(SYS_TAG, header);
            ModelDataIOLog.Instance.LogTaskEvent(SYS_TAG, eventRow);

        }

        /**
         @fn    public void Init()
        
         @brief Initialise the views, models and controllers for the System Monitoring task.
        
         @author    Ryan Beruldsen
         @date  22/09/2016
         */

        public void Init() {
            //init div status
            _divStatus = new CCLabel("", "roboto", 12) {
                Position = new CCPoint(_division.Width / 2, _division.Y - _division.Height * 0.95f),
                Color = CCColor3B.White,
                AnchorPoint = CCPoint.AnchorMiddle
            };
            AddChild(_divStatus);

            //instantiate button models
            for (int j = 0; j < 2; j++) {
                _sysChannelList.Add(
                    new SysModelChannel(label: "SW" + (j + 1),
                                        level: 0.4f + 0.4f * Convert.ToSingle(_rnd.NextDouble()),
                                        height: 0.15f,
                                        warnOnly: true)
                );

                _sysChannelList[j].SysModelChannelEventHandler +=
                    new SysModelChannelEventHandler(SysModelChannelChanged);
            }

            //instantiate button panel controller
            _division.AddContent(new SysControllerButtonPanel(_sysChannelList, _division));

            //instantiate gauge models
            for (int j = 0; j < 4; j++) {

                _sysChannelList.Add(new SysModelChannel(label: Convert.ToChar('A' + j).ToString(),
                                                        level: 0.4f + 0.4f * Convert.ToSingle(_rnd.NextDouble()),
                                                        height: 0.15f,
                                                        minRange: 0.4f,
                                                        maxRange: 0.8f));

                _sysChannelList[2 + j].SysModelChannelEventHandler +=
                    new SysModelChannelEventHandler(SysModelChannelChanged);
            }

            //instantiate gauge panel controller
            _division.AddContent(new SysControllerGaugePanel(_sysChannelList, _division));

        }

        /**
         @fn    bool getEnabled();
        
         @brief Enabled status of task (sched start/stop event).
        
         */
        public bool GetEnabled() {
            return _enabled;
        }

        /**
         @fn    void EventExternal(string type, Dictionary<string, string> parameters)
        
         @brief Trigger for task event.
        
         @param type        Distinguishing event type (ie. task-level schedule or component level action).
         @param parameters  Event parameters.
         */

        public void EventExternal(string type, Dictionary<string, string> parameters) {

            if (_ioEventDict.ContainsKey(type)) {
                _ioEventDict[type](parameters);
            }
        }

        /**
         @fn    void SetEnabled(bool enabled);
        
         @brief Enabled status of task.
        
         @param enabled    Enabled status of task (sched start/stop event).
         */
        public void SetEnabled(bool enabled) {

            foreach (SysModelChannel channel in _sysChannelList) {
                channel.Enabled = enabled;
                //System.Diagnostics.Debug.Write("SYS ENABLED");
            }

            _enabled = enabled;

            var t = new TaskControllerEventArgs {
                EnabledEvent = true,
                Task = this
            };
            TasksControllerChanged(t);
        }

        /**
         @fn    public void SetDivisionStatus(string statusText)
        
         @brief Sets division status string.
        
         @author    Ryan Beruldsen
         @date  22/09/2016
        
         @param statusText  The status text.
         */

        public void SetDivisionStatus(string statusText) {
            _divStatus.Text = statusText;

        }

        /**
         @fn    private void SysModelChannelChanged(object sender, SysModelChannelEventArgs e)
        
         @brief Handles sys channel events.
        
         @author    Ryan Beruldsen
         @date  22/09/2016
        
         @param sender  Source of the event.
         @param e       System model channel event information.
         */

        private void SysModelChannelChanged(object sender,
                                            SysModelChannelEventArgs e) {
            bool randomMode = ModelData.Instance.Settings[ModelData.RANDOM_MODE] != ModelData.PARAM_OFF;

            //warning raised
            if (e.WarnIndicateChange && e.Sys.WarnIndicate) {
                TaskControllerEventArgs t = new TaskControllerEventArgs();

                //if warning is raised from a gauge
                if (!e.Sys.WarnOnlyMode) {
                    var scaleNumList = new List<string> { "one", "two", "three", "four" };
                    int scaleNumIndex = _sysChannelList.IndexOf(e.Sys) - _SYSMON_SCALE_ONE;
                    e.Sys.Trigger = ModelData.Instance.DurationDouble;
                    e.Sys.Index = _phantomTimeoutIndex;
                    var timeoutOr = ModelData.Instance.DurationDouble.ToString("0.0");
                    TriggerTimeout(
                        type: "task_sysmon_scales",
                        callback: SysmonTrigger,
                        parameters: new CDRKeyValData() {
                            {"monitoringscalenumber_timeout", scaleNumList[scaleNumIndex] },
                            {"timeout_originator", timeoutOr},
                            {"index", (_phantomTimeoutIndex++)+""}
                        }
                    );

                }
                else if (!e.Sys.WarnOnlyMode && randomMode) {
                    e.Sys.Trigger = ModelData.Instance.DurationDouble;
                }

                t.WarningRaised = true;
                t.Task = this;
                _warnings++;
                TasksControllerChanged(t);

                //warning resolved
            }
            else if (e.WarnIndicateChange && !e.Sys.WarnIndicate) {

                //if resolution is not a timeout
                if (e.Sys.Trigger != 0) {
                    double responseTime = ModelData.Instance.DurationDouble -
                        e.Sys.Trigger;
                    bool scale = !e.Sys.WarnOnlyMode;
                    var scaleNumList = new List<string> { "green", "red", "one", "two", "three", "four" };
                    var eventRow = new CDRLogRow() {
                        ModelData.Instance.DurationStr24Hr,
                        responseTime.ToString("00.0")+"",
                        scale ? "scale": "light",
                        scaleNumList[_sysChannelList.IndexOf(e.Sys)],
                        "true",
                        ""
                        };
                    ModelDataIOLog.Instance.LogTaskEvent(SYS_TAG, eventRow);
                    ModelDataIOLog.Instance.LogEventBrief(EVENT_BRIEF);

                    //raise resolved event to scheduler
                    TaskControllerEventArgs t = new TaskControllerEventArgs {
                        WarningResolved = true,
                        Task = this
                    };
                    TasksControllerChanged(t);

                }

                _warnings--;
            }
        }

        /**
         @fn    public string GetTag()
        
         @brief Gets the tag of the division for task scheduler identification purposes
        
         @author    Ryan Beruldsen
         @date  22/09/2016
        
         @return    The unique string tag of the division ("sysmon").
         */

        public string GetTag() { return SYS_TAG; }

        /**
         @fn    private int sysmonTrigger(Dictionary<string, string> parameters)
        
         @brief Trigger function for the "sysmon" IO event tag.

            - handles all main System Monitoring events, currently light on/off, scale up/down
        
         @author    Ryan Beruldsen
         @date  5/10/2016
        
         @param parameters  IO Event parameters hash map.
        
         @return    An int - this result is ignored.
         */
        private int SysmonTrigger(Dictionary<string, string> parameters) {

            //lights triggers
            if (parameters.ContainsKey("monitoringlighttype") ||
                parameters.ContainsKey("monitoringlighttype_timeout")) {
                SysmonTriggerLights(parameters);

                //scales triggers
            }
            else if (parameters.ContainsKey("monitoringscalenumber") ||
                parameters.ContainsKey("monitoringscalenumber_timeout")) {
                SysmonTriggerScales(parameters);
            }

            return 1;
        }

        /**
         @fn    private void SysmonTriggerLights(Dictionary<string, string> parameters)
        
         @brief Sysmon lights IO parameter trigger handler.
        
         @author    Ryan Beruldsen
         @date  5/10/2016
        
         @param parameters  IO Event parameters hash map.
         */

        private void SysmonTriggerLights(Dictionary<string, string> parameters) {
            //raise light warning
            if (parameters.ContainsKey("monitoringlighttype") && _enabled) {

                //log light warning raise
                ModelDataIOLog.Instance.LogEvent(ModelData.Instance.DurationStr24Hr,
                    EVENT_BRIEF);

                //if light one - MATB "green"
                if (parameters["monitoringlighttype"].Equals("green")) {
                    _sysChannelList[_BUTTON_1_MATB_GREEN].WarnIndicate = true;

                    //record time of warning raise for future referece RE reaction time
                    _sysChannelList[_BUTTON_1_MATB_GREEN].Trigger =
                        ModelData.Instance.DurationDouble;
                    _sysChannelList[_BUTTON_1_MATB_GREEN].Index =
                        _phantomTimeoutIndex;
                    //trigger timeout for light warning
                    var timeoutOr = ModelData.Instance.DurationDouble.ToString("0.0");
                    TriggerTimeout(
                        type: "task_sysmon_lights",
                        callback: SysmonTrigger,
                        parameters: new CDRKeyValData() {
                            { "monitoringlighttype_timeout", "green" },
                            {"timeout_originator", timeoutOr},
                            {"index", (_phantomTimeoutIndex++)+"" },
                        }
                    );

                    //if light two - MATB "red"
                }
                else if (_enabled) {
                    _sysChannelList[_BUTTON_2_MATB_RED].WarnIndicate = true;
                    _sysChannelList[_BUTTON_2_MATB_RED].Trigger = ModelData.Instance.DurationDouble;
                    _sysChannelList[_BUTTON_2_MATB_RED].Index = _phantomTimeoutIndex;
                    //trigger timeout for light warning
                    var timeoutOr = ModelData.Instance.DurationDouble.ToString("0.0");
                    TriggerTimeout(
                        type: "task_sysmon_lights",
                        callback: SysmonTrigger,
                        parameters: new CDRKeyValData() {
                            { "monitoringlighttype_timeout", "red" },
                            {"timeout_originator", timeoutOr},
                            {"index", (_phantomTimeoutIndex++)+"" },
                        }
                    );
                }
                //timeout light warning
            }
            else if (parameters.ContainsKey("monitoringlighttype_timeout")) {

                bool greenLightType = parameters["monitoringlighttype_timeout"].Equals("green");
                double responseTime = Convert.ToDouble(parameters["timeout_originator"]) -
                    ModelData.Instance.DurationDouble;
                int lightRef = greenLightType ? _BUTTON_1_MATB_GREEN : _BUTTON_2_MATB_RED;

                bool phantomTimeout = Convert.ToInt32(parameters["index"]) !=
                    _sysChannelList[lightRef].Index;

                //if timeout is valid
                if (!phantomTimeout && _sysChannelList[lightRef].WarnIndicate) {
                    //log new task event row for timeout event
                    var eventRow = new CDRLogRow() {
                        ModelData.Instance.DurationStr24Hr,
                        responseTime.ToString("00.0")+"",
                        "light",
                        greenLightType ? "green" : "red",
                        "",
                        "- event timed out"
                    };
                    ModelDataIOLog.Instance.LogTaskEvent(SYS_TAG, eventRow);
                    ModelDataIOLog.Instance.LogEventBrief(EVENT_BRIEF, true);

                    _sysChannelList[lightRef].Trigger = 0;
                    WarningOvertime(this);
                    _sysChannelList[lightRef].WarnIndicate = false;
                }
            }
        }

        /**
         @fn    private void SysmonTriggerScales(Dictionary<string, string> parameters)
        
         @brief Sysmon scales IO parameter trigger handler.
        
         @author    Ryan Beruldsen
         @date  5/10/2016
        
         @param parameters  IO Event parameters hash map.
         */

        private void SysmonTriggerScales(Dictionary<string, string> parameters) {
            if (parameters.ContainsKey("monitoringscalenumber") ||
                parameters.ContainsKey("monitoringscalenumber_timeout")) {

                bool timeout = parameters.ContainsKey("monitoringscalenumber_timeout");

                //index number string of scale to trigger event
                var scaleNum = timeout ?
                    parameters["monitoringscalenumber_timeout"] : parameters["monitoringscalenumber"];

                var scaleNumList = new List<string> { "one", "two", "three", "four" };

                //index number int of scale to trigger event
                int scaleNumInt = scaleNumList.IndexOf(scaleNum);

                //move scale up or down if not timeout event and enabled
                if (!timeout && scaleNumInt >= 0 && _enabled) {

                    //log scale move event
                    ModelDataIOLog.Instance.LogEvent(
                        ModelData.Instance.DurationStr24Hr,
                        EVENT_BRIEF);

                    var channel = _sysChannelList[_SYSMON_SCALE_ONE + scaleNumInt];
                    if (parameters["monitoringscaledirection"].Equals("up")) {
                        channel.Level += _SYSMON_SCALE_INCREMENT;

                        //trigger time recorded for reaction time log metric
                        channel.Trigger = ModelData.Instance.DurationDouble;
                    }
                    else {
                        channel.Level -= _SYSMON_SCALE_INCREMENT;

                        //trigger time recorded for reaction time log metric
                        channel.Trigger = ModelData.Instance.DurationDouble;
                    }

                    //if timeout event move scale back to center
                }
                else if (timeout && _sysChannelList[_SYSMON_SCALE_ONE + scaleNumInt].WarnIndicate) {

                    bool phantomTimeout = Convert.ToInt32(parameters["index"]) !=
        _sysChannelList[_SYSMON_SCALE_ONE + scaleNumInt].Index;

                    if (!phantomTimeout) {
                        double responseTime = Convert.ToDouble(parameters["timeout_originator"]) -
                            ModelData.Instance.Duration;

                        //log scale timeout
                        var eventRow = new CDRLogRow() {
                            ModelData.Instance.DurationStr24Hr,
                            responseTime.ToString("00.0")+"",
                            "scale",
                            scaleNumList[scaleNumInt],
                            "",
                            "- event timed out"
                        };
                        ModelDataIOLog.Instance.LogTaskEvent(SYS_TAG, eventRow);
                        ModelDataIOLog.Instance.LogEventBrief(EVENT_BRIEF, true);

                        WarningOvertime(this);

                        //reset scale to center by forcing selection
                        _sysChannelList[_SYSMON_SCALE_ONE + scaleNumInt].Trigger = 0;
                        _sysChannelList[_SYSMON_SCALE_ONE + scaleNumInt].IsSelected = true;
                    }

                }

            }
        }

        /**
         @fn    private int SchedTrigger(Dictionary<string, string> parameters)
        
         @brief Trigger function for the "sysmon" IO event tag - starts and stops sysmon task.
        
         @author    Ryan Beruldsen
         @date  5/10/2016
        
         @param parameters  IO Event parameters hash map.
        
         @return    An int - this result is ignored.
         */

        private int SchedTrigger(Dictionary<string, string> parameters) {
            string task = "NULL", action = "NULL", update = "NULL", response = "NULL";

            if (parameters.ContainsKey("task")) task = parameters["task"];
            if (parameters.ContainsKey("action")) action = parameters["action"];
            if (parameters.ContainsKey("update")) update = parameters["update"];
            if (parameters.ContainsKey("response")) response = parameters["response"];

            //switch task parameter string
            switch (task) {
                case "ressys":
                    switch (action) {
                        case "start":
                            ModelDataIOLog.Instance.LogEvent(
                                    ModelData.Instance.DurationStr24Hr,
                                    "Scheduling - SYSMON Active"
                            );
                            SetEnabled(true);
                            break;
                        default:
                            SetEnabled(false);
                            break;
                    }
                    break;
            }

            return 1;
        }


        /**
         @fn    public void Trigger(float time)
        
         @brief Random task trigger for user 
            
            - also moves scale randomly (without triggering any warnings) outside of random mode.
        
         @author    Ryan Beruldsen
         @date  22/09/2016
        
         @param time    Time elapsed in seconds (not used).
         */

        public void Trigger(float time) {
            if (_enabled) {
                bool randomMode = ModelData.Instance.Settings[ModelData.RANDOM_MODE] != ModelData.PARAM_OFF;

                float increment = 0.025f;
                for (int j = 0; j < _sysChannelList.Count; j++) {
                    //50% chance of moving scale up
                    if (_rnd.NextDouble() > 0.5) {
                        //if channel is a scale
                        if (!_sysChannelList[j].WarnOnlyMode) {
                            if (!_sysChannelList[j].WarnIndicate) {
                                //if random mode increment randomly
                                if (randomMode) {
                                    _sysChannelList[j].Level += increment;

                                    //if warning has been raised, 
                                    //handle event with normal trigger
                                    if (_sysChannelList[j].WarnIndicate) {
                                        int sysNumIndex = j - _SYSMON_SCALE_ONE;
                                        string[] nums = { "one", "two", "three", "four" };

                                        SysmonTrigger(new Dictionary<string, string>() {
                                            {"monitoringscalenumber", nums[sysNumIndex]},
                                            {"monitoringscaledirection", "up"},
                                        });
                                    }


                                    //if not random mode increment randomly without triggering any warnings
                                }
                                else {
                                    if ((_sysChannelList[j].Level + increment) <
                                        _sysChannelList[j].MaxVal) {
                                        _sysChannelList[j].Level += increment;
                                    }
                                }
                                //if in warning zones, move randomly only within warning zones
                            }
                            else if (_sysChannelList[j].WarnIndicate &&
                                        _sysChannelList[j].Level > _sysChannelList[j].MaxVal) {

                                _sysChannelList[j].Level = _sysChannelList[j].MaxVal +
                                    (_rnd.NextDouble() * (1 - 0.5 * _sysChannelList[j].MaxVal));

                                if (_sysChannelList[j].Level == _sysChannelList[j].MaxVal) {
                                    _sysChannelList[j].Level += increment;
                                }

                            }
                            else if (_sysChannelList[j].WarnIndicate &&
                                        _sysChannelList[j].Level < _sysChannelList[j].MinVal) {

                                _sysChannelList[j].Level = _rnd.NextDouble() * 0.5 * _sysChannelList[j].MinVal;
                                if (_sysChannelList[j].Level == _sysChannelList[j].MinVal) {
                                    _sysChannelList[j].Level -= increment;
                                }
                            }
                        }
                        //50% chance of moving scale down
                    }
                    else {
                        //if channel is a scale
                        if (!_sysChannelList[j].WarnOnlyMode) {
                            if (!_sysChannelList[j].WarnIndicate) {
                                //if random mode decrement randomly
                                if (randomMode) {
                                    _sysChannelList[j].Level -= increment;

                                    //if warning has been raised, 
                                    //handle event with normal trigger
                                    if (_sysChannelList[j].WarnIndicate) {
                                        int sysNumIndex = j - _SYSMON_SCALE_ONE;
                                        string[] nums = { "one", "two", "three", "four" };

                                        SysmonTrigger(new Dictionary<string, string>() {
                                            {"monitoringscalenumber", nums[sysNumIndex]},
                                            {"monitoringscaledirection", "down"},
                                        });
                                    }

                                    //if not random mode decrement randomly without triggering any warnings
                                }
                                else {
                                    if ((_sysChannelList[j].Level - increment) > _sysChannelList[j].MinVal) {
                                        _sysChannelList[j].Level -= increment;
                                    }
                                }

                                //if in warning zones, move randomly only within warning zones
                            }
                            else if (_sysChannelList[j].WarnIndicate &&
                                        _sysChannelList[j].Level > _sysChannelList[j].MaxVal) {

                                _sysChannelList[j].Level = _sysChannelList[j].MaxVal +
                                    (_rnd.NextDouble() * (1 - 0.5 * _sysChannelList[j].MaxVal));

                                if (_sysChannelList[j].Level == _sysChannelList[j].MaxVal) {
                                    _sysChannelList[j].Level += increment;
                                }

                            }
                            else if (_sysChannelList[j].WarnIndicate &&
                                        _sysChannelList[j].Level < _sysChannelList[j].MinVal) {

                                _sysChannelList[j].Level = _rnd.NextDouble() * 0.5 * _sysChannelList[j].MinVal;
                                if (_sysChannelList[j].Level == _sysChannelList[j].MinVal) {
                                    _sysChannelList[j].Level -= increment;
                                }
                            }
                            //if channel is not a scale, and random mode, 
                            //raise warning randomly (if not already raised)
                        }
                        else if (randomMode) {
                            if (_rnd.NextDouble() < 0.1 && !_sysChannelList[j].WarnIndicate) {
                                SysmonTrigger(new Dictionary<string, string>() {
                                    {"monitoringlighttype", j == _BUTTON_1_MATB_GREEN ? "green" : "red"},
                                });
                            }
                        }
                    }
                }
            }

        }
    }
}
