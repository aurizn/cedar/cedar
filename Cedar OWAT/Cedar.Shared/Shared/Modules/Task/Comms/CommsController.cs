﻿/*
 * CEDAR Operator Workload Assessment Tool
 * Copyright (C) 2023  aurizn
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using CocosSharp;
using CEDAR.Shared;
using CEDAR.Shared.CedarCollection;
using System;
using System.Collections.Generic;
using System.Text;

/*! \r\namespace CEDAR.Shared.Modules.Task
 * @brief Specific task-level content
 * 
 */

/*! \r\namespace CEDAR.Shared.Modules.Task.Comms
 * @brief Communications task content
 * 
 */

namespace CEDAR.Shared.Modules.Task.Comms {
    /**
     @class CommsController
    
     @brief Communications task content controller - handles instantiating instances of child Comms models and controllers.
    
     @author    Ryan Beruldsen
     @date  15/09/2016
     */

    class CommsController : DivisionContent, IDivisionContent, ITaskContent {

        /** @brief The frequency precision (divisions in one unit) for comms channel models. */
        private static readonly int FREQ_PRECISION = 3;

        /** @brief The event brief description in task log file. */
        private static readonly string EVENT_BRIEF = "Communications";

        /** @brief The unique communications task tag. */
        private static readonly string COMM_TAG = "comm";

        /** @brief Random number generator for random events. */
        private readonly Random _rnd = new Random();

        /** @brief Last voicover call in seconds
        
            - to ensure user has enough time to respond to a random triggered voiceover call. */
        private long _lastCall = 0;

        /** @brief IO event to function register. */
        private readonly CDRIOEventFn _ioEventDict;


        /** @brief Hashmap of label to respective comms channel. */
        private readonly Dictionary<string, CommsModelChannel> _channelLabel;

        /** @brief Division status label - contains test related information for division (see user guide).*/
        private CCLabel _divStatus;

        /** @brief Call sign label - audible call sign information.*/
        private CCLabel _callSign;

        /** @brief Enabled status of task (sched start/stop event).*/
        private bool _enabled = false;

        /** @brief List of communications channels. */
        private readonly List<CommsModelChannel> _commChannelList;

        /**
         @fn    public CommsController(ControllerDivision division) : base(division)

         @brief Constructor for Communications task.

         @author    Ryan Beruldsen
         @date  19/09/2016

         @param division    The division to contain the Communications task content.
         */

        public CommsController(Division division) : base(division) {
            _commChannelList = new List<CommsModelChannel>();
            _channelLabel = new Dictionary<string, CommsModelChannel>();

            //register IO event functions
            _ioEventDict = new CDRIOEventFn {
                ["sched"] = SchedTrigger,
                [COMM_TAG] = CommTrigger
            };

            //add plaintext log file header
            var eventRow = new CDRLogRow() {
                    "-TIME-",
                    "-EVENT-",
                    "-SHIP-",
                    "-RADIO_T-",
                    "-FREQ_T-",
                    "-RADIO_S-",
                    "-FREQ_S_",
                    "-FREQ_S_-",
                    "-F_OK-"
            };
            string header = "# Timeout (in seconds) = " + ModelDataIOConfig.Instance.GetConfigNum("timeout", "task_comm") + "\r\n" +
                            "#\r\n" +
                            "# _T = Target, _S = Selected\r\n" +
                            "# R_ = Radio,  F_ = Frequency\r\n" +
                            "#";
            ModelDataIOLog.Instance.LogTaskHeader(COMM_TAG, header);
            ModelDataIOLog.Instance.LogTaskEvent(COMM_TAG, eventRow);

        }

        /**
         @fn    public void Init()
        
         @brief Initialise the views, models and controllers for the Communications task.
        
         @author    Ryan Beruldsen
         @date  19/09/2016
         */

        public void Init() {
            bool silentMode = ModelData.Instance.Settings[ModelData.SILENT_MODE] == ModelData.PARAM_ON;

            //default status string
            _divStatus = new CCLabel("", "roboto", 12) {
                Position = new CCPoint(_division.Width / 2, _division.Y - _division.Height * 0.95f),
                Color = CCColor3B.White,
                AnchorPoint = CCPoint.AnchorMiddle
            };
            if (!silentMode) {
                _callSign = new CCLabel("CALL SIGN: NASA 504", "robotob", 14) {
                    Position = new CCPoint(_division.Width / 2, _division.Y - _division.Height * 0.5f),
                    Color = CCColor3B.White,
                    AnchorPoint = CCPoint.AnchorMiddle
                };

                AddChild(_callSign);
            }

            AddChild(_divStatus);

            //unchanged MATB-II frequencies, designed to be similar to common aviation comms frequencies
            float[] minFreqRange = { 108, 108, 118, 118 };
            float[] maxFreqRange = { 117.950f, 117.950f, 135.975f, 135.975f };
            float[] freqIncrements = { 0.05f, 0.05f, 0.025f, 0.025f };

            //create 4 channels for user to manage
            for (int i = 0; i < 4; i++) {
                //label NAV 1,2, COM 1,2 as per MATB-II
                string commsLabel = i < 2 ? "NAV" : "COM";
                commsLabel += ((i % 2) + 1) + "";
                _commChannelList.Add(new CommsModelChannel(label: commsLabel,
                                                            startPerc: _rnd.NextDouble(),
                                                            suffix: "",
                                                            min: minFreqRange[i],
                                                            max: maxFreqRange[i],
                                                            increment: freqIncrements[i],
                                                            precision: FREQ_PRECISION,
                                                            index: i,
                                                            isSelected: i == 0));
                _commChannelList[i].CommsModelEventHandlerQueue +=
                    new CommsModelEventHandler(CommsChannelChanged);
                _channelLabel[commsLabel.ToLower()] = _commChannelList[i];
            }

            //add select bar for comms channel to division
            _division.AddContent(new CommsControllerSelectBar(_commChannelList, _division));

            //add select slider for selected comms freq to division
            _division.AddContent(new CommsControllerSelectSlider(_commChannelList, _division));

        }

        /**
         @fn    void EventExternal(string type, Dictionary<string, string> parameters)
        
         @brief Trigger for task event.
        
         @param type        Distinguishing event type (ie. task-level schedule or component level action).
         @param parameters  Event parameters.
         */

        public void EventExternal(string type, Dictionary<string, string> parameters) {

            if (_ioEventDict.ContainsKey(type)) {
                _ioEventDict[type](parameters);
            }
        }

        /**
         @fn    private int CommTrigger(Dictionary<string, string> parameters)
        
         @brief Trigger function for the "comm" IO event tag.

            - handles all main comms events such as setting a target frequency (playing relevant audio file if mode is not set to silent)
        
         @author    Ryan Beruldsen
         @date  5/10/2016
        
         @param parameters  IO Event parameters hash map.
        
         @return    An int - this result is ignored.
         */

        private int CommTrigger(Dictionary<string, string> parameters) {
            bool silentMode = ModelData.Instance.Settings[ModelData.SILENT_MODE] == ModelData.PARAM_ON;

            //confirm parameters are known
            if ((parameters.ContainsKey("radio") && _enabled) || parameters.ContainsKey("radio_timeout")) {
                //if timeout event (generally a callback from original trigger)
                bool timeout = parameters.ContainsKey("radio_timeout");

                //own ship - whether the event should be responded to by the user
                // or is just radio noise
                bool ownShip = !parameters["ship"].Equals("other");

                //the radio channel label to which the event pertains
                string radio = timeout ? parameters["radio_timeout"] : parameters["radio"];

                //the radio frequency for the event
                double freq = timeout ? 0 : Convert.ToDouble(parameters["freq"]);

                //if the event is not a timeout (callback)
                if (!timeout) {
                    _channelLabel[radio].other = false;
                    //log index of event (for reference in log file metrics)
                    int eventIndex = ModelDataIOLog.Instance.LogEvent(
                        ModelData.Instance.DurationStr24Hr,
                        "Communications"
                    );

                    //compute strings for audio code
                    string valStr = parameters["freq"];
                    string freqDec = valStr.Substring(0, valStr.IndexOf('.'));
                    string freqF = valStr.Substring(valStr.IndexOf('.') + 1);

                    //if event is not radio noise
                    if (ownShip) {
                        //set model properties to indicate event
                        _channelLabel[radio].TargetFrequency = freq;
                        _channelLabel[radio].EventIndex = eventIndex;

                        //trigger event audio if mode is not silent
                        string audioCode = "OWN_" +
                            _channelLabel[radio].LabelStr + "_" + freqDec + "-" + freqF;
                        if (!silentMode) {
                            CCAudioEngine.SharedEngine.PlayEffect(filename: audioCode);
                        }

                        //trigger timeout (callback) event
                        TriggerTimeout(
                            type: "task_comm",
                            callback: CommTrigger,
                            parameters: new CDRKeyValData() {
                                { "radio_timeout", radio },
                                { "index", eventIndex +""},
                                { "ship", ownShip ? "own" : "other"}
                            }
                        );

                        //if event is radio noise
                    }
                    else {
                        _channelLabel[radio].other = true;
                        _channelLabel[radio].WarnIndicate = false;
                        _channelLabel[radio].EventIndex = eventIndex;

                        //trigger event audio if mode is not silent
                        string audioCode = "OTHER_" +
                            _channelLabel[radio].LabelStr + "_" + freqDec + "-" + freqF;

                        if (!silentMode) {
                            CCAudioEngine.SharedEngine.PlayEffect(filename: audioCode);
                        }

                        TriggerTimeout(
                            type: "task_comm",
                            callback: CommTrigger,
                            parameters: new CDRKeyValData() {
                                { "radio_timeout", radio },
                                { "index", eventIndex +""},
                                { "ship", ownShip ? "own" : "other"}
                            }
                        );
                    }
                    //timeout event
                }
                else {
                    //if user has not responded to the request and changed frequency to the target
                    //and timeout is valid (not phantom)
                    bool phantomTimeout =
                        _channelLabel[radio].EventIndex != Convert.ToInt32(parameters["index"]);
                    if (_channelLabel[radio].WarnIndicate &&
                            _channelLabel[radio].EventIndex != 0 &&
                            !phantomTimeout) {

                        //log the lack of user response
                        var eventRow = new CDRLogRow() {
                            ModelData.Instance.DurationStr24Hr,
                            _channelLabel[radio].EventIndex+"",
                            ownShip ? "own" : "other",
                            _channelLabel[radio].LabelStr,
                            _channelLabel[radio].TargetStr,
                            _channelLabel[radio].LabelStr,
                            _channelLabel[radio].ValueStr,
                            _channelLabel[radio].IsSelected ? "true" : "false",
                            ownShip ? "false" : "true",
                        };
                        ModelDataIOLog.Instance.LogTaskEvent(COMM_TAG, eventRow);
                        ModelDataIOLog.Instance.LogEventBrief(EVENT_BRIEF, true);

                        //penalise user in training score
                        if (ownShip) {
                            WarningOvertime(this);
                        }

                        //set model properties to indicate event
                        _channelLabel[radio].EventIndex = -1;
                        _channelLabel[radio].Frequency = _channelLabel[radio].TargetFrequency;
                        _channelLabel[radio].WarnIndicate = false;
                    }
                }
            }
            return 1;
        }
        /**
         @fn    bool getEnabled();
        
         @brief Enabled status of task (sched start/stop event).
        
         */
        public bool GetEnabled() {
            return _enabled;
        }

        /**
         @fn    void SetEnabled(bool enabled);
        
         @brief Enabled status of task.
        
         @param enabled    Enabled status of task (sched start/stop event).
         */
        public void SetEnabled(bool enabled) {
            foreach (CommsModelChannel channel in _commChannelList) {
                channel.Enabled = enabled;
            }
            _enabled = enabled;

            var t = new TaskControllerEventArgs {
                EnabledEvent = true,
                Task = this
            };
            TasksControllerChanged(t);
        }

        /**
         @fn    public string GetTag()
        
         @brief Gets the tag of the division for task scheduler identification purposes
        
         @author    Ryan Beruldsen
         @date  19/09/2016
        
         @return    The unique string tag of the division ("comms").
         */

        public string GetTag() { return COMM_TAG; }

        /**
         @fn    public void SetDivisionStatus(string statusText)
        
         @brief Sets the division status string (_divStatus).
        
         @author    Ryan Beruldsen
         @date  19/09/2016
        
         @param statusText  The new status text string.
         */

        public void SetDivisionStatus(string statusText) {
            _divStatus.Text = statusText;
        }

        /**
         @fn    private int SchedTrigger(Dictionary<string, string> parameters)
        
         @brief Trigger function for the "sched" IO event tag - starts and stops comms task.
        
         @author    Ryan Beruldsen
         @date  5/10/2016
        
         @param parameters  IO Event parameters hash map.
        
         @return    An int - this result is ignored.
         */

        private int SchedTrigger(Dictionary<string, string> parameters) {
            //initialise event parameter strings
#pragma warning disable IDE0059 // Unnecessary assignment of a value
            string task = "NULL", action = "NULL", update = "NULL", response = "NULL";
#pragma warning restore IDE0059 // Unnecessary assignment of a value

            //set event parameter strings
            if (parameters.ContainsKey("task")) {
                task = parameters["task"];
            }
            if (parameters.ContainsKey("action")) action = parameters["action"];
#pragma warning disable IDE0059 // Unnecessary assignment of a value
            if (parameters.ContainsKey("update")) update = parameters["update"];
#pragma warning restore IDE0059 // Unnecessary assignment of a value
#pragma warning disable IDE0059 // Unnecessary assignment of a value
            if (parameters.ContainsKey("response")) response = parameters["response"];
#pragma warning restore IDE0059 // Unnecessary assignment of a value

            //switch task parameter string
            switch (task) {
                case "comm":
                    switch (action) {
                        case "start":
                            ModelDataIOLog.Instance.LogEvent(
                                ModelData.Instance.DurationStr24Hr,
                                "Scheduling - COMM Session Started"
                            );
                            SetEnabled(true);
                            break;
                        default:
                            ModelDataIOLog.Instance.LogEvent(
                                ModelData.Instance.DurationStr24Hr,
                                "Scheduling - COMM Session Ended"
                            );
                            SetEnabled(false);
                            break;
                    }
                    break;
            }

            return 1;
        }

        /**
         @fn    private void CommsChannelChanged(object sender, CommsModelChannelEventArgs e)
        
         @brief Listener for communications channel events on a task level, raises Division level event for TasksController.
        
         @author    Ryan Beruldsen
         @date  19/09/2016
        
         @param sender  Source of the event.
         @param e       Communications event arguments.
         */

        private void CommsChannelChanged(object sender,
                                        CommsModelChannelEventArgs e) {
            //if a warning has been raised, notify task scheduler and/or other listeners
            //don't notify if other callsign
            if (e.WarnIndicateChange && e.Comms.WarnIndicate) {
                //warningCount++;
                TaskControllerEventArgs t = new TaskControllerEventArgs {
                    WarningRaised = true,
                    Task = this
                };
                _warnings++;
                TasksControllerChanged(t);
                if (e.Comms.EventIndex == -1) {
                    e.Comms.EventIndex = 0;
                }
                //if a warning has been resolved, notify task scheduler and/or other listeners
                //don't notify if other callsign
            }
            else if (e.WarnIndicateChange && !e.Comms.WarnIndicate) {
                //log successful change of frequency to target
                if (e.Comms.EventIndex > 0) {
                    var eventRow = new CDRLogRow() {
                        ModelData.Instance.DurationStr24Hr,
                        e.Comms.EventIndex+"",
                        e.Comms.other ? "other" : "own",
                        e.Comms.LabelStr,
                        e.Comms.TargetStr,
                        e.Comms.LabelStr,
                        e.Comms.ValueStr,
                        "true",
                        "true"
                    };
                    ModelDataIOLog.Instance.LogTaskEvent("comm", eventRow);
                    ModelDataIOLog.Instance.LogEventBrief(EVENT_BRIEF);
                    e.Comms.EventIndex = 0;

                    //raise event to task controller if not a timeout
                    TaskControllerEventArgs t = new TaskControllerEventArgs {
                        WarningResolved = true,
                        Task = this
                    };
                    TasksControllerChanged(t);

                }
                else if (e.Comms.EventIndex != -1) {
                    var eventRow = new CDRLogRow() {
                        ModelData.Instance.DurationStr24Hr,
                        e.Comms.EventIndex+"",
                        e.Comms.other ? "other" : "own",
                        e.Comms.LabelStr,
                        e.Comms.TargetStr,
                        e.Comms.LabelStr,
                        e.Comms.ValueStr,
                        "true",
                        "true"
                    };
                    ModelDataIOLog.Instance.LogTaskEvent("comm", eventRow);
                    ModelDataIOLog.Instance.LogEventBrief(EVENT_BRIEF);

                }

                _warnings--;
            }
        }

        /**
         @fn    void TriggerRandomVoiceover()
        
         @brief Trigger random voiceover and comms frequency target event.

            - for random mode to emulate a planned, valid comms frequency target with voiceover
        
         @author    Ryan Beruldsen
         @date  5/10/2016
         */

        void TriggerRandomVoiceover() {

            long durationSinceLastCall = ModelData.Instance.Duration - _lastCall;
            int minResponseTime = 15;
            int callCutoffDuration = 100; // calls after 100 seconds will be cutoff and the user will be unable to respond

            if ((durationSinceLastCall > minResponseTime || _lastCall == 0) && ModelData.Instance.Duration < callCutoffDuration) {
                string[] voiceover = ModelDataIOConfig.VoiceOverListing();
                double[] voiceoverFreq = ModelDataIOConfig.VoiceOverFreqListing();
                int index = Convert.ToInt32(_rnd.NextDouble() * (voiceover.Length - 1));
                string audioCode = voiceover[index];
                string channel = audioCode.Substring(audioCode.IndexOf('_') + 1, 4);

                CommTrigger(new Dictionary<string, string>() {
                    {"ship", audioCode.Contains("OTHER") ? "other" : "own"},
                    {"radio", channel.ToLower()},
                    {"freq", voiceoverFreq[index].ToString("000.000")},
                });
                _lastCall = ModelData.Instance.Duration;
                /*
                if (!audioCode.Contains("OTHER")) {
                    for (int i = 0; i < 4; i++) {
                        if (_commChannelList[i].labelStr.Equals(channel)) {
                            if (_commChannelList[i].warnIndicate) {
                                triggerRandomVoiceover();
                            } else {
                                int eventIndex = 
                                    ModelDataIOLog.instance.logEvent(
                                        ModelData.instance.durationStr24Hr, 
                                        "Communications"
                                );
                                _commChannelList[i].other = false;
                                _commChannelList[i].targetFrequency = voiceoverFreq[index];
                                _commChannelList[i].eventIndex = eventIndex;
                                CCAudioEngine.SharedEngine.PlayEffect(filename: audioCode);
                                _lastCall = ModelData.instance.duration;
                            }
                        }
                    }
                }else {
                    _lastCall = ModelData.instance.duration;
                    CCAudioEngine.SharedEngine.PlayEffect(filename: audioCode);
                }*/
            }

        }

        /**
         @fn    public void Trigger(float time)
        
         @brief Random task trigger for user.
        
         @author    Ryan Beruldsen
         @date  19/09/2016
        
         @param time    Time elapsed in seconds (not used).
         */

        public void Trigger(float time) {
            if (_enabled) {
                //50% chance of random freq target
                if (_rnd.NextDouble() > 0.5) {
                    TriggerRandomVoiceover();
                }
            }
        }
    }
}
