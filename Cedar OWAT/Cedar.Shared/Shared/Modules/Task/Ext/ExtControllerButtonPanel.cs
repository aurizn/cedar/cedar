﻿/*
 * CEDAR Operator Workload Assessment Tool
 * Copyright (C) 2023  aurizn
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using System.Collections.Generic;
using CocosSharp;
using CEDAR.Shared.CedarCollection;

namespace CEDAR.Shared.Modules.Task.Ext {

    /**
     @class ExtControllerButtonPanel
    
     @brief Controller for panel of ext buttons (ExtViewButton) - button collection component controller.
    
     @author    Ryan Beruldsen
     @date  10/10/2016
     */

    class ExtControllerButtonPanel : DivisionContent, IDivisionContent {


        /** @brief List of buttons views. */
        private readonly List<ExtViewButton> _buttonList;

        /** @brief List of button models. */
        private readonly List<ExtModel> _modelList;

        /** @brief Dictionary of button models to views. */
        private readonly Dictionary<ExtModel, ExtViewButton> _modelButtonDict;

        /** @brief Listener for button touch events. */
        private readonly CCEventListenerTouchAllAtOnce _touchListener;

        /**
         @fn    public ExtControllerButtonPanel(List<ExtModel> modelList, Division division) : base(division)
        
         @brief Button collection component controller constructor.
        
         @author    Ryan Beruldsen
         @date  10/10/2016
        
         @param modelList   List of models.
         @param division    The division.
         */

        public ExtControllerButtonPanel(    List<ExtModel> modelList, 
                                            Division division) : base(division) {
            _modelList = modelList;
            _buttonList = new List<ExtViewButton>();
            _modelButtonDict = new Dictionary<ExtModel, ExtViewButton>();

            _touchListener = new CCEventListenerTouchAllAtOnce {
                OnTouchesBegan = HandleInput
            };
            AddEventListener(_touchListener, this);


        }

        /**
         @fn    public void Init()
        
         @brief Initialise Button collection component.
        
         @author    Ryan Beruldsen
         @date  22/09/2016
         */
        public void Init() {
            for(int i = 0; i < _modelList.Count; i++) {
                //width, height, position and spacing calculations
                float ySpacer = _division.Height * 0.1f;
                float width = _division.Width * 0.3f;
                float height = _division.Width * 0.075f;

                float y = _division.PrevDivisionY + 
                                     (height * i) + 
                                     (ySpacer * (i+1));

                //instantiate button view
                var button = new ExtViewButton(
                    x: _division.Width/2 - width/2,
                    y: ySpacer + y,
                    width: width,
                    height: height,
                    label: "BUTTON "+i,
                    colourWarning: ModelData.Instance.GetColourPallete4B(ModelData.COLOUR_PARAM_WARNING),
                    colourActive: ModelData.Instance.GetColourPallete3B(ModelData.COLOUR_PARAM_ACTIVE),
                    division: _division
                );

                //initialise button view
                button.Init();

                //add to button list
                _buttonList.Add(button);

                //set dictionary of buttons to models to bind view to model at controller level
                _modelButtonDict[_modelList[i]] = button;

                //handle button model events
                _modelList[i].ExtModelEventHandler += ExtModelChanged;
                AddChild(button);
            }
        }

        /**
         @fn    private bool ButtonHitTest(System.Collections.Generic.List<CCTouch> touches, MenuViewButton m)

         @brief Hit test for button, shorthand function.

         @author    Ryan Beruldsen
         @date  16/09/2016

         @param touches CocosSharp injected touch list (from touch handler)
         @param m       The MenuViewButton to test for bounding box hit.

         @return    True if within button/drop down bounds, otherwise false
         */

        private bool ButtonHitTest(System.Collections.Generic.List<CCTouch> touches,
                                    ExtViewButton m) {

            if (    touches[0].Location.X > m.minButtonBounds.X && 
                    touches[0].Location.X < m.maxButtonBounds.X &&
                    touches[0].Location.Y > m.minButtonBounds.Y && 
                    touches[0].Location.Y < m.maxButtonBounds.Y) {
                return true;
            }

            ModelDataIOLog.Instance.LogTouch(
                   x: touches[0].Location.X,
                   y: touches[0].Location.Y,
                   status: CDRTouchData.StatusEnum.Miss
            );
            return false;
        }

        /**
         @fn    private void ExtModelChanged(object sender, ExtModelEventArgs e)

         @brief Handle ext model events.

         @author    Ryan Beruldsen
         @date  21/09/2016

         @param sender  Source of the event.
         @param e       Resource model tank event information.
        */
        private void ExtModelChanged(object sender, ExtModelEventArgs e) {
            if (e.WarnIndicateChange && e.Ext.WarnIndicate) {
                _modelButtonDict[e.Ext].WarningActive = true;
            }else if(e.WarnIndicateChange){
                _modelButtonDict[e.Ext].WarningActive = false;
            }
        }

        /**
         @fn    private void HandleInput(System.Collections.Generic.List<CCTouch> touches, CCEvent touchEvent)

         @brief Handles touch input end

         @author    Ryan Beruldsen
         @date  16/09/2016

         @param touches     CocosSharp injected touch list
         @param touchEvent  CocosSharp injected touch event
         */
        private void HandleInput(System.Collections.Generic.List<CCTouch> touches, CCEvent touchEvent) {
            if (touches.Count > 0) {
                for(int i = 0; i < _buttonList.Count; i++) {
                    var button = _buttonList[i];
                    bool hitTest = ButtonHitTest(touches, button);
                    if (hitTest && button.WarningActive) {
                        _modelList[i].WarnIndicate = false;

                        ModelDataIOLog.Instance.LogTouch(
                                           x: touches[0].Location.X,
                                           y: touches[0].Location.Y,
                                           status: CDRTouchData.StatusEnum.Hit
                        );
                    } else if(hitTest && !button.WarningActive) {
                        ModelDataIOLog.Instance.LogTouch(
                                           x: touches[0].Location.X,
                                           y: touches[0].Location.Y,
                                           status: CDRTouchData.StatusEnum.FalsePos
                        );
                    }
                }

            }
        }
    }
}
