﻿/*
 * CEDAR Operator Workload Assessment Tool
 * Copyright (C) 2023  aurizn
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

using CocosSharp;
using CEDAR.Shared.CedarCollection;
using CEDAR.Shared.Modules.Task;
using System;
using System.Collections.Generic;

namespace CEDAR.Shared.Modules {
    internal delegate void DivisionTaskEventHandler(object sender,
                                                    TaskControllerEventArgs e);

    /**
     @class DivisionContent
    
     @brief Division content instance, subclassed by all division content controllers.

        Divsion content can either be view or controller content, so this class is neither.
        Division content can be added as either interactive content(controller) or static content (view).
    
     @author    Ryan Beruldsen
     @date  15/09/2016
     */

    class DivisionContent : CCNode {
        internal Division _division;
        internal Dictionary<long, List<CDREventFnParam>> _eventInternalDict;

        /** @brief Number of warnings raised to the user during testing. */
        internal int _warningCount = 0;

        /** @brief The timestamp of the last warning, for info in _divStatus. */
        internal long _lastWarn = 0;

        /** @brief The number of current warnings raised to the user. */
        internal int _warnings = 0;

        /** @brief Event queue for all listeners interested in divisionTaskEventHandler events. */
        public event DivisionTaskEventHandler DivisionTaskEventHandler;

        /**
         @fn    public DivisionContent(Division division) : base()
        
         @brief Division content constructor - the parent division must be passed in as a reference.
        
         @author    Ryan Beruldsen
         @date  6/10/2016
        
         @param division    The parent division.
         */

        public DivisionContent(Division division) : base() {
            this._division = division;
            _eventInternalDict = new Dictionary<long, List<CDREventFnParam>>();
        }

        /**
         @fn    public void AddListener(Modules.DivisionTaskEventHandler c)

         @brief Adds a listener for task scheduler relevant events.

         @author    Ryan Beruldsen
         @date  21/09/2016

         @param c   The DivisionTaskEventHandler to process.
         */

        public void AddListener(Modules.DivisionTaskEventHandler c) { DivisionTaskEventHandler += c; }

        /**
         @fn    protected virtual void TasksControllerChanged(TasksControllerEventArgs e)
        
         @brief Raises task controller events to listeners.
        
         @author    Ryan Beruldsen
         @date  6/10/2016
        
         @param e   Tasks controller event information.
         */

        protected virtual void TasksControllerChanged(TaskControllerEventArgs e) {
            DivisionTaskEventHandler?.Invoke(this, e);
        }

        /**
         @fn    public void EventInternal(long timestamp)
        
         @brief Internal division controller event trigger

            - this is generally for IO event callbacks/timeouts
        
         @author    Ryan Beruldsen
         @date  6/10/2016
        
         @param timestamp   The current test duration timestamp.
         */

        public void EventInternal(long timestamp) {
            if (_eventInternalDict.ContainsKey(timestamp)) {
                foreach (CDREventFnParam internalEventFnParam in _eventInternalDict[timestamp]) {
                    foreach (KeyValuePair<Func<Dictionary<string, string>, int>,
                        CDRKeyValData> internalEvent in internalEventFnParam) {

                        internalEvent.Key(internalEvent.Value);
                    }
                }
            }
        }

        /**
         @fn    protected void TriggerTimeout(string type, Func<Dictionary<string, string>, int> callback, CDRKeyValData parameters)
        
         @brief Trigger callback timeout to allow warnings to timeout.
            
            - these callbacks are triggered with a call to eventInternal
            - the function and its required parameters are both stored
        
         @author    Ryan Beruldsen
         @date  6/10/2016
        
         @param type        The timeout config type (timeout duration loaded from I/O config).
         @param callback    The callback function.
         @param parameters  The parameters to send to the callback function.
         */

        protected void TriggerTimeout(string type,
                                        Func<Dictionary<string, string>, int> callback,
                                        CDRKeyValData parameters) {

            var timeoutFnParam = new CDREventFnParam {
                [callback] = parameters
            };

            long timestamp = ModelData.Instance.Duration;
            long timeout = Convert.ToInt64(
                ModelDataIOConfig.Instance.GetConfigValue("timeout", type));
            long timeoutStamp = timestamp + timeout;

            if (!_eventInternalDict.ContainsKey(timeoutStamp)) {
                _eventInternalDict[timeoutStamp] = new List<CDREventFnParam>();
            }
            _eventInternalDict[timeoutStamp].Add(timeoutFnParam);
        }

        /**
         @fn    protected void TriggerTimeout(long interval, Func<Dictionary<string, string>, int> callback, CDRKeyValData parameters)
        
         @brief Trigger callback timeout to allow warnings to timeout, overload to allow specifying timeout in seconds.
            
            - these callbacks are triggered with a call to eventInternal
            - the function and its required parameters are both stored
            - purpose of overload is to manually set the timeout duration, rather than 

         @author    Ryan Beruldsen
         @date  6/10/2016
        
         @param interval    The timeout interval.
         @param callback    The callback function.
         @param parameters  The parameters to send to the callback function.
         */

        protected void TriggerTimeout(long interval,
                                        Func<Dictionary<string, string>, int> callback,
                                        CDRKeyValData parameters) {

            var timeoutFnParam = new CDREventFnParam {
                [callback] = parameters
            };

            long timestamp = ModelData.Instance.Duration;
            long timeout = interval;
            long timeoutStamp = timestamp + timeout;
            if (!_eventInternalDict.ContainsKey(timeoutStamp)) {
                _eventInternalDict[timeoutStamp] = new List<CDREventFnParam>();
            }
            _eventInternalDict[timeoutStamp].Add(timeoutFnParam);
        }

        /**
         @fn    public int GetWarningCount()

         @brief Gets number of warnings raised to the user during testing.

         @author    Ryan Beruldsen
         @date  21/09/2016

         @return    The number of warnings raised to the user during testing.
         */

        public int GetWarningCount() { return _warningCount; }

        /**
         @fn    public void WarningOvertime(ITaskContent task)

         @brief Overtime warning calculation - calculates the cumulative score after warnings continue to overtime unresolved.

         @param task    The task the overtime warning is emanating from.
            
         @author    Ryan Beruldsen
         @date  21/09/2016
         */

        public void WarningOvertime(ITaskContent task) {
            _warningCount += _warnings;
            TaskControllerEventArgs t = new TaskControllerEventArgs {
                PenaltyRaised = true,
                Task = task,
                PenaltyNum = _warnings
            };
            TasksControllerChanged(t);
        }

        /**
         @fn    public void SetLastWarning(long lastWarning)
        
         @brief Sets the last warning raised timestamp (for _divStatus).
        
         @author    Ryan Beruldsen
         @date  21/09/2016
        
         @param lastWarning The last warning raised timestamp.
         */

        public void SetLastWarning(long lastWarning) { _lastWarn = lastWarning; }

        /**
         @fn    public int WarningsActive()
        
         @brief Gets cumulative number of warnings in current test session (_warningCount).
        
         @author    Ryan Beruldsen
         @date  21/09/2016
        
         @return    The warning count.
         */

        public int WarningsActive() { return _warnings; }

        /**
         @fn    public long GetLastWarning()
        
         @brief Gets the last warning raised timestamp (for _divStatus).
        
         @return    The last warning timestamp.
         @author    Ryan Beruldsen
         @date  21/09/2016
         */

        public long GetLastWarning() {
            return _lastWarn;
        }
    }
}
