﻿/*
 * CEDAR Operator Workload Assessment Tool
 * Copyright (C) 2023  aurizn
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


using System.Collections.Generic;

/*! \namespace CEDAR.Shared.Modules.Task
 * @brief Task/Schedule level content
 * 
 */

namespace CEDAR.Shared.Modules.Task {
    /**
     @interface ITaskContent
    
     @brief Task content interface, ensures required functions for a task are implemented
    
     @author    Ryan Beruldsen
     @date  15/09/2016
     */

    interface ITaskContent
    {
        /**
         @fn    void SetDivisionStatus(string statusText);
        
         @brief Sets division status string: user information about how many warning raised
            
            , success in resolving warnings within required time period.
        
         @param statusText  The division status text.
         */

        void SetDivisionStatus(string statusText);

        /**
         @fn    void Trigger(float time);
        
         @brief Generic trigger for task event.
        
         @param time    Time elapsed in seconds (not used).
         */

        void Trigger(float time);

        /**
         @fn    void EventExternal(string type, Dictionary<string, string> parameters)
        
         @brief Trigger for external parameters task event.
        
         @param type        Distinguishing event type (ie. task-level schedule or component level action).
         @param parameters  Event parameters.
         */

        void EventExternal(string type, Dictionary<string, string> parameters);

        /**
         @fn    void EventInternal(long timestamp);
        
         @brief Trigger for internal parameters task event.

            - this is used to trigger internal timeout events and other registered callbacks.
        
         @param timestamp   Timestamp of internal event to trigger.
         */

        void EventInternal(long timestamp);

        /**
         @fn    bool getEnabled();
        
         @brief Enabled status of task (sched start/stop event).
        
         */

        bool GetEnabled();

        /**
         @fn    void SetEnabled(bool enabled);
        
         @brief Enabled status of task.
        
         @param enabled    Enabled status of task (sched start/stop event).
         */

        void SetEnabled(bool enabled);



        /**
         @fn    int getWarningCount();
        
         @brief Gets cumulative number of warnings in current test session (_warningCount).
        
         @return    The warning count.
         */

        int GetWarningCount();

        /**
         @fn    long getLastWarning();
        
         @brief Gets the timestamp of the last warning.
        
         @return    Timestamp of the last warning.
         */

        long GetLastWarning();

        /**
         @fn    void SetLastWarning(long lastWarning);
        
         @brief Sets the timestamp of the last warning.
        
         @param lastWarning Timestamp of the last warning.
         */

        void SetLastWarning(long lastWarning);

        /**
         @fn    int warningsActive();
        
         @brief Gets the number of current warnings raised to the user.
        
         @return    Number of current warnings raised to the user.
         */

        int WarningsActive();

        /**
         @fn    void WarningOvertime(ITaskContent t);
        
         @brief Overtime warning calculation - calculates the cumulative score after warnings continue to overtime unresolved.
         */

        void WarningOvertime(ITaskContent t);

        /**
         @fn    void AddListener(DivisionTaskEventHandler c);
        
         @brief Adds a listener for task scheduler relevant events (local CommsModelChannel events).
        
         @param c   The DivisionTaskEventHandler to process.
         */

        void AddListener(DivisionTaskEventHandler c);

        /**
         @fn    string GetTag();
        
         @brief Gets the unique tag for the task content.
        
         @return    The tank content unique tag.
         */

        string GetTag();
    }
}
